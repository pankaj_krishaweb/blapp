<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\{Auth, Mail};
use App\Discount;
use App\User;
use App\OurCommunity;
class DiscountController extends Controller
{
    public function index()
    {
    	return view('client.discount');
    }

    public function dicountPost(Request $request)
    {
    	$rules=[
			'full_name' => 'required',
			'email' => 'required|email',
			'contact_number' => 'numeric|required',
			'location' => 'required',
		];
		//passed rules to validator
		$validator = Validator::make($request->all(),$rules);
		//if validation fails then it will redirect back with errors
		if ($validator->fails()) { 
			return redirect()->back()->withErrors($validator)->withInput();
		}

		$discount = new Discount;
		$discount->user_id = Auth::user()->id;
		$discount->full_name = $request->full_name;
		$discount->email = $request->email;
		$discount->contact_number = $request->contact_number;
		$discount->location = $request->location;
		$discount->save();

		$mail = Mail::send('emails.discount', ['full_name' => $request->full_name],
		    function ($message) use($request) {
		        $message->subject('Retrieve your Berry Lodge Surveying Discount Code Now!');
		        $message->to($request->email);
		    }
		);

        $mail = Mail::send('emails.discount_admin', ['full_name' => $request->full_name,'user_email'=>$request->email,'location'=>$request->location,'contact_number'=>$request->contact_number],
            function ($message) use($request) {
                $message->subject('Retrieve your Berry Lodge Surveying Discount Code Now!');
                $message->to('info@blsurveyors.com');
            }
        );
        // $mail = Mail::to($request->email)->send(new demo($request->all()));
		return redirect('client/discount')->withSuccess("Yippee! We have your discount code! One of our team will get in touch with the recipient, hopefully they will become a Berry Lodge Client. Thank you!");
    }

    public function ourCommunity()
    {
        $datas = OurCommunity::all();
    	return view('client.our_community', compact('datas'));
    }

    public function meetYourSurveyour()
    {
        $jobs = Auth::user()->jobs;
        


    	// $surveyors = [];
    	// foreach (Auth::user()->jobs as $job) {
     //        //dd($job->id);
    	// 	for ($i=0; $i <9 ; $i++) { 
    	// 		if ($i >0) 
    	// 		{
    	// 			$a = 'ao'.$i;
    	// 			if ($job->$a != null) {
     //                    $content = $job->$a->surveyor_full_information;
     //                    preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/i", $content, $matches);
     //                    //dd($matches);

     //                    if ($matches == []) {
     //                        $image = null;
     //                        $year = null;
     //                    }
     //                    else
     //                    {
     //                        $image = user::where('email', '=', $matches[0])->first();
     //                        if ($image != null) {
     //                            $year = date('Y', strtotime($image->created_at));
     //                        }
     //                        //dd($content);
     //                    }

     //                   if ($image == null) 
     //                   {
     //                    array_push($surveyors, ['name' => $job->$a->surveyor_name, 'qualification' => $job->$a->surveyor_qualifications ,'picture' => '','contact' => $job->$a->surveyor_contact_details, 'year' => '']);
     //                   }
     //                   else
     //                   {
    	// 				array_push($surveyors, ['name' => $job->$a->surveyor_name, 'qualification' => $job->$a->surveyor_qualifications, 'picture' => $image->picture ,'contact' => $job->$a->surveyor_contact_details, 'year' => $year] );
     //                   } 
                        
    	// 			}
    				
    	// 		}
    	// 		else
    	// 		{

    	// 			if ($job->ao != null) {
     //                    $content = $job->ao->surveyor_full_information;
     //                    //dd($job->ao);
     //                    preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/i", $content, $matches);

     //                    if ($matches == []) {
     //                        $image = null;
     //                        $year = null;
     //                    }
     //                    else
     //                    {
     //                        $image = user::where('email', '=', $matches[0])->first();
     //                        if ($image != null) {
     //                            $year = date('Y', strtotime($image->created_at));
     //                        }
     //                    }

     //                    if ($image == null) {
     //                        array_push($surveyors, ['name' => $job->ao->surveyor_name, 'qualification' => $job->ao->surveyor_qualification ,'picture' => '' ,'contact' => $job->ao->surveyor_contact_details, 'year' => '']);
     //                    }
     //                    else
     //                    {
    	// 				   array_push($surveyors, ['name' => $job->ao->surveyor_name, 'qualification' => $job->ao->surveyor_qualification, 'picture' => $image->picture ,'contact' => $job->ao->surveyor_contact_details, 'year' => $year]);
     //                    }
    	// 			}
    	// 		}
    	// 	}
    	// 	if ($job->bo != null) 
    	// 	{
     //            $content = $job->bo->surveyor_full_information;
     //            preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/i", $content, $matches);
     //            if ($matches == []) {
     //                $image = null;
     //                $year = null;
     //            }
     //            else
     //            {
     //                $image = user::where('email', '=', $matches[0])->first();
     //                if ($image != null) {
     //                    $year = date('Y', strtotime($image->created_at));
     //                }
     //            }

     //            if ($image == null) 
     //            {
     //                array_push($surveyors,['name' => $job->bo->surveyor_name, 'qualification' => $job->bo->surveyor_qualifications ,'picture' => '', 'year' => '' ,'contact' => $job->bo->surveyor_contact_details]);
     //            }
     //            else
     //            {
    	// 		    array_push($surveyors,['name' => $job->bo->surveyor_name, 'qualification' => $job->bo->surveyor_qualifications, 'picture' => $image->picture, 'year' => $year, 'contact' => $job->bo->surveyor_contact_details]);
     //            }
    	// 	}
    	// 	if ($job->surveying != null) 
    	// 	{
     //            $content = $job->surveying->s_surveyor_full_information;
     //            preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/i", $content, $matches);
                
     //            if ($matches == []) {
     //                $image = null;
     //                $year = null;
     //            }
     //            else
     //            {
     //                $image = user::where('email', '=', $matches[0])->first();
     //                if ($image != null) {
     //                    $year = date('Y', strtotime($image->created_at));
     //                }
     //            }

     //            if ($image == null) {
     //                array_push($surveyors,['name' => $job->surveying->surveryor_name, 'qualification' => $job->surveying->surveyor_qualification ,'picture' => '', 'contact' => $job->surveying->surveyor_contact_details, 'year' => '']);
     //            }
     //            else{
    	// 		    array_push($surveyors,['name' => $job->surveying->surveryor_name, 'qualification' => $job->surveying->surveyor_qualification,'picture' => $image->picture ,'contact' => $job->surveying->surveyor_contact_details, 'year' => $year]);
     //            }
    	// 		//array_push($surveyors,$job->surveying->surveyor_qualification);
    	// 	}
    	// }
    	// //dd($surveyors);
    	// $surveyors = $this->unique_multidim_array($surveyors,'name');
    	return view('client.meet_surveyor',compact('jobs'));
    }

    function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 
        
        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 
}
