<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job_User extends Model
{
	public function getCreatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value)); 
    }
    public function getUpdatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value));   
    }
    protected $table="job_users";

    
}
