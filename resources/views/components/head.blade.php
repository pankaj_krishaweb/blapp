<header class="primary">
	<div class="full-wrapper">
    	<div class="header-top">
            <a class="primary-logo" href="https://www.blsurveyors.com/" title="Berry Lodge">
            <img src="/old/main-logox2.png" alt="Berry Lodge logo">
            </a>
            <div class="top-wrapper">

            	<form role="search" method="get" class="search-form tablet tablet-large desktop" action="https://www.blsurveyors.com/">
            		
		    <div class="inner">
		    <input type="search" class="search-field" placeholder="Search..." value="" name="s" title="Search...">
		    </div>
		    <input type="submit" class="search-submit" value="">
		</form>               
		 <div class="contact-number tablet tablet-large desktop">
                    <div class="title">Call us today on</div>
                    <div class="number">020 7935 2502</div>
                </div>
            </div>
              {{-- logoin --}}
              @if (!Auth::guest())
                <a class="members-login-btn hidden-sm hidden-xs" href="{{ url('/logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    <span>Logout</span>
                </a>

                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form> 
              @endif                     
        	{{-- logout --}}
        
		<a class="mobile-btn mobile mobile-large" href="#mobile-menu" id="mobile-btn"><img src="/old/mobile-menu-button.png"></a>
    	</div><!-- .header-top -->
    </div><!-- .full-wrapper -->

    <nav class="primary tablet tablet-large desktop">
    	<div class="full-wrapper" id="main-navigation">
    		<div class="menu-primary-menu-container"><ul id="menu-primary-menu" class="menu"><li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-23"><a href="https://www.blsurveyors.com/about-us/">About</a>
<ul class="sub-menu"><div class="full-wrapper">
	<li id="menu-item-1495" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1495"><a href="https://www.blsurveyors.com/about-us/clients/">Our Clients</a></li>
	<li id="menu-item-182" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-182"><a title="Our Testimonials" href="https://www.blsurveyors.com/testimonial/">Testimonials</a></li>
	<li id="menu-item-156" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-156"><a href="https://www.blsurveyors.com/podcast/">Our Podcasts</a></li>
</div></ul>
</li>
<li id="menu-item-1617" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1617"><a href="https://www.blsurveyors.com/projects/">Projects</a></li>
<li id="menu-item-24" class="mega-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-24"><a href="https://www.blsurveyors.com/party-wall-surveying/">Surveying Services</a>
<ul class="sub-menu"><div class="full-wrapper">
	<li id="menu-item-2894" class="bold menu-item menu-item-type-post_type menu-item-object-services menu-item-has-children menu-item-2894"><a href="https://www.blsurveyors.com/services/party-wall/">Party Wall</a>
	<ul class="sub-menu"><div class="full-wrapper">
		<li id="menu-item-2903" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2903"><a href="https://www.blsurveyors.com/services/party-wall/surveyors/">Party Wall Surveyor Roles</a></li>
		<li id="menu-item-2897" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2897"><a href="https://www.blsurveyors.com/services/party-wall/agreement/">Party Wall Award/Agreement</a></li>
		<li id="menu-item-2902" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2902"><a href="https://www.blsurveyors.com/services/party-wall/roles/">Party Wall Surveyor Types</a></li>
		<li id="menu-item-2901" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2901"><a href="https://www.blsurveyors.com/services/party-wall/party-wall-notices/">Party Wall Notices</a></li>
		<li id="menu-item-2900" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2900"><a href="https://www.blsurveyors.com/services/party-wall/interactive-guide/">Party Wall Interactive Guide</a></li>
		<li id="menu-item-3122" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3122"><a href="https://www.blsurveyors.com/services/party-wall/party-wall-faq/">Party Wall FAQ</a></li>
		<li id="menu-item-2899" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2899"><a href="https://www.blsurveyors.com/services/party-wall/fixed-costs/">Party Wall Fixed Costs</a></li>
		<li id="menu-item-2898" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2898"><a href="https://www.blsurveyors.com/services/party-wall/fee-quote/">Party Wall Fee Quote</a></li>
	</div></ul>
</li>
	<li id="menu-item-2895" class="bold menu-item menu-item-type-post_type menu-item-object-services menu-item-has-children menu-item-2895"><a href="https://www.blsurveyors.com/services/surveying/">Surveying</a>
	<ul class="sub-menu"><div class="full-wrapper">
		<li id="menu-item-2910" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2910"><a href="https://www.blsurveyors.com/services/surveying/schedule-condition-report/">Schedule of Condition</a></li>
		<li id="menu-item-2904" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2904"><a href="https://www.blsurveyors.com/services/surveying/boundary-dispute/">Boundary Dispute</a></li>
		<li id="menu-item-2909" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2909"><a href="https://www.blsurveyors.com/services/surveying/rics-homebuyer-report/">RICS Homebuyer Report</a></li>
		<li id="menu-item-2906" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2906"><a href="https://www.blsurveyors.com/services/surveying/full-building-surveys/">Full Building Surveys</a></li>
		<li id="menu-item-2905" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2905"><a href="https://www.blsurveyors.com/services/surveying/defect-analysis-report/">Defect Analysis Report</a></li>
		<li id="menu-item-2911" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2911"><a href="https://www.blsurveyors.com/services/surveying/snagging-list-report/">Snagging List</a></li>
		<li id="menu-item-2907" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2907"><a href="https://www.blsurveyors.com/services/surveying/licence-to-alter/">Licence to Alter</a></li>
		<li id="menu-item-2908" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2908"><a href="https://www.blsurveyors.com/services/surveying/property-mediation/">Property Mediation</a></li>
		<li id="menu-item-3022" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3022"><a href="https://www.blsurveyors.com/services/surveying/surveying-fee-quote/">Surveying Fee Quote</a></li>
	</div></ul>
</li>
	<li id="menu-item-2896" class="bold menu-item menu-item-type-post_type menu-item-object-services menu-item-has-children menu-item-2896"><a href="https://www.blsurveyors.com/services/valuation/">Valuation</a>
	<ul class="sub-menu"><div class="full-wrapper">
		<li id="menu-item-3009" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3009"><a href="https://www.blsurveyors.com/services/valuation/market-valuation-2/">Market Valuation</a></li>
		<li id="menu-item-2912" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2912"><a href="https://www.blsurveyors.com/services/valuation/fixed-valuation-costs/">Insurance Valuation</a></li>
		<li id="menu-item-2914" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2914"><a href="https://www.blsurveyors.com/services/valuation/market-valuation/">Retrospective Valuation</a></li>
		<li id="menu-item-3017" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3017"><a href="https://www.blsurveyors.com/services/valuation/leasehold-extension-valuation/">Leasehold Extension</a></li>
		<li id="menu-item-2913" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2913"><a href="https://www.blsurveyors.com/services/valuation/leasehold-extensions-explained/">Leasehold Extensions FAQ</a></li>
		<li id="menu-item-3014" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3014"><a href="https://www.blsurveyors.com/services/valuation/matrimonial-valuation/">Matrimonial Valuation</a></li>
		<li id="menu-item-3013" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3013"><a href="https://www.blsurveyors.com/services/valuation/probate-valuation/">Probate Valuation</a></li>
		<li id="menu-item-3023" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3023"><a href="https://www.blsurveyors.com/services/valuation/fee-quote/">Valuation Fee Quote</a></li>
	</div></ul>
</li>
</div></ul>
</li>
<li id="menu-item-191" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-191"><a href="https://www.blsurveyors.com/video-gallery/">Videos</a></li>
<li id="menu-item-27" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a href="https://www.blsurveyors.com/blog/">Blog</a></li>
<li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://www.blsurveyors.com/contact-us/">Contact</a></li>
</ul></div>        </div>
    </nav><!-- .primary -->
    
    <nav class="mobile" id="mobile-menu-container">
		<div class="menu-primary-menu-container"><ul id="menu-primary-menu-1" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-23"><a href="https://www.blsurveyors.com/about-us/">About</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1495"><a href="https://www.blsurveyors.com/about-us/clients/">Our Clients</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-182"><a title="Our Testimonials" href="https://www.blsurveyors.com/testimonial/">Testimonials</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-156"><a href="https://www.blsurveyors.com/podcast/">Our Podcasts</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1617"><a href="https://www.blsurveyors.com/projects/">Projects</a></li>
<li class="mega-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-24"><a href="https://www.blsurveyors.com/party-wall-surveying/">Surveying Services</a>
<ul class="sub-menu">
	<li class="bold menu-item menu-item-type-post_type menu-item-object-services menu-item-has-children menu-item-2894"><a href="https://www.blsurveyors.com/services/party-wall/">Party Wall</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2903"><a href="https://www.blsurveyors.com/services/party-wall/surveyors/">Party Wall Surveyor Roles</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2897"><a href="https://www.blsurveyors.com/services/party-wall/agreement/">Party Wall Award/Agreement</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2902"><a href="https://www.blsurveyors.com/services/party-wall/roles/">Party Wall Surveyor Types</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2901"><a href="https://www.blsurveyors.com/services/party-wall/party-wall-notices/">Party Wall Notices</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2900"><a href="https://www.blsurveyors.com/services/party-wall/interactive-guide/">Party Wall Interactive Guide</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3122"><a href="https://www.blsurveyors.com/services/party-wall/party-wall-faq/">Party Wall FAQ</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2899"><a href="https://www.blsurveyors.com/services/party-wall/fixed-costs/">Party Wall Fixed Costs</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2898"><a href="https://www.blsurveyors.com/services/party-wall/fee-quote/">Party Wall Fee Quote</a></li>
	</ul>
</li>
	<li class="bold menu-item menu-item-type-post_type menu-item-object-services menu-item-has-children menu-item-2895"><a href="https://www.blsurveyors.com/services/surveying/">Surveying</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2910"><a href="https://www.blsurveyors.com/services/surveying/schedule-condition-report/">Schedule of Condition</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2904"><a href="https://www.blsurveyors.com/services/surveying/boundary-dispute/">Boundary Dispute</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2909"><a href="https://www.blsurveyors.com/services/surveying/rics-homebuyer-report/">RICS Homebuyer Report</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2906"><a href="https://www.blsurveyors.com/services/surveying/full-building-surveys/">Full Building Surveys</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2905"><a href="https://www.blsurveyors.com/services/surveying/defect-analysis-report/">Defect Analysis Report</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2911"><a href="https://www.blsurveyors.com/services/surveying/snagging-list-report/">Snagging List</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2907"><a href="https://www.blsurveyors.com/services/surveying/licence-to-alter/">Licence to Alter</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2908"><a href="https://www.blsurveyors.com/services/surveying/property-mediation/">Property Mediation</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3022"><a href="https://www.blsurveyors.com/services/surveying/surveying-fee-quote/">Surveying Fee Quote</a></li>
	</ul>
</li>
	<li class="bold menu-item menu-item-type-post_type menu-item-object-services menu-item-has-children menu-item-2896"><a href="https://www.blsurveyors.com/services/valuation/">Valuation</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3009"><a href="https://www.blsurveyors.com/services/valuation/market-valuation-2/">Market Valuation</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2912"><a href="https://www.blsurveyors.com/services/valuation/fixed-valuation-costs/">Insurance Valuation</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2914"><a href="https://www.blsurveyors.com/services/valuation/market-valuation/">Retrospective Valuation</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3017"><a href="https://www.blsurveyors.com/services/valuation/leasehold-extension-valuation/">Leasehold Extension</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2913"><a href="https://www.blsurveyors.com/services/valuation/leasehold-extensions-explained/">Leasehold Extensions FAQ</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3014"><a href="https://www.blsurveyors.com/services/valuation/matrimonial-valuation/">Matrimonial Valuation</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3013"><a href="https://www.blsurveyors.com/services/valuation/probate-valuation/">Probate Valuation</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3023"><a href="https://www.blsurveyors.com/services/valuation/fee-quote/">Valuation Fee Quote</a></li>
	</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-191"><a href="https://www.blsurveyors.com/video-gallery/">Videos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a href="https://www.blsurveyors.com/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://www.blsurveyors.com/contact-us/">Contact</a></li>
</ul></div>    </nav>
</header>