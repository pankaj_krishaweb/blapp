<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
 
    public function index()
    {

        
        if (Auth::user()) {
            $user =Auth::user();
            if ($user->role==9 || $user->role==7 || $user->role==5 || $user->role==11) {
            return redirect('admin');
            }
            // user is client
            elseif($user->role==3 ||  $user->role==1){
                return redirect('client');
            }
        
        }
   
            return view('auth.login');
         
        
    }
}
