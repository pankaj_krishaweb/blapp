<?php

namespace App\Src\Docs\Invoice;
use Illuminate\Support\Facades\Storage;

class  InvoiceNoBLPW
{
	public $documentFolder="Invoice / Job Fee Quote";
	public $documentName="Invoice No BLPW";

    	public function create(\App\Job $job){

	    	# make storage directory
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(10);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
			$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);

		$section->addTextBreak();
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Invoice Reference: '.$job->invoice_no,['bold' => true], ['alignment' => 'right']);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('INVOICE NUMBER '.$job->invoice_no, ['bold'=>true, 'size'=>14]);

		$section->addTextBreak();
		$section->addText('RE: '.
			$job->bo->property_address_proposed_work
			.' / '.$job->ao->property_address_adjoining
			.' / '.$job->ao2->property_address_adjoining
			.($job->ao3->property_address_adjoining ? ' / '.$job->ao3->property_address_adjoining : '')
			.($job->ao4->property_address_adjoining ? ' / '.$job->ao4->property_address_adjoining : '')
			.($job->ao5->property_address_adjoining ? ' / '.$job->ao5->property_address_adjoining : '')
			.($job->ao6->property_address_adjoining ? ' / '.$job->ao6->property_address_adjoining : '')
			.($job->ao7->property_address_adjoining ? ' / '.$job->ao7->property_address_adjoining : '')
			.($job->ao8->property_address_adjoining ? ' / '.$job->ao8->property_address_adjoining : '')
			.($job->ao9->property_address_adjoining ? ' / '.$job->ao9->property_address_adjoining : '')
			.($job->ao10->property_address_adjoining ? ' / '.$job->ao10->property_address_adjoining : '')
			,['bold' => true]);

		$section->addTextBreak();
		$phpWord->addTableStyle('myTable', ['borderColor' => '006699', 'borderSize'  => 6, 'cellMargin'  => 10, 'width' =>100]);
		$table = $section->addTable('myTable');
		$table->addRow();
		$table->addCell(6500)->addText('DESCRIPTION OF SERVICE:', ['bold'=>true, 'size'=>10]);
		$table->addCell(2500)->addText('AMOUNT:', ['bold'=>true, 'size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('Serving Party Wall Notices', ['size'=>10]);
		$table->addCell(2500)->addText($job->notice_costs, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('Party Wall Agreement Costs ', ['size'=>10]);
		$table->addCell(2500)->addText($job->award_costs, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('Vat at 20%', ['size'=>10]);
		$table->addCell(2500)->addText($job->vat_amount, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('Land Registry costs at £6.00 per owner', ['size'=>10]);
		$table->addCell(2500)->addText($job->land_registry_costs, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('Printing & Postage Costs ', ['size'=>10]);
		$table->addCell(2500)->addText($job->printing_postage_costs, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('TOTAL DUE: ', ['bold'=>true, 'size'=>10]);
		$table->addCell(2500)->addText($job->final_amount, ['bold'=>true, 'size'=>10]);
		$section->addTextBreak();
		$section->addText('Please kindly make payment within 7 days of the invoice date. ', ['size'=>10]);
		$section->addTextBreak();
		$section->addText('For ease of identification please enter the Invoice number '.$job->invoice_no.' as the payment reference.', ['size'=>10]);
		$section->addTextBreak();
		$section->addText('Payment Methods',['bold'=>true,'size'=>10, 'underline'=>'single']);
		$section->addTextBreak();
		$section->addText('Bank Payment',['bold'=>true, 'size'=>10, 'underline'=>'single']);
		$section->addTextBreak();
		$section->addText('Please make a bank/online payment to the following account:', ['size'=>10]);
		$section->addTextBreak();
		$section->addText('BERRY LODGE SURVEYORS ',['bold'=>true, 'size'=>10]);

		$textrun = $section->addTextRun();
		$textrun->addText("Bank Account Number:			", ['size'=>10]);
		$textrun->addText(' 2 4 0 9 6 3 2 6', ['bold' => true, 'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("Sort Code:				", ['size'=>10]);
		$textrun->addText(' 5 6 - 0 0 - 1 4', ['bold' => true, 'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("Bank:					", ['size'=>10]);
		$textrun->addText(' Natwest', ['bold' => true, 'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("Branch:					", ['size'=>10]);
		$textrun->addText(' Baker Street, 69 Baker Street, W1U 6AT', ['bold' => true, 'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("IBAN:					", ['size'=>10]);
		$textrun->addText(' B40NWBK56001424096326', ['bold' => true,'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("BIC:					", ['size'=>10]);
		$textrun->addText(' NWBKGB2L', ['bold' => true, 'size'=>10]);
		//$section->addTextBreak();
		//$section->addText('Cheque',['bold'=>true, 'size'=>10, 'underline'=>'single']);
		$section->addTextBreak();
		$textrun = $section->addTextRun();
		// $textrun->addText('Please send a cheque made payable to ', ['size'=>10]);
		// $textrun->addText('‘BERRY LODGE SURVEYORS’', ['bold' => true, 'size'=>10]);
		// $textrun->addText(' to ', ['size'=>10]);
		// $textrun->addText('Upper Floor, 61 Highgate High Street, London, N6 5JY.', ['bold' => true, 'size'=>10]);
		// $textrun->addText(' For ease of identification please enter', ['size'=>10]);
		// $textrun->addText(' Invoice number '.$job->invoice_no , ['bold' => true, 'size'=>10]);
		// $textrun->addText(' on the rear of the cheque. ', ['size'=>10]);
		// $section->addTextBreak();
		$section->addText('THANK YOU IN ADVANCE FOR YOUR PAYMENT', ['bold' => true, 'size'=>10], [ 'align' => 'center' ]);
		$section->addTextBreak();
		$section->addText('Company VAT number is 218 9003 17', ['size'=>10], [ 'align' => 'center' ]);


		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
