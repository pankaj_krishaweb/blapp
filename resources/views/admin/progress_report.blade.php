@extends('layouts.frame')

@section('content')
<style type="text/css">
    h4{
            font-weight: bold;
    color: #414860;
    }
    h5{
color: #cccccc;
padding-top: 35px;
/*padding-bottom: 20px; */
    }
    h2 {
    color: #00b29c;
    font-weight: bold;
    font-size: 4em;
}
</style>
<div class="container-fluid">
    <div class="container" style="min-height: 80vh;">
         <div class="row">
            <div class="col-md-12">
                <h1>Progress Report</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ url('admin') }}">Admin</a></li>
                    <li>Progress Report</li>
                </ol>
            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>

                                <th></th>
                                <th> <h4>Name</h4></div>
                                <th> <h4 class="text-center">No of Jobs</h4></th>
                                <th> <h4 class="text-center">Tasks Completed</h4></th>
                                <th> <h4 class="text-center"> Outstanding Tasks</h4></th>
                                 <th> <h4 class="text-center"> Totals</h4></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td><a href="{{ url("/admin/progress-report/{$user->id}") }}"> @if ($user->picture)
                                        <img src="{{ asset("{$user->picture}") }}" style="width: 100px; height: 100px;"></a>
                                        @else
                                        <img src="{{ asset("/images/icons/male-icon.png") }}" style="width: 100px; height: 100px;"></a>
                                    @endif
                                    </td>


                                    <td><h5>{{ $user->name }}</h5> </td>
                                    <td><h2 class="text-center">{{ $user->jobs->where('status',1)->count() }} </h2></td>
                                    <td> <h2 class="text-center"> {{ $user->jobs->sum(function($job) { return $job->tasks->where('status',1)->count(); }) }}</h2></td>
                                    <td> <h2 class="text-center"> {{ $user->jobs->sum(function($job) { return $job->tasks->where('status',0)->count(); }) }}</h2></td>
                                     <th> <h4><small>Invoiced: </small><br> £{{ $user->jobs->sum(function($job) {
                    return $job->payments->sum('amount');
                }) }} <br> <small>Settled: </small><br><span style="color: #00B39D">£{{ $user->jobs->sum(function($job) {
                    return $job->payments->where('paid',1)->sum('amount');
                }) }}</span> </h4></th>
                                <tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
</div>
@endsection
@section('script')

<script>

$(document).ready(function() {

    $(".tasks").click(function() {

        var taskid = $(this).attr('data-key');

        if ($(this).is(":checked")) {
           var status="1";
        } else {
            var status="0";

        }

          $.ajax({
        url: '/tasks/checked',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {taskid: taskid, status: status},
        success: function(data){
        console.log(data);
        location.reload()

        },
    })
    });

});
</script>
@endsection
