<html>
<head>
	<style>
		.float-md-left,
		.float-md-right{
			text-align: center;
		}
		h2{
			font-size: 30px;
			line-height: 40px;
			color: #00b29c;
			font-weight: bold;
		}
		@media (min-width: 768px) {
			.title-wrap{
				display: table;
				max-width: 800px;
				width: 100%;
			}
			h2{
				font-size: 47px;
				line-height: 57px;
			}
			.title-wrap .float-md-left,
			.title-wrap .float-md-right{
				display: table-cell;
				vertical-align: middle;
			}
			.title-wrap h2{
				font-size: 47px;
				line-height: 1.5;
			}
			.float-md-left{
				float: left;
				width: calc(100% - 250px);
				text-align: left;
			}
			.float-md-right{
				float: right;
				width: 250px;
				text-align: right;
			}
			.float-md-right img{
				max-width: 100%;
			}
		}

	</style>
</head>
<body >

	<div class="text-center title-wrap">
		<div class="float-md-left">
			<h2>Unlock your Berry Lodge Portal Now!</h2>
		</div>
		<div class="float-md-right">
			<img src="{!! asset('images/login_email.png') !!}" width="200px">
		</div>
	</div>
<p>Dear {{$client_name}},</p>
<p>A very warm welcome to the Berry Lodge Surveying Client Login.</p>

<p>Our Surveying team will keep you well informed throughout the {!! $surveying_service !!} process so be sure to check your emails and login to see updates, documents and maybe even the occasional video from your Surveyor, {!! $surveyor_name !!}.</p>

<p>Your Berry Lodge Surveying Client Login is now active and you can access it via the following details:</p>

<p>E-Mail Address: {!! $client_email !!}</p>
<p>Password: {!! $client_password !!}</p>

<p>You can sign into your account <a href="https://app.blsurveyors.com/" target="_blank">here</a>.</p>
<p>Kind Regards, </p>

<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;">
<p><strong>Berry Lodge Surveyors</strong></p>
@include('shard.footer')

</div>
</body>
</html>