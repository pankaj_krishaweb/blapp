<?php

namespace App\Src\My_Jobs;

interface JobsInterface
{
	// create Job
	public function create(\Illuminate\Http\Request $request);

	// update Job
	public function update($id);

	public function data($model,  $request);

} 