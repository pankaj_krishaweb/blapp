
function contactFormPicker(){
	var oContactForm = $('.contact-form-wrapper'), oFormsSelector;
	if(oContactForm.length <= 0)return;

	oFormsSelectorLi = oContactForm.find('.forms-selector li');

	oContactForm.find('.form-picker .buttons li a').on("click",function(){
		oContactForm.find('.form-picker .buttons li').removeClass("current");
		oFormsSelectorLi.removeClass("current");

		var iIndex = $(this).closest('li').index();

		$(this).closest('li').addClass("current");
		oFormsSelectorLi.eq(iIndex).addClass("current");
	});
}


	





/*
*	projects Listing
*/
function projectsPage(){
	$('#existing-work-types a').on("click",function(){
		$(this).fadeOut(function(){
			$(this).remove();
			$('#projects_form').submit();
		});
		
	});
	
	
		
					
	
	var oGallery = $('#gallery-images');
	if(oGallery.length > 0){
		oGallery.find('ul.gallery-thumbs li').on("click",function(){
			var iIndex = $(this).index(),
				oGalleryLargeItem = oGallery.find('ul.gallery-large-images li').eq(iIndex);
				
			oGallery.find('ul.gallery-large-images li').removeClass('current');
			oGallery.find('ul.gallery-thumbs li').removeClass('current');
			
			oGalleryLargeItem.addClass('current');
			$(this).addClass('current');
		});
	}
	
	//	Custom select dropdown
	var oSelectDropdown = $('.custom-select-box');
	if(oSelectDropdown.length > 0){
		
		var oUlLi = oSelectDropdown.find('li'),
			oDropDownButton = oSelectDropdown.find('.title-header');
		
		oDropDownButton.on("click",function(){
			$(this).closest('.custom-select-box').find('ul').slideToggle();
			$(this).closest('.custom-select-box').toggleClass('open');
		});
		
		oUlLi.on("click",function(){
			var sValue = $(this).attr('data-value');
			$(this).closest('.custom-select-box').find('input').val(sValue);
			$(this).closest('.custom-select-box').find('ul').slideUp();
			$(this).closest('.custom-select-box').removeClass('open');
			$('#projects_form').submit();
		});
	}
	
}







/*
*	redirect from these hash links
*/
function hashLinkredirect(){
	var sHashLink = window.location.hash,
		sPathname = window.location.pathname,
		sReference = sHashLink.substring(0, 2);
	
	if(sReference == '#!' && sPathname == "/"){
		
		var sRawhash = window.location.hash,
			sUrl = "/" + window.location.hash.substring(2);
			
		window.location = sUrl;
	}
	
	
	
}





/*
*	Interactive Guide
*/
function interactiveGuidePage(){
	//var oInteractivePage = $('main.template-interactive-guide');
	var oInteractivePage = $('#property-interactive-wrapper');

	if(oInteractivePage.length <= 0) return;
	
	var oHouseMenus = oInteractivePage.find('.house-menus ul li'),
		oHouseSlides = oInteractivePage.find('ul.house-builder li.slide'),
		oInputFields = oHouseSlides.find('.form-area .buttons li input');
		
	//	Tick all boxes off
	oInputFields.attr('checked', false);
	
	//	House Menus
	oHouseMenus.on("click",	function(){
		if($(this).hasClass('current'))return;
		var iIndex = $(this).index();
		oHouseMenus.removeClass('current');
		$(this).addClass('current');
		oHouseSlides.removeClass('current').hide();
		oHouseSlides.eq(iIndex).fadeIn('current');
	});
	
	
	//	Input fields
	oInputFields.on("click", function(){
		if($(this).hasClass('current'))return;
		
		oInputFields.removeClass('current');
		$(this).addClass('current');
		var iIndex		= $(this).index(),
			sImageSrc	= $(this).attr("date-image"),
			sPdfLink	= $(this).attr("data-download"),
			oThisSlide	= $(this).closest('li.slide'),
			sDescription = $(this).attr("data-description");
			
			
			//	#property-interactive-wrapper .house-builder .description_wrapper .description-lists li{
		//oThisSlide.find('.image-wrapper .image img').attr("src",sImageSrc);
		
		oThisSlide.find('.image-wrapper .image .primary').fadeOut(100,function() {
			var oImagePrimary = $(this);
			oImagePrimary.find('img').one("load", function() {
				oImagePrimary.fadeIn(800);
			}).attr("src", sImageSrc);
		});
		
		$('#property-interactive-wrapper .house-builder .description_wrapper .description-lists li').hide();
		$(sDescription).slideDown();
		console.log(sDescription);
		
		oThisSlide.find('.download-area').slideDown();
		oThisSlide.find('.download-area .cta-block').attr("href",sPdfLink);
	});
	
	
	
	
}











/*
*	Contact Page
*/
function contactPage(){
	var oContactPage = $('main.template-contact-us-page');
	if(oContactPage.length <= 0) return;
	
	/*
	*	Set Maps
	*/
	
	
	$('.acf-map').on("click",function(e){
		e.preventDefault();
	});
	
	var oButtons = oContactPage.find('ul.ul-buttons li'),
		oMaps	=	oContactPage.find('.iframe-map-details li.map-single');
		

	oButtons.on("click",function(){
		oButtons.find('a').removeClass('current');
		$(this).find('a').addClass('current');
		oMaps.removeClass('current');
		oMaps.eq($(this).index()).addClass('current');
	});
	
}
	





/*
*	Carrer Form
*/
function carrerForm(){
	var oCareerField 	= $('.wpcf7-form #career-field'),
		oCareerChoices 	= $('.contact-form-wrapper .career-choice a');
		
	oCareerChoices.on("click", function(){
		oCareerChoices.removeClass('current');
		$(this).addClass('current');
		var sCareerVal = $(this).html();
		oCareerField.val(sCareerVal);
	});
}




/*
*	Main Menu
*/
function mainMenu(){
	var oPrimaryNav = $('nav.primary')	;	
	oPrimaryNav.find('ul.sub-menu').wrapInner( "<div class='full-wrapper' />");

	
	$('.touch nav.primary li.menu-item-has-children > a').click(function(e){
		e.preventDefault();
		$('#menu-primary-menu li.menu-item-has-children ul.sub-menu').hide();
		var oSubMenu = $(this).closest('li.menu-item-has-children').find('ul.sub-menu');
		oSubMenu.slideToggle();
	});
	
	//	Add additional menu item for touch menus
	if(!$('html').hasClass('touch')) return;
	 
	$('#menu-primary-menu li.menu-item-has-children').each(function(){
		var oALink = $(this).find('a').eq(0),
			oUl = $(this).find('ul.sub-menu .full-wrapper'),
			sTitle = oALink.text(),
			sLink  = oALink.attr('href');
			console.log(sTitle);
			oUl.prepend('<li><a title="'+sTitle+'" href="'+sLink+'">'+sTitle+'</a></li>');
	});
	 
	
}






function flexImages(sSelector){
	$(sSelector).each(function(){
		var oIMage = $(this),
			iWidth = oIMage.width();	
		oIMage.css({"max-width" : iWidth + "px", "width" : "100%", "height" : "auto"});	
	});
}




/*
	Mobile Menu Slider
	http://www.berriart.com/sidr/#usage
*/
function mobileMenu(){
	$('#mobile-btn').sidr({
		name: 'mobile-menu',
		side: 'right',
		source: '#mobile-menu-container',
		onOpen : function(){
			$('body').addClass('mobile-menu-open');
		},
		onClose : function(){
			$('body').addRemove('mobile-menu-open');
		},
		
	});
}


	
/* 
*	POPUP EVENT (replacement for target="_blank") 
*/
function nicePopUp(){
	$( "body" ).on( "click", ".popup",function( event ) {
		event.preventDefault();
		var sTarget = $( this ).attr( "href" );
		var iWidth = 1024;
		var iHeight = 768;
		var iScrollbars = 2;
		var iLeft = (screen.width)?(screen.width-iWidth)/2:100;
		var iTop = (screen.height)?(screen.height-iHeight)/2:100;
		var sSettings='width='+iWidth+',height='+iHeight+',top='+iTop+',left='+iLeft+',scrollbars='+iScrollbars+',location=yes,directories=no,status=yes,menubar=yes,toolbar=yes,resizable=yes';
		win=window.open( sTarget, "", sSettings );
	} );
}
	
	
	



/*
	Html5 Modernizer
	http://modernizr.com/docs/
*/
function moderizr(){
	Modernizr.load({
	  test: Modernizr.mq('all'),
	  nope: '/wp-includes/js/html5/respond.min.js'
	});
	
	Modernizr.addTest('preserve3d', function(){return Modernizr.testAllProps('transformStyle', 'preserve-3d');});
	
	function GetIEVersion() {
		var sAgent = window.navigator.userAgent;
		var Idx = sAgent.indexOf("MSIE");
		
		// If IE, return version number.
		if (Idx > 0) 
			return parseInt(sAgent.substring(Idx+ 5, sAgent.indexOf(".", Idx)));
		// If IE 11 then look for Updated user agent string.
		else if (!!navigator.userAgent.match(/Trident\/7\./)) 
			return 11;
		else
			return 0; //It is not IE
		}
	
		//	Place IE version into the html tag
		var IeVersion = GetIEVersion();
		if(IeVersion > 0){
			$('html').addClass('ie-'+IeVersion);
			$('html').addClass('is-ie');
		}else{
			$('html').addClass('not-ie');
		}
	}

/*
	Colourbox lightbox
	http://www.jacklmoore.com/colorbox/
*/
function lightbox(){
	$('.lightbox').colorbox({
			"maxWidth" : 950,
			"maxHeight" : 950,
			"width"	: "100%"
		
		});
		
	$('.iframe-lightbox-tablet').colorbox({
		'innerWidth'	:	'640',
		'innerHeight'	:	'360',
		'iframe'	:	true,
		'title'		:	false
	});
	
	$('.iframe-lightbox').colorbox({
		'innerWidth'	:	'800',
		'innerHeight'	:	'450',
		//'html'			:'<iframe width="853" height="480" src="https://www.youtube-nocookie.com/embed/p2HGIdQ-kUw?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>',
		'iframe'	:	true,
		'title'		:	false
	});
		
	
}



/*

	Flexslider image marquee
	http://www.woothemes.com/flexslider/#tabs-flexslider-info-tabber-tab-1
*/
function imageSliders(){
	$('.flexslider-banner').flexslider({
		animation: "slide",
		animationSpeed: 400,
		/*
		controlNav : false,
		directionNav : false,
		*/
		slideshow : true,
		smoothHeight: true,
		controlsContainer: ".flexslider-banner .flex-navigation .inner-nav",
		start: function(oSlider){
		/*
			//	CallBack after loading of slides
			var oSlides = oSlider.find('ul.slides li'),
				iTallestContent = 0,
				oResizeObject = oSlider.find('.flex-viewport, ul.slides, ul.slides li .banner-content'),
				oAllContent = oSlider.find('ul.slides li .banner-content');
			
			
			//	Find tallest slide
			oSlides.each(function(){
				var oContentWrapper = $(this),
				oContent = oContentWrapper.find('.banner-content'),
				iContentHeight = oContent.height();
			console.log(iContentHeight);
				if(iContentHeight > iTallestContent){
					iTallestContent = iContentHeight; //50 is just for some padding
				}
			});
			console.log(iTallestContent);
			
			//	Video Resizer
			marqueeResizer(oResizeObject,oAllContent,iTallestContent);
			
			$(window).bind("resize", function() {
				marqueeResizer(oResizeObject,oAllContent,iTallestContent);
			});
			oSlider.addClass('loaded');
			*/
		}
	});
	
	
	
	
	
	
	$('.flexslider').flexslider({
		animation: "slide",
		animationSpeed: 400,
		slideshow : true,
		smoothHeight: false,
		controlsContainer: ".flexslider .flex-navigation .inner-nav",
		start: function(oSlider){
			
		}
	});
	
	
	
	
	
}
	  



/*
*	Resizes the outter wrapper within the inner height
*/
function marqueeResizer(oResizeObject,oContent,iContentHeight){
	
	oContent.css({'height' : 'auto'});
	//	 section.header-banner.full-screen iframe,
	var oResizeMe = oResizeObject,
		oHeader = $('header.primary'),
		iHeaderHeight = oHeader.height(),
		iAdminBarHeight = $('#wpadminbar').height(),
		iDocumentHeight = $(window).height(),
		iPageHeight = (iDocumentHeight - iAdminBarHeight -iHeaderHeight);

		//	Banner viewpoint area to small or smaller than content within make non scalable
		if(iContentHeight >= iPageHeight){
			//oResizeMe.css({'height' : 'auto'});
			oResizeMe.css({'height' : iContentHeight});
		//	Scale me please
		}else{
			oResizeMe.css({'height' : iPageHeight});
			oContent.css({'height' : iPageHeight});
		}		
}//	bannerMatchDocHeight





/*
*	Moves clouds on homepage
*/
function moveClouds(){
	init();
	$(document).on("resize", function(){
		init();
	});

	function init(){
		var oCloudWrapper = $('section.cloud-marquee');
		if(oCloudWrapper.length <= 0) return;
		var oCloudsLi = oCloudWrapper.find('ul.clouds li');

		oCloudsLi.each(function(){
			var iBaseLeft = $(this).attr('data-left'),
				iBaseTop = $(this).attr('data-top'),
				iMovement	= $(this).attr('data-move'),
				oCloud = $(this);
				
				oCloud.css({"left": iBaseLeft + "%","top": iBaseTop + "%"});
				oCloud.removeClass('hidden');
				
				if($('html').hasClass('no-touch')){
					$(window).on('mousemove', function(e){
					
						var mouseX = e.pageX,
							windowWidth = oCloudWrapper.width(),
							iPercentageX = (((windowWidth/2) - mouseX)/10),
							iAddToX = ((windowWidth/100) / iMovement) * (iPercentageX/100),
							iNewLeft = (parseInt(iBaseLeft) + iAddToX);
							
						var mouseY = e.pageY,
							windowHeight = oCloudWrapper.height(),
							iPercentageY = (((windowHeight/2) - mouseY)/10),
							iAddToY = ((windowHeight/100) / (iMovement / 10)) * (iPercentageY/100),
							iNewTop = (parseInt(iBaseTop) + iAddToY);
						oCloud.css({"left": iNewLeft + "%","top": iNewTop + "%"});
					});
				}
			
		});	
	}

}

















