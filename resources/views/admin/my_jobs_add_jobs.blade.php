@extends('layouts.frame')

@section('content')

<div class="container-fluid">
	{{-- <link href="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet"> --}}
    	<div class="container" style="min-height: 400px; padding-bottom: 50px;">
	       <h1>Add Jobs</h1>
	        <ol class="breadcrumb"> 
			<li><a href="{{ url('admin') }}">Home</a></li>
			<li><a href="{{ url('admin/my-jobs') }}">My Jobs</a></li>
			<li class="admin/jobs">{{ $addOrUpdate=='add' ? 'Add' : 'BLSN'.$jobid }}</li>	  
		</ol>
		<form action="{{ url('/admin/jobs/add/partywall').($addOrUpdate=='add' ? '' : '/'.$jobid) }}" method="post">
			{{ csrf_field() }}
			<input type="hidden" name="j_type" value="partywall">
		       <div class="row">
		         	<div class="col-md-12">
			         	@if (session('message'))
						<div class="alert alert-info">
						       {{ session('message') }}
						</div>
					@endif
				</div>
				
		         	<style>.nav-tabs{ margin-top: 10px; margin-bottom: 20px; }</style>
		         	<div class="col-md-12">
		         		<div class="tab-content">
		         			<button class="btn btn-success pull-right">{{ ucwords($addOrUpdate) }} </button>
		         			<ul class="nav nav-tabs" id="party-wall-menu">
							   	<li class="active"><a data-toggle="pill" href="#home"> Party Wall Job</a></li>
							    <li><a data-toggle="pill" href="#menu1">BO</a></li>
							    <li><a data-toggle="pill" href="#menu2">AO 1</a></li>
							   	<li><a data-toggle="pill" href="#menu3">AO 2</a></li>
							   	<li><a data-toggle="pill" href="#menu4">AO 3</a></li>
							   	<li><a data-toggle="pill" href="#menu5">AO 4</a></li>
							   	<li><a data-toggle="pill" href="#menu6">AO 5</a></li>
							   	<li><a data-toggle="pill" href="#menu7">AO 6</a></li>
							   	<li><a data-toggle="pill" href="#menu8">AO 7</a></li>
							   	<li><a data-toggle="pill" href="#menu9">AO 8</a></li>
							   	<li><a data-toggle="pill" href="#menu10">AO 9</a></li>
							   	<li><a data-toggle="pill" href="#menu11">AO 10</a></li>
							</ul>
							
						<div id="home" class="tab-pane fade in active">
					       	@include('components.jobs.job')
					    	</div>
					    	<div id="menu1" class="tab-pane fade">
					    		@include('components.jobs.bo')					     
					    	</div>
					    	<div id="menu2" class="tab-pane fade">
					     		@include('components.jobs.ao1')
					    	</div>
					    	<div id="menu3" class="tab-pane fade">
					     		@include('components.jobs.ao2')
					    	</div>
					    	<div id="menu4" class="tab-pane fade">
					     		@include('components.jobs.ao3')
					    	</div>
					    	<div id="menu5" class="tab-pane fade">
					     		@include('components.jobs.ao4')
					    	</div>
					    	<div id="menu6" class="tab-pane fade">
					     		@include('components.jobs.ao5')
					    	</div>
					    	<div id="menu7" class="tab-pane fade">
					     		@include('components.jobs.ao6')
					    	</div>
					    	<div id="menu8" class="tab-pane fade">
					     		@include('components.jobs.ao7')
					    	</div>
					    	<div id="menu9" class="tab-pane fade">
					     		@include('components.jobs.ao8')
					    	</div>
					    	<div id="menu10" class="tab-pane fade">
					     		@include('components.jobs.ao9')
					    	</div>
					    	<div id="menu11" class="tab-pane fade">
					     		@include('components.jobs.ao10') 
					    	</div>
					    	
				  	</div>
		         	</div>
		        </div>
	       </form>
	</div>
</div>
@endsection
@section('script')
{{-- <script src="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script> --}}

<script type="text/javascript">
	$('#party-wall-tab').on('show.bs.tab', function(){
        	$('#party-wall-menu').show();
    	});
    	$('#survey-tab').on('show.bs.tab', function(){
        	$('#party-wall-menu').hide();
    	});

	$(document).on("focus keyup", "input.autocomplete", function() {
    // Cache useful selectors
    var $input = $(this);
    var $dropdown = $input.next("ul.dropdown-menu");
    
    // Create the no matches entry if it does not exists yet
    if (!$dropdown.data("containsNoMatchesEntry")) {
        $("input.autocomplete + ul.dropdown-menu").append('<li class="no-matches hidden"><a>No matches</a></li>');
        $dropdown.data("containsNoMatchesEntry", true);
    }
    
    // Show only matching values
    $dropdown.find("li:not(.no-matches)").each(function(key, li) {
        var $li = $(li);
        $li[new RegExp($input.val(), "i").exec($li.text()) ? "removeClass" : "addClass"]("hidden");
    });
    
    // Show a specific entry if we have no matches
    $dropdown.find("li.no-matches")[$dropdown.find("li:not(.no-matches):not(.hidden)").length > 0 ? "addClass" : "removeClass"]("hidden");
});

$(document).on("click", "input.autocomplete + ul.dropdown-menu li", function(e) {
    // Prevent any action on the window location
    e.preventDefault();
    
    // Cache useful selectors
    $li = $(this);
    var data_sr = $li.attr('data-sr');
    $input = $li.parent("ul").prev("input");
    
   if ($li.attr('data-sr')=='b-sr') {
   	$('#surveyor_name').val($li.attr('data-b-name'));    	
   	$('#surveyor_qualifications').val($li.attr('data-b-qualifications'));
   	$('#surveyor_company_name').val($li.attr('data-b-company'));
   	$('#surveyor_company_address').val($li.attr('data-b-address'));
   	$('#surveyor_contact_details').val($li.attr('data-b-contact'));
   	$('#surveyor_email').val($li.attr('data-b-email'));
   }
    
   if ( Math.floor(data_sr) == data_sr && $.isNumeric(data_sr) ) {
      data_a = data_sr; //(data_sr != 1)? data_sr: '';
   	$(`#ao_${data_sr}_surveyor_name`).val($li.data(`a${data_a}-name`));    	 	
   	$('#ao_'+data_sr+'_surveyor_qualifications').val($li.attr('data-a'+data_a+'-qualifications'));
   	$('#ao_'+data_sr+'_surveyor_company_name').val($li.attr('data-a'+data_a+'-company'));
   	$('#ao_'+data_sr+'_surveyor_company_address').val($li.attr('data-a'+data_a+'-address'));
   	$('#ao_'+data_sr+'_surveyor_contact_details').val($li.attr('data-a'+data_a+'-contact'));
   	$('#ao_'+data_sr+'_surveyor_email').val($li.attr('data-a'+data_a+'-email'));
   }
       
    // Update input text with selected entry
    if (!$li.is(".no-matches")) {
        $input.val($li.text());
    }
});


$('#final_amount').keyup(function(e){
	//alert($('#final_amount').val());
	
	//if(e.keyCode == 8)alert('backspace trapped')

	var final_amount = $('#final_amount').val();
	if (final_amount == 0) {
		final_amount = 0;
	}
	//alert(parseFloat(final_amount));
	var vat_amount = (parseFloat(final_amount) * 20) / 100;
	$('#vat_amount').val(vat_amount.toFixed(2));  
});

$('#land_registry_costs , #final_amount , #printing_postage_costs, .land_registry_costs ').on('keyup click change keypress', function(e){
	var final_amount = $('#final_amount').val();
	var vat_amount = $('#vat_amount').val();
	var printing_postage_costs = $('#printing_postage_costs').val();
	var land_registry_costs = ($('#land_registry_costs').val()).slice(1);

	if (land_registry_costs == 0) {
		land_registry_costs = 0;
	}
	if (final_amount == 0) {
		final_amount = 0;
	}
	if (vat_amount == 0) {
		vat_amount = 0;
	}
	if (printing_postage_costs == 0) {
		printing_postage_costs = 0;
	}
	var cost_of_service = parseFloat(land_registry_costs) + parseFloat(final_amount) + parseFloat(printing_postage_costs) + parseFloat(vat_amount);
	console.log(cost_of_service);
	$('#cost_of_service').val(cost_of_service);

});
	
	
</script>
@endsection
