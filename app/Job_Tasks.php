<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job_Tasks extends Model
{
	public function getCreatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value)); 
    }
    public function getUpdatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value));   
    }
	protected $table='job_tasks';

	protected $fillable = [
        'job_id', 'job_type','task', 'is_internal','status','task_id'
    ];

    public function job()
    {
        return $this->belongsTo(Job::class);
    }
    public function tasks(){
        // return $this->belongsTo(Task::class);
        return $this->hasMany('App\Task','id');
    }
    
}
