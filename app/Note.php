<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = ['job_id','user_id','slug' ];

    public function note_history(){
    	return $this->hasMany(NoteHistory::class);
    }
}
