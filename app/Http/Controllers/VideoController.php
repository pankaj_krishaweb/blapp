<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job_Tasks;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    public function video(Request $request)
    {
    	
	    if (!isset($_POST['audio-filename']) && !isset($_POST['video-filename'])) {
	        return 'Empty file name.';
	    }

	    // do NOT allow empty file names
	    if (empty($_POST['audio-filename']) && empty($_POST['video-filename'])) {
	        return 'Empty file name.';
	    }

	    // do NOT allow third party audio uploads
	    if (false && isset($_POST['audio-filename']) && strrpos($_POST['audio-filename'], "RecordRTC-") !== 0) {
	        return 'File name must start with "RecordRTC-"';
	    }

	    // do NOT allow third party video uploads
	    if (false && isset($_POST['video-filename']) && strrpos($_POST['video-filename'], "RecordRTC-") !== 0) {
	        return 'File name must start with "RecordRTC-"';
	    }

	    if ($request->file('audio-blob')) 
	    {
    		$file = $request->file('audio-blob');
		    Storage::makeDirectory("public/task/{$request->get('task_id')}/video/");
		    $filePath = base_path()."/storage/app/public/task/{$request->get('task_id')}/video/";
		    $file->move($filePath,$file->getClientOriginalName());
	    	
	    }
	    else
	    {
	    	$file = $request->file('video-blob');
	    	Storage::makeDirectory("public/task/{$request->get('task_id')}/video/");
		    $filePath = base_path()."/storage/app/public/task/{$request->get('task_id')}/video/";
		    $file->move($filePath,$file->getClientOriginalName());
	    }
    	//dd($request->file('video-blob')->getClientOriginalName());
	    
	    // $fileName = '';
	    // $tempName = '';
	    // $file_idx = '';
	    
	    // if (!empty($_FILES['audio-blob'])) {
	    //     $file_idx = 'audio-blob';
	    //     $fileName = $_POST['audio-filename'];
	    //     $tempName = $_FILES[$file_idx]['tmp_name'];
	    // } else {
	    //     $file_idx = 'video-blob';
	    //     $fileName = $_POST['video-filename'];
	    //     $tempName = $_FILES[$file_idx]['tmp_name'];
	    // }
	    
	    // if (empty($fileName) || empty($tempName)) {
	    //     if(empty($tempName)) {
	    //         echo 'Invalid temp_name: '.$tempName;
	    //         return;
	    //     }

	    //     echo 'Invalid file name: '.$fileName;
	    //     return;
	    // }

	    /*
	    $upload_max_filesize = return_bytes(ini_get('upload_max_filesize'));

	    if ($_FILES[$file_idx]['size'] > $upload_max_filesize) {
	       echo 'upload_max_filesize exceeded.';
	       return;
	    }

	    $post_max_size = return_bytes(ini_get('post_max_size'));

	    if ($_FILES[$file_idx]['size'] > $post_max_size) {
	       echo 'post_max_size exceeded.';
	       return;
	    }
	    */

	    //$filePath = 'video/' . $fileName;
	    
	    // make sure that one can upload only allowed audio/video files
	    // $allowed = array(
	    //     'webm',
	    //     'wav',
	    //     'mp4',
	    //     'mkv',
	    //     'mp3',
	    //     'ogg'
	    // );
	    // $extension = pathinfo($filePath, PATHINFO_EXTENSION);
	    // if (!$extension || empty($extension) || !in_array($extension, $allowed)) {
	    //     echo 'Invalid file extension: '.$extension;
	    //     return;
	    // }
	    
	    // if (!move_uploaded_file($tempName, $filePath)) {
	    //     if(!empty($_FILES["file"]["error"])) {
	    //         $listOfErrors = array(
	    //             '1' => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
	    //             '2' => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
	    //             '3' => 'The uploaded file was only partially uploaded.',
	    //             '4' => 'No file was uploaded.',
	    //             '6' => 'Missing a temporary folder. Introduced in PHP 5.0.3.',
	    //             '7' => 'Failed to write file to disk. Introduced in PHP 5.1.0.',
	    //             '8' => 'A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help.'
	    //         );
	    //         $error = $_FILES["file"]["error"];

	    //         if(!empty($listOfErrors[$error])) {
	    //             echo $listOfErrors[$error];
	    //         }
	    //         else {
	    //             echo 'Not uploaded because of error #'.$_FILES["file"]["error"];
	    //         }
	    //     }
	    //     else {
	    //         echo 'Problem saving file: '.$tempName;
	    //     }
	    //     return;
	    // }

	    $task = Job_Tasks::find($request->get('task_id'));
	    $task->video = "/storage/task/{$request->get('task_id')}/video/". $file->getClientOriginalName();
	    $task->save();

	    
	    return 'success';
    }
}
