@extends('layouts.frame') 
@section('content')
<style>
    /* Style the input field */

    #myInput {
        padding: 20px;
        margin-top: -6px;
        border: 0;
        border-radius: 0;
        background: #f1f1f1;
    }

    #job-search-btn {
        margin-top: 40px;
        background-color: #fff;
        color: #b3b3b3;
        border: solid 0.5px #b3b3b3;
        border-radius: 5px;
    }

    h4 h3 {
        color: #414860;
        font-weight: bold;
    }

    h4 span {  color: #909090;   }
    h2 {  color: #00b29c;  font-weight: bold;    }

    .filter-input {
        padding: 20px;
        margin: 10px 0px;
        border: 0;
        border-radius: 0;
        background: #eef2f6;
    }
    .tasksmsg{ width:95%; height:30px; font-size:14px;  }
    .hgclass{ /*background-color:yellowgreen; color:white !important;*/ border:1px solid #00B29D; padding-left:3px; }
    .adellipsis { overflow: hidden;  /*white-space: nowrap;*/  text-overflow: ellipsis; }
</style>


<div class="container-fluid">
  <div class="container" style="padding-bottom: 40px;">
    <h1 id="job_id" data-id="">Storage</h1>
    <ol class="breadcrumb">
			<li><a href="{{ url('admin') }}">Home</a></li>
	   	<li><a href="{{ url('admin/storage') }}">Storage</a></li>
	  	<!--<li class="admin/jobs">BLSN </li>-->
	 </ol>
    
		@if (session('message'))
		    <div class="alert alert-info">
		        {{ session('message') }}
		    </div>
		@endif
      <div class="col-md-6 col-sm-12">
      <input type="text" name="search" class="form-control filter-input searchfolder" placeholder="Search ...">
      </div>
	</div>
	<div class="container" style="">
      <!--<div class="row">-->
      <div class="col-md-12 col-sm-12">
         @if($sel_folder != $rootdir)
           <a href="#" class="prevDir" ><i class="fa fa-level-up"> Up One Level</i></a>
           <span style="color:#3097D1; padding:0 0 0 10px">{{preg_replace('#InHouse/#', '../', $sel_folder, 1)}}</span>
         @else
            <!--<a href="#" onclick="return false;" > /(root)</a>-->
         @endif
      </div>
	 	@foreach($folders as $k=>$folder_v)
          @php
            $folder = $folder_v['folder'];
            $sub_folders = ($folder_v['sub_folders'])?? array();
            $files = ($folder_v['files'])?? array();
            $folder_full = $folder_v['folder_full'];
            $adminonly = (in_array($folder_full, $stgtable))? "checked":"";
            
            /*$collapse="data-toggle='collapse'";
            if( isset($_POST['nav_action']) ){
            	if( $_POST['nav_action']=='search_file' ){
						$collapse="data-toggle='collapse in' aria-expanded='true'";
					}
					
				}*/
          @endphp
          
        <!--</div>-->
        
 			<div class="col-md-4 col-sm-4">
	 			<div class="well storage item" >
	        		<a style="color: #00B29D;" title="Click to open this folder" data-fullpath="{{$folder_full}}" href="#{{str_slug($folder, '_')}}" class="sel_folder"  onclick="return false;"><i class="fa fa-folder-o" aria-hidden="true"></i>{{ $folder }}</a><span class="adminchk pull-right" >
                 @if(Auth::user()->role==9)
                 <input type="checkbox" data-fullpath="{{$folder_full}}" title="Mark to restrict this folder to admin only." name="adminonly" class="adminonly" value="yes" {{$adminonly}} /> Admin
                 @endif
                 @if(Auth::user()->role==9)
                  <button data-fullpath="{{$folder_full}}" class="btn btn-danger btn-xs pull-right delete-folder"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                  @endif
                 <a href="#{{str_slug($folder, '_')}}" data-toggle="collapse" class="" onclick="return false;"><i class="fa fa-chevron-down" style="font-size:24px"></i></a>
                 </span>
	        		<hr>
	        		
	        		<div id="{{str_slug($folder, '_')}}" class="collapse">
	               @foreach($sub_folders as  $item)
	                  {{-- @if ($item) --}}
	                     @php
	                        $subfname = $item['folder'];
	                        $subfolder_full = $item['folder_full'];
	                        
	                     @endphp
	               		<p>
	                  		<a href="#{{str_slug($subfname, '_')}}" title="Click to open this folder" style="color: #00B29D;" data-fullpath="{{$subfolder_full}}" class="text-right sel_folder"><i class="fa fa-folder-o" aria-hidden="true"></i>{{$subfname}}</a>
	                        @if(Auth::user()->role==9)
	                        <button data-fullpath="{{$subfolder_full}}" class="btn btn-danger btn-xs pull-right delete-folder"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
	                        @endif
	               			
	               		</p>
	               	{{-- @endif --}}
	               @endforeach
		        		<hr>
	               @foreach($files as  $eachfile)
	                  {{-- @if ($item) --}}
	                     @php
	                        $filename = ltrim(strrchr($eachfile, "/"), "/");
	                        $fileinfo = pathinfo(asset('storage/'.$eachfile));
	                        //$filename = substr(strrchr($eachfile, "/"), 1);
	                        $trimfilen = mb_strimwidth($filename, 0, 20, "…");
                           
                           $hgclass = ($eachfile==@$_POST['sel_folder'])? "hgclass":"";
	                     @endphp
	               		<p>
	                  		<a href="#{{str_slug($filename, '_')}}" class="{{$hgclass}}" style="color: #00B29D;width:63%;display:inline-block;" data-fullpath="" ><span class="adellipsis" title="{{$filename}}" style="word-break:break-all;display:inline-block;"><i class="fa fa-file" aria-hidden="true"></i>{{$trimfilen}}</span></a>
	                        @if(Auth::user()->role==9)
	                        <button data-fullpath="{{$eachfile}}" class="btn btn-danger btn-xs pull-right delete-upload-doc"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
	                        <button data-fullpath="{{$eachfile}}" class="btn btn-info btn-xs pull-right transfer-to-jobs">
	                           <i class="fa fa-exchange" aria-hidden="true"></i>
	                        </button>
	                        @endif
	                    		<a href="{{ asset('storage/'.$eachfile) }}" class="btn btn-success btn-xs pull-right" download><span><i class="fa fa-cloud-download" aria-hidden="true"></i></span></a>
	                        <button type="button" class="btn btn-primary btn-xs pull-right view-doc" data-link="{{asset('storage/'.$eachfile)}}" data-type="{{$fileinfo['extension']}}">
	                           <i class="fa fa-search"></i>
	                        </button>
	               			
	               		</p>
	               	{{-- @endif --}}
	               @endforeach
	        		</div>
		        		

	         </div>
	      </div>
 		@endforeach
       <form method="post" class="navform" action="{{url('/admin/storage')}}">
       <input type="hidden" value="{{csrf_token()}}" name="_token" />
       <input type="hidden" value="" name="nav_action" />
       <input type="hidden" name="sel_folder" id="sel_folder" value="{{$sel_folder}}"/>
       </form>
    </div>
</div>


{{-- upload --}}

{{-- doc preview modal --}}
  <div class="modal fade" id="docViewModal" role="dialog">
    <div class="modal-dialog modal-lg fullmod">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Doc View</h4>
        </div>
        <div class="modal-body" id="doc-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<!--</div>-->

<div class="modal fade" id="dirViewModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select folder to transfer into</h4>
        </div>
        <div class="modal-body" id="doc-body">
         <div id="treeview5" class=""></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

@if(Auth::user()->role==9)
@include('admin.parts.upload')
@endif

@endsection 

@section('script')

<script src="{{ asset('js/bootstrap-treeview.js') }}"></script>

<link rel="stylesheet" href="{{ asset('css/jquery.auto-complete.css') }}" />
<script src="{{ asset('js/jquery.auto-complete.js') }}"></script>

<script>
   //Search by Job Number

   $(document).ready(function() {
     $('[data-toggle="tooltip"]').tooltip({
         html: true
     });  
     
      $(document).on('click', '.sel_folder', function(event) {
        
         var _sel_subfolder = $(this).data('fullpath');
         $('#sel_folder').val(_sel_subfolder);
         $('.navform').submit();
      });
      
      $(document).on('click', '.prevDir', function(event) {
        
         var _sel_subfolder = $(this).data('fullpath');
         $('input[name=nav_action]').val('nav_back');
         $('.navform').submit();
      });
      
      /*$( document ).ajaxComplete(function( event, xhr, settings ) {
         if ( settings.url === "/admin/storage" ) {  }
      });*/
      
      $(document).on('click', '.view-doc', function(event) {

		   if($(this).attr('data-type')=='jpg' || $(this).attr('data-type')=='jpeg' || $(this).attr('data-type')=='png' )
		   {
		   	window.open($(this).attr('data-link'));
		   } else
		   {
		   	event.preventDefault();
		   	
		   	if ($(this).attr('data-type')=='doc' || $(this).attr('data-type')=='docx' || $(this).attr('data-type')=='xlsx' || $(this).attr('data-type')=='xls' )
		   	{
				   window.open($(this).attr('data-link'));
		   		//window.open("https://docs.google.com/gview?url="+encodeURI($(this).attr('data-link'))+"&embedded=true");
		   		/*$('#doc-body').html('<iframe src="https://docs.google.com/gview?url='+encodeURI($(this).attr('data-link'))+'&embedded=true"></iframe>');*/
               /*$('#doc-body').html('<iframe src="https://docs.google.com/gview?url=http://vps.mamretechnologies.com/~vpsmamre/Storage_estimate.docx&embedded=true"></iframe>');*/
               $('#doc-body').html('<i class="fa fa-gear faa-spin animated"></i>');
               
		   		/*$('#doc-body').html('<iframe src="https://view.officeapps.live.com/op/embed.aspx?src='+encodeURI($(this).attr('data-link'))+'&embedded=true" width="100%" height="100%"></iframe>');*/
				/*$('#doc-body').html('<iframe src="https://docs.google.com/gview?url='+encodeURI($(this).attr('data-link'))+'&embedded=true" width="100%" height="100%"></iframe>');*/
               /*$('#doc-body').html('<iframe src="https://view.officeapps.live.com/op/embed.aspx?src=http://vps.mamretechnologies.com/~vpsmamre/Storage_estimate.docx'+'&embedded=true" width="100%" height="100%"></iframe>');*/
		   	} else if( $(this).attr('data-type')=='pdf' ){
				   /*$("#docViewModal").modal();
               $('#doc-body').html('<i class="fa fa-ellipsis-h faa-bounce animated"></i>');
               $('#doc-body').html('<iframe src="https://docs.google.com/gview?url='+encodeURI($(this).attr('data-link'))+'&embedded=true" width="100%" height="100%"></iframe>');*/
               window.open($(this).attr('data-link'));
            } else {
		   		$('#doc-body').html('<h2> Sorry on-line preview not available for this file type.</h2>');
		   	}
		   }

      });
      
      
      $(document).on('click', '.delete-folder', function(event) {
   
         var _selfold = $(this).data('fullpath');
         if( _selfold ){
            swal("Are you sure you wish to delete this folder and all its contents?", {
                 buttons: ["Cancel", "Yes Delete!"],
            }).then(function(value){
              if(value==true){
               $('input[name=nav_action]').val('del_folder');
               $('<input>').attr({ type: 'hidden', name: 'sel_delfolder', value: _selfold}).appendTo('.navform');
               $('.navform').submit();
               }
              return false;
            });
         }
      });
      
      $(document).on('click', '.adminonly', function(event) {
         var _obj = $(this);
         var _selfold = $(this).data('fullpath');
         if( _selfold ){
            _val = $(this).is(':checked');
            if( _val ){
               _msg = "Are you sure you wish to make this folder restricted to admin only?";
            }else{
               _msg = "Are you sure you wish to remove this folder restriction as admin only?";
            }
            swal(_msg, {
                 buttons: ["Cancel", "Yes"],
            }).then(function(value){
              if(value==true){
               $('input[name=nav_action]').val('admin_chk');
               $('<input>').attr({ type: 'hidden', name: 'admin_folder', value: _selfold}).appendTo('.navform');
               $('<input>').attr({ type: 'hidden', name: 'admin_chk', value: +_val}).appendTo('.navform');
               $('.navform').submit();
               }else{
                  //$(event).preventDefault();
                  $(_obj).prop("checked", false);
               }
              return false;
            });
         }
      });
      
      $(document).on('click', '.delete-upload-doc', function(event) {

           event.preventDefault();
           var _selfile = $(this).data('fullpath');
           swal("Are you sure you wish to delete this file?", {
              buttons: ["Cancel", "Yes Delete!"],
           }).then(function(value){
           if(value==true){
              $('input[name=nav_action]').val('del_file');
              $('<input>').attr({ type: 'hidden', name: 'sel_delfile', value: _selfile}).appendTo('.navform');
              $('.navform').submit();
           }
           return false;
          });
      });
      
      $(document).on('click', '.transfer-to-jobs', function(event) { //dirViewModal

        event.preventDefault();
        var _selfile = $(this).data('fullpath');
        $('#treeview5').treeview({
          color: "#428bca",
          expandIcon: 'glyphicon glyphicon-chevron-right',
          collapseIcon: 'glyphicon glyphicon-chevron-down',
          nodeIcon: 'glyphicon glyphicon-folder-close',
          data: <?=$jsondt?>
        }).on('nodeSelected', function (event, data) {
            swal("Are you sure to move this file to this folder?", {
              buttons: ["Cancel", "Yes Move!"],
            }).then(function(value){
               if(value==true){
                  //console.log('data = ' + JSON.stringify(data));
                  $('input[name=nav_action]').val('move_file');
                  $('<input>').attr({ type: 'hidden', name: 'moveto_f', value: data.href}).appendTo('.navform');
                  $('<input>').attr({ type: 'hidden', name: 'movefrom_f', value: _selfile}).appendTo('.navform');
                  $('.navform').submit();
               }
            });
        });
        $("#dirViewModal").modal(); 

     });
     
     var _alldirs = '<?=json_encode($alldirs); ?>';
     
     $('input.searchfolder').autoComplete({
          minChars: 1,
          source: function(term, suggest){
              term = term.toLowerCase();
              //var choices = [['Australia', 'au'], ['Austria', 'at'], ['Brasil', 'br']];
              var choices = JSON.parse(_alldirs);
              var suggestions = [];
              for (i=0;i<choices.length;i++){
                 var endstring = /[^/]*$/.exec(choices[i])[0];
                 if (~(endstring).toLowerCase().indexOf(term)) suggestions.push([endstring, choices[i]]);
              }
              suggest(suggestions);
          },
          renderItem: function (item, search){
              search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
              var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
              //return '<div class="autocomplete-suggestion" data-langname="'+item[0]+'" data-lang="'+item[1]+'" data-val="'+search+'"> '+item[0].replace(re, "<b>$1</b>")+'</div>';
              var ti = item[1].replace("InHouse/", "../");
              return '<div class="autocomplete-suggestion" title="'+ti+'" data-langname="'+item[0]+'" data-lang="'+item[1]+'" data-val="'+search+'"> '+item[0].replace(re, "<b>$1</b>")+'</div>';
          },
          onSelect: function(e, term, item){
              var ti = item.data('lang').replace("InHouse/", "../");
              $('input.searchfolder').val(item.data('langname')+' ('+ti+')');
              var _sel_subfolder = item.data('lang');
              var _sel_subname = item.data('langname');
              //$('#sel_folder').val(_sel_subfolder+":"+_sel_subname);
              $('#sel_folder').val(_sel_subfolder);
              $('input[name=nav_action]').val('search_file');
              $('.navform').submit();
          }
    });

     $(document).on('click', '.searchfolder', function(event) {

        event.preventDefault();
       // var _selfile = $(this).data('fullpath');
        /*$.ajax({
            url: '/tasks/checked',
            type: 'POST',
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {  },
            success: function(data) {
               console.log(data);
            },
        });*/

    });
      
     
});
</script>
@endsection