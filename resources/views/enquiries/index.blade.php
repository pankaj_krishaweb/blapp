@extends('layouts.frame')

@section('content')
<style type="text/css">
    h4{
            font-weight: bold;
    color: #414860;
    }
    h5{
color: #cccccc;
padding-top: 35px; 
/*padding-bottom: 20px; */
    }
    h2 {
    color: #00b29c;
    font-weight: bold;
    font-size: 4em;
}
 #job-search-btn{

    background-color: #fff;
    color: #b3b3b3;
    border: solid 0.5px #b3b3b3;
    border-radius: 5px;
    width: 100%;
        border-radius: 0px;
  }
  .open > .dropdown-menu {
    display: block;
    width: 100%;

    margin-top: 0px;
    border-radius: 0px;
}
#myInput{
    border-right: none;
    border-left: none;
    border-top: none;
    border-radius: 0px;
    margin-top: -6px;
}
</style>
<div class="container-fluid">
    <div class="container" style="min-height: 80vh;">
            <h1>Enquiries</h1>
            <div class="row">
                <div class="col-md-3">
                    <a href="{{ url('/admin/enquiries/add') }}" class="btn btn-default btn-block ">New </a> <br>
                    <div class="dropdown">
                        <button class="btn btnblock dropdown-toggle" type="button" data-toggle="dropdown" id="job-search-btn">
                        Select Job
                            <span class="pull-right"><span class="caret "></span></span>
                        </button>
                        <ul class="dropdown-menu">
                            <input class="form-control" id="myInput" type="text" placeholder="Search..">
                            @foreach ($all as $e)
                               <li><a href="{{ url("/admin/enquiries/{$e->id}") }}">BRI{{ $e->id}}</a></li>
                            @endforeach
                            
                        </ul>   
                    </div>

                </div>
             
                <div class="col-md-9">
                    <form action="{{ url('/admin/enquiries') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $enq->id ?? null }}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="name">Name:</label>
                                  <input type="text" class="form-control" id="name" name="name" value="{{ $enq->name ?? null}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="contact">Contact:</label>
                                  <input type="text" class="form-control" id="contact" name="contact" value="{{ $enq->contact ?? null}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="area">Area:</label>
                                  <input type="text" class="form-control" id="area" name="area" value="{{ $enq->area ?? null }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="calls">Calls:</label>
                                  <input type="text" class="form-control" id="calls" name="calls" value="{{ $enq->calls ?? null}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="quote">Quote:</label>
                                  <input type="text" class="form-control" id="quote" name="quote" value="{{ $enq->quote ?? null }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="hear">How did you hear about us:</label>
                                  <input type="text" class="form-control" id="hear" name="hear" value="{{ $enq->hear ?? null}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="details">Details:</label>
                                   <textarea class="form-control" rows="5" id="details" name="details">{{ $enq->details ?? null }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="notes">Further Notes:</label>
                                   <textarea class="form-control" rows="5" id="notes" name="notes">{{ $enq->notes ?? null }}</textarea>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-default"> Update</button>
                    </form>
                </div>
            </div>
            
        </div>   
    </div>
</div>
@endsection
@section('script')

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});    
$(document).on('click', '#new', function(event) {
    event.preventDefault();

    
});
$(document).ready(function() {

    $(".tasks").click(function() {

        var taskid = $(this).attr('data-key');
        
        if ($(this).is(":checked")) {
           var status="1";
        } else {
            var status="0";
        
        }

          $.ajax({
        url: '/tasks/checked',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {taskid: taskid, status: status},
        success: function(data){
        console.log(data);
        location.reload()
            
        },
    })  
    });

});
</script>
@endsection
