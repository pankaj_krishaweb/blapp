@extends('layouts.frame')

@section('content')
<style type="text/css">
    img {
      margin: 10px 0px 20px 0px;
    }
    .community-wrap{
      line-height: 1.5;
    }
    .community-wrap h3{
      margin-top: 10px;
    }
    .community-wrap h3, .community-wrap h4{
      font-family: 'Poppins';    
    }
    .community-wrap h4{
      margin-top: 0;
    }
    .container.community-wrap .row{
      padding: 0 8px;
    }
</style>
<div class="container-fluid">
    <div class="container community-wrap">

        <h1 style="color: #00b29d">Our Community</h1>

        <p>Here at Berry Lodge Surveyors we like to think of our clients as a community, we have spoken to our community and have listed those service providers that have received good feedback. </p>

        <p>Here at Berry Lodge Surveyors we want you to get the most for your money, why not give these guys a shout now and see if they can be of assistance to you. </p>

        <p>All of these service providers have been used by clients of Berry Lodge Surveyors and have all received positive feedback. Berry Lodge Surveyors is in no way affiliated with them and doesn’t receive any financial inducement should you chose to use them.</p>

        @foreach($datas as $data)
        <div class="row">
          <div class="col-md-3">
            <img src="{!! url('storage/'.$data->logo) !!}" style="height: auto; width: 214px">
            
          </div>
          <div class="col-md-9">
            
            <h3>{!! $data->name !!}</h3>

            <p>{!! $data->description !!}</p>
          </div>

        </div>

        <h4>Contact Information:</h4> 
        {!! $data->contact !!}
        <br><br>
        @endforeach
    </div>
</div>
@endsection

