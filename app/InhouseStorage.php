<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InhouseStorage extends Model
{
    protected $table= "storage_folders";
    protected $fillable = array('path', 'is_admin');
}
