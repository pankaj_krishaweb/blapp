@extends('layouts.frame')

@section('content')
<style>.panel-body {
  padding: 5px 15px;
}

.addfoldname, .addfolder-button{   display: none; }
.inode{
 margin-left: 5px;
}

.item {
  background-color: #fff;
  border-radius: 0px;
}
.survey-document .item p{
  display: table;
  width: 100%;
}
.survey-document .item p small{
  width: calc(100% - 80px);
  float: left;
  text-align: left;
}
.survey-document .item .myUL{
  padding: 0;
  margin: 0;
  list-style: none;
}
.survey-document .item .myUL li [class*="col-"], .survey-document .item .myUL li [class^="col-"]{
  word-break: break-all;
}
iframe{
 width: 100%;
 height: 600px;
}
.singlefi{
 padding:7px 0;display: inline-block; width: 100%;
 border-top: 1px dashed #ccc;
}
/* ----Bk1-------- */
ul, .myUL {
  list-style-type: none;
}

.myUL {
  margin: 0;
  padding: 0; width: 100%; display: inline-block;
}

.carett {
  cursor: pointer;
  -webkit-user-select: none; /* Safari 3.1+ */
  -moz-user-select: none; /* Firefox 2+ */
  -ms-user-select: none; /* IE 10+ */
  user-select: none;
}

.carett::before {
  content: "\25B6";
  color: #ccc;
  display: inline-block;
  margin-right: 6px;
}
li.echli{
 width:100%; display: inline-block;
}

.caret-down::before {
  -ms-transform: rotate(90deg); /* IE 9 */
  -webkit-transform: rotate(90deg); /* Safari */'
  transform: rotate(90deg);  
}

.nested {
  display: none;
}

.active {
  display: block;  padding-left:5px;
}
.download-btn-wrap{
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  -webkit-box-pack: justify;
  -ms-flex-pack: justify;
  justify-content: space-between;
}
/* ----Bk1-------- */
@media screen and (min-width:1200px)
{

}
.loder-img {
  width: 100%;
  height: 100%;
  position: fixed;
  top: 0;
  right: 0;
  background: rgb(63, 186, 166,0.5);
  z-index: 123;
}
.loder-img img{
  position: absolute;
  left: 0;
  top: 50%;
  right: 0;
  margin: 0 auto;
  margin-top: -50px;
}
@media (min-width: 1200px){
  .container {
    width: 100% !important;
    max-width: 1170px !important;
    margin: 0 auto !important;
  }
}

#myInput {
    padding: 20px;
    margin-top: -6px;
    border: 0;
    border-radius: 0;
    background: #f1f1f1;
  }

  #job-search-btn {
    margin-top: 40px;
    background-color: #fff;
    color: #b3b3b3;
    border: solid 0.5px #b3b3b3;
    border-radius: 5px;
  }

  h4 h3 {
    color: #414860;
    font-weight: bold;
  }

  h4 span {
    color: #909090;
  }

  h2 {
    color: #00b29c;
    font-weight: bold;
  }

  .filter-input {
    padding: 30px;
    margin: 10px 0px;
    border: 0;
    border-radius: 0;
    background: #eef2f6;
  }
  .tasksmsg{ width:95%; height:30px; font-size:14px;  }
#history-display ul{
  list-style-type: none;
  padding-left: 40px;
}
#history-display ul li{
  position: relative;
}
#history-display ul li:before{
  content: '';
  position: absolute;
  top: 18px;
  left: -20px;
  width: 1px;
  height: 100%;
  background-color: #00b29d;
}
#history-display ul li:last-child:before{
  display: none;
}
#history-display ul li:after{
  content: '';
  position: absolute;
  top: 6px;
  left: -27px;
  width: 15px;
  height: 15px;
  border-radius: 100%;
  background-color: #00b29d;
}
#noteHistoryModal .modal-body{
  max-height: 400px;
  overflow: auto;
}
#noteHistoryModal .modal-body > p{
  font-family: "Raleway", sans-serif;
  font-size: 18px;
  line-height: 1.4;
  font-weight: 600;
  margin-bottom: 20px;
}
</style>        
<div class="container-fluid">
  <div class="loder-img" style="display: none;"><img src="{{ asset('images/loading.gif') }}" style="height: 100px;"></div>
  <div class="container" style="padding-bottom: 50px;">
    <h1 id="job_id" data-id="{{ $job->id }}">Documents</h1>
    <ol class="breadcrumb">
     <li><a href="{{ url('admin') }}">Home</a></li>
     <li><a href="{{ url('admin/my-jobs') }}">Jobs</a></li>
     <li class="admin/jobs">BLSN{{$job->id }} </li>
   </ol>
   <p><strong>Address of Inspection: </strong>{{ $job->surveying->address_of_inspection }}</p>
   <div class="download-btn-wrap">
    <a class="btn blue-btn pull-right" href="{{ url("/admin/my-jobs/download/{$job->id}") }}">Download Job Documents</a>
    @if($job->status != 8)
    <button data-id="{{ $job->id }}" class="btn blue-btn pull-center download-standard-documents" >Download Standard Documents</button>
    @endif
    <button data-id="{{ $job->id }}" class="btn blue-btn pull-center download-uploaded-documents" >PDF Creator</button>
  </div>
  @if (session('message'))
  <div class="alert alert-info">
   {{ session('message') }}
 </div>
 @endif

</div>
<div class="container survey-document" style="min-height: 400px; padding-bottom: 50px;">
  <div class="row">
    @foreach($docs as $folder=>$doc)
    @php
          //$folders[] = $folder;
    @endphp
    @if ($loop->index == 3 || $loop->index == 6 || $loop->index == 9 )
  </div>
  <div class="row">
    @endif
    <div class="col-md-4 col-sm-4">
      <div class="well item" >
        <a style="color: #00B29D;" data-toggle="collapse" data-target="#{{str_slug($folder, '_')}}" href="#{{str_slug($folder, '_')}}"  onclick="return false;"><i class="fa fa-folder-o" aria-hidden="true"></i> {{ $folder }} <i class="fa fa-chevron-down pull-right" style="font-size:24px"></i></a>
        <hr>
        <div id="{{str_slug($folder, '_')}}" class="collapse">
          @if($folder == "Job information/Job Sketch")
          <a href="{!! url("/admin/my-jobs/sketch-pad/$job->id") !!}">sketch</a>
          @endif
          @if($folder == "Communication Notes")
          <a href="" id="note">Create Note</a>
          @endif
          @foreach($doc as $link => $item)
          @if($job->status != 8)
          @if ($link)
          <p>
            <small style="color: #414860;" class="text-right">{{$item}}</small>
            <a href="{{ asset($link) }}" class="btn btn-success btn-xs pull-right download_file" data-job="{!! $job->id !!}" data-id="{!! $item !!}" download><span><i class="fa fa-cloud-download" aria-hidden="true"></i></span></a>
            <a style="display: none" href="{{ asset($link) }}" class="btn btn-success btn-xs pull-right " download><span class="downloadFile"><i class="fa fa-cloud-download" aria-hidden="true"></i></span></a>
            <button type="button" class="btn btn-primary btn-xs pull-right view-doc" data-link="{{ asset($link) }}" data-type="docx">View</button>
          </p>
          @endif  
          @endif  
          @endforeach
          <hr>
        </div>
        <div style="color: #00B29D;">
         <a style="color: #00B29D;" data-toggle="collapse" data-target="#upl{{str_slug($folder, '_')}}" href="#upl{{str_slug($folder, '_')}}" onclick="return false;">
          <i class="fa fa-upload"></i>View Uploads
          <i class="fa fa-chevron-down pull-right" style="font-size:24px"></i>
        </a>
        <div id="upl{{str_slug($folder, '_')}}" class="collapse">

          @if (\App\JobDocs::where('job_id', $job->id )->where('folder', $folder)->where('upload','!=',2)->count()==0)
            @if($folder != "Communication Notes")
            <p class="text-danger">No documents uploaded here yet</p>
            @endif
          @endif

          @php 
            $subf_names = $subfolder = array();
          @endphp
          @foreach (\App\JobDocs::where('job_id', $job->id )->where('folder', $folder)->whereNotNull('subfolder')->orderBy('original_name','asc')->where('upload','!=',2)->get() as $k=>$upload)
          @php
          if( !empty($upload->subfolder) ){
            $subfolder[$k][$upload->subfolder]['original_name'] = $upload->original_name;
            $subfolder[$k][$upload->subfolder]['link'] = $upload->link;
            $subfolder[$k][$upload->subfolder]['id'] = $upload->id;
            $subfolder[$k][$upload->subfolder]['type'] = $upload->type;
            $subf_names[$upload->subfolder][] = $k;
          }
          @endphp
          @endforeach
          @foreach ($subf_names as $dirname=>$keys)
          <ul class="myUL">
            <li><span class="carett"><i class="fa fa-folder-o" aria-hidden="true"></i>{{$dirname}}</span>
              <ul class="nested">

               @foreach ($keys as $key)
               @foreach ($subfolder[$key] as $upload)
               @php //$upload = (object) array_shift($upload); @endphp
               @php $upload = (object) $upload; @endphp

               @if ( !empty($upload->original_name) && !empty($upload->id) )
               <!--<div class="row">-->
                 <li class="echli">
                   <small class="col-md-6 col-sm-6 col-xs-6 "> {{$upload->original_name }}</small>
                   <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                     <button data-id="{{ $upload->id }}" data-job_id="{{ $job->id }}" class="btn btn-danger btn-xs pull-right delete-upload-doc"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                     <button data-id="{{ $upload->id }}" class="btn btn-info btn-xs pull-right transfer-to-jobs">
                       <i class="fa fa-exchange" aria-hidden="true"></i>
                     </button>
                     <a href="{{ asset($upload->link) }}" class="btn btn-success btn-xs pull-right download_file" data-job="{!! $job->id !!}" data-id="{!! $upload->original_name !!}" download><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                     <a style="display: none" href="{{ asset($upload->link) }}" class="btn btn-success btn-xs pull-right " download><i class="fa fa-cloud-download downloadFile" aria-hidden="true"></i></a>

                     <button type="button" class="btn btn-primary btn-xs pull-right view-doc" data-link="{{asset($upload->link)}}" data-type="{{ $upload->type}}">
                       <i class="fa fa-search"></i>
                     </button>
                     <button data-id="{{ $upload->id }}" data-name="{!! $upload->original_name !!}" class="btn btn-success btn-xs pull-right rename-file" ><i class="fa fa-pencil" aria-hidden="true"></i></button>
                     {{-- Lightbox view --}}

                     @if (substr($upload->original_name, -3)!= 'pdf')
                     @if (substr($upload->original_name, -4)!= 'docx')
                     @if (substr($upload->original_name, -3)!= 'doc')
                     <img src="{!! asset('/'.$upload->link) !!}" style="height: 30px; width: 30px;">
                     <a href="{{ asset($upload->link) }}" data-lightbox="{{$job->id}}/{{str_slug($folder, '_')}}" class="btn btn-primary btn-xs pull-right"><i class="fa fa-arrows"></i></a>
                     @endif
                     @endif
                     @endif

                   </div>
                   <!--</div>-->
                 </li>
                 @endif

                 @endforeach
                 @endforeach


               </ul>
             </li>
           </ul>
           @endforeach


           <div class="singlefi">
            @if($folder == "Communication Notes")
              @foreach($job->notes as $data)
                <div style="display: inline-block;width:100%;">
                  <small class="col-md-6 col-sm-6 col-xs-6"> {{$data->slug }}</small>

                  <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                    <button data-id="{{ $data->id }}" data-job_id="{{ $job->id }}" class="btn btn-danger btn-xs pull-right delete-note"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    <button data-note_id="{{ $data->id }}" class="btn btn-success btn-xs pull-right edit-note"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button data-note_id="{{ $data->id }}" class="btn btn-success btn-xs pull-right note-history"><i class="fa fa-history" aria-hidden="true"></i></button>
                  </div>
                </div>
              @endforeach
            @endif


            @foreach (\App\JobDocs::where('job_id', $job->id )->where('folder', $folder)->WhereNull('subfolder')->where('upload','!=',2)->orderBy('original_name','asc')->get() as $k=>$upload)
             <div style="display: inline-block;width:100%;">
              <small class="col-md-6 col-sm-6 col-xs-6"> {{$upload->original_name }}</small>

              <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                <button data-id="{{ $upload->id }}" data-job_id="{{ $job->id }}" class="btn btn-danger btn-xs pull-right delete-upload-doc"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                <button data-id="{{ $upload->id }}" class="btn btn-info btn-xs pull-right transfer-to-jobs">
                  <i class="fa fa-exchange" aria-hidden="true"></i>
                </button>
                <a href="{{ asset($upload->link) }}" class="btn btn-success btn-xs pull-right download_file" data-job="{!! $upload->job_id !!}" data-id="{!! $upload->original_name !!}" download><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                <a style="display: none" href="{{ asset($upload->link) }}" class="btn btn-success btn-xs pull-right " download><i class="fa fa-cloud-download downloadFile" aria-hidden="true"></i></a>

                <button type="button" class="btn btn-primary btn-xs pull-right view-doc" data-link="{{asset($upload->link)}}" data-type="{{ $upload->type}}">
                  <i class="fa fa-search"></i>
                </button>
                @if($upload->upload == 1)
                <button data-id="{{ $upload->id }}" data-name="{!! $upload->original_name !!}" class="btn btn-success btn-xs pull-right rename-file" ><i class="fa fa-pencil" aria-hidden="true"></i></button>
                @endif
                {{-- Lightbox view --}}

                @if (substr($upload->original_name, -3)!= 'pdf')
                @if (substr($upload->original_name, -4)!= 'docx')
                @if (substr($upload->original_name, -3)!= 'doc')
                @if (substr($upload->original_name, -3)!= 'PDF')
                <img src="{!! asset('/'.$upload->link) !!}" onerror="this.src='{!! asset("/images/default.png")!!}'" style="height: 30px; width: 30px;">
                <a href="{{ asset($upload->link) }}" data-lightbox="{{$job->id}}/{{str_slug($folder, '_')}}" class="btn btn-primary btn-xs pull-right"><i class="fa fa-arrows"></i></a>
                @endif
                @endif
                @endif
                @endif

              </div>
            </div>
            @endforeach
          </div>

        </div>
      </div>

    </div>
  </div>
  @endforeach
</div>
</div>
</div>
<div class="container-fluid" style="background-color: #414861">
  <div class="container">
    <div class="row" style="padding-top: 50px;">
      <div class="col-md-6 col-sm-6 col-xs-12">
       <h4  style="color: #fff;"><strong>Upload a File</strong></h4>
       <p>Here you can upload documents, just drag and drop anywhere on the screen to start the process. Please be aware that you can only upload the following file types:  jpg, mov, mp4, pdf, doc, docx, zip</p>  
      </div>
      <div id="actions"  class="col-md-6 col-sm-6 col-xs-12">
        <div class="main-buttons  text-right">
          <select name="massSelect" id="massSelect" onchange="updateMassSelect(this)">           
            @foreach($folderslist as $fname=>$subfname)
            <option value="{{$fname}}">{{$fname}}</option>
            @foreach( $subfname as $subfolder )
            <option class="inode" data-parent="{{$fname}}" value="{{$subfolder}}"> - {{$subfolder}}</option>
            @endforeach
            @endforeach
          </select>
          {{-- mass select --}}
          <div class="btn-group">        
            <button class="btn btn-success fileinput-button"><i class="glyphicon glyphicon-plus"></i><span>Add files...</span></button>
            <button type="submit" class="btn btn-primary start"><i class="glyphicon glyphicon-upload"></i><span>Upload</span></button>
            <button type="reset" class="btn btn-danger cancel"><i class="glyphicon glyphicon-ban-circle"></i><span>Cancel </span></button>
          </div>
          <div class="foldern">
           <button class="btn btn-success addfolder-button"><i class="glyphicon glyphicon-plus"></i><span>Add Folder</span></button>
           <div class="addfoldname">
             <input name="foldname" id="foldname" value="" placeholder="Folder Name"  >
             <button class="btn btn-success createfolder-btn"><i class="glyphicon glyphicon-plus"></i><span>Create</span></button>
           </div>
          </div>
          <div class="progress-bars">
            <span class="fileupload-process">
              <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="background-color: #414860;">
                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
              </div>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- end row -->
<div class="table table-striped" class="files" id="previews">
  <div id="template" class="file-row row" style="margin-top: 20px;">
    <div class="col-md-2 col-sm-2">
      <span class="preview"><img data-dz-thumbnail /></span>
    </div>
    <div class="col-md-2 col-sm-3">
      <p><small class="name" data-dz-name><span class="size" data-dz-size>-</span></small></p>
      <strong class="error text-danger" data-dz-errormessage></strong>
    </div>
    <div class="col-md-5 col-sm-2 folderpath" data-parent="">
    </div>
    <div class="col-md-1 col-sm-1">
      <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="background-color: #414860; box-shadow: none;">
        <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
      </div>
    </div>
    <div class="col-md-2 col-sm-4">
      <div class="text-right">
        <button class="btn btn-primary btn-xs start"><i class="glyphicon glyphicon-upload"></i><span>Start</span></button>
        <button data-dz-remove class="btn btn-danger btn-xs cancel"><i class="glyphicon glyphicon-ban-circle"></i><span>Cancel</span></button>
      </div>
    </div>
  </div>
</div>
</div>


{{-- =========================task list=========================== --}}

<div class="container">
  <div class="col-md-8 col-sm-6">
    <h3>
      @if ($job->ten_4_party_wall_notice_date)
      @if ($job->tasks->contains(function ($value, $key) {
        return $value->task == 'Section 10(4) served' && $value->status == 1;
      }))
      @else
      <i class="fa fa-bell-o" style="font-size:24px; color:red" data-toggle="tooltip" title="<h4>10.04 date set: <br> {{$job->ten_4_party_wall_notice_date}}</h4>" ></i>
      @endif

      @else

      @endif
      {{ $job->job_type}}
    </h3>
    <h2 id="percent{{ $job->id }}">
      {{ $percent }} % 
    </h2>
      <p style="color: #00b29b;">Complete</p>
  </div>
  @php 
    $no_email = [3,4,6,7,9,10,11,12,14,15,20,21,24,25,27,28,30,32,36,42,43,44,46,47,49,50,55]
  @endphp
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="pull-right">
      <div class="row">
        <div>
          <h4 class="col-md-6 col-sm-6 col-xs-6">Job Number</h4>
          <h4 class="col-md-6 col-sm-6 col-xs-6"><span class="text-right pull-right">  <a href="{{ url("/admin/my-jobs/view/{$job->id}") }}">BLSN{{ $job->id }}</a></span></h4>
        </div>
        <div>
            <h4 class="col-md-6 col-sm-6 col-xs-6">Property Address</h4>
            <h4 class="col-md-6 col-sm-6 col-xs-6"><span class="text-right pull-right">
              @if($job->surveying)
              {{$job->surveying->address_of_inspection}}
              @else
              {{$job->bo->property_address_proposed_work}}
              @endif
          </span></h4>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 hidden-xs">
    <div class="progress">
      <div class="progress-bar" id="percent-bar{{ $job->id }}" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:  {{ $percent }}%">
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="row ">
      <div class="col-md-6 col-sm-7 hidden-xs">
        <h4>
          Task Name
        </h4>
      </div>
      <div class="col-md-2 col-sm-2 hidden-xs">
        <h4>Date of Surveyor Action
        </h4>
      </div>
      <div class="col-md-2 col-sm-1 hidden-xs">
        {{-- <h4>Record</h4> --}}
      </div>
      <div class="col-md-1 col-sm-1 hidden-xs">
        <h4 class="text-right">
          Task Completed 
        </h4>
      </div>
      <div class="col-md-1 col-sm-1 col-xs-12">
        <a class="text-right pull-right" data-toggle="collapse" data-target="#dp{{$job->id}}" href="#dp{{$job->id}}" onclick="return false;">
          <i class="fa fa-chevron-down" style="font-size:24px; color: #00b29b"></i>
        </a>
      </div>
    </div>
    <br>
    <hr>
    <br>
    <div id="dp{{$job->id}}" class="collapse">
      {{-- First email task --}}
      @foreach($job->tasks as $task)
        @if($task->task_id == 65 || $task->task_id == 66 || $task->task_id == 67 || $task->task_id == 68)
        <?php $crc = ""; if( in_array($task->task, ['Update building owner','Update Owners']) )
        $crc = "updto";  ?>
        <div class="row echtask_p">
          <div class="col-md-6 col-sm-7 col-xs-12">
            <h4><span>{{ $task->task }}</span></h4>
         <!-- @if( in_array($task->task, ['Update building owner','Update Owners']) )
            <textarea class="tasksmsg" data-key="{{ $task->id }}" maxlength="250" placeholder="Your message">{{$task->additional_msg}}</textarea>
            @endif -->
          </div>
          <div class="col-md-2 col-sm-1 col-xs-9">
            @if($task->status==1)<h4> <span>{{ $task->updated_at }}</span></h4>@endif
          </div>
          <div class="col-md-2 col-sm-1 col-xs-9">
            @if($task->status==1 && !in_array($task->task_id, $no_email))
            <button type="button" class="btn btn-success sendmail"  data-type="{!! $task->job_type !!}" data-name="{{ $task->task }}" data-id="{{ $task->id }}">Mail</button>
            {{-- <img src="{!! asset('images/rec.png') !!}" class="addrecord" style="width: 60px; height: 60px;"  data-id="{!! $task->id !!}"> --}}
            @endif
          </div>
          <div class="col-md-1 col-sm-1 col-xs-1">
            <span class="pull-right">
              <input class="tasks <?=$crc ?>" type="checkbox" data-name="{{ $task->task }}" data-key="{{ $task->id }}" @if($task->status==1) checked @endif>
            </span>
          </div>
          <div class="col-md-1 col-sm-1 col-xs-1"></div>
        </div>
        <hr> 
        @php break; @endphp
        @endif
      @endforeach
      {{-- End first email --}}

      @foreach($job->tasks as $task)
        @if($task->task_id != 65 && $task->task_id != 66 && $task->task_id != 67 && $task->task_id != 68)
          <?php $crc = ""; if( in_array($task->task, ['Update building owner','Update Owners']) )
          $crc = "updto";  ?>
          <div class="row echtask_p">
            <div class="col-md-6 col-sm-7 col-xs-12">
              <h4><span>{{ $task->task }}</span></h4>
           <!-- @if( in_array($task->task, ['Update building owner','Update Owners']) )
              <textarea class="tasksmsg" data-key="{{ $task->id }}" maxlength="250" placeholder="Your message">{{$task->additional_msg}}</textarea>
              @endif -->
            </div>
            <div class="col-md-2 col-sm-1 col-xs-9">
              @if($task->status==1)<h4> <span>{{ $task->updated_at }}</span></h4>@endif
            </div>
            <div class="col-md-2 col-sm-1 col-xs-9">
              @if($task->status==1 && !in_array($task->task_id, $no_email))
              <button type="button" class="btn btn-success sendmail"  data-type="{!! $task->job_type !!}" data-name="{{ $task->task }}" data-id="{{ $task->id }}">Mail</button>
              {{-- <img src="{!! asset('images/rec.png') !!}" class="addrecord" style="width: 60px; height: 60px;"  data-id="{!! $task->id !!}"> --}}
              @endif
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1">
              <span class="pull-right">
                <input class="tasks <?=$crc ?>" type="checkbox" data-name="{{ $task->task }}" data-key="{{ $task->id }}" @if($task->status==1) checked @endif>
              </span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1"></div>
          </div>
          <hr>
        @endif
      @endforeach
      <a class="text-right pull-right" data-toggle="collapse" data-target="#dp{{$job->id}}" href="#dp{{$job->id}}">
        <i class="fa fa-chevron-up" style="font-size:24px; color: #00b29b"></i>
      </a>
    </div>
  </div>
</div>
{{-- upload modal --}}
<div id="uploadModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Document</h4>
      </div>
      <div class="modal-body">
        <p>Select document to upload</p>
        <div class="input-group">
          <label class="input-group-btn">
            <span class="btn btn-primary">
             <i class="fa fa-upload" style="font-size:24px"></i>  Browse&hellip; <input type="file" style="display: none;" multiple>
           </span>
         </label>
         <input type="text" class="form-control" readonly>
       </div>
       <span class="help-block">
        You can select multiple files by clicking ctrl and selecting
      </span>
    </div>
          {{--  <label class="btn btn-default btn-file">
         Browse <input type="file" style="display: none;">
       </label> --}}
     </div>
   </div>
 </div>
</div>
{{--  standard document model --}}
<div id="standardModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Standard Document </h4>
      </div>
      <div class="modal-body">

        <form action="/admin/my-jobs/download-standard-documents/$job->id" class="form-horizontal" role="form" id="standardDocForm">                  
          <div class="form-group">
            <div class="col-md-12" id="standard_class">
              @php
              $folders = []; 
              @endphp
              @foreach($docs as $folder=>$doc)
              @foreach($doc as $link => $item)
              @if ($item)
              @php
              array_push($folders,$folder);
              @endphp
              @endif
              @endforeach
              @endforeach
              @php
              $final_folders = array_unique($folders);
              @endphp
              @foreach($final_folders as $folder_name)
              <div class="col-md-12"><input type="checkbox" name="{!! $folder_name !!}" value="{!! $folder_name !!}" class="custom" />
                <label for="checkbox">{!! $folder_name !!}</label>
              </div>
              @endforeach  
              <!-- <input  type="hidden" value="" name="job_id" id="job_id"> -->
            </div>
          </div>
          <div class="modal-footer basic-buttons">
            <button type="submit" class="btn blue-btn" id="download-standard">Download</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a href="" id="download-link" type="hidden" download></a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
{{-- create note --}}

  <div id="notesModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Communication Notes </h4>
        </div>
        <div class="modal-body">

          <form action="/admin/my-jobs/download-standard-documents/$job->id" class="form-horizontal" role="form" id="standardDocForm">                  
            <div class="form-group">
              <div class="col-md-12" id="standard_class">
                <input type="hidden" name="" id="note_id" value="">
                <div class="col-md-12">
                  <label for="checkbox">Title</label>
                  <input type="text" name="title" id="note_title" >
                </div>
                <div class="col-md-12">
                  <label for="checkbox"></label>
                  <textarea id="user_note" rows="4" cols="50"></textarea>
                </div>
                
                <!-- <input  type="hidden" value="" name="job_id" id="job_id"> -->
              </div>
            </div>
            <div class="modal-footer basic-buttons">
              <button type="submit" class="btn blue-btn" id="submit-note">Submit</button>
              <button type="submit" class="btn blue-btn" id="update-note" style="display: none">Update</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

{{-- Note History Modal --}}
  <div id="noteHistoryModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Notes History </h4>
        </div>
        <div class="modal-body">
          <p>Note Title: <span class="note_title"></span></p>
          <div id="history-display">
            
          </div>
            <div class="modal-footer basic-buttons">
              {{-- <button type="submit" class="btn blue-btn" id="submit-note">Submit</button> --}}
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          {{-- </form> --}}
        </div>
      </div>
    </div>
  </div>

{{--  rename model --}}
<div id="renameModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Rename </h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" >
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-2 control-label">Name</label>
            <div class="col-md-6">
              <input id="name" type="text" class="form-control"  value="{{ old('name') }}" required autofocus>
              <input  type="hidden" value="" id="user_id">
              <input  type="hidden" value="" id="old_name">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn blue-btn" id="edit-user-btn">Save </button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
{{--  upload document model --}}
<div id="uploadedModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">PDF Creator </h4>
      </div>
      <div class="modal-body">
        <form  class="form-horizontal" role="form" id="uploadedDocForm">
          <div class="form-group">
            <div class="col-md-12" id="uploaded_class">
              <!-- <input  type="hidden" value="" name="job_id" id="job_id"> -->
            </div>
          </div>
          <div class="modal-footer basic-buttons">
            <button type="submit" class="btn blue-btn" id="download-uploaded">Download</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <a href="" id="uploaded-doc-link" type="hidden" download></a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>  
{{-- doc preview modal --}}
<div class="modal fade" id="docViewModal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Doc View</h4>
      </div>
      <div class="modal-body" id="doc-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
{{-- add mail modal --}}
<div id="addmail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="category_name" class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div id="error-display"></div>
        <form class="form-horizontal" role="form" >
          <div id="render"></div>
          <input type="hidden" name="name" id="name">
          <input type="hidden" name="type" id="type">
          <input type="hidden" name="task_id" id="task_id">

        {{-- </div> --}}
          <div class="modal-footer">
            <button type="button" class="btn blue-btn" id="add-mail-btn">Send </button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js" integrity="sha256-Rnxk6lia2GZLfke2EKwKWPjy0q2wCW66SN1gKCXquK4=" crossorigin="anonymous"></script>

{{-- <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script> --}}
<link href="{!! asset('/css/summernote.css') !!}" rel="stylesheet">
<script src="{!! asset('/js/summernote.js') !!}"></script>
<script type="text/javascript" src="{{ asset('js/dropdowns.js') }}"></script>


<script type="text/javascript">
  $(document).ready(function() {
    $('#user_note').summernote({
      height: 200
    });
  });
  $(document).on('click','#note',function(e){
    e.preventDefault();
    $('#user_note').summernote('code','');
    $('#note_title').val('');
    $('#notesModal').modal('show');
  });
  $(document).on('click','#submit-note',function(e){
    e.preventDefault();
    var summernote = $('#user_note').summernote('code');
    var title = $('#note_title').val();
    if (summernote != '' && title != '') 
    {
      $.ajax({
        url:'/admin/user-note',
        type:'post',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data:{summernote:summernote, title:title , job_id:{{ $job->id }} },
        success:function(response){
          console.log(response);
          location.reload();
        },
        error:function(error){
          console.log(error);
          alert('Please try after some time');
        }
      })
    }
    else{
      alert('Please Fill values!');
    }
  });

  $(document).on('click','#update-note',function(e){
    e.preventDefault();
    var summernote = $('#user_note').summernote('code');
    var title = $('#note_title').val();
    var note_id = $('#note_id').val();
    $.ajax({
      url:'/admin/user-note/update',
      type:'post',
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
      data:{summernote:summernote, title:title ,note_id:note_id, job_id:{{ $job->id }} },
      success:function(response){
        console.log(response);
        location.reload();
      },
      error:function(error){
        console.log(error);
        alert('Please try after some time');
      }
    })
  });


  $(document).on('click','.edit-note',function(e){
    e.preventDefault();
    var note_id = $(this).data('note_id');
    $.ajax({
      url:'/admin/user-note/details/'+note_id,
      type:'get',
      success:function(response){
        console.log(response);
        $('#notesModal').modal('show');
        $('#note_id').val(note_id);
        $('#note_title').val(response.title);
        $('#user_note').summernote('code',response.body);
        $('#update-note').show();
        $('#submit-note').hide();
        
      },
      error:function(error){
        console.log(error);
        alert('Please try after some time');
      }
    })
  });

  $(document).on('click','.note-history',function(e){
    e.preventDefault();
    var note_id = $(this).attr('data-note_id');
    $.ajax({
      url:'/admin/user-note/history/'+note_id,
      type:'get',
      success:function(response){
        console.log(response);
        $('#noteHistoryModal').modal('show');
          var histories = response.histories;
          var displayhistory='<ul>';
            $.each(histories, function(index, value) {
              displayhistory+='<li><strong> '+value.created_at+' <br></strong> '+value.body+'</li>';
            });
            displayhistory+='</ul>';
            $('#history-display').html(displayhistory);
            var title = response.title;
        $('.note_title').html(title);
      },
      error:function(error){
        console.log(error);
        alert('Please try after some time');
      }
    })
  });
  $(document).on('click', '.delete-note', function(event) {

    event.preventDefault();
    var note_id = $(this).attr('data-id');
    var job_id = $(this).attr('data-job_id');
    var doc = $(this).parent().parent();
    swal("Are you sure you wish to delete this Note?", {
      buttons: ["Cancel", "Yes Delete Note!"],
    }).then(function(value){
      if(value==true){
        $.ajax({
          url: '/admin/user-notes/delete/'+note_id,
          type: 'delete',
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
          data: {},
          success: function(data){
            location.reload();
          },
          error:function(error){
            console.log(error);
          }
      })
      }
      return false;
    },
    function(){
      console.log('Not deleted');
      return false;
    });
  });
  var checkajax = false;

// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
var previewNode = document.querySelector("#template");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
  url: "/admin/my-jobs/upload", // Set the url
  maxFilesize:10,
  maxThumbnailFilesize: 10,
  thumbnailWidth: 80,
  thumbnailHeight: 80,
  parallelUploads: 20,
  params:{ job_id: {{ $job->id }} },
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  previewTemplate: previewTemplate,
  autoQueue: false, // Make sure the files aren't queued until manually added
  previewsContainer: "#previews", // Define the container to display the previews
  clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
});

var toggler = document.getElementsByClassName("carett");
var itree;
for (itree = 0; itree < toggler.length; itree++) {
  toggler[itree].addEventListener("click", function(event) {
   event.stopPropagation();
   this.parentElement.querySelector(".nested").classList.toggle("active");
   this.classList.toggle("caret-down");
 });
}

myDropzone.on("addedfile", function(file) {
  var e = document.getElementById("massSelect");
  

  var selval = $('.main-buttons select').find('option:selected').val();
   //alert(selval);
   var selhtml = $('.main-buttons select').html();
   file.previewElement.querySelector('.folderpath').innerHTML = `<select class="listselct">${selhtml}</select>`;
   $(file.previewElement.querySelector('.listselct')).find(`option[value="${selval}"]`).attr('selected','selected').change();
   
   
   /*var fobj = file.previewElement;
   $(fobj).attr('data-parent', $('#massSelect').find('option:selected').data('parent'));
   $(fobj).data('parent', $('#massSelect').find('option:selected').data('parent'));*/

  // Hookup the start button
  file.previewElement.querySelector(".start").onclick = function() {
    
    /*if($(`.folder-type[name="${file.name}"]:checked`).length==0) {

      swal("Upload "+file.name, "You need to select the folder to upload into!", "info");
      console.log($(`.folder-type[name="${file.name}"]:checked`).length)
      return;
    }*/
    if( e.options[e.selectedIndex].text=="" ) {

      swal("Upload "+file.name, "You need to select the folder to upload into!", "info");
      console.log(e.options[e.selectedIndex].text)
      return;
    }
    
    //alert( file.previewElement.querySelector(".name").data('dz-name') );
    myDropzone.enqueueFile(file);
  };
});

function updateMassSelect(selectObj)
{

   //Reinitialize all the radio buttons
   /*document.querySelectorAll(".folder-type").forEach(function(radio){
      radio.removeAttribute("checked");
    });*/
    var _selfold = selectObj.options[selectObj.selectedIndex].value;
    if( _selfold=='Desk Top Information' || _selfold=='Inspection Photographs' ){
      $('.addfolder-button').fadeIn();
    }else{
      $('.addfolder-button, .addfoldname').fadeOut();
    }

  //Set the new checked value
  /*document.querySelectorAll(".folder-type").forEach(function(radio){
    if(radio.getAttribute("value")==selectObj.options[selectObj.selectedIndex].value)
    {
      radio.setAttribute("checked","checked");
    }
  });*/
}

// Update the total progress bar
myDropzone.on("totaluploadprogress", function(progress) {
  document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
});

myDropzone.on("sending", function(file, xhr, data) {
  // Show the total progress bar when upload starts

  document.querySelector("#total-progress").style.opacity = "1";
  //var selected = $(`.folder-type[name="${file.name}"]:checked`);
  /*var selected = $.trim(file.previewElement.querySelector('.folderpath').innerHTML); //$('#massSelect').find('option:selected');
  var parentfold = '';
  parentfold = $(file.previewElement.querySelector('.folderpath')).attr('data-parent');*/

   var selected = $(file.previewElement.querySelector('.listselct')).find(":selected").val(); //$('#massSelect').find('option:selected');
   //alert(selected);
   var parentfold = '';
   parentfold = $(file.previewElement.querySelector('.listselct')).find(":selected").attr('data-parent');
   parentfold = (parentfold)? parentfold : "";

  /*if( typeof $('#massSelect').find(`[value="${selected}"]`).data('parent') != "undefined" ){
    var parentfold = $('#massSelect').find(`[value="${selected}"]`).data('parent');
  }*/
  if (selected.length > 0) {
   data.append("filetype",  selected );
   data.append("parentfold",  parentfold );
 }
   // And disable the start button
   file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
 });

// Hide the total progress bar when nothing's uploading anymore
myDropzone.on("queuecomplete", function(progress) {
  document.querySelector("#total-progress").style.opacity = "0";
});

// Setup the buttons for all transfers
// The "add files" button doesn't need to be setup because the config
// `clickable` has already been specified.
document.querySelector("#actions .start").onclick = function() {

  var check = true;
  $("input:radio").each(function(){
    var name = $(this).attr("name");
    if($(`input:radio[name="${name}"]:checked`).length == 0){
      check = false;
    }
  });

  if(!check){
    swal("Upload", "You need to select the upload folder for all files!", "info");

    return;
  }
        // else{
        //     alert('Please select one option in each question.');
        //      return;
        // }

        myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
      };
      document.querySelector("#actions .cancel").onclick = function() {
        myDropzone.removeAllFiles(true);
      };


      myDropzone.on("success", function(file, res) {
        $('.dz-success').remove();
        console.log(res);
      });


      myDropzone.on("queuecomplete", function(file) {
        location.reload();
      });



      $(document).ready(function(file){

        var $radios = $('input[name=fm]').change(function () {
          var value = $radios.filter(':checked').val();
    //alert(value);

  });


      });

      $(document).on('click', '.addfolder-button', function(event) {
       $('.addfoldname').fadeIn();
       $('#foldname').focus();
      });
      $(document).on('click', '.createfolder-btn', function(event) {

       var _selfold = $('#massSelect').find('option:selected').val();
       if( _selfold=='Desk Top Information' || _selfold=='Inspection Photographs' ){
        var _insname = $.trim($('#foldname').val());
        var _insfold = `<option class="inode" data-parent="${_selfold}" value="${_insname}"> - ${_insname}</option>`;
          //var _insfoldopt = `<li><input name="docs" type="radio" value="${_insname}" class="folder-type am"><label class="radio-inline">${_insname}</label></li>`;
          
          $("#massSelect option[value='"+_selfold+"']").after(_insfold);
          
         // $("#massSelect").append(_insfold);
       }
       $('.addfoldname').fadeOut();
      });

//
$(document).on('click', '.upload_btn', function(event) {
  $('#uploadModal').modal()
});

$(document).on('click', '.add-task', function(event) {
  event.preventDefault();

  if ($(this).text()=="Add All")
  {
    var type = 'all' ;
  }
  else if($(this).text()=="Add")
  {
    var type = 'sub' ;
  }

  var key = $(this).attr('data-key');
  var job_id = $('#job_id').attr('data-id');
  $.ajax({
    url: '/admin/jobs/tasks',
    type: 'POST',
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
    data: {job_id: job_id, key:key, type:type},
    success: function(data){
      var status = $.parseJSON(data);

      if(status.status=='good'){
        swal("Task added successfully ").then(() => {
         location.reload();
       });
      }
    },
    error: function(data){
      var errors = $.parseJSON(data.responseText);
      console.log(errors);
      var displayerror='<div class="alert alert-dismissible alert-danger">';
      $.each(errors, function(index, value) {
        displayerror+='<li>'+value+'</li>';
      });
      displayerror+='</div>';
      $('#error-display').html(displayerror);
    }

  })

});
    $(document).on('click', '#add-mail-btn', function(event) {
      event.preventDefault();
      var type = $('#type').val();
      var name =  $('#name').val();
      var task_id =  $('#task_id').val();
      $(this).attr('disabled',true);
      $.ajax({
        url: '/admin/send-mail',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {type: type, name: name, task_id:task_id},
        success: function(data){
          console.log(data);
                if (data == 'true') {
            //location.reload();
            $('#addmail').modal('hide');
          }
          if (data == 'false') {
            alert('plz try some time');
          }
        },
        error: function(data){
          console.log(data);
        }
      })
    });

$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
    numFiles = input.get(0).files ? input.get(0).files.length : 1,
    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
    $(':file').on('fileselect', function(event, numFiles, label) {

      var input = $(this).parents('.input-group').find(':text'),
      log = numFiles > 1 ? numFiles + ' files selected' : label;

      if( input.length ) {
        input.val(log);
      } else {
              //if( log ) alert(log);
            }

          });

  });
     $(".tasks").click(function(e) {
      if( $(this).hasClass('updto') ){
       if( $(this).is(':checked')==false ){
        e.preventDefault(); 
        alert('Not allowed to untick this task.');
        return;
      }
      var conf = confirm('A completion email will be send to the client. Are you sure to proceed?');
      if( conf==false ){
        e.preventDefault();
        return;
      }
    }
    
    var taskid = $(this).attr('data-key');
    var taskname = $(this).attr('data-name');
    var msg = $(this).closest('.echtask_p').find('.tasksmsg').val();
    if ($(this).is(":checked")) {
      var status = "1";
    } else {
      var status = "0";
    }

    $.ajax({
      url: '/tasks/checked',
      type: 'POST',
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     },
     data: {
       taskid: taskid,
       taskmsg: msg,
       status: status,
       taskname:taskname
     },
     success: function(data) {

       console.log(data);
       $('#percent' + data.id).text(data.percent + '%')
       $('#percent-bar' + data.id).css("width", data.percent + '%');
     },
   });
    
  });
    $('.sendmail').on('click',function(){
       var type = $(this).data('type');
       var name = $(this).data('name');
       var task_id = $(this).data('id')
       $('#type').val($(this).data('type'));
       $('#name').val($(this).data('name'));
       $('#task_id').val($(this).data('id'));
       $('#category_name').html("Send Mail "+name);
       $.ajax({
         url: '/admin/render',
         type: 'post',
         headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
         data:{type: type,name:name,task_id:task_id},
         success : function(data){
           console.log(data);
           $('#render').html(data);
           $('#add-mail-btn').attr('disabled',false);
           $('#addmail').modal();
         }
       })
     });
});


$(document).on('click', '.view-doc', function(event) {

  if($(this).attr('data-type')=='jpg' || $(this).attr('data-type')=='jpeg' || $(this).attr('data-type')=='png' ){
    window.open($(this).attr('data-link'));
  } else  {
   event.preventDefault();
   $("#docViewModal").modal();
   if ($(this).attr('data-type')=='doc' || $(this).attr('data-type')=='docx' || $(this).attr('data-type')=='pdf' ) {
    //window.open("https://docs.google.com/gview?url="+encodeURI($(this).attr('data-link'))+"&embedded=true");
    $('#doc-body').html('<iframe src="https://docs.google.com/gview?url='+encodeURI($(this).attr('data-link'))+'&embedded=true"></iframe>');
  } else {
    $('#doc-body').html('<h2> Sorry on-line preview not available</h2>');
  }
}


});

$(document).on('click', '.delete-upload-doc', function(event) {

  event.preventDefault();
  var doc_id = $(this).attr('data-id');
  var job_id = $(this).attr('data-job_id');
  var doc = $(this).parent().parent();
  swal("Are you sure you wish to delete this document?", {
    buttons: ["Cancel", "Yes Delete Document!"],
  }).then(function(value){
    if(value==true){
      $.ajax({
        url: '/admin/my-jobs/doc/delete',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {doc_id: doc_id, job_id:job_id},
        success: function(data){
          console.log(data)
        // var status = $.parseJSON(data);
        if(data=='done'){
          location.reload();
        }
      },
    })
    }
    return false;
  },
  function(){
    console.log('Not deleted');
    return false;
  });
});

// transfer to jobs
$(document).on('click', '.transfer-to-jobs', function(event) {
  event.preventDefault();
  var doc_id = $(this).attr('data-id');
  var doc = $(this).parent().parent();
  swal("Select Job folder to transfer into?", {
    buttons: {
      Documents: { text: "Documents", value: "1",},
      Drawing: {  text: "Drawing", value: "2", },
      Photographs: { text: "Photographs", value: "3",},
    },
  })
  .then((folder) => {

    $.ajax({
      url: '/admin/my-jobs/doc/transfer',
      type: 'POST',
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
      data: {doc_id: doc_id, folder: folder},
      success: function(data){
        console.log(data);
        if(data=='false'){
          alert('try after some time');
        }
        else if(data == "already exists"){
          alert('File already transfered');
        }
        else{
          alert('successfully transfered');
        }
      },
    })

  });
});
  // rename of file
  $(document).on('click', '.rename-file', function(event) {
    event.preventDefault();
    var doc_id = $(this).attr('data-id');
    var doc_name = $(this).data('name');
    var name = doc_name.substring(0,doc_name.length-4);
    $('#name').val(name);
    $('#user_id').val(doc_id);
    $('#old_name').val(doc_name);
    $('#renameModal').modal();

  });
//download history 
$('.download_file').on('click',function(e){
  e.preventDefault();
  var file = $(this).data('id');
  var link = $(this).attr('href');
  var job_id = $(this).data('job');

  var image = $(this).attr('download');
  var data = $(this).parent();
  $.ajax({
    url:"/admin/my-jobs/doc/download-file",
    type: 'POST',
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
    data: {file: file, job_id:job_id},
    success: function(response){
      console.log(response)
      data.find('.downloadFile').click();

    },
  });
});
  //rename file
  $(document).on('click', '#edit-user-btn', function(event) {
    event.preventDefault();   
    var old_name =  $('#old_name').val();
    var name =  $('#name').val();
    var job_id = $('#user_id').val();
    if (name != "") {

      $.ajax({
        url: '/admin/my-jobs/doc/rename-file',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: { name:name , job_id : job_id , old_name : old_name},
        success: function(data){
          console.log(data);
          location.reload();
        },
        error: function(response){
          console.log(response);      
        }
      });
    }
    else{
      alert("Please Enter Name.");
    }

  });
// uploaded document modal show
$(document).on('click', '.download-uploaded-documents', function(event) {
  event.preventDefault();
  var job_id = $(this).attr('data-id');
  $('#job_id').val(job_id);
  $.ajax({
    type:'post',
    url:"/admin/my-jobs/doc/get-uploaded-folder",
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
    data:{job_id:job_id },
    success: function(response){
      data = response;

      var options = '';
      $.each(data, function(data,val){
        var name = val.original_name.split(".");
        options += '<div class="col-md-12"><input type="checkbox" name="'+ val.folder +'" value="' + val.original_name + '" class="custom" />';
        options += '<label for="checkbox">' + val.original_name + '</label></div>';
      });
      $('#uploaded_class').html('');
      $('#uploaded_class').append(options);
      $('#uploadedModal').modal();
    }
  });      
});
  //checked checkbox of uploaded move 
  $(document).on('click','#uploaded_class input[type="checkbox"]',function(e){
    var $this = $(this);
    if ( $this.prop('checked') )
         { // move to top
          $('#uploaded_class').prepend(($(this).parent()));
        }
        else 
         { // move to bottom
          $('#uploaded_class').append(($(this).parent()));
        }
      });
  $(document).on('click', '#download-uploaded', function(event) {
    event.preventDefault();
    var selected = [];
    var i=0;
    $("#uploaded_class input:checkbox[name]:checked").each(function() {
      var nm = $(this).prop('name');
      var value = $(this).val();
      selected.push([nm , value]);
    });
    console.log(selected);
    if (selected.length != 0) {

      $.ajax({
        url: '/admin/my-jobs/download-uploaded-documents/{!! $job->id !!}',
        type: 'GET',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: { formData : selected,"job_type":"surveying"},
        success: function(data){
          console.log(data);
          $('#uploaded-doc-link').attr('href',data);
          document.getElementById('uploaded-doc-link').click();
          $('#uploadedModal').modal('hide');

        },
        error: function(response){
          alert('Please try after some time');
        }
      });
    }
    else{
      alert("Please Select At Least One.");
    }
  });
// standard document modal show
$(document).on('click', '.download-standard-documents', function(event) {
  event.preventDefault();
  var job_id = $(this).attr('data-id');
  $('#job_id').val(job_id);
  $('#standard_class input[type="checkbox"]').prop('checked',false);
  $('#download-standard').attr('disabled',false);
  $('#standardModal').modal();    
});
//checked checkbox of standard move 
$(document).on('click','#standard_class input[type="checkbox"]',function(e){
  var $this = $(this);
  if ( $this.prop('checked') )
         { // move to top
          $('#standard_class').prepend(($(this).parent()));
        }
        else 
         { // move to bottom
          $('#standard_class').append(($(this).parent()));
        }
      });
//standard save
$(document).on('click', '#download-standard', function(event) {
  event.preventDefault();   
  var selected = [];
  var i=0;
  $("#standard_class input:checkbox[name]:checked").each(function() {
   var nm = $(this).prop('name');
   var value = $(this).val();
   selected.push([nm , value]);
 });
  if (selected.length != 0) {
    $('.loder-img').show();
    $('#standardModal').modal('hide');
    $.ajax({
      url: '/admin/my-jobs/download-standard-documents/{!! $job->id !!}',
      type: 'GET',
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
      data: { formData : selected,'job_type':"surveying"},
      success: function(data){
        $('#download-link').attr('href',data);
        document.getElementById('download-link').click();
        $('.loder-img').hide();

      },
      error: function(response){
        alert('Please try after some time');
        $('.loder-img').hide();
      }
    });
  }
  else{
    alert("Please Select At Least One.");
  }
});
  
</script>
@endsection
