@extends('layouts.frame')

@section('content')
<div class="container-fluid">
    <div class="container mt-3" style="min-height: 50vh;">
        <div class="row">
            <div class="col-md-12">
                @if (session('success'))
                <div class="alert alert-info">
                       {{ session('success') }}
                </div>
            @endif
            </div>
        </div>
        <form action="{!! url('admin/post-client-video') !!}" method="post">
        {{ csrf_field() }}
            <div class="">
                <div class="form-group{{ $errors->has('client_video') ? ' has-error' : '' }}">
                    <label for="client_video" class="control-label" style="margin-top: 50px">URL</label>
                    <input id="client_video" type="url" class="form-control" name="client_video" value="@isset( $jobs->ao->surveyor_name ){{$jobs->ao->surveyor_name }}@endisset{{ old('client_video') }}" >
                    <span>@if($errors->has('client_video')) {!! $errors->first('client_video') !!} @endif</span>
                </div>
            </div>
            <div class="col-md-12">
                <button class="wpcf7-form-control wpcf7-submit pull-right" style="margin-right: -8px">Submit</button>
            </div>    
        </form>  
    </div>
</div>
@endsection
