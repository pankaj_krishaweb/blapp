@extends('layouts.frame')

@section('content')
<style>


     #job-search-btn{

    background-color: #fff;
    color: #b3b3b3;
    border: solid 0.5px #b3b3b3;
    width: 250px;
    border-radius: 0px;
    text-align: left;
  }
  .open > .dropdown-menu {
    display: block;
    width: 100%;

    margin-top: 0px;
    border-radius: 0px;
}
#myInput{
    border-right: none;
    border-left: none;
    border-top: none;
    border-radius: 0px;
    margin-top: -6px;
}
</style>
@php
    $stripe = array(
  "secret_key"      => "sk_test_cTtRAp2zFSYTdz5hLS0svSq0",
  "publishable_key" => "pk_test_ioIRT437uhLmktbdtIOyVodu"
);

\Stripe\Stripe::setApiKey($stripe['secret_key']);

@endphp



<div class="container-fluid" style="min-height: 80vh;">
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <h1>Payment
            <span class="pull-right">
                @if (Auth::user()->jobs->count()>1)
                    <div class="dropdown">
                        <button class="btn btnblock dropdown-toggle" type="button" data-toggle="dropdown" id="job-search-btn">
                        Select Job
                            <span class="pull-right"><span class="caret "></span></span>
                        </button>
                        <ul class="dropdown-menu">
                            <input class="form-control" id="myInput" type="text" placeholder="Search..">
                            @foreach (Auth::user()->jobs as $job)
                               <li><a href="{{ url('/client/payment/'.$job->id) }}">BLSN{{ $job->id}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </span>
        </h1>
@if(isset($job))
        <h4>{{ $job->job_no}}</h4>
        @if ($job->payments->count()>=1)
            @foreach ($payments as $payment)
                @if ($payment->paid == 1)
                <div class="row" style="margin: 20px 0px; padding: 10px 0px; border: solid 0.5px #cccccc;">
                    <div class="col-md-10 col-md-offset-1" style="margin-top: 50px; margin-bottom: 50px;">
                        <p> <strong> Invoice Reference:</strong> {{ $job->invoice_no }}<br>
                                    <strong>Our Ref:</strong> BLSN{{ $job->id }}</p>
                        <p>Settled on {{ $payment->updated_at}} Thank You </p>
                    </div>
                </div>
                @else
                    <div class="row" style="margin: 20px 0px; padding: 10px 0px; border: solid 0.5px #cccccc;">
                        <div class="col-md-10 col-md-offset-1" style="margin-top: 50px; margin-bottom: 50px;">
                            <p>
                                {{ $job->bo->full_name}} <br>
                                {{ $job->bo->property_address_proposed_work}}
                            </p>
                            <p class="text-right"> {{ date('d/m/Y')}}<br>
                                <strong> Invoice Reference:</strong> {{ $job->invoice_no }}<br>
                                <strong>Our Ref:</strong> {{ $job->job_no }}
                            </p>
                            <h2>INVOICE NUMBER {{ $job->invoice_no }}</h2>
                            <h4>RE: {{ $job->bo->property_address_proposed_work}}</h4>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>DESCRIPTION OF SERVICE:</th>
                                        <th>AMOUNT:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Party Wall Agreement costs acting as the Adjoining owner’s Surveyor</td>
                                        <td> £{{ $payment->amount}} </td>
                                    </tr>
                                    <tr>
                                        <td>Vat at 20%</td>
                                        <td> £{{ ($payment->amount /100)*20 }} </td>
                                    </tr>
                                    <tr>
                                        <td>Land Registry costs at £6.00 per owner</td>
                                        <td> {{ $job->land_registry_costs }}</td>
                                    </tr>
                                    <tr>
                                        <td>Printing &amp; Postage Costs</td>
                                        <td>  {{ $job->printing_postage_costs }} </td>
                                    </tr>
                                    <tr>
                                        <th>TOTAL DUE: </th>
                                        <th> {{ array_sum([ (($payment->amount /100)*20), $payment->amount, str_replace('£','',$job->land_registry_costs ), str_replace('£','',$job->printing_postage_costs )  ])}}</th>
                                    </tr>
                                </tbody>
                            </table>
                            <p>Please kindly make payment within 7 days of the invoice date. </p>
                            <h4>For ease of identification please enter the Invoice number BLPWI1344 as the payment reference.</h4>
                            <h4>Payment Methods</h4>
                            <h4>Bank Payment</h4>
                            <p>Please make a bank/online payment to the following account:</p>
                            <h5>BERRY LODGE SURVEYORS </h5>
                            <p>
                                Bank Account Number:            <span>2 4 0 9 6 3 2 6</span><br>
                                Sort Code:             <span> 5 6 - 0 0 - 1 4</span></br>
                                Bank:                   <span>Natwest</span></br>
                                Branch:             <span>Baker Street, 69 Baker Street, W1U 6AT</span></br>
                                IBAN:                   <span>B40NWBK56001424096326</span></br>
                                BIC:                    <span>NWBKGB2L</span>
                            </p>
                            <h4>Cheque </h4>
                            <p>Please send a cheque made payable to ‘BERRY LODGE SURVEYORS’ to Upper Floor, 61 Highgate High Street, London, N6 5JY. For ease of identification please enter Invoice number BLPWI1335 on the rear of the cheque. </p>
                            <form action="/client/payment-process" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="payment_id" value="{{ $payment->id }}">

                                <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="pk_test_ioIRT437uhLmktbdtIOyVodu"
                                    data-amount="{{ str_replace('.','',$payment->amount) }}"
                                    data-name=" Berry Lodge Surveyors"
                                    data-description="{{ $job->id}} - {{ $payment->description}}"
                                    data-image="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-180x180.jpg"
                                    data-locale="auto"
                                    data-currency="gbp">
                                </script>
                            </form>
                            <h3>THANK YOU IN ADVANCE FOR YOUR PAYMENT</h3>
                        </div>
                    </div>
                @endif
            @endforeach
        @else
        <h3>Your Invoice has not been generated yet</h3>
        @endif
    @else
      <h3>Nothing here yet!</h3>
    @endif 
    </div>
</div>




@endsection
@section('script')




@endsection
