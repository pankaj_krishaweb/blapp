@extends('layouts.frame')

@section('content')
<div class="container-fluid">
    <div class="container" style="min-height: 400px; padding-bottom: 50px;">
        <h1>Tasks</h1>
         <div class="row">
         	<div class="col-md-3">
         		<ul class="nav nav-pills nav-stacked">
         			<li role="presentation" @if (Request::is('admin/tasks')) class="active" @endif><a href="{{ url('admin/tasks') }}">All</a></li>
         			<li role="presentation" @if (Request::is('admin/tasks/adjoining-owners-surveyor')) class="active" @endif><a href="{{ url('admin/tasks/adjoining-owners-surveyor') }}">Adjoining Owners Surveyor</a></li>
  					<li role="presentation" @if (Request::is('admin/tasks/building-owners-surveyor')) class="active" @endif><a href="{{ url('admin/tasks/building-owners-surveyor') }}">Building Owners Surveyor</a></li>
  					<li role="presentation" @if (Request::is('admin/tasks/agreed-surveyor')) class="active" @endif><a href="{{ url('admin/tasks/agreed-surveyor') }}">Agreed Surveyor</a></li>
				</ul>
         	</div>
         	<div class="col-md-9">
         		<table class="table table-bordered table-responsive">
				    <thead>
				      <tr>
				      	<th>ID</th>
				      	<th>Type</th>
				        <th>Task</th>
				        <th>Internal</th>
				        <th>Option</th>

				      </tr>
				    </thead>
				    <tbody>

				    	@foreach($tasks as $task)
				      	<tr data-id="{{ $task->id }}">
				      		<td>{{ $task->id }}</td>
				      		<td>
				      			@if($task->job_type==1)
				      			Adjoining Owners Surveyor
				      			@elseif($task->job_type==2)
				      			Building Owners Surveyor
				      			@elseif($task->job_type==3)
				      			Agreed Surveyor
				      		
				      			@endif
				      		</td>
				        	<td>{{ $task->task }}</td>
				        	<td>@if($task->is_internal==1) yes @endif </td>
				        	<td>
				        		<button type="button" class="btn btn-xs btn-danger delete-task-btn">Delete</button>
				        		<button type="button" class="btn btn-xs btn-primary task-jobs-btn">Edit</button>
				        	</td>
				      	</tr>
				      @endforeach
				     
				    </tbody>
				  </table>
				{{-- add task Modal --}}
				<button type="button" class="btn blue-btn btn-lg" data-toggle="modal" data-target="#addtaskModal">Add task</button>
				<div id="addtaskModal" class="modal fade" role="dialog">
				  	<div class="modal-dialog">
				   		<div class="modal-content">
				      		<div class="modal-header">
				       	 		<button type="button" class="close" data-dismiss="modal">&times;</button>
				        		<h4 class="modal-title">Add task</h4>
				      		</div>
				      		<div class="modal-body">
                    			<form class="form-horizontal" role="form" >
                        			<div class="form-group{{ $errors->has('task') ? ' has-error' : '' }}">
                            			<label for="task" class="col-md-4 control-label">Task</label>
                            			<div class="col-md-6">
                                			<textarea id="task"  class="form-control" name="task" value="{{ old('task') }}" required autofocus> </textarea>
                            			</div>
                        			</div>
			                        <div class="form-group">
			                            <label for="job_type" class="col-md-4 control-label">Job Type</label>
			                            <div class="col-md-6">
				                           <select class="form-control" name="job_type" id="job_type">
					                           	<option value="1">Adjoining Owners Surveyor</option>
							  					<option value="2">Building Owners Surveyor</option>
							  					<option value="3">Agreed Surveyor</option>
							  					
				                           </select>
			                            </div>
			                        </div>
			                       
			                        <div class="form-group">
			                            <label for="internal" class="col-md-4 control-label">Internal</label>
			                            <div class="col-md-6">
			                                <label class="radio-inline">
										      <input type="radio" name="internal" value="1">Yes
										    </label>
										    <label class="radio-inline">
										      <input type="radio" name="internal" value="0">No
										    </label>
			                            </div>
			                        </div>
			                        <div id="error-display"></div>
								    <div class="modal-footer">
								      	<button type="button" class="btn blue-btn" id="add-task-btn">Add </button>
								        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								    </div>
				       			</form>
				   			</div>
				  		</div>
					</div>
				</div>		
				<div id="taskHistoryModal" class="modal fade" role="dialog">
				  	<div class="modal-dialog">
				    	<div class="modal-content">
				      		<div class="modal-header">
				       	 		<button type="button" class="close" data-dismiss="modal">&times;</button>
				        		<h4 class="modal-title">task History</h4>
				      		</div>
				      		<div class="modal-body">
				        		<div id="history-display"></div>
				      		</div>
				      		<div class="modal-footer">
				        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      		</div>
				    	</div>
				  	</div>
				</div>
         	</div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).on('click', '#add-task-btn', function(event) {
		event.preventDefault();
		var task= $('#task').val();
		var internal= $("input[name='internal']:checked").val();
		var job_type=$('#job_type').val();
	

	$.ajax({
		url: '/admin/add-task',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {task: task, internal:internal, job_type:job_type},
		success: function(data){
			console.log(data);
			var status = $.parseJSON(data);
			if(status.status=='good'){
				swal("task added successfully ").then(() => {
				 location.reload();
				});				
			}
		},
		error: function(data){
	    	var errors = $.parseJSON(data.responseText);
		    console.log(errors);
			var displayerror='<div class="alert alert-dismissible alert-danger">';
		   $.each(errors, function(index, value) {
		      displayerror+='<li>'+value+'</li>';
		    });
		    displayerror+='</div>';
		    $('#error-display').html(displayerror);
	    }
		
	})

});


$('.delete-task-btn').on('click',  function(event) {
	event.preventDefault();
	var task_id = $(this).parent().parent().attr('data-id');
	swal("Are you sure you wish to delete this task?", {
  buttons: ["Cancel", "Yes Delete task!"],
}).then(function(value){
	if(value==true){
		$.ajax({
			url: '/admin/delete-task',
			type: 'POST',
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
			data: {task_id: task_id},
			success: function(data){
				var status = $.parseJSON(data);
				if(status.status=='good'){
					// location.reload();			
				}
			},
		})		
	}
	return false;
}, 
	function(){
		console.log('Not deleted');
		return false;
	});
});

$('.task-history-btn').on('click',  function(event) {
	event.preventDefault();
	var task_id = $(this).parent().parent().attr('data-id');
	$.ajax({
		url: '/admin/task-history',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {task_id: task_id},
		success: function(data){
			console.log(data);
			$('#taskHistoryModal').modal();
			var history = $.parseJSON(data);
			var displayhistory='<ul>';
		   	$.each(history, function(index, value) {
		    	displayhistory+='<li><strong> '+value.created_at+' :  '+value.action+' <br></strong> '+value.description+'</li>';
		    });
		    displayhistory+='</ul>';
		    $('#history-display').html(displayhistory);
	    

		},
	})		
});

</script>
@endsection
