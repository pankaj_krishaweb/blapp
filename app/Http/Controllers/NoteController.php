<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;
use App\NoteHistory;
use Auth;
class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $note = new Note;
        $note->job_id = $request->get('job_id');
        $note->slug = $request->get('title').date('y-m-d');
        // $note->note_history_id = $note_history->id;
        $note->save();


        $note_history = new NoteHistory;
        $note_history->note_id = $note->id;
        $note_history->user_id = Auth::user()->id; 
        $note_history->title = $request->get('title');
        $note_history->body = $request->get('summernote');
        $note_history->save();

        
        return response('success',200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $note_histories = NoteHistory::where('note_id',$id)->get()->last();
        return response($note_histories,202);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $note = Note::find($request->get('note_id'));
        $note->slug = $request->get('title').date('y-m-d');
        // $note->note_history_id = $note_history->id;
        $note->save();


        $note_history = new NoteHistory;
        $note_history->note_id = $note->id;
        $note_history->user_id = Auth::user()->id; 
        $note_history->title = $request->get('title');
        $note_history->body = $request->get('summernote');
        $note_history->save();


        return response('success',200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Note::find($id)->delete();
        NoteHistory::where('note_id',$id)->delete();
        return response('success',200);
    }

    public function history($id){
        $histories = NoteHistory::where('note_id',$id)->latest()->limit(15)->get();
        $title = Note::find($id)->slug;
        // return response($histories,202);
        return response(['histories'=>$histories,'title'=>$title], 202);

    }
}
