<?php

namespace App\Src\Docs\Appointment;
use Illuminate\Support\Facades\Storage;

class  LetterOfAppointmentAO10
{
	public $documentFolder="Appointment Letters";
	public $documentName="Letter of Appointment AO 10";

    	public function create(\App\Job $job){
	    	# make storage directory
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();

		$section->addTextBreak(6);
		$section->addText(ucwords($job->ao10->full_names), ['bold' => true],['alignment' => 'center']);
		$section->addText('of', ['bold' => true],['alignment' => 'center']);
		$section->addText(ucwords($job->ao10->contact_address), ['bold' => true],['alignment' => 'center']);
		$section->addTextBreak();
		$section->addText($job->ao10->surveyor_name.' '.$job->bo->surveyor_qualifications, ['bold' => true]);
		$section->addText($job->ao10->surveyor_company_name, ['bold' => true]);
		$section->addText($job->ao10->surveyor_company_address, ['bold' => true]);
		$section->addTextBreak();
		$section->addText(date("d/m/Y"), [], [ 'align' => 'right' ]);
		$section->addTextBreak();
		$section->addText('Dear '.$job->ao10->surveyor_name);
		$section->addTextBreak();
		$section->addText('Re: '.$job->ao10->property_address_adjoining,['bold' => true]);
		$section->addText('The Party Wall etc. Act 1996	',['bold' => true]);
		$section->addTextBreak();
		$section->addText('As '.$job->ao10->owners_referral.' of the above property '.$job->ao10->i_we_referral_lower.' hereby authorise you to sign, issue and receive any Notices relating to the Party Wall etc. Act 1996 which affect '.$job->ao10->my_our_refferal.' property.');
		$section->addTextBreak(1);
		$section->addText($job->ao10->i_we_referral.' appoint you, '.$job->ao10->surveyor_name.' '.$job->ao10->surveyor_qualifications.'  to act as '.$job->ao10->my_our_refferal.' surveyor in accordance with Section 10 of the Party Wall etc. Act 1996.');
		$section->addTextBreak();
		$section->addText('Yours sincerely, ');
		$section->addTextBreak(3);
		$section->addText('…………………………………………',['bold' => true]);
		$section->addText($job->ao10->full_names,['bold' => true]);
		$section->addTextBreak(3);
		$section->addText('…………………………………………',['bold' => true]);
		$section->addText($job->ao10->full_names,['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
