@extends('layouts.frame')
@section('content')
<div class="container-fluid">
    <div class="container">
    	{{-- <div class="row"> --}}
	       	<div class="col-md-12">
		  	   	<h1>Our Community</h1> 
		  	   	<ol class="breadcrumb"> 
                    <li><a href="{{ url('admin') }}">Admin</a></li>
                    <li><a href="{{ url('admin/our-community') }}">Our Community</a></li>
                    <li>View</li>    
              	</ol>
		    </div>
		    <div class="table-responsive table-bordered">
	  			<table class="table">
	  				<tr>
	  					<td>Id :</td>
	  					<td>{!! $data->id !!}</td>
	  				</tr>
	  				<tr>
	  					<td>name :</td>
	  					<td>{!! $data->name !!}</td>
	  				</tr>
	  				<tr>
	  					<td>logo :</td>
	  					<td><img src="{!! url('storage/'.$data->logo) !!}" style="height: 100px; width: 100px"> </td>
	  				</tr>
	  				<tr>
	  					<td>Description :</td>
	  					<td>{!! $data->description !!}</td>
	  				</tr>
	  				<tr>
	  					<td>Contact :</td>
	  					<td>{!! $data->contact !!}</td>
	  				</tr>
	  			</table>
		  	</div>
	</div>
</div>

@endsection