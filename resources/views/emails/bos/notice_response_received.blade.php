<html>
<head>
	<style type="text/css">
		.float-md-left,
		.float-md-right{
			text-align: center;
		}
		h2{
			font-size: 30px;
			line-height: 40px;
			color: #00b29c;
			font-weight: bold;
		}
		@media (min-width: 768px) {
			.title-wrap{
				display: table;
				max-width: 800px;
				width: 100%;
			}
			h2{
				font-size: 47px;
				line-height: 57px;
			}
			.title-wrap .float-md-left,
			.title-wrap .float-md-right{
				display: table-cell;
				vertical-align: middle;
			}
			.title-wrap h2{
				font-size: 47px;
				line-height: 1.5;
			}
			.float-md-left{
				float: left;
				width: calc(100% - 250px);
				text-align: left;
			}
			.float-md-right{
				float: right;
				width: 250px;
				text-align: right;
			}
			.float-md-right img{
				max-width: 100%;
			}
		}
	</style>
	
</head>
<body >
	<div class="text-center title-wrap">
		<div class="float-md-left">
			<h2>We’re getting the Schedule of Condition booked into the diary!</h2>
		</div>
		<div class="float-md-right">
			<img src="{!! asset('images/bos/5.png') !!}" width="200px">
		</div>
	</div>

<div>

<p>Dear {{$salutation}},</p>

<p>I am pleased to confirm that a Party Wall Surveyor has been appointed on behalf of the adjoining owners.</p>

<p>You can find out the name of the Surveyor by logging into the <a href="https://app.blsurveyors.com/">Berry Lodge Client Portal</a> now
to see the status of your Party Wall Surveying job.</p>

<p>The next step in the Party Wall process is to undertake a Schedule of Condition Report of the adjoining owner’s property. The <a href="https://www.blsurveyors.com/services/surveying/schedule-condition-report/">Schedule of Condition</a> itself will take in the region of 45 minutes to an hour at which time we will be taking detailed photographs and an in depth record of the property to ensure there is a legal record in place.</p>

<p>You can see an example of one of our Schedule of Condition Reports on our website in our experience we have found that Schedule of Condition Reports are one of the key steps in the Party Wall process. You can also watch an informative video which we’d suggest having
a quick watch of.</p>
<p>We will be in touch to confirm when this has been booked in.</p>

<p>Login to the  <a href="https://app.blsurveyors.com/">Berry Lodge Client Portal</a> now to see the status of your Party Wall Surveying job.</p>

<p>Kind Regards, </p>

<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;" >
<p><strong>Berry Lodge Surveyors</strong></p>
</div>
@include('shard.footer')

</div>
</body>
</html>