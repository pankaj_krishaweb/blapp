<html>
<head>
<style type="text/css">
	.img-wrap{
		margin-bottom: 10px;
	}
	.img-wrap img{
		max-width: 250px;
		width: 100%;
		height: auto;
	}
</style>
</head>
<body >
	<div class="text-center img-wrap">
		<img src="{!! asset('images/Thank_You.png') !!}" width="150px">
	</div>
<p>Dear {{$client_name}},</p>
<p>On behalf of Berry Lodge Surveyors, we would like to say a big Thank You!</p>

<p>We hope your Surveyor {!! $surveyor_name !!} has met and perhaps even exceeded your expectations and that your experience with Berry Lodge Surveyors has been a pleasant one.</p>

<p>If you can spare a minute, we’d love for you to leave us a quick review. Your positive feedback will help us grow as a business and stand out from the crowd.</p>

<p>You can leave a review <a href="https://www.trustpilot.com/evaluate/www.blsurveyors.com?stars=5" target="_blank">here</a>.</p>
<p>Thank you for your custom, we hope to work with you in the future.</p>
<p>Kind Regards, </p>

<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;">
<p><strong>Berry Lodge Surveyors</strong></p>
@include('shard.footer')

</div>
</body>
</html>