 <div class="col-md-9 col-xs-12" style="margin-bottom: -1px;">
    <div class="row hidden-xs" id="admin-menu">                                                    
        <div class="col-md-2 col-xs-4 pull-right">
            <a href="{{ url('admin/enquiries') }}">
                <div class="admin-menu-item @if (Request::is('admin/enquiries*')) item-active @endif">
                    <i class="fa fa-users" style="font-size:24px"></i><br><p style="font-family: Poppins;">Enquiries</p> 
                </div>
            </a>
        </div>
        <div class="col-md-2 col-xs-4 pull-right">
            <a href="{{ url('admin/my-jobs') }}">
                <div class="admin-menu-item @if (Request::is('admin/my-jobs*')) item-active @endif">
                    <i class="fa fa-briefcase" style="font-size:24px"></i><br><p style="font-family: Poppins;">My Jobs </p>
                </div>
            </a>
        </div>
        <div class="col-md-2 col-xs-4 pull-right">
            <a href="{{ url('admin/message') }}">
                <div class="admin-menu-item @if (Request::is('admin/message*')) item-active @endif">
                   <i class="fa fa fa-commenting-o" style="font-size:24px"></i><br><p style="font-family: Poppins;">Messages </p>
                </div>
            </a>
        </div>
        <div class="col-md-2 col-xs-4 pull-right">
            <a href="{{ url('admin/jobs') }}">
                <div class="admin-menu-item @if (Request::is('admin/jobs*')) item-active @endif">
                    <i class="fa fa-file-o" style="font-size:24px"></i><br><p style="font-family: Poppins;">Jobs</p>
                </div>
            </a>
        </div>
        <div class="col-md-2 col-xs-4 pull-right">
            <a href="{{ url('admin/tasks') }}">
                <div class="admin-menu-item @if (Request::is('admin/tasks*')) item-active @endif">
                        <i class="fa fa-clipboard" style="font-size:24px"></i> <br><p style="font-family: Poppins;">Tasks </p>
                </div>
            </a>
        </div>
        <div class="col-md-2 col-xs-4 pull-right">
            <a href="{{ url('admin/storage') }}">
                <div class="admin-menu-item @if (Request::is('admin/storage*')) item-active @endif">
                        <i class="fa fa-archive" style="font-size:24px"></i> <br><p style="font-family: Poppins;">Storage</p>
                </div>
            </a>
        </div>
        <div class="col-md-2 col-xs-4 pull-right">
            <a href="{{ url('admin') }}">
            <div class="admin-menu-item @if (Request::is('admin') || Request::is('admin/users*') || Request::is('admin/progress-report') ) item-active @endif ">
                <i class="fa fa-tasks" style="font-size:24px"></i><br><p style="font-family: Poppins;">Admin </p>
            </div>
            </a>
        </div> 
    </div>
    
</div>
<style>
.mob-admin-menu-item {
    background-color: #414861;
    color: #fff;
    padding: 15px 5px 1px 5px;
    min-height: 40px;
    text-align: center;
    border-bottom: solid 0.5px #fff;
}</style>
<div class="visible-xs" id="admin-menu">   
      
        <button class="btn btn-block" data-toggle="collapse" data-target="#demo" style="background-color: #414861; color: #fff; border-radius: 0px; margin: 10px 0px;">Menu</button>
        <div id="demo" class="collapse" style="border: solid 0.5px #414861;">          
            <a href="{{ url('admin/enquiries') }}">
                <div class="mob-admin-menu-item @if (Request::is('admin/enquiries*')) item-active @endif">
                    <i class="fa fa-users" ></i> Enquiries
                </div>
            </a>
            <a href="{{ url('admin/my-jobs') }}">
                <div class="mob-admin-menu-item @if (Request::is('admin/my-jobs*')) item-active @endif">
                    <i class="fa fa-briefcase" ></i> My Jobs 
                </div>
            </a>
            <a href="{{ url('admin/message') }}">
                <div class="mob-admin-menu-item @if (Request::is('admin/message*')) item-active @endif">
                   <i class="fa fa fa-commenting-o" ></i> Messages 
                </div>
            </a>
            <a href="{{ url('admin/jobs') }}">
                <div class="mob-admin-menu-item @if (Request::is('admin/jobs*')) item-active @endif">
                    <i class="fa fa-file-o" ></i> Jobs
                </div>
            </a>
            <a href="{{ url('admin/tasks') }}">
                <div class="mob-admin-menu-item @if (Request::is('admin/tasks*')) item-active @endif">
                        <i class="fa fa-clipboard" ></i> Tasks 
                </div>
            </a>
            <a href="{{ url('admin/storage') }}">
                <div class="mob-admin-menu-item @if (Request::is('admin/storage*')) item-active @endif">
                        <i class="fa fa-archive" ></i> Storage 
                </div>
            </a>
            <a href="{{ url('admin') }}">
            <div class="mob-admin-menu-item @if (Request::is('admin') || Request::is('admin/users*') || Request::is('admin/progress-report') ) item-active @endif ">
                <i class="fa fa-tasks" ></i> Admin 
            </div>
            </a>
        {{-- logoin --}}
              @if (!Auth::guest())
                <a class="members-login-btn" href="{{ url('/logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    <div class="mob-admin-menu-item"> Logout</div>
                </a>

                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form> 
              @endif                     
            {{-- logout --}}
        </div>
    </div>

                              

                       