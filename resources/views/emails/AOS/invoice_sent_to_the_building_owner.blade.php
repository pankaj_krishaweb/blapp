
<html>
<head>
	<style type="text/css">
		.float-md-left,
		.float-md-right{
			text-align: center;
		}
		h2{
			font-size: 30px;
			line-height: 40px;
			color: #00b29c;
			font-weight: bold;
		}
		@media (min-width: 768px) {
			.title-wrap{
				display: table;
				max-width: 800px;
				width: 100%;
			}
			h2{
				font-size: 47px;
				line-height: 57px;
			}
			.title-wrap .float-md-left,
			.title-wrap .float-md-right{
				display: table-cell;
				vertical-align: middle;
			}
			.title-wrap h2{
				font-size: 47px;
				line-height: 1.5;
			}
			.float-md-left{
				float: left;
				width: calc(100% - 250px);
				text-align: left;
			}
			.float-md-right{
				float: right;
				width: 250px;
				text-align: right;
			}
			.float-md-right img{
				max-width: 100%;
			}
		}
	</style>
	
</head>
<body >
	<div class="text-center title-wrap">
		<div class="float-md-left">
			<h2>We’ve popped our invoice over to the Building Owner for Payment</h2>
		</div>
		<div class="float-md-right">
			<img src="{!! asset('images/aos/8.png') !!}" width="200px">
		</div>
	</div>
	<div>
		<p>Just a quick email to confirm that we have popped over our invoice to the building owner’s Surveyor.</p>
		<p>On behalf of all the team here at Berry Lodge Surveyors, we would like to thank you for selecting Berry Lodge Surveyors for your Party Wall Surveying requirements.</p>
		<p>We hope we have met your expectations and look forward to working together again in the future.</p>
		<p>Don’t forget, you can save money and also save money for your friends, colleagues and acquaintances in the Discounts tab of the <a href="https://app.blsurveyors.com/"> Berry Lodge Client Portal </a>.</p>

		<p>Kind Regards </p>

		<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;">
		<p><strong>Berry Lodge Surveyors</strong></p>
	</div>
	@include('shard.footer')

</div>
</body>
</html>