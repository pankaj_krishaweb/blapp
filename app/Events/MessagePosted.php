<?php

namespace App\Events;

use App\Message;
use App\User;
use Illuminate\Support\Facades\Auth;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessagePosted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Message
     *
     * @var Message
     */
    public $message;

    /**
     * User
     *
     * @var User
     */
    public $user;
    public $job_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Message $message, User $user, $job_id)
    {

     // foreach ($message as $msg) {
     //        if ($user->user_id==Auth::user()->id) {
     //           $msg->who="bubble-right";
     //        }else{
     //        $msg->who="bubble-left";
     //        } 
     //    }
        $this->job_id = $job_id;
        $this->message = $message;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('chatroom.'.$this->job_id);
    }
}
