@extends('layouts.frame')

@section('content')
  <style>
  /* Style the input field */


  #job-search-btn{

    background-color: #fff;
    color: #b3b3b3;
    border: solid 0.5px #b3b3b3;
    border-radius: 5px;
    width: 100%;
        border-radius: 0px;
  }
  .open > .dropdown-menu {
    display: block;
    width: 100%;
    margin-top: 0px;
    border-radius: 0px;
     height: auto;
    max-height: 200px;
    overflow-x: hidden;
}
.list-group{
    max-height: 400px;
    margin-bottom: 10px;
    overflow:scroll;
    -webkit-overflow-scrolling: touch;
}
  a {
      color: #00b29c;
      text-decoration: none;
  }

  .filter-input {
    padding: 30px;
    margin: 10px 0px;
    border: 0;
    border-radius: 0;
    background: #eef2f6;
    }
  </style>
<div class="container-fluid"> 
  <div class="container" style="min-height: 200px;">
    <div class="row">
      <div class="col-md-12">
        <h1>Jobs</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin') }}">Home</a></li>
          <li class="admin/jobs">  Jobs</li>
        </ol>
      </div>
      <div class="col-md-4">
        <input class="form-control filter-input" id="myInput" type="text" placeholder="Search by Job No ...">
      </div>
      <div class="col-md-5">
        <input class="form-control filter-input" id="myInputAdd" type="text" placeholder="Search by Address ...">
      </div>
      <div class="col-md-3 text-center">
        @if (Request::is('admin/jobs/archived'))
          <a href="{{ url('/admin/jobs') }}" class="btn btn-block" style="margin: 10px 0px; border-radius: 0px; padding: 18px; background-color: #414861; color: #fff;">View All Jobs</a>
        @else
          <a href="{{ url('/admin/jobs/archived') }}" class="btn btn-block" style="margin: 10px 0px; border-radius: 0px; padding: 18px; background-color: #414861; color: #fff;">View Archived Jobs</a>
        @endif
      </div>
      <div class="col-md-5 col-sm-4 hidden-xs">
        <br>
        <div class="list-group">
          @foreach ($jobs as $job)
            @if($job->surveying)
              <a class="list-group-item" href="{{ url('/admin/jobs/'.$job->id) }}"><strong>BLSN{{ $job->id}}</strong> - {{$job->surveying->address_of_inspection}}</a>
            @else
              <a class="list-group-item" href="{{ url('/admin/jobs/'.$job->id) }}"><strong>BLSN{{ $job->id}}</strong> - {{$job->bo->property_address_proposed_work}}</a>
            @endif

          @endforeach
        </div>
      </div>
      <div class="col-md-7" style="min-height: 400px;">
        <br>
        <div class="row">
          @if($docs->count() >= 1)
            @foreach($docs as $key =>$value)
              <div class="col-md-2 col-sm-2 col-xs-6 text-center" style="color: #00B29D;">
                <a href="{{ url("/admin/jobs/{$key}") }}">
                  <i class="fa fa-folder-o" aria-hidden="true" style="font-size: 35px;"></i>
                  <p>{{ $key}} <small>({{ $value->count() }})</small></p>
                </a>
              </div>
            @endforeach
          @else
            <h2 class="text-danger"> No documents in storage at the moment</h2>
            <p>Use the form the drag and drop feature to start uploading documents. </p>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>


@isset ($type)
  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Preview</h4>
        </div>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            @foreach($docs->where('type', $type)  as $doc)
            <div class="item  @if ($loop->first)
              active
            @endif " style="min-height: 400px;">
             <img class="center-block" src="{{ asset($doc->link) }}" style="max-height: 500px;">
              <div class="carousel-caption">
               <h2 > {{ $doc->name }}</h2>
               <a  class="btn btn-success btn-sm"  href="{{ asset($doc->link) }}" download>
                download
               </a>
              </div>
            </div>
            @endforeach
          </div>
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div>
      </div>
    </div>
  </div>
@endisset
@endsection

@section('script')
<script>
    $(document).ready(function(){
      $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".list-group a").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });

        $(document).ready(function(){
      $("#myInputAdd").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".list-group a").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
</script>

@endsection
