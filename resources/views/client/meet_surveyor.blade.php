@extends('layouts.frame')

@section('content')
<style type="text/css">
    .meet-img {
      float: right;
      margin: 60px 0px 15px 20px;
    }
    .surveyor-desc-box{
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
      padding: 30px 0;
    }
    .flex-row-reverse {
      -ms-flex-direction: row-reverse!important;
      flex-direction: row-reverse!important;
    }
    .surveyor-desc-box h1{
      margin: 0 !important;
      padding-bottom: 20px; 
    }
    .surveyor-desc-box-left{
      width: calc(100% - 400px);
    }
    .surveyor-desc-box-right{
      width: 400px;
    }
    @media(max-width: 991px){
      .surveyor-desc-box{
        display: block;
      }
      .surveyor-desc-box-left{
        width: 100%;
        padding-bottom: 20px;
      }
      .surveyor-desc-box-right{
        width: 100%;
        padding-bottom: 20px;
        text-align: center;
      }
      .meet-img{
        float: none;
        margin: 0;
      }
    }
    @media (max-width: 767px){
      .surveyor-desc-box h1{
        font-size: 2.3em;
      }
    }
</style>
<div class="container-fluid">
    <div class="container">
      <h1 style="color: #00b29d">Meet your Surveying Team</h1>
      @foreach($jobs as $job)
        @foreach($job->users as $user)
        @php
          $job_user = App\Job_User::where([['job_id','=',$job->id],['user_id','=',$user->id]])->first();
        @endphp

          @if($user->role != 3 && $job_user->user_role == "Named Surveyor")
            <div class="surveyor-desc-box flex-row-reverse">
              <div class="surveyor-desc-box-right">
                @if($user->picture == null)
                  <img class="meet-img" src="{!! asset('images/avtar.png') !!}" style="height: 300px"> 
                @else
                  @if(file_exists($user->picture))
                    <img class="meet-img" src="{!! asset($user->picture) !!}" style="height: 300px">
                  @else
                    <img class="meet-img" src="{!! asset('images/avtar.png') !!}" style="height: 300px">
                  @endif
                @endif
              </div>
              <div class="surveyor-desc-box-left">
                <div class="surveyor-desc">
                  <h1 style="color: #00b29d">Meet {!! $user->name !!}</h1>
                  <p>{!! $user->notes !!}</p>
                  @if($user->message)
                  <p style="color: #00b29d; font-family:poppins">A message from {!! $user->name !!}:</p>

                  <p>{!! $user->message !!}</p>
                  @endif
                  <p><img src="{!! asset($user->signature) !!}" style="height: 100px;"></p>
                  <p><b>{!! $user->name !!}</b></p>
                  <p>{!! $user->qualifications !!}</p>
                </div>
              </div>
              
            </div>
          @endif
        @endforeach
      @endforeach

      @foreach($jobs as $job)
        @foreach($job->users as $user)
        @php
          $job_user = App\Job_User::where([['job_id','=',$job->id],['user_id','=',$user->id]])->first();
        @endphp
        
          @if($user->role != 3 && $job_user->user_role == "Assisting Surveyor")
            <div class="surveyor-desc-box flex-row-reverse">
              <div class="surveyor-desc-box-right">
                @if($user->picture == null)
                  <img class="meet-img" src="{!! asset('images/avtar.png') !!}" style="height: 300px"> 
                @else
                  @if(file_exists($user->picture))
                    <img class="meet-img" src="{!! asset($user->picture) !!}" style="height: 300px">
                  @else
                    <img class="meet-img" src="{!! asset('images/avtar.png') !!}" style="height: 300px">
                  @endif
                @endif
              </div>
              <div class="surveyor-desc-box-left">
                <div class="surveyor-desc">
                  <h1 style="color: #00b29d">Meet {!! $user->name !!}</h1>
                  <p>{!! $user->notes !!}</p>
                  @if($user->message)
                  <p style="color: #00b29d; font-family:poppins">A message from {!! $user->name !!}:</p>

                  <p>{!! $user->message !!}</p>
                  @endif
                  <p><img src="{!! asset($user->signature) !!}" style="height: 100px;"></p>
                  <p><b>{!! $user->name !!}</b></p>
                  <p>{!! $user->qualifications !!}</p>
                </div>
              </div>
              
            </div>
          @endif
        @endforeach
      @endforeach

      @foreach($jobs as $job)
        @foreach($job->users as $user)
        @php
          $job_user = App\Job_User::where([['job_id','=',$job->id],['user_id','=',$user->id]])->first();
        @endphp
        
          @if($user->role != 3 && $job_user->user_role == "Administrator")
            <div class="surveyor-desc-box flex-row-reverse">
              <div class="surveyor-desc-box-right">
                @if($user->picture == null)
                  <img class="meet-img" src="{!! asset('images/avtar.png') !!}" style="height: 300px"> 
                @else
                  @if(file_exists($user->picture))
                    <img class="meet-img" src="{!! asset($user->picture) !!}" style="height: 300px">
                  @else
                    <img class="meet-img" src="{!! asset('images/avtar.png') !!}" style="height: 300px">
                  @endif
                @endif
              </div>
              <div class="surveyor-desc-box-left">
                <div class="surveyor-desc">
                  <h1 style="color: #00b29d">Meet {!! $user->name !!}</h1>
                  <p>{!! $user->notes !!}</p>
                  @if($user->message)
                  <p style="color: #00b29d; font-family:poppins">A message from {!! $user->name !!}:</p>

                  <p>{!! $user->message !!}</p>
                  @endif
                  <p><img src="{!! asset($user->signature) !!}" style="height: 100px;"></p>
                  <p><b>{!! $user->name !!}</b></p>
                  <p>{!! $user->qualifications !!}</p>
                </div>
              </div>
              
            </div>
          @endif
        @endforeach
      @endforeach
    </div>
</div>
@endsection

