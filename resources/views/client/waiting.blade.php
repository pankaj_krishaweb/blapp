@extends('layouts.frame')

@section('content')
<div class="container-fluid">
    <div class="container text-center" style="padding: 100px 0px; min-height: 60vh;">
          <img src="{{ asset('images/loading.gif') }}" style="height: 100px;">
          @if ($error == 'No Jobs')
             <h2 class="">There are no jobs assigned for this user ...</h2>
          @else
             <h2 class="">Your Account is being created ...</h2>
          @endif
        </div>
    </div>
@endsection
@section('script')

<script>
    
$(document).ready(function() {

    $(".tasks").click(function() {

        var taskid = $(this).attr('data-key');
        
        if ($(this).is(":checked")) {
           var status="1";
        } else {
           var status="0";
        
        }

          $.ajax({
        url: '/tasks/checked',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {taskid: taskid, status: status},
        success: function(data){
        console.log(data);
        location.reload()
            
        },
    })  
    });

});
</script>
@endsection

