<?php

namespace App\Src\Docs\Appointment;
use Illuminate\Support\Facades\Storage;

class  LetterOfAppointmentBO
{
	public $documentFolder="Appointment Letters";
	public $documentName="Letter of Appointment BO";

    	public function create(\App\Job $job){
	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both', 'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(0)));
		$section = $phpWord->addSection();
		$section->addTextBreak();
		$section->addText(ucwords($job->bo->full_name), ['bold' => true],['alignment' => 'center']);
		$section->addText('of', ['bold' => true],['alignment' => 'center']);
		$section->addText(ucwords($job->bo->contact_address), ['bold' => true],['alignment' => 'center']);
		$section->addTextBreak();
		$section->addText($job->bo->surveyor_full_information, ['bold' => true]);		
		$section->addText(date("d/m/Y"), [], [ 'align' => 'right' ]);
		$section->addText('Dear '.$job->bo->surveyor_name);
		$section->addTextBreak();
		$section->addText('Re: '.$job->bo->property_address_proposed_work,['bold' => true]);
		$section->addText('The Party Wall etc. Act 1996	',['bold' => true]);
		$section->addTextBreak();		
		$section->addText('As '.$job->bo->owner_referral.' of the above property '.$job->bo->i_we_referral_lower_case.' hereby authorise you to sign, issue and receive any notices in connection with the Party Wall etc. Act 1996 relating to matters which affect '.$job->bo->my_our_referral.' property.');
		$section->addTextBreak();
		$section->addText('In the event of a dispute arising within the meaning of the Party Wall etc. Act 1996 '.$job->bo->i_we_referral_lower_case.' appoint you, '.$job->bo->surveyor_name.' '.$job->bo->surveyor_qualifications.', to act as '.$job->bo->my_our_referral.' surveyor in accordance with Section 10 of the Party Wall etc. Act 1996.'.$job->bo->i_we_referral_upper_case.' further authorise you to make any necessary appointments under the Party Wall etc. Act 1996 on '.$job->bo->my_our_referral. ' behalf which may be necessary to expedite matters.');
		$section->addTextBreak();
		$section->addText('Yours sincerely, ');
		$section->addTextBreak(3);
		$section->addText('…………………………………………',['bold' => true]);
		$section->addText($job->bo->full_name,['bold' => true]);
		$section->addTextBreak(3);
		$section->addText('…………………………………………',['bold' => true]);
		$section->addText($job->bo->full_name,['bold' => true]);				
		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007'); 
		$objWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 

		//Save it
		//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
		//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$objWriter1 = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
		//$objWriter1->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.html");
//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
