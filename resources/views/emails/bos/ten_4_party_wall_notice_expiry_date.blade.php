<html>
<head>
	<style type="text/css">
		.float-md-left,
		.float-md-right{
			text-align: center;
		}
		h2{
			font-size: 30px;
			line-height: 40px;
			color: #00b29c;
			font-weight: bold;
		}
		@media (min-width: 768px) {
			.title-wrap{
				display: table;
				max-width: 800px;
				width: 100%;
			}
			h2{
				font-size: 47px;
				line-height: 57px;
			}
			.title-wrap .float-md-left,
			.title-wrap .float-md-right{
				display: table-cell;
				vertical-align: middle;
			}
			.title-wrap h2{
				font-size: 47px;
				line-height: 1.5;
			}
			.float-md-left{
				float: left;
				width: calc(100% - 250px);
				text-align: left;
			}
			.float-md-right{
				float: right;
				width: 250px;
				text-align: right;
			}
			.float-md-right img{
				max-width: 100%;
			}
		}
	</style>
	
</head>
<body >
	<div class="text-center title-wrap">
		<div class="float-md-left">
			<h2>We’re reviewing &amp; checking the information now</h2>
		</div>
		<div class="float-md-right">
			<img src="{!! asset('images/login_email.png') !!}" width="200px">
		</div>
	</div>

	<div>

		<p>Dear {{$salutation}},</p>
		<p>Further to our recent discussions, please find attached the 10(4) Party Wall Notices we have served upon the adjoining owners.</p>

		<p>If we don’t have a response by the {!! $ten_4_party_wall_notice_expiry_date !!} we will get a Party Wall
		Surveyor appointed on behalf of the non responsive adjoining owners.</p>

		<p>We will let you know as soon as we have received a response, or alternatively when the 10(4) Party Wall Notice has expired.</p>

		<p>Login to the  <a href="https://app.blsurveyors.com/">Berry Lodge Client Portal</a> now to see the status of your job and access PDF copies of the 10(4) Party Wall Notices for your records.</p>
		<p>Kind Regards, </p>

		<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;" >
		<p><strong>Berry Lodge Surveyors</strong></p>
	</div>
	@include('shard.footer')

</div>
</body>
</html>