<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{User,Role, User_History};
use Illuminate\Support\Facades\{Auth, Mail};

class UserController extends Controller
{   
    /**
     * @return blade view
     */
    public function index($role=null){

        switch ($role) {
            case "admins":
                $users= User::where('role', 9)->get();
                break;
            case "employees":
                $users= User::where('role', 7)->get();
                break;
            case "third-party":
                $users= User::where('role', 5)->get();
                break;
            case "clients":
                 $users= User::where('role',3)->get();
                break;
            case "meditation-clients":
                 $users= User::where('role',1)->get();
                break;
            case "admin-2-user":
                 $users= User::where('role',11)->get();
                break;
            default:
                $users= User::all();
        }
    	return view('admin.users',['users'=>$users]);
    }
   

    public function addUser(Request $request){
        $user = User::find($request->user_id);
        if($user){
            if($request->picture != "undefined")
            {
                $this->validate($request,  [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255',
                    'role'=> 'required|max:255',
                    'picture' => 'nullable|min:500',
                ]);
            }
            else{
                $this->validate($request,  [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255',
                    'role'=> 'required|max:255',
                ]);   
            }
            if ($request->file('signature')) {
                $upload = $request->file('signature');
                $store= $upload->store("public/signature");
                $user->signature = str_replace('public','storage',$store);
            }
            if ($request->file('picture')) {
                $upload = $request->file('picture');
                $store= $upload->store("public/user");
                $user->picture = str_replace('public','storage',$store);
            }
     
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->company = $request->input('company');
            $user->telephone = $request->input('telephone');
            $user->address = $request->input('address');
            $user->qualifications = $request->input('qualifications');            
            $user->role =  $request->input('role');
            $user->notes =  $request->get('notes');
            $user->message =  $request->get('message');
            $user->credentials =  $request->get('credentials');
            $user->chatcolor =  $request->get('chatcolor');
            if (strlen($request->input('password'))> 5) {
               $user->password = bcrypt($request->input('password'));
                $user->secret_key = $request->input('password');

               // $sento = 'eo@blsurveyors.com'; //env('TEST_MAIL');
               //  Mail::send('emails.credentials_update', 
               //      [  'user_name' => $user->name, 'user_password'=>$request->input('password') ], 
               //      function ($message) use($sento){
               //          $message->subject('Berry Lodge Surveyors');
               //          $message->to($sento);  
               //  });
            }
            
            $user->save();

        }
        // new user
        else{ 

            if ($request->file('picture')) {
                $upload = $request->file('picture');
                $store= $upload->store("public/user");    
            }
            else{
                 $store= null;
            }

            if ($request->file('signature')) {
                $upload1 = $request->file('signature');
                $store1= $upload1->store("public/signature");
            }
            else{
                 $store1= null;
            }

        $this->validate($request,  [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'role'=> 'required|max:255',
            'picture' => 'nullable|min:500'
            // 'password' => 'required|min:6|confirmed',
        ]);

            $user=User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'picture' => str_replace('public','storage',$store ),
                'signature' => str_replace('public','storage',$store1 ),
                'role'=> $request->input('role'),
                'notification'=>1,
                'password' => bcrypt($request->input('password')),
                'secret_key' => $request->input('password'),
                'notes'=>$request->get('notes'),
                'message'=>$request->get('message'),
                'credentials'=>$request->get('credentials'),
                'chatcolor'=>$request->get('chatcolor')
            ]);


            // $sento= 'eo@blsurveyors.com'; //env('TEST_MAIL');
            // Mail::send('emails.credentials', 
            //     [  'user_name' => $user->name, 'user_password'=>$request->input('password') ], 
            //     function ($message) use($sento){
            //         $message->subject('Berry Lodge Surveyors');
            //         $message->to($sento);
            //     }
            // );
        }
        
        // update History
        User_History::create([
            'user_id' => Auth::user()->id,
            'action' => 'Created user',
            'description'=> 'created user id: '.$user->id.' - name: '.$user->name.' - email: '.$user->email.' - role: '.$user->role,
        ]);

        return json_encode(['status'=>'good'], true);
    }





    public function deleteUser(Request $request){
        $user = User::find($request->user_id);
        User_History::create([
            'user_id' => Auth::user()->id,
            'action' => 'Deleted user',
            'description'=> 'User deleted id:: '.$user->id.' - name: '.$user->name.' - email: '.$user->email.' - role: '.$user->role,
        ]);
        $user->delete();
        return json_encode(['status'=>'good'], true);
    }


    public function userHistory(Request $request){

        $h= User_History::where('user_id',$request->user_id)->latest()->limit(15)->get();

        return json_encode($h, true);
    }

    public function userEdit(Request $request){


        return json_encode(User::find($request->user_id), true);
    }
    public function notification(Request $request){

        $jobtasks = User::find($request->id);
        $jobtasks->notification= $request->notification;
        $jobtasks->save();

        return  $request->notification;


    }


}
