<html>
<head><style type="text/css">
	.float-md-left,
	.float-md-right{
		text-align: center;
	}
	h2{
		font-size: 30px;
		line-height: 40px;
		color: #00b29c;
		font-weight: bold;
	}
	@media (min-width: 768px) {
		.title-wrap{
			display: table;
			max-width: 800px;
			width: 100%;
		}
		h2{
			font-size: 47px;
			line-height: 57px;
		}
		.title-wrap .float-md-left,
		.title-wrap .float-md-right{
			display: table-cell;
			vertical-align: middle;
		}
		.title-wrap h2{
			font-size: 47px;
			line-height: 1.5;
		}
		.float-md-left{
			float: left;
			width: calc(100% - 250px);
			text-align: left;
		}
		.float-md-right{
			float: right;
			width: 250px;
			text-align: right;
		}
		.float-md-right img{
			width: 100%;
		}
	}
</style>

</head>
<body >
	<div class="text-center title-wrap">
		<div class="float-md-left">
			<h2>We’ve booked the Schedule of Condition Report into our diary!</h2>
		</div>
		<div class="float-md-right">
			<img src="{!! asset('images/as/4.png') !!}" width="200px">
		</div>
	</div>
	<div>
		<p>Dear {{ $salutation }}</p>

		<p>Good News! Berry Lodge Surveyors have progressed your Party Wall Surveying Matter!</p>

		<p>Further to our previous email, we are pleased to confirm that the Schedule of Condition inspection of the adjoining owner’s property has been booked in.</p>

		<p>We will be in touch further once the inspection is complete to confirm if there is any information we require from you post visit.</p>

		<p>Login to the <a href="https://app.blsurveyors.com/" target="_blank"> Berry Lodge Client Portal </a> now to see the status of your Party Wall Surveying job.</p>

		<p>Kind Regards </p>

		<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;">
		<p><strong>Berry Lodge Surveyors</strong></p>
	</div>
	@include('shard.footer')

</div>
</body>
</html>