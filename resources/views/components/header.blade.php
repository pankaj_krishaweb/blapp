<!DOCTYPE html>
<!-- saved from url=(0065)https://www.blsurveyors.com/login/?login=failed&reason=both_empty -->
<html class="page-template-default page page-id-2460  js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths preserve3d not-ie" lang="en-GB" prefix="og: http://ogp.me/ns#"><!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>Login - Berry Lodge</title>

        <!--[if lt IE 9]>
        	<script src="/wp-includes/js/html5/html5shiv.js') }}"></script>
        <![endif]-->
        
        <meta name="viewport" content="width=device-width">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="https://www.blsurveyors.com/xmlrpc.php">
        
        
        
<!-- This site is optimized with the Yoast SEO plugin v7.5.3 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="https://www.blsurveyors.com/login/">
<meta property="og:locale" content="en_GB">
<meta property="og:type" content="article">
<meta property="og:title" content="Login - Berry Lodge">
<meta property="og:url" content="https://www.blsurveyors.com/login/">
<meta property="og:site_name" content="Berry Lodge">
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="Login - Berry Lodge">
<!-- / Yoast SEO plugin. -->

<link rel="dns-prefetch" href="https://ajax.googleapis.com/">
<link rel="dns-prefetch" href="https://s.w.org/">
<link rel="alternate" type="application/rss+xml" title="Berry Lodge » Feed" href="https://www.blsurveyors.com/feed/">
<link rel="alternate" type="application/rss+xml" title="Berry Lodge » Comments Feed" href="https://www.blsurveyors.com/comments/feed/">
		<script async="" src="{{ asset('bl_site/impl-1_29.js') }}" nonce="null"></script>
		<script async="" src="{{ asset('bl_site/loader.js') }}"></script>
		<script async="" src="{{ asset('bl_site/analytics.js') }}"></script>
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png') }}","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.blsurveyors.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.7"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		{{-- <script src="{{ asset('bl_site/wp-emoji-release.min.js') }}" type="text/javascript" defer=""></script> --}}
		<style type="text/css">
			img.wp-smiley,
			img.emoji {
				display: inline !important;
				border: none !important;
				box-shadow: none !important;
				height: 1em !important;
				width: 1em !important;
				margin: 0 .07em !important;
				vertical-align: -0.1em !important;
				background: none !important;
				padding: 0 !important;
			}
		</style>
<link rel="stylesheet" id="avatar-manager.css" href="{{ asset('bl_site/avatar-manager.min.css') }}" type="text.css" media="all">
<link rel="stylesheet" id="contact-form-7.css" href="{{ asset('bl_site/styles.css') }}" type="text.css" media="all">
<link rel="stylesheet" id="imagelinks_imagelinks.css" href="{{ asset('bl_site/imagelinks.css') }}" type="text.css'" media="all">
<link rel="stylesheet" id="imagelinks_imagelinks_wp.css" href="{{ asset('bl_site/imagelinks.wp.css') }}" type="text.css" media="all">
{{-- <link rel="stylesheet" id="boilerplate-reset.css" href="{{ asset('bl_site/normalize.min.css') }}" type="text.css" media="all"> --}}
<link rel="stylesheet" id="boilerplate-helpers.css" href="{{ asset('bl_site/html5-main.css') }}" type="text.css" media="all">
<link rel="stylesheet" id="colorbox.css" href="{{ asset('bl_site/colorbox.css') }}" type="text.css'" media="all">
<link rel="stylesheet" id="sidr.css" href="{{ asset('bl_site/jquery.sidr.dark.css') }}" type="text.css" media="all">
<link rel="stylesheet" id="dropkick.css" href="{{ asset('bl_site/dropkick.css') }}" type="text.css" media="all">
<link rel="stylesheet" id="inherit.css" href="{{ asset('bl_site/inherit.css') }}" type="text.css" media="all">
<link rel="stylesheet" id="theme-style.css" href="{{ asset('bl_site/style.css') }}" type="text.css" media="all">
<link rel="stylesheet" id="contact-form.css" href="{{ asset('bl_site/section.contact-form.css') }}" type="text.css" media="all">
<link rel="stylesheet" id="wordpress-popular-posts.css" href="{{ asset('bl_site/wpp.css') }}" type="text.css" media="all">

<script type="text/javascript" src="{{ asset('bl_site/jquery.min.js') }}"></script>

<!-- <script src="//code.jquery.com/jquery-3.3.1.min.js"></script> -->
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js"></script>




<script type="text/javascript" src="{{ asset('bl_site/avatar-manager.min.js') }}"></script>
<link rel="https://api.w.org/" href="https://www.blsurveyors.com/wp-json/">
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.blsurveyors.com/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.blsurveyors.com/wp-includes/wlwmanifest.xml"> 
<meta name="generator" content="WordPress 4.9.7">
<link rel="shortlink" href="https://www.blsurveyors.com/?p=2460">
<link rel="alternate" type="application/json+oembed" href="https://www.blsurveyors.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.blsurveyors.com%2Flogin%2F">
<link rel="alternate" type="text/xml+oembed" href="https://www.blsurveyors.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.blsurveyors.com%2Flogin%2F&amp;format=xml">
<link href="https://fonts.googleapis.com/css?family=Poppins:500,600,700" rel="stylesheet">
<script type="text/javascript">
	window._se_plugin_version = '8.1.9';
</script>
				
				<link rel="icon" href="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-32x32.jpg" sizes="32x32">
<link rel="icon" href="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-192x192.jpg" sizes="192x192">
<link rel="apple-touch-icon-precomposed" href="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-180x180.jpg">
<meta name="msapplication-TileImage" content="https://www.blsurveyors.com/wp-content/uploads/2016/07/cropped-Berry-Lodge-Surveyors-3-270x270.jpg">

       


    </head>
<body class="page-template-default page page-id-2460 desktop-device">

<!--[if lt IE 9]>
    <div class="outdated">You are using an <strong>outdated</strong> browser, please <a href="http://whatbrowser.org/" class="popup" title="Click to upgrade your browser">upgrade your browser</a> to enjoy all the available features of this site.</div>
<![endif]-->

<!-- Logo/Main Nav -->
<header class="primary">
	<div class="full-wrapper"  style="max-width: 1060px !important; font-family: 'Poppins', sans-serif !important; font-weight: 500;">
    	<div class="header-top">
            <a class="primary-logo" href="https://www.blsurveyors.com/" title="Berry Lodge">
            <img src="{{ asset('bl_site/main-logox2.png') }}" alt="Berry Lodge logo">
            </a>
            
            <div class="top-wrapper">
            	<form role="search" method="get" class="search-form tablet tablet-large desktop" action="https://www.blsurveyors.com/">
    <div class="inner">
    <input type="search" class="search-field" placeholder="Search..." value="" name="s" title="Search...">
    </div>
    <input type="submit" class="search-submit" value="">
</form>                <div class="contact-number tablet tablet-large desktop">
                    <div class="title" style="font-family: 'Poppins', sans-serif; font-weight: 500; word-spacing: 1px !important;">Call us today on</div>
                    <div class="number" style="font-family: 'Poppins', sans-serif; font-weight: 700; word-spacing: 3.2px !important; font-size: 1.34em !important;">020 7935 2502</div>
                </div>
            </div>
             
	         @if (!Auth::guest())  
	        	<a class="members-login-btn" href="{{ url('/logout') }}"
	                    onclick="event.preventDefault();
	                             document.getElementById('logout-form').submit();">
	                    <span>Logout</span>
	                   <i class="fa fa-unlock" style="font-size:24px; padding: 5px;"></i>
	                </a>

	                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
	                    {{ csrf_field() }}
	                </form> 

	       @endif
		
			
    	</div><!-- .header-top -->
    </div><!-- .full-wrapper -->

    <nav class="primary tablet tablet-large desktop">
    	<div class="full-wrapper" id="main-navigation">
    		<div class="menu-primary-menu-container" style="font-family: 'Poppins', sans-serif !important; font-weight: 500;"><ul id="menu-primary-menu" class="menu"><li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-23"><a href="https://www.blsurveyors.com/about-us/">About</a>
<ul class="sub-menu"><div class="full-wrapper">
	<li id="menu-item-1495" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1495"><a target="_blank" href="https://www.blsurveyors.com/about-us/clients/">Our Clients</a></li>
	<li id="menu-item-182" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-182"><a title="Our Testimonials" target="_blank" href="https://www.blsurveyors.com/testimonial/">Testimonials</a></li>
	<li id="menu-item-156" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-156"><a target="_blank" href="https://www.blsurveyors.com/podcast/">Our Podcasts</a></li>
	<li id="menu-item-7688" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-7688"><a target="_blank" href="https://www.blsurveyors.com/services/careers/">Careers</a></li>
</div></ul>
</li>
<li id="menu-item-1617" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1617"><a href="https://www.blsurveyors.com/projects/">Projects</a></li>
<li id="menu-item-24" class="mega-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-24"><a href="https://www.blsurveyors.com/party-wall-surveying/">Surveying Services</a>
<ul class="sub-menu" ><div class="full-wrapper"   >
	<li id="menu-item-2894" class="bold menu-item menu-item-type-post_type menu-item-object-services menu-item-has-children menu-item-2894" ><a style="font-family: 'Poppins', sans-serif !important; font-weight: 600; font-size: 14px !important;" target="_blank" href="https://www.blsurveyors.com/services/party-wall/">Party Wall Surveying</a>
	<ul class="sub-menu" style="padding-left: 0px;"><div class="full-wrapper">



		<li id="menu-item-2903" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2903"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/property-surveying/party-wall-surveyors/">What is a Party Wall Surveyor</a></li>
		<li id="menu-item-2902" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2902"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/property-surveying/roles/">Party Wall Surveyor Types</a></li>
		<li id="menu-item-2901" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2901"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/property-surveying/party-wall-notices/">Party Wall Notices</a></li>
		<li id="menu-item-2897" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2897"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/property-surveying/agreement/">Party Wall Award/Agreement</a></li>
		<li id="menu-item-7289" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-7289"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/property-surveying/basement-extension-guide/">Basement Extension Guide</a></li>
		<li id="menu-item-7412" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-7412"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/loft-conversion-guide/">Loft Conversion Guide</a></li>
		<li id="menu-item-3794" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3794"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/property-surveying/free-advice/">Free Party Wall Advice</a></li>
		<li id="menu-item-2900" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2900"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/property-surveying/notice-creator/">Party Wall Notice Creator</a></li>
		<li id="menu-item-3122" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3122"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/property-surveying/party-wall-faq/">Party Wall FAQ</a></li>
		<li id="menu-item-2899" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2899"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/property-surveying/fixed-costs/">Party Wall Fixed Costs</a></li>
		<li id="menu-item-2898" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2898"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/property-surveying/fee-quote/">Party Wall Fee Quote</a></li>






		{{-- <li id="menu-item-2903" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2903">
			<a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;"  target="_blank" href="https://www.blsurveyors.com/services/party-wall/surveyors/">Party Wall Surveyor Roles</a>
		</li>
		<li id="menu-item-2897" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2897">
			<a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;"  target="_blank" href="https://www.blsurveyors.com/services/party-wall/agreement/">Party Wall Award/Agreement</a>
		</li>
		<li id="menu-item-2902" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2902">
			<a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;"  target="_blank" href="https://www.blsurveyors.com/services/party-wall/roles/">Party Wall Surveyor Types</a>
		</li>
		<li id="menu-item-2901" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2901">
			<a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;"  target="_blank" href="https://www.blsurveyors.com/services/party-wall/party-wall-notices/">Party Wall Notices</a>
		</li>
		<li id="menu-item-2900" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2900">
			<a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;"  target="_blank" href="https://www.blsurveyors.com/services/party-wall/notice-creator/">Party Wall Notice Creator</a>
		</li>
		<li id="menu-item-3794" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3794">
			<a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;"  target="_blank" href="https://www.blsurveyors.com/services/party-wall/free-advice/">Free Party Wall Advice</a>
		</li>
		<li id="menu-item-3122" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3122">
			<a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;"  target="_blank" href="https://www.blsurveyors.com/services/party-wall/party-wall-faq/">Party Wall FAQ</a>
		</li>
		<li id="menu-item-2899" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2899">
			<a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;"  target="_blank" href="https://www.blsurveyors.com/services/party-wall/fixed-costs/">Party Wall Fixed Costs</a>
		</li>
		<li id="menu-item-2898" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2898">
			<a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;"  target="_blank" href="https://www.blsurveyors.com/services/party-wall/fee-quote/">Party Wall Fee Quote</a>
		</li> --}}
	</div></ul>
</li>
	<li id="menu-item-2895" class="bold menu-item menu-item-type-post_type menu-item-object-services menu-item-has-children menu-item-2895"><a style="font-family: 'Poppins', sans-serif !important; font-weight: 600; font-size: 14px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/">Building Surveying</a>
	<ul class="sub-menu" style="padding-left: 0px;"><div class="full-wrapper">

		<li id="menu-item-2910" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2910"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/schedule-condition-report/">Schedule of Condition</a></li>
		<li id="menu-item-2911" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2911"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/snagging-list-report/">Property Snagging List</a></li>
		<li id="menu-item-2904" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2904"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/boundary-dispute/">Boundary Dispute</a></li>
		<li id="menu-item-5154" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-5154"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/access-licence/">Access Licence</a></li>
		<li id="menu-item-2905" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2905"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/defect-analysis-report/">Defect Analysis Report</a></li>
		<li id="menu-item-2909" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2909"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/rics-homebuyer-report/">RICS Homebuyer Report</a></li>
		<li id="menu-item-2906" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2906"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/full-building-surveys/">Full Building Surveys</a></li>
		<li id="menu-item-2908" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2908"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/property-mediation/">Property Mediation</a></li>
		<li id="menu-item-2907" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2907"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/licence-to-alter/">Licence to Alter Guide</a></li>
		<li id="menu-item-3859" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3859"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/the-property-doctor/">The Property Doctor</a></li>
		<li id="menu-item-3022" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3022"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/surveying-fee-quote/">Surveying Fee Quote</a></li>

		{{-- <li id="menu-item-2910" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2910"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/schedule-condition-report/">Schedule of Condition</a></li>
		<li id="menu-item-2904" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2904"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/boundary-dispute/">Boundary Dispute</a></li>
		<li id="menu-item-2909" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2909"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/rics-homebuyer-report/">RICS Homebuyer Report</a></li>
		<li id="menu-item-2906" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2906"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/full-building-surveys/">Full Building Surveys</a></li>
		<li id="menu-item-2905" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2905"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/defect-analysis-report/">Defect Analysis Report</a></li>
		<li id="menu-item-2911" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2911"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/snagging-list-report/">Snagging List</a></li>
		<li id="menu-item-2907" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2907"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/licence-to-alter/">Licence to Alter</a></li>
		<li id="menu-item-2908" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2908"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/property-mediation/">Property Mediation</a></li>
		<li id="menu-item-3859" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3859"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/the-property-doctor/">The Property Doctor</a></li>
		<li id="menu-item-3022" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3022"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/surveying/surveying-fee-quote/">Surveying Fee Quote</a></li> --}}
	</div></ul>
</li>
	<li id="menu-item-2896" class="bold menu-item menu-item-type-post_type menu-item-object-services menu-item-has-children menu-item-2896"><a style="font-family: 'Poppins', sans-serif !important; font-weight: 600; font-size: 14px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/">Property Valuation</a>
	<ul class="sub-menu" style="padding-left: 0px;"><div class="full-wrapper">
		{{-- <li id="menu-item-3009" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3009"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/market-valuation-2/">Market Valuation</a></li>
		<li id="menu-item-2912" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2912"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/fixed-valuation-costs/">Insurance Valuation</a></li>
		<li id="menu-item-2914" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2914"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/market-valuation/">Retrospective Valuation</a></li>
		<li id="menu-item-3017" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3017"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/leasehold-extension-valuation/">Leasehold Extension</a></li>
		<li id="menu-item-2913" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2913"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/leasehold-extensions-explained/">Leasehold Extensions FAQ</a></li>
		<li id="menu-item-3014" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3014"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/matrimonial-valuation/">Matrimonial Valuation</a></li>
		<li id="menu-item-3013" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3013"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/probate-valuation/">Probate Valuation</a></li>
		<li id="menu-item-3023" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3023"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/fee-quote/">Valuation Fee Quote</a></li> --}}
		<li id="menu-item-3009" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3009"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/market-valuation-2/">Market Valuation</a></li>
		<li id="menu-item-2912" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2912"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/insurance-valuation/">Insurance Valuation</a></li>
		<li id="menu-item-2914" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2914"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/retrospective-valuation-2/">Retrospective Valuation</a></li>
		<li id="menu-item-3014" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3014"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/matrimonial-valuation/">Matrimonial Valuation</a></li>
		<li id="menu-item-3013" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3013"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/probate-valuation/">Probate Valuation</a></li>
		<li id="menu-item-3017" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3017"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/leasehold-extension-valuation/">Leasehold Extension</a></li>
		<li id="menu-item-2913" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2913"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/leasehold-extensions-explained/">Leasehold Extensions FAQ</a></li>
		<li id="menu-item-8357" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-8357"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/property-valuation-explained/">Property Valuation Explained</a></li>
		<li id="menu-item-8359" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-8359"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation-faq/">Property Valuation FAQ</a></li>
		<li id="menu-item-8361" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-8361"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/property-valuation-fixed-costs/">Property Valuation Fixed Costs</a></li>

		<li id="menu-item-3023" class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3023"><a style="font-family: 'Poppins', sans-serif !important; font-size: 13px !important;" target="_blank" href="https://www.blsurveyors.com/services/valuation/fee-quote/">Valuation Fee Quote</a></li>
	</div></ul>
</li>
</div></ul>
</li>
<li id="menu-item-191" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-191"><a href="https://www.blsurveyors.com/video-gallery/">Videos</a></li>
<li id="menu-item-27" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a href="https://www.blsurveyors.com/blog/">Blog</a></li>
<li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://www.blsurveyors.com/contact-us/">Contact</a></li>
</ul></div>        </div>
    </nav><!-- .primary -->
    
    <nav class="mobile" id="mobile-menu-container">
		<div class="menu-primary-menu-container"><ul id="menu-primary-menu-1" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-23"><a href="https://www.blsurveyors.com/about-us/">About</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1495"><a href="https://www.blsurveyors.com/about-us/clients/">Our Clients</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-182"><a title="Our Testimonials" href="https://www.blsurveyors.com/testimonial/">Testimonials</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-156"><a href="https://www.blsurveyors.com/podcast/">Our Podcasts</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1617"><a href="https://www.blsurveyors.com/projects/">Projects</a></li>
<li class="mega-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-24"><a href="https://www.blsurveyors.com/party-wall-surveying/">Surveying Services</a>
<ul class="sub-menu">
	<li class="bold menu-item menu-item-type-post_type menu-item-object-services menu-item-has-children menu-item-2894"><a href="https://www.blsurveyors.com/services/party-wall/">Party Wall</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2903"><a href="https://www.blsurveyors.com/services/party-wall/surveyors/">Party Wall Surveyor Roles</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2897"><a href="https://www.blsurveyors.com/services/party-wall/agreement/">Party Wall Award/Agreement</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2902"><a href="https://www.blsurveyors.com/services/party-wall/roles/">Party Wall Surveyor Types</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2901"><a href="https://www.blsurveyors.com/services/party-wall/party-wall-notices/">Party Wall Notices</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2900"><a href="https://www.blsurveyors.com/services/party-wall/notice-creator/">Party Wall Notice Creator</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3794"><a href="https://www.blsurveyors.com/services/party-wall/free-advice/">Free Party Wall Advice</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3122"><a href="https://www.blsurveyors.com/services/party-wall/party-wall-faq/">Party Wall FAQ</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2899"><a href="https://www.blsurveyors.com/services/party-wall/fixed-costs/">Party Wall Fixed Costs</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2898"><a href="https://www.blsurveyors.com/services/party-wall/fee-quote/">Party Wall Fee Quote</a></li>
	</ul>
</li>
	<li class="bold menu-item menu-item-type-post_type menu-item-object-services menu-item-has-children menu-item-2895"><a href="https://www.blsurveyors.com/services/surveying/">Surveying</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2910"><a href="https://www.blsurveyors.com/services/surveying/schedule-condition-report/">Schedule of Condition</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2904"><a href="https://www.blsurveyors.com/services/surveying/boundary-dispute/">Boundary Dispute</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2909"><a href="https://www.blsurveyors.com/services/surveying/rics-homebuyer-report/">RICS Homebuyer Report</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2906"><a href="https://www.blsurveyors.com/services/surveying/full-building-surveys/">Full Building Surveys</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2905"><a href="https://www.blsurveyors.com/services/surveying/defect-analysis-report/">Defect Analysis Report</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2911"><a href="https://www.blsurveyors.com/services/surveying/snagging-list-report/">Snagging List</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2907"><a href="https://www.blsurveyors.com/services/surveying/licence-to-alter/">Licence to Alter</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2908"><a href="https://www.blsurveyors.com/services/surveying/property-mediation/">Property Mediation</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3859"><a href="https://www.blsurveyors.com/services/the-property-doctor/">The Property Doctor</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3022"><a href="https://www.blsurveyors.com/services/surveying/surveying-fee-quote/">Surveying Fee Quote</a></li>
	</ul>
</li>
	<li class="bold menu-item menu-item-type-post_type menu-item-object-services menu-item-has-children menu-item-2896"><a href="https://www.blsurveyors.com/services/valuation/">Valuation</a>
	<ul class="sub-menu">
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3009"><a href="https://www.blsurveyors.com/services/valuation/market-valuation-2/">Market Valuation</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2912"><a href="https://www.blsurveyors.com/services/valuation/fixed-valuation-costs/">Insurance Valuation</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2914"><a href="https://www.blsurveyors.com/services/valuation/market-valuation/">Retrospective Valuation</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3017"><a href="https://www.blsurveyors.com/services/valuation/leasehold-extension-valuation/">Leasehold Extension</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-2913"><a href="https://www.blsurveyors.com/services/valuation/leasehold-extensions-explained/">Leasehold Extensions FAQ</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3014"><a href="https://www.blsurveyors.com/services/valuation/matrimonial-valuation/">Matrimonial Valuation</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3013"><a href="https://www.blsurveyors.com/services/valuation/probate-valuation/">Probate Valuation</a></li>
		<li class="menu-item menu-item-type-post_type menu-item-object-services menu-item-3023"><a href="https://www.blsurveyors.com/services/valuation/fee-quote/">Valuation Fee Quote</a></li>
	</ul>
</li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-191"><a href="https://www.blsurveyors.com/video-gallery/">Videos</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a href="https://www.blsurveyors.com/blog/">Blog</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://www.blsurveyors.com/contact-us/">Contact</a></li>
</ul></div>    </nav>
</header>