<html>
<head>
	<style type="text/css">
		.float-md-left,
		.float-md-right{
			text-align: center;
		}
		h2{
			font-size: 30px;
			line-height: 40px;
			color: #00b29c;
			font-weight: bold;
		}
		@media (min-width: 768px) {
			.title-wrap{
				display: table;
				max-width: 800px;
				width: 100%;
			}
			h2{
				font-size: 47px;
				line-height: 57px;
			}
			.title-wrap .float-md-left,
			.title-wrap .float-md-right{
				display: table-cell;
				vertical-align: middle;
			}
			.title-wrap h2{
				font-size: 47px;
				line-height: 1.5;
			}
			.float-md-left{
				float: left;
				width: calc(100% - 250px);
				text-align: left;
			}
			.float-md-right{
				float: right;
				width: 250px;
				text-align: right;
			}
			.float-md-right img{
				max-width: 100%;
			}
		}
	</style>
	
</head>
<body >
	<div class="text-center title-wrap">
		<div class="float-md-left">
			<h2>We’ve served the Party Wall Award! Log in to view now</h2>
		</div>
		<div class="float-md-right">
			<img src="{!! asset('images/bos/11.png') !!}" width="200px">
		</div>
	</div>

	<div>

		<p>Dear {{$salutation}},</p>

		<p>Good News! We are pleased to confirm that we’ve progressed your Party Wall Surveying Matter!</p>

		<p>The Party Wall Award has today been served! Please login to the <a href="https://app.blsurveyors.com/">Berry Lodge Client Portal</a> now to access the Party Wall Award.</p>
		<p>The Party Wall Award is a legally binding document and not only governs the specifics of the notifiable Party Wall works, it also legally protects you in the event of damage alleged by the adjoining owners.</p>

		<p>In accordance with Section 10(17) of the Party Wall etc Act 1996 you have a legal right to appeal the Party Wall Award within 14 days of its date, however to the best of our knowledge there is nothing within the Party Wall Award that should cause you to do so.</p>

		<p>Should you wish to discuss anything to do with this matter please do not hesitate to contact your Party Wall Surveyor directly and he will be happy to discuss.</p>

		<p>Kind Regards, </p>

		<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;">
		<p><strong>Berry Lodge Surveyors</strong></p>
	</div>
	@include('shard.footer')

</div>
</body>
</html>