<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSurveyingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveying', function (Blueprint $table) {
            $table->string('date_of_confirmation_letter');
            $table->string('address_of_boundary_determination');
            $table->string('boundary_determination_address');
            $table->string('type_of_property');
            $table->string('age_of_property');
            $table->string('address_of_adjoining_property');
            $table->string('adjoining_property');
            $table->string('name_of_legal_owner');
            $table->string('boundary_being_determined');
            $table->string('direction_of_boundary');
            $table->string('plane_of_boundary');
            $table->string('inspection_weather');
            $table->string('inspection_humidity');
            $table->string('local_authority');
            $table->string('historic_maps_were_present');
            $table->string('planning_records_of_assistance');
            $table->string('date_of_report');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveying', function (Blueprint $table) {
            //
        });
    }
}
