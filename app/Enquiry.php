<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Enquiry extends Model
{
    protected $table = "enquiries";

    public function getCreatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value)); 
    }
    public function getUpdatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value));   
    }

   
}
