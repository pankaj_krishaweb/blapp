<?php

namespace App\Src\Docs\JobInformation;
use Illuminate\Support\Facades\Storage;

class  JobInfo
{
	public $documentFolder="Job information/Job Sketch";
	public $drawingFolder="Drawings";
	public $documentName="Job Info";
	public $landRegistyFolder="Land Registry Details";
	public $scannedFolder="Scanned File";

    	public function create(\App\Job $job){

	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory  
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
	    	$drawingFolder = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->drawingFolder}/");
	    	$landRegistyFolder=Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->landRegistyFolder}/");
	    	$scannedFolder=Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->scannedFolder}/");


	    	
	    	\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);    			
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection(['orientation'=>'landscape']);
		$tableStyle = array(
		    'borderColor' => '006699',
		    'borderSize'  => 6,
		    'cellMargin'  => 10,
		    'width' =>120
		);
	
		$phpWord->addTableStyle('myTable', $tableStyle);
		$table = $section->addTable('myTable');
		$table->addRow();
		$table->addCell(6900)->addText('Job Number:', ['bold'=>true]);
		$table->addCell(6900)->addText('BLSN'.$job->id, ['bold'=>true]);
		$table->addRow();
		$table->addCell()->addText('Invoice No:', ['bold'=>true]);
		$table->addCell()->addText($job->invoice_no, ['bold'=>true]);
		$table->addRow();
		$table->addCell()->addText('Surveyor dealing with day to day:', ['bold'=>true]);
		$table->addCell()->addText($job->surveyor_dealing_with_day_to_day, ['bold'=>true]);
		$table->addRow();
		$table->addCell()->addText('Surveyor who had first contact:', ['bold'=>true]);
		$table->addCell()->addText($job->surveyor_who_had_first_contact_with_owner, ['bold'=>true]);
		
		$section->addTextBreak(1);

		$phpWord->addTableStyle('table2', $tableStyle);
 		$table = $section->addTable('table2');
	
		$table->addRow(500);
		$table->addCell(4600)->addText('Building Owner:', ['bold'=>true]);
		$table->addCell(4600)->addText('Adjoining Owner 1:', ['bold'=>true]);
		$table->addCell(4600)->addText('Adjoining Owner 2:', ['bold'=>true]);
		$table->addCell(4600)->addText('Adjoining Owner 3:', ['bold'=>true]);
		$table->addCell(4600)->addText('Adjoining Owner 4:', ['bold'=>true]);
		$table->addCell(4600)->addText('Adjoining Owner 5:', ['bold'=>true]);
		$table->addCell(4600)->addText('Adjoining Owner 6:', ['bold'=>true]);
		$table->addCell(4600)->addText('Adjoining Owner 7:', ['bold'=>true]);
		$table->addCell(4600)->addText('Adjoining Owner 8:', ['bold'=>true]);
		$table->addCell(4600)->addText('Adjoining Owner 9:', ['bold'=>true]);
		$table->addCell(4600)->addText('Adjoining Owner 10:', ['bold'=>true]);
		$table->addRow();
		$table->addCell()->addText($job->bo->full_name);
		$table->addCell()->addText($job->ao->full_names);
		$table->addCell()->addText($job->ao2->full_names);
		$table->addCell()->addText($job->ao3->full_names);
		$table->addCell()->addText($job->ao4->full_names);
		$table->addCell()->addText($job->ao5->full_names);
		$table->addCell()->addText($job->ao6->full_names);
		$table->addCell()->addText($job->ao7->full_names);
		$table->addCell()->addText($job->ao8->full_names);
		$table->addCell()->addText($job->ao9->full_names);
		$table->addCell()->addText($job->ao10->full_names);
		$table->addRow();
		$table->addCell()->addText('Contact:', ['bold'=>true]);
		$table->addCell()->addText('Contact:', ['bold'=>true]);
		$table->addCell()->addText('Contact:', ['bold'=>true]);
		$table->addCell()->addText('Contact:', ['bold'=>true]);
		$table->addCell()->addText('Contact:', ['bold'=>true]);
		$table->addCell()->addText('Contact:', ['bold'=>true]);
		$table->addCell()->addText('Contact:', ['bold'=>true]);
		$table->addCell()->addText('Contact:', ['bold'=>true]);
		$table->addCell()->addText('Contact:', ['bold'=>true]);
		$table->addCell()->addText('Contact:', ['bold'=>true]);
		$table->addCell()->addText('Contact:', ['bold'=>true]);
		$table->addRow();
		$table->addCell()->addText($job->bo->contact_details_telephone_numbers_email);
		$table->addCell()->addText($job->ao->contact_details);
		$table->addCell()->addText($job->ao2->contact_details);
		$table->addCell()->addText($job->ao3->contact_details);
		$table->addCell()->addText($job->ao4->contact_details);
		$table->addCell()->addText($job->ao5->contact_details);
		$table->addCell()->addText($job->ao6->contact_details);
		$table->addCell()->addText($job->ao7->contact_details);
		$table->addCell()->addText($job->ao8->contact_details);
		$table->addCell()->addText($job->ao9->contact_details);
		$table->addCell()->addText($job->ao10->contact_details);
		$table->addRow();
		$table->addCell()->addText('Surveyor:', ['bold'=>true]);
		$table->addCell()->addText('Surveyor:', ['bold'=>true]);
		$table->addCell()->addText('Surveyor:', ['bold'=>true]);
		$table->addCell()->addText('Surveyor:', ['bold'=>true]);
		$table->addCell()->addText('Surveyor:', ['bold'=>true]);
		$table->addCell()->addText('Surveyor:', ['bold'=>true]);
		$table->addCell()->addText('Surveyor:', ['bold'=>true]);
		$table->addCell()->addText('Surveyor:', ['bold'=>true]);
		$table->addCell()->addText('Surveyor:', ['bold'=>true]);
		$table->addCell()->addText('Surveyor:', ['bold'=>true]);
		$table->addCell()->addText('Surveyor:', ['bold'=>true]);
		$table->addRow();
		$table->addCell()->addText($job->bo->surveyor_full_information);
		$table->addCell()->addText($job->ao->surveyor_full_information);
		$table->addCell()->addText($job->ao2->surveyor_full_information);
		$table->addCell()->addText($job->ao3->surveyor_full_information);
		$table->addCell()->addText($job->ao4->surveyor_full_information);
		$table->addCell()->addText($job->ao5->surveyor_full_information);
		$table->addCell()->addText($job->ao6->surveyor_full_information);
		$table->addCell()->addText($job->ao7->surveyor_full_information);
		$table->addCell()->addText($job->ao8->surveyor_full_information);
		$table->addCell()->addText($job->ao9->surveyor_full_information);
		$table->addCell()->addText($job->ao10->surveyor_full_information);
		$table->addRow();
		$table->addCell()->addText('Building Address:', ['bold'=>true]);
		$table->addCell()->addText('Building Address:', ['bold'=>true]);
		$table->addCell()->addText('Building Address:', ['bold'=>true]);
		$table->addCell()->addText('Building Address:', ['bold'=>true]);
		$table->addCell()->addText('Building Address:', ['bold'=>true]);
		$table->addCell()->addText('Building Address:', ['bold'=>true]);
		$table->addCell()->addText('Building Address:', ['bold'=>true]);
		$table->addCell()->addText('Building Address:', ['bold'=>true]);
		$table->addCell()->addText('Building Address:', ['bold'=>true]);
		$table->addCell()->addText('Building Address:', ['bold'=>true]);
		$table->addCell()->addText('Building Address:', ['bold'=>true]);
		$table->addRow();
		$table->addCell()->addText($job->bo->property_address_proposed_work);
		$table->addCell()->addText($job->ao->property_address_adjoining);
		$table->addCell()->addText($job->ao2->property_address_adjoining);
		$table->addCell()->addText($job->ao3->property_address_adjoining);
		$table->addCell()->addText($job->ao4->property_address_adjoining);
		$table->addCell()->addText($job->ao5->property_address_adjoining);
		$table->addCell()->addText($job->ao6->property_address_adjoining);
		$table->addCell()->addText($job->ao7->property_address_adjoining);
		$table->addCell()->addText($job->ao8->property_address_adjoining);
		$table->addCell()->addText($job->ao9->property_address_adjoining);
		$table->addCell()->addText($job->ao10->property_address_adjoining);
		$table->addRow();
		$table->addCell()->addText('Contact Address:', ['bold'=>true]);
		$table->addCell()->addText('Contact Address:', ['bold'=>true]);
		$table->addCell()->addText('Contact Address:', ['bold'=>true]);
		$table->addCell()->addText('Contact Address:', ['bold'=>true]);
		$table->addCell()->addText('Contact Address:', ['bold'=>true]);
		$table->addCell()->addText('Contact Address:', ['bold'=>true]);
		$table->addCell()->addText('Contact Address:', ['bold'=>true]);
		$table->addCell()->addText('Contact Address:', ['bold'=>true]);
		$table->addCell()->addText('Contact Address:', ['bold'=>true]);
		$table->addCell()->addText('Contact Address:', ['bold'=>true]);
		$table->addCell()->addText('Contact Address:', ['bold'=>true]);
		$table->addRow();
		$table->addCell()->addText($job->bo->contact_address);
		$table->addCell()->addText($job->ao->contact_address);
		$table->addCell()->addText($job->ao2->contact_address);
		$table->addCell()->addText($job->ao3->contact_address);
		$table->addCell()->addText($job->ao4->contact_address);
		$table->addCell()->addText($job->ao5->contact_address);
		$table->addCell()->addText($job->ao6->contact_address);
		$table->addCell()->addText($job->ao7->contact_address);
		$table->addCell()->addText($job->ao8->contact_address);
		$table->addCell()->addText($job->ao9->contact_address);
		$table->addCell()->addText($job->ao10->contact_address);
		
		$section->addTextBreak(1);


		$phpWord->addTableStyle('Table', $tableStyle);
		$table = $section->addTable('Table');
		$table->addRow();
		$table->addCell(6900)->addText('Notice Date:', ['bold'=>true]);
		$table->addCell(6900)->addText($job->party_wall_notice_date);
		$table->addRow();
		$table->addCell()->addText('10(4) Notice Date:', ['bold'=>true]);
		$table->addCell()->addText($job->ten_4_party_wall_notice_date);
		$table->addRow();
		$table->addCell()->addText('10(4) Notice Expiry Date:', ['bold'=>true]);
		$table->addCell()->addText($job->ten_4_party_wall_notice_expiry_date);
		$table->addRow();
		$table->addCell()->addText('Schedule of Condition Date:', ['bold'=>true]);
		$table->addCell()->addText($job->schedule_of_condition_date);
		$table->addRow();
		$table->addCell()->addText('Award Date:', ['bold'=>true]);
		$table->addCell();
		
		$section->addPageBreak();

		$section->addText('File Checklist:',['bold' => true]);
		$section->addTextBreak();
		$phpWord->addTableStyle('Table', $tableStyle);
		$table = $section->addTable('Table');
		$table->addRow(600);
		$table->addCell(6000)->addText('Action:', ['bold'=>true]);
		$table->addCell(3900)->addText('Date', ['bold'=>true]);
		$table->addCell(3900)->addText('Tick', ['bold'=>true]);
		$table->addRow(500);
		$table->addCell()->addText('Letter of appointment signed and received:');
		$table->addCell();
		$table->addCell();
		$table->addRow(500);
		$table->addCell()->addText('Party Wall Notices Valid:');
		$table->addCell();
		$table->addCell();
		$table->addRow(500);
		$table->addCell()->addText('Third Surveyor Selected:');
		$table->addCell();
		$table->addCell();
		$table->addRow(500);
		$table->addCell()->addText('SOC date booked:');
		$table->addCell();
		$table->addCell();
		$table->addRow(500);
			$table->addCell()->addText('Update Appointing Owner:');
		$table->addCell();
		$table->addCell();
		$table->addRow(500);
			$table->addCell()->addText('SOC Sent for Type:');
		$table->addCell();
		$table->addCell();
		$table->addRow(500);
			$table->addCell()->addText('SOC Sent to Surveyor:');
		$table->addCell();
		$table->addCell();
				$table->addRow(500);
			$table->addCell()->addText('Update Appointing Owner:');
		$table->addCell();
		$table->addCell();
				$table->addRow(500);
			$table->addCell()->addText('Award sent to Surveyor:');
		$table->addCell();
		$table->addCell();
				$table->addRow(500);
			$table->addCell()->addText('Award Agreed:');
		$table->addCell();
		$table->addCell();
			$table->addRow(500);
			$table->addCell()->addText('Update Appointing Owner:');
		$table->addCell();
		$table->addCell();
			$table->addRow(500);
			$table->addCell()->addText('Invoice Sent:');
		$table->addCell();
		$table->addCell();

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
