<?php

return [
    'storagePath' => '/var/www/blapp/storage/inhouse',
    'settingsPath' => '/mnt/volume_lon1_01/var/www/blapp/resources/views/admin/engine-fm/engine-fm/settings',
    'temporaryPath' => '/mnt/volume_lon1_01/var/www/blapp/resources/views/admin/engine-fm/engine-fm/runtime/tmp',
    'csrfTokenName' => 'engine.fileManager.Application.csrfToken',
];

