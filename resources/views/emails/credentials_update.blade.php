<html>
<head></head>
<body >
<p>Dear {{$user_name}},</p>

<p>I hope this email finds you well.</p>
<p>Your password has changed. To start click <a href="{{ env('APP_URL') }}">here</a></p>
<p>Password: {{$user_password}},</p>

<p>If you have any further questions, please do not hesitate to contact us.</p>

<p>Kind Regards </p>
<p>Berry Lodge Surveyors</p>

<img src="{{ asset('/images/email_footer.png') }}">
<p>
	W: <a href="http://www.blsurveyors.com">www.blsurveyors.com </a>- T: <a href="tel:02079352502">020 7935 2502</a> –E <a href="mailto:info@blsurveyors.com">info@blsurveyors.com</a>
</p>

</body>
</html>