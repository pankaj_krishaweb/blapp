import Pusher from 'pusher-js';

window.Vue = require('vue');

window.Echo = require('laravel-echo');


window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '8352962562d7c8d08706', 
      cluster: 'eu',
    encrypted: true
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('chat-message', require('./components/ChatMessage.vue'));
Vue.component('chat-log', require('./components/ChatLog.vue'));
Vue.component('chat-composer', require('./components/ChatComposer.vue'));

const app = new Vue({
    el: '#app',
    data: {
        messages: [],
        usersInRoom: [],
   
    },
    methods: {
        addMessage(message) {
            // Add to existing messages
            this.messages.push(message);

            // Persist to the database etc
            // 
            axios.post('/admin/messages', message).then(response => {
                // Do whatever;
            })
             $(".chat-log").animate({ scrollTop: $('.chat-log').prop("scrollHeight")}, 1000); 
        }
    },
    created() {
         var array = window.location.href.split('/');
        var lastsegment = array[array.length-1];
        this.jobid = lastsegment;

        axios.get('/admin/messages/'+lastsegment).then(response => {
            this.messages = response.data;
        });
        
        // var jobid = $('#jobid').attr('data-jobid');
        
        Echo.join(`chatroom.${this.jobid}`)
            .here((users) => {
                this.usersInRoom = users;
            })
            .joining((user) => {
                this.usersInRoom.push(user);
            })
            .leaving((user) => {
                this.usersInRoom = this.usersInRoom.filter(u => u != user)
            })
            .listen('MessagePosted', (e) => {
                this.messages.push({
                    message: e.message.message,
                    who: 'bubble-left',
                    user: e.user
                });
                 $(".chat-log").animate({ scrollTop: $('.chat-log').prop("scrollHeight")}, 1000); 
            });
    }
});