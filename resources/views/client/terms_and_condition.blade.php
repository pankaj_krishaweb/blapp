@extends('layouts.terms')
@section('content')
<style type="text/css">
  .mce{
    color: #404761;
  }
  .mce p {
    margin: 0 0 1.2em;
    line-height: 1.85em;
    font-size: 0.938em;
  }
  h2.mce, .mce h2 {
      color: #404761;
      font-family: 'poppinsbold', sans-serif, Helvetica, Arial;
      font-size: 1.500em;
      line-height: 1.2em;
      margin-bottom: 1.1em;
      letter-spacing: 0.05em;
  }
  h2 {
    display: block;
    font-size: 1.5em;
    margin: 0;
    margin-block-start: 0.83em;
    margin-block-end: 0.83em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
    font-weight: bold;
}
.mce p{
  line-height: 1.5;
  margin-bottom: 10px;
}
.mce h1,
.mce h2,
.mce h3,
.mce h4,
.mce h5,
.mce h6{
  margin-top: 1.1em;
}
</style>
<div class="container">
  <h1 class="mce">Berry Lodge Surveyors Terms and Conditions of Service</h1>
      <div class="mce">
        <h2> 1) Contact and Response Time:</h2>
          <p>Please remember that unlike many other professions, Surveyors spend as much time out of the office as they do in, it is the very nature of the job.</p>

          <p>This means that Surveyors will often be inspecting properties, attending site meetings or travelling to and from site. With this in mind response time to calls, emails or voice messages may not always be as quick as we’d like.</p>
        
        <h2>Emails:</h2>
          <p>We aim to respond to all emails as soon as they are received, however there are occasions where the Surveyor handling the file will be in meetings, site inspections or dealing with other files and case work.We therefore aim to have a response to any email within 1 to 4 business days of receiving it.</p>

        <h2>Calls & Voice Messages: </h2>
          <p>We aim to respond to all calls as soon as they are received, however there are occasions where the Surveyor handling the file will be in meetings, site inspections or dealing with other files and case work.We therefore aim to have a response to any call or voice message within 1 to 4 business days of receiving it.</p>

          <p>We understand these timings aren’t always ideal, however please bear in mind most of the legal Act’s we operate under, allow up to 12 days to respond and our governing body the RICS allows up to 14 days to respond.</p>

        <h2> 2) Complaints</h2>
          <p>We do our best to avoid complaints and issue, however do appreciate that sometimes that isn’t possible.</p>

          <p>As per our governing bodies requirements, we have complaint handling procedures in place, these will vary depending upon the type of Surveying service we are assisting you on.</p>

          <p>If you would like to make a complaint, please do so by confirming in writing the nature of your complaint and whom it is against. Please send your complaint to <a href="mailto:info@blsurveyors.com">info@blsurveyors.com</a>. It will then be directed to the appropriate complaints handler within Berry Lodge and dealt with directly.</p>
        <h2> 3) Discounts Codes</h2>
          <p>Berry Lodge are pleased to offer discounts to their clients both present and future. However please note that as much as we’d like to be able to offer discounts on Party Wall Surveying work, we can’t!</p>

          <p>We are therefore only able to offer discounts on non Party Wall Surveying work. Don’t worry, with the range of services that Berry Lodge offer, there is still a lot to gain from this!</p>

          <p>All discount codes will be honoured and settled at the final invoice stage or upon settlement of the invoice.</p>

        <h2> 4) Additional Costs</h2>
          <p>Whilst we always try and avoid extra costs, there are some occasions where we may need to charge them.</p>

          <p>Don’t worry, we will always make you fully aware of these costs in advance of action and in most cases and where we are legally able to, we will await your confirmation to the additional costs before they are incurred.</p>

          <p>If you are concerned about additional costs, we always encourage speaking to the Surveyor handling the job. You can find them in the Meet the Surveyor tab!</p>

        <h2> 5) GDPR</h2>
          <p>Without getting to bogged down in the specifics, if you select “on” in the email notification you will be agreeing to the receipt of emails from Berry Lodge Surveyors.</p>

          <p>You can switch this “on” or “off” as you choose with the click of a button. </p>

        <h2> 6) Access to the Berry Lodge Client Portal</h2>
          <p>Believe it or not the internet will eventually run out of space! So will the Berry Lodge Client Portal!</p>

          <p>We will store all of the information within your Client Portal for 6 months post completion of Berry Lodge’s action. The file will then be placed in an archive for a further 3 months before being transferred to a secure cloud based server where it will be held for a period of 5 years and 3 months.</p>

          <p>As with any website, transferring the information from the cloud back to the Berry Lodge Client Portal isn’t as simple as a couple of clicks. If you’d like to do this we will need to charge a fixed administration cost of £65.00 plus Vat.</p>

          <p>We would encourage you to store all of the information within the Client Portal on your own server as that will save the future cost and also keep the internet squeaky clean for the future!</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </div>
</div>
@endsection