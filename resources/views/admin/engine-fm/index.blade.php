@section('content')
<!--<link rel="shortcut icon" href="{{ asset('engine-fm/favicon.ico') }}"/>-->
<link rel="stylesheet" type="text/css" href="{{ asset('engine-fm/styles/default.css') }}" />
<div class="full-wrapper">
	<div class="row" id="">
            <div class="col-md-12">
                <h1>Storage</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ url('admin') }}">Home</a></li>
                    <li>Storage</li>
                </ol>
            </div>
            
		  </div>

	<div id="file-manager" ></div>
</div>

@endsection
@section('script')
<script src="{{ asset('engine-fm/js/app-es5.js') }}"></script>
<script type="text/javascript">
    new engine.fileManager.Application({
        wrapper: document.getElementById('file-manager'),
        configUrl: 'storage/server?action=config&',
        csrfToken: 'csrfToken'
    });
</script>
@endsection


