@extends('layouts.frame')

@section('content')
    <style>
        
      /* Style the input field */
      #myInput {
        padding: 20px;
        margin-top: -6px;
        border: 0;
        border-radius: 0;
        background: #f1f1f1;
      }

      #job-search-btn{
          margin-top: 40px;
        background-color: #fff;
        color: #b3b3b3;
        border: solid 0.5px #b3b3b3;
        border-radius: 5px;
            

      }
      h4 h3{
        color: #414860;
        font-weight: bold;
    }
      
      h4 span {
        color: #909090;

      }
      h2{
        color: #00b29c;
        font-weight: bold;
      }
    </style>
    <div class="container-fluid">
        <div class="container">
            <h1>Job for {{ $user->name}} </h1>
            <ol class="breadcrumb"> 
                <li><a href="{{ url('admin') }}">Admin</a></li>
                <li><a href="{{ url('admin/progress-report') }}">Progress Report</a></li>
                <li><a href="{{ url("admin/progress-report/{$user->id}") }}">{{ $user->name}}</a> </li>
                <li>BLSN{{ $job->id}} </li>
            </ol>
        </div>    
        @if($job->tasks->count()<1)
            <div class="container">
                <h3>BLSN{{ $job->id }}</h3>
                <h3 class="text-danger"> This job has no tasks assigned!</h3>
            </div>
        @else
            <div class="container">   
                <div class="row">
                    <div class="col-md-2  col-sm-2 col-xs-12">
                        <a href="{{ url("/admin/my-jobs/view/{$job->id}") }}" style="text-decoration: none;"> 
                            <h3>BLSN{{ $job->id }}</h3>
                        </a>
                        <h2>{{ round((  $job->tasks->where('status',1)->count() / $job->tasks->count() ) * 100) }} % </h2>
                        <p style="color: #00b29b;">Complete</p>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-3 text-center">
                        <h3 style="font-size: 1.5em;"> Total </h3>
                        <h1 style="font-size: 2.8em;">{{ $job->tasks->count() }}</h1>
                    </div>

                    <div class="col-md-2  col-sm-2 col-xs-4 text-center">
                        <h3 style="font-size: 1.5em;"> Completed</h3>
                        <h1 style="font-size: 2.8em;">{{ $job->tasks->where('status',1)->count() }}</h1>
                    </div>
                    <div class="col-md-2  col-sm-2 col-xs-5 text-center">
                        <h3 style="font-size: 1.5em;"> Outstanding</h3>
                        <h1 style="font-size: 2.8em;">{{ $job->tasks->where('status',0)->count() }}</h1>
                    </div>
                    <div class="col-md-2  col-sm-2 col-xs-5 text-center">
                        <h3 style="font-size: 1.5em;"> Invoice</h3>
                        <h1 style="font-size: 1.8em;">{{ $job->invoice_no }}</h1>
                    </div>
                    <div class="col-md-2  col-sm-2 col-xs-5 text-right">
                        <h3 style="font-size: 1.5em;" > Cost</h3>
                        <h1 style="font-size: 2.8em;"> £{{ $job->payments->sum('amount') }}</h1>
                    </div>
                </div>
            </div>    
            <div class="container">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:  {{ (($job->ticked )/$job->tasks->count()) * 100 }}%"></div>
                </div>
            </div>
            @if($job->tasks)   
                <div class="container">
                    <div class="row hidden-xs">
                        <div class="col-md-6 col-sm-7"><h4>Task Name</h4></div>
                        <div class="col-md-3 col-sm-3"><h4>Date of Surveyor Action</h4></div>
                        <div class="col-md-3 col-sm-2"><h4 class="pull-right text-right">Task Completed</h4></div>
                    </div>
                    @foreach($job->tasks as $task)
                        <div class="row">
                            <div class="col-md-6 col-sm-7 col-xs-12"><h4><span>{{ $task->task }}</span></h4></div>
                            <div class="col-md-3 col-sm-3 col-xs-10"><h4> <span>{{ $task->updated_at }}</span></h4></div>
                            <div class="col-md-3 col-sm-2 col-xs-2"><span  class="pull-right">
                                @if($task->status==1) <i class="fa fa-check" aria-hidden="true" style="color: #00B29D; font-size: 30px;"></i> @endif
                            </div>
                        </div>
                        <hr> 
                    @endforeach
                </div>
            @endif             
        @endif 
    </div>
@endsection

@section('script')

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
    
$(document).ready(function() {

    $(".tasks").click(function() {

        var taskid = $(this).attr('data-key');
        
        if ($(this).is(":checked")) {
           var status="1";
        } else {
            var status="0";
        
        }

          $.ajax({
        url: '/tasks/checked',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {taskid: taskid, status: status},
        success: function(data){
        console.log(data);
        location.reload()
            
        },
    })  
    });

});
</script>
@endsection
