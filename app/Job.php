<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public function getCreatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value)); 
    }
    public function getUpdatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value));   
    }
    
    public function surveying(){
        return $this->hasOne(Surveying::class, 'job_id');
    }

    public function ao(){
        return $this->hasOne(Ao_1::class, 'job_id');
    }

    public function ao2(){
        return $this->hasOne(Ao_2::class);
    }

    public function ao3(){
        return $this->hasOne(Ao_3::class);
    }

    public function ao4(){
        return $this->hasOne(Ao_4::class);
    }

    public function ao5(){
        return $this->hasOne(Ao_5::class);
    }

    public function ao6(){
        return $this->hasOne(Ao_6::class);
    }

    public function ao7(){
        return $this->hasOne(Ao_7::class);
    }

    public function ao8(){
        return $this->hasOne(Ao_8::class);
    }

    public function ao9(){
        return $this->hasOne(Ao_9::class);
    }

    public function ao10(){
        return $this->hasOne(Ao_10::class);
    }

    public function bo(){
        return $this->hasOne(Bo::class);
    }

    public function tasks(){
        return $this->hasMany(Job_Tasks::class);
    }

    public function payments(){
        return $this->hasMany(Payment::class);
    }


    public function history(){
        return $this->hasMany(Job_History::class);
    }

    public function users(){
        return $this->belongsToMany(User::class, 'job_users');
    }

    public function messages(){
        return $this->hasMany(Message::class);
    }

    public function docs(){
        return $this->hasMany(Document::class, 'job_id', 'id');
    }

    public function notes()
    {
        return $this->hasMany(Note::class,'job_id');
    }
}
