<html>
<head>
	<style type="text/css">
		.float-md-left,
		.float-md-right{
			text-align: center;
		}
		h2{
			font-size: 30px;
			line-height: 40px;
			color: #00b29c;
			font-weight: bold;
		}
		@media (min-width: 768px) {
			.title-wrap{
				display: table;
				max-width: 800px;
				width: 100%;
			}
			h2{
				font-size: 47px;
				line-height: 57px;
			}
			.title-wrap .float-md-left,
			.title-wrap .float-md-right{
				display: table-cell;
				vertical-align: middle;
			}
			.title-wrap h2{
				font-size: 47px;
				line-height: 1.5;
			}
			.float-md-left{
				float: left;
				width: calc(100% - 250px);
				text-align: left;
			}
			.float-md-right{
				float: right;
				width: 250px;
				text-align: right;
			}
			.float-md-right img{
				max-width: 100%;
			}
		}
	</style>
	
</head>
<body >
	<div class="text-center title-wrap">
		<div class="float-md-left">
			<h2>In the event of issue, don’t be shy! Get in touch!</h2>
		</div>
		<div class="float-md-right">
			<img src="{!! asset('images/as/8.png') !!}" width="200px">
		</div>
	</div>
	<div>
		<p>We hope you’ve now had the chance to review the Party Wall Award and understand its contents.</p>

		<p>We thought we would just send a quick email to confirm that should you notice any issues or damage as a result of the Party Wall Works on site, please contact your Party Wall Surveyor {!! $surveyor_name !!} at the first opportunity and pop him over some photographs of the issue or upload these onto the <a href="https://app.blsurveyors.com/" target="_blank"> Berry Lodge Client Portal </a>.</p>

		<p>He will then be able to assist and take the appropriate action.</p>

		<p>Kind Regards </p>

		<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;">
		<p><strong>Berry Lodge Surveyors</strong></p>
	</div>
	@include('shard.footer')

</div>
</body>
</html>