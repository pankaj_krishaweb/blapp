@extends('layouts.frame')

@section('content')
<style>.panel-body {
    padding: 5px 15px;
}

  /* Style the input field */
  #myInput {
    padding: 20px;
    margin-top: -6px;
    border: 0;
    border-radius: 0;
    background: #f1f1f1;
  }

  #job-search-btn{
      margin-top: 10px;
    background-color: #fff;
    color: #b3b3b3;
    border: solid 0.5px #b3b3b3;
    border-radius: 5px;

  }
  </style>
<div class="container-fluid">
    	<div class="container" style="min-height: 400px; padding-bottom: 50px;">
        	<h1 id="job_id" data-id="{{ $job_id }}">Users</h1>
        	<ol class="breadcrumb"> 
			<li><a href="{{ url('admin') }}">Home</a></li>
			<li><a href="{{ url('admin/my-jobs') }}">My Job</a></li>
			<li class="admin/jobs">Users</li>
		</ol>
	       <div class="row">
	         	<div class="col-md-12">
	         		@if (session('message'))
				    <div class="alert alert-info">
				        {{ session('message') }}
				    </div>
				@endif
			</div>
	       </div>
	    
		<div class="row">
			<div class="col-md-3 col-sm-3 col-xs-12">
				<h2>Add User</h2>
	 			
			            	<button class="btn dropdown-toggle btn-block" type="button" data-toggle="dropdown" id="job-search-btn">
			            		<span style="padding-right: 40px;">Add User to Job</span>
			            		<span class="caret" style="color: #424961"></span>
			        	</button>
			            	<ul class="dropdown-menu">
			              	<input class="form-control" id="myInput" type="text" placeholder="Search..">
				            	@foreach($users as $user)
				            		<li><a href="{{ url('/admin/my-jobs/users/'.$job_id.'/'.$user->id) }}">{{ $user->name}}</a></li>
				            	@endforeach
			            		<li><a  class="btn btn-success"	href="{{ url('/admin/users/') }}" style="border-radius: 0px;"> Create new</a></li>
			            	</ul>
			       
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12">
				<h2>Current users on Job ({{ $job_users->users->count()}})</h2>
				@if($job_users->users->count()< 1)
					<div class="alert alert-danger">
				 		Currently no users assigned to this job 
					</div>
			    	@else
			    		<div class="table-bordered table-responsive" style="margin-top: 20px">
					    	<table class="table">
							<thead style="background-color: #00b29c; color: #fff;">
							      	<tr>
							        	<th>Role</th>
								       <th>Name</th>
								       <th>Email</th>
								       <th>Role</th>
								       <th>Delete</th>
							      	</tr>
							</thead>
							<tbody>
					       		@foreach($job_users->users as $user)
					       			@php 
					       			$job_user = App\Job_User::where([['job_id','=',$job_id],['user_id','=',$user->id]])->first();
					       			@endphp
									<tr>
										<td>
											@if($user->role==9)
								      				Admin
								      			@elseif($user->role==7)
								      				Employee
								      			@elseif($user->role==5)
								      				Third Party
								      			@elseif($user->role==3)
								      				Client
								      			@elseif($user->role==1)
								      				Meditation Client
								      			@endif
							      			</td>
									      	<td>
									      		{{ $user->name }} 
									      	</td>
									      	<td>
									      		{{ $user->email }}
									      	</td>
									      	<td>
									      		@if($user->role != 3)
									      		<select data-id="{!! $user->id !!}" class="role" id="user_role">
									      			<option disabled="" selected="">select role</option>
									      			<option value="Named Surveyor" @if($job_user->user_role == "Named Surveyor") selected @endif>Named Surveyor</option>
									      			<option value="Assisting Surveyor" @if($job_user->user_role == "Assisting Surveyor") selected @endif>Assisting Surveyor</option>
									      			<option value="Administrator" @if($job_user->user_role == "Administrator") selected @endif>Administrator</option>
									      		</select>
									      		@endif
									      	</td>
									      	<td>
										       <a href="{{ url('/admin/my-jobs/users/delete/'.$job_id.'/'.$user->id) }}" class="btn btn-danger btn-xs">
										       	Remove
											</a> 			      	
									      	</td>
									</tr>						
								@endforeach
							</tbody>
						</table>
					</div>
			      @endif
    			</div>	
	       </div>
    	</div>
</div>
@endsection
@section('script')
	<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<script type="text/javascript">


$(document).on('change','#user_role',function(e){
	e.preventDefault();
	var role =$(this).val();
	var job_id = "{!! $job_id !!}";
	var user_id = $(this).data('id');
	$.ajax({
		url:'/admin/jobs/surveyor-role',
		type:'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {job_id: job_id,user_id:user_id ,role:role},
		success:function(response){
			console.log(response);
		},
		error:function(error){
			console.log(error);
		}
	})
})
$(document).on('click', '.add-task', function(event) {
		event.preventDefault();

	if ($(this).text()=="Add All") 
	{
		var	type = 'all' ;
	}
	else if($(this).text()=="Add")
	{
		var	type = 'sub' ;
	}

	var key = $(this).attr('data-key');
	var job_id = $('#job_id').attr('data-id');
	$.ajax({
		url: '/admin/jobs/tasks',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {job_id: job_id, key:key, type:type},
		success: function(data){
			var status = $.parseJSON(data);
		
			if(status.status=='good'){
				swal("Task added successfully ").then(() => {
				 location.reload();
				});				
			}
		},
		error: function(data){
	    	var errors = $.parseJSON(data.responseText);
		    console.log(errors);
			var displayerror='<div class="alert alert-dismissible alert-danger">';
		   $.each(errors, function(index, value) {
		      displayerror+='<li>'+value+'</li>';
		    });
		    displayerror+='</div>';
		    $('#error-display').html(displayerror);
	    }
		
	})

});


// $('.delete-user-btn').on('click',  function(event) {
// 	event.preventDefault();
// 	var user_id = $(this).parent().parent().attr('data-id');
// 	swal("Are you sure you wish to delete this user?", {
//   buttons: ["Cancel", "Yes Delete User!"],
// }).then(function(value){
// 	if(value==true){
// 		$.ajax({
// 			url: '/delete-user',
// 			type: 'POST',
// 			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
// 			data: {user_id: user_id},
// 			success: function(data){
// 				var status = $.parseJSON(data);
// 				if(status.status=='good'){
// 					location.reload();			
// 				}
// 			},
// 		})		
// 	}
// 	return false;
// }, 
// 	function(){
// 		console.log('Not deleted');
// 		return false;
// 	});
// });

// $('.user-history-btn').on('click',  function(event) {
// 	event.preventDefault();
// 	var user_id = $(this).parent().parent().attr('data-id');
// 	$.ajax({
// 		url: '/user-history',
// 		type: 'POST',
// 		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
// 		data: {user_id: user_id},
// 		success: function(data){
// 			console.log(data);
// 			$('#userHistoryModal').modal();
// 			var history = $.parseJSON(data);
// 			var displayhistory='<ul>';
// 		   	$.each(history, function(index, value) {
// 		    	displayhistory+='<li><strong> '+value.created_at+' :  '+value.action+' <br></strong> '+value.description+'</li>';
// 		    });
// 		    displayhistory+='</ul>';
// 		    $('#history-display').html(displayhistory);
	    

// 		},
// 	})		
// });

</script>
@endsection
