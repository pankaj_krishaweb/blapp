<html>
<head>
	<style type="text/css">
		.float-md-left,
		.float-md-right{
			text-align: center;
		}
		h2{
			font-size: 30px;
			line-height: 40px;
			color: #00b29c;
			font-weight: bold;
		}
		@media (min-width: 768px) {
			.title-wrap{
				display: table;
				max-width: 800px;
				width: 100%;
			}
			h2{
				font-size: 47px;
				line-height: 57px;
			}
			.title-wrap .float-md-left,
			.title-wrap .float-md-right{
				display: table-cell;
				vertical-align: middle;
			}
			.title-wrap h2{
				font-size: 47px;
				line-height: 1.5;
			}
			.float-md-left{
				float: left;
				width: calc(100% - 250px);
				text-align: left;
			}
			.float-md-right{
				float: right;
				width: 250px;
				text-align: right;
			}
			.float-md-right img{
				max-width: 100%;
			}
		}
	</style>
	
</head>
<body >
	<div class="text-center title-wrap">
		<div class="float-md-left">
			<h2>We’ve Agreed the Party Wall Award! We’ll send it to you shortly</h2>
		</div>
		<div class="float-md-right">
			<img src="{!! asset('images/aos/') !!}" width="200px">
		</div>
	</div>
	<div>
		<p>Good News! We are pleased to confirm that we’ve progressed your Party Wall Surveying Matter!</p>
		<p>The Schedule of Condition inspection has been booked into the diary.</p>

		<p>The inspection will be attended by the Party Wall Surveyor acting on behalf of the building owners surveyor, {{ $surveyor_name }} and a Surveyor from Berry Lodge Surveyors.</p>
		<p>We look forward to seeing you then.</p>


		<p>Login to the <a href="https://app.blsurveyors.com/"> Berry Lodge Client Portal</a> now to see the status of your Party Wall Surveying job.</p>
		<p>Kind Regards,</p>

		<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;">
		<p><strong>Berry Lodge Surveyors</strong></p>
	</div>
	@include('shard.footer')

</div>
</body>
</html>