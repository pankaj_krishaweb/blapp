<?php

namespace App\Src\Docs\NoticesAndCovers;
use Illuminate\Support\Facades\Storage;

class  AO6ConfofBLS104Appointment
{
	public $documentFolder="Notices and Covers";
	public $documentName="AO 6 Conf of BLS 10.4 Appointment";

    	public function create(\App\Job $job){

	    	//$domPdfPath = base_path( 'vendor/dompdf/dompdf');
//\PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
//\PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory  
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(10);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both','spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(0), 'lineHeight' => 1.15));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		

		$section->addTextBreak(1);
		$section->addText($job->ao6->full_names);
		$section->addText(ucwords($job->ao6->contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addText('Dear '.$job->ao6->salutation.',');
		$section->addTextBreak();
		$section->addText('Re: The Party Wall etc. Act 1996 ',['bold' => true]);
		
		$section->addText($job->bo->property_address_proposed_work.' / ',['bold' => true]);
		$section->addText($job->ao6->property_address_adjoining,['bold' => true]);
		$section->addTextBreak();
			$section->addText('I understand my colleague '.$job->bo->surveyor_name.' served Party Wall '.$job->ao6->notice_notices.' upon you on the '.$job->ao6->date_of_notice.' describing the works which are due to take place at '.$job->bo->property_address_proposed_work.'.');
		$section->addTextBreak();
			$section->addText(''.$job->bo->surveyor_name.' has confirmed that no response was received, I therefore understand that he served a further Party Wall Notice upon you on '.$job->ten_4_party_wall_notice_date.' to confirm that a response was needed to enable the Party Wall procedures to be progressed ');
		$section->addTextBreak();
			$section->addText('As it has now been more than 10 days since '.$job->bo->surveyor_name.' sent that further Party Wall Notice to you and a response still hasn’t been received, I have today been appointed to act as your Party Wall Surveyor in accordance with Section 10(4) of the Party Wall etc Act 1996. ');
		$section->addTextBreak();
			$section->addText('Please note that like '.$job->bo->surveyor_name.' I also work for Berry Lodge Surveyors, however I work in a different office to '.$job->bo->surveyor_name.' and I can confirm that we have established a tried and tested information barrier for this file, meaning that I will not be able to access '.$job->bo->surveyor_name.' files and he will likewise be unable to access mine. Furthermore, I can confirm that as part of my statutory duty as a Party Wall Surveyor I am duty bound to be impartial and I can categorically confirm I will do that.');
		$section->addTextBreak();
			$section->addText('I would like to take this opportunity to request a suitable date for '.$job->bo->surveyor_name.' and I to inspect your property to complete a Schedule of Condition Report. ');
		$section->addTextBreak();
			$section->addText('A Schedule of Condition Report involves '.$job->bo->surveyor_name.' & I visiting your property before the building owner'.$job->bo->s_s.' works commence, to record the condition of your property in both written and photographic format, we would then provide a copy of this report to each respective owner.');
		$section->addPageBreak();$section->addTextBreak();

			$section->addText('At this point it would be prudent of me to confirm that under the Party Wall etc Act 1996 there is no statutory requirement for the surveyors to undertake a Schedule of Condition Report. Therefore, in the interest of enabling the Party Wall procedures to progress, should I not hear from within the next 7 days, '.$job->bo->surveyor_name.' and I will agree a Party Wall Award making note that internal access could not be obtained. ');
		$section->addTextBreak();
			$section->addText('This is not our preferred course of action therefore please contact me at the first instance. ');
		$section->addTextBreak();
			$section->addText('I can confirm that my costs in respect of this matter will be met by the building '.$job->bo->owners_owners.'.');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to contact me. ');
		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(2);
		$section->addText(ucwords($job->ao6->surveyor_name));
		$section->addText(ucwords($job->ao6->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);
		$section->addTextBreak();
		$section->addText('cc '.$job->bo->surveyor_name);
		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save( base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx"); 
//Save it
//$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
//$xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.pdf");

		//$file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");
//$file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}");
//$file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
