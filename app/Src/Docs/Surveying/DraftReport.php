<?php

namespace App\Src\Docs\Surveying;

use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class DraftReport
{
	public $documentFolder = "Draft Report";
	public $documentName="Draft Report";

	public function create(\App\Job $job){
		$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/");

		    $spreadsheet = new Spreadsheet();

		    $sheet = $spreadsheet->getActiveSheet();
		    $sheet->setCellValue('A1', 'Job Number:');
		    $sheet->setCellValue('A2', 'BLSN'.$job->id);
		    $sheet->setCellValue('B1', 'Date of Confirmation Letter:');
		    $sheet->setCellValue('B2', $job->surveying->date_of_confirmation_letter);
		    $sheet->setCellValue('C1', 'Date of Visit:');
		    $sheet->setCellValue('C2', $job->surveying->created_at->format('d-M-Y'));
		    $sheet->setCellValue('D1', 'Surveyor Name:');
		    $sheet->setCellValue('D2', $job->surveying->surveryor_name);
		    $sheet->setCellValue('E1', 'Client Name:');
		    $sheet->setCellValue('E2', $job->surveying->client_full_name);
		    $sheet->setCellValue('F1', "Client's Address:");
		    $sheet->setCellValue('F2', $job->surveying->client_contact_address);
		    $sheet->setCellValue('G1', "Address of Boundary Determination:");
		    $sheet->setCellValue('G2', $job->surveying->address_of_boundary_determination);
		    $sheet->setCellValue('H1', "Boundary Determination Address HMLR Title No:");
		    $sheet->setCellValue('H2', $job->surveying->boundary_determination_address);
		    $sheet->setCellValue('I1', "Type of Property:");
		    $sheet->setCellValue('I2', $job->surveying->type_of_property);
		    $sheet->setCellValue('J1', "Age of Property:");
		    $sheet->setCellValue('J2', $job->surveying->age_of_property);
		    $sheet->setCellValue('K1', "Address of Adjoining Property:");
		    $sheet->setCellValue('K2', $job->surveying->address_of_adjoining_property);
		    $sheet->setCellValue('L1', "Adjoining Property HMLR Title No:");
		    $sheet->setCellValue('L2', $job->surveying->adjoining_property);
		    $sheet->setCellValue('M1', "Name of Legal owner of Adjoining Property:");
		    $sheet->setCellValue('M2', $job->surveying->name_of_legal_owner);
		    $sheet->setCellValue('N1', "Boundary Being Detemined:");
		    $sheet->setCellValue('N2', $job->surveying->boundary_being_determined);
		    $sheet->setCellValue('O1', "Direction of Boundary:");
		    $sheet->setCellValue('O2', $job->surveying->direction_of_boundary);
		    $sheet->setCellValue('P1', "Plane of Boundary:");
		    $sheet->setCellValue('P2', $job->surveying->plane_of_boundary);
		    $sheet->setCellValue('Q1', "Inspection Weather:");
		    $sheet->setCellValue('Q2', $job->surveying->inspection_weather);
		    $sheet->setCellValue('R1', "Inspection Humidity:");
		    $sheet->setCellValue('R2', $job->surveying->inspection_humidity);
		    $sheet->setCellValue('S1', "Local Authority:");
		    $sheet->setCellValue('S2', $job->surveying->local_authority);
		    $sheet->setCellValue('T1', "How Many Historic Maps were present?: ");
		    $sheet->setCellValue('T2', $job->surveying->historic_maps_were_present);
		    $sheet->setCellValue('U1', "Were planning records of assistance?:");
		    $sheet->setCellValue('U2', $job->surveying->planning_records_of_assistance);
		    $sheet->setCellValue('V1', "Date of Report:");
		    $sheet->setCellValue('V2', $job->surveying->date_of_report);

		    $styleArray = [
			    'font' => [
			    	'bold' => true,
			    	'size' => 12,
			    	'name' => 'Gill Sans',
			    ],
			    'fill' => [
			    	'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
				    'startColor' => [
				    	'argb' => 'BDD7EE',
			    	],
			    	'endColor' => [
			    	    'argb' => 'BDD7EE',
			    	 ],
		    	],
		    ];

		    $spreadsheet->getActiveSheet()->getStyle('A1:v1')->applyFromArray($styleArray);


		    /*$spreadsheet->getActiveSheet()->getStyle('A1:V1')->getFont()->applyFromArray([ 'name' => 'Gill Sans', 'bold' => TRUE, 'italic' => FALSE,'strikethrough' => FALSE, 'size' => 12]);*/
		    $spreadsheet->getActiveSheet()->getStyle('A2:V2')->getFont()->applyFromArray([ 'name' => 'Calibri', 'bold' => false, 'italic' => FALSE,'strikethrough' => FALSE, 'size' => 12]);

		    /*$spreadsheet->getActiveSheet()->getStyle('A1:V1')->getFill()
			    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			    ->getStartColor()->setARGB('BDD7EE');*/
		    $spreadsheet->getActiveSheet()->getStyle('A2:V2')->getFill()
			    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
			    ->getStartColor()->setARGB('E2EFDA');
		    //$sheet = $PHPExcel->getActiveSheet();
		    $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		    $cellIterator->setIterateOnlyExistingCells( true );
		    /* @var PHPExcel_Cell $cell */
		    foreach( $cellIterator as $cell ) {
		            $sheet->getColumnDimension( $cell->getColumn() )->setAutoSize( true );
		    }
		    $writer = new Xlsx($spreadsheet);
		    $writer->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.xlsx");

		    return "/storage/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.xlsx";
	}
}