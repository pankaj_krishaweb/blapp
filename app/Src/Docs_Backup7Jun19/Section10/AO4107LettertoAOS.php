<?php

namespace App\Src\Docs\Section10;
use Illuminate\Support\Facades\Storage;

class  AO4107LettertoAOS
{
	public $documentFolder="Section 10 Letters";
	public $documentName="AO 4 10(7) Letter to AOS";

    	public function create(\App\Job $job){

	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		

		$section->addTextBreak(3);
		$section->addText(ucwords($job->ao4->surveyor_name));
		$section->addText(ucwords($job->ao4->surveyor_company_name));
		$section->addText(ucwords($job->ao4->surveyor_company_address));
	
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addTextBreak();
		$section->addText('Dear '.$job->ao4->surveyor_name.',');
		$section->addTextBreak();
		$section->addText('Re: '.$job->bo->property_address_proposed_work.' / ' ,['bold' => true]);
		$section->addText($job->ao4->property_address_adjoining ,['bold' => true]);
		$section->addText('The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();
		$section->addText('Further to our recent telephone and email discussions, in accordance with Sections 10(6) and 10(7) of the Act, I formally call upon you to ACTION REQUIRED.'); 
		$section->addTextBreak();
		$section->addText('Should I not receive response to this letter by DATE (12 days from date of letter), I will have no choice but to proceed ex parte and ACTION TAKEN.'); 

		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(2);
		$section->addText(ucwords($job->bo->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
