@extends('layouts.frame')

@section('content')
<style>

  /* Style the input field */
  #myInput {
    padding: 20px;
    margin-top: -6px;
    border: 0;
    border-radius: 0;
    background: #f1f1f1;
  }

  #job-search-btn{
      margin-top: 40px;
    background-color: #fff;
    color: #b3b3b3;
    border: solid 0.5px #b3b3b3;
    border-radius: 5px;


  }
  h4 h3{
    color: #414860;
    font-weight: bold;
}

  h4 span {
    color: #909090;

  }
  h2{
    color: #00b29c;
    font-weight: bold;
  }
.filter-input {
   padding: 30px;
   margin: 10px 0px;
   border: 0;
   border-radius: 0;
   background: #eef2f6;
  }
</style>
<div class="container-fluid" id="jobs_list">
    <div class="container">
        <h1>Job for {{ $user->name}} </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}">Admin</a></li>
            <li><a href="{{ url('admin/progress-report') }}">Progress Report</a></li> 
            <li> {{ $user->name }}</li>
        </ol>
        <div class="row">
            <div class="col-md-3">
                <input class="form-control filter-input" id="jobNoInput" type="text" placeholder="Search by Job No ...">
            </div>
        </div> 
    </div>
    
    @foreach($user->jobs as $job)
      @if($job->status==1)
        @if($job->tasks->count()<1)
            <div class="container job_item"  data-no="{{ $job->id }}">
                <h3><a href="{{ url("/admin/my-jobs/tasks/{$job->id}") }}">BLSN{{ $job->id }}</a></h3>
                <h2>0 % </h2>
                <h3 class="text-danger"> This job has no tasks assigned!</h3>
                <hr>
            </div>
        @else
            <div class="container job_item" data-no="{{ $job->id }}">
                <div class="row">
                    <div class="col-md-2  col-sm-2 col-xs-12">
                        <a href="{{ url("/admin/progress-report/{$user->id}/{$job->id}") }}" style="text-decoration: none;">
                            <h3>BLSN{{ $job->id }}</h3>
                        </a>
                        <h2>{{ round((  $job->tasks->where('status',1)->count() / $job->tasks->count() ) * 100) }} % </h2>
                        <p style="color: #00b29b;">Complete</p>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-3 text-center">
                        <h3 style="font-size: 1.5em;"> Total </h3>
                        <h1 style="font-size: 2.8em;">{{ $job->tasks->count() }}</h1>
                    </div>
                    <div class="col-md-2  col-sm-2 col-xs-4 text-center">
                        <h3 style="font-size: 1.5em;"> Completed</h3>
                        <h1 style="font-size: 2.8em;">{{ $job->tasks->where('status',1)->count() }}</h1>
                    </div>
                    <div class="col-md-2  col-sm-2 col-xs-5 text-center">
                        <h3 style="font-size: 1.5em;"> Outstanding</h3>
                        <h1 style="font-size: 2.8em;">{{ $job->tasks->where('status',0)->count() }}</h1>
                    </div>
                   <div class="col-md-2  col-sm-2 col-xs-5 text-center">
                        <h3 style="font-size: 1.5em;"> Invoice</h3>
                        <h1 style="font-size: 1.8em;">{{ $job->invoice_no }}</h1>
                    </div>
                    <div class="col-md-2  col-sm-2 col-xs-5 text-right">
                        <h3 style="font-size: 1.5em;" > Cost</h3>
                        <h1 style="font-size: 2.8em;"> £{{ $job->payments->sum('amount') }}</h1>
                    </div>
                </div>
                            <div class="container">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:  {{ round((  $job->tasks->where('status',1)->count() / $job->tasks->count() ) * 100)  }}%"></div>
                </div>
                <hr>
            </div>
            </div>

        @endif
      @endif
    @endforeach
</div>
@endsection
@section('script')

<script>
//Search by Job Number
$(document).ready(function(){
  $("#jobNoInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#jobs_list .job_item").filter(function() {
      $(this).toggle($(this).attr('data-no').toLowerCase().indexOf(value) > -1)
    });
  });
});

$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

$(document).ready(function() {

    $(".tasks").click(function() {

        var taskid = $(this).attr('data-key');

        if ($(this).is(":checked")) {
           var status="1";
        } else {
            var status="0";

        }

          $.ajax({
        url: '/tasks/checked',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {taskid: taskid, status: status},
        success: function(data){
        console.log(data);
        location.reload()

        },
    })
    });

});
</script>
@endsection
