<footer class="primary">
    
    	<nav class="primary tablet tablet-large desktop">
            <div class="full-wrapper">
                <div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="menu"><li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32"><a href="https://www.blsurveyors.com/about-us/">About</a></li>
<li id="menu-item-1616" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1616"><a href="https://www.blsurveyors.com/projects/">Projects</a></li>
<li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33"><a href="https://www.blsurveyors.com/party-wall-surveying/">Surveying Services</a></li>
<li id="menu-item-510" class="menu-item menu-item-type-post_type_archive menu-item-object-video-gallery menu-item-510"><a href="https://www.blsurveyors.com/video-gallery/">Videos</a></li>
<li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a href="https://www.blsurveyors.com/blog/">Blog</a></li>
<li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="https://www.blsurveyors.com/contact-us/">Contact</a></li>
</ul></div>            </div>
        </nav><!-- .primary -->
        
    <div class="full-wrapper">
    	<div class="lower-footer-area">
        
        	<div class="contact-col col">
            	<h3>Contact Details</h3>
                <a href="mailto:info@blsurveyors.com" title="info@blsurveyors.com" class="email popup">info@blsurveyors.com</a>
                
				<div class="title">Head Office:</div>
                <a class="tel" href="tel:020 7935 2502">020 7935 2502</a>
            </div><!-- .contact-col -->
            
            
            
            <div class="offices-col col">
            	<h3>Our Offices</h3>
                <ul class="our-offices" style="list-style: none;">
                                	<li>
                        <div class="title">Central London</div>
                        <address>Registered Office:<br>
4 Treborough House<br>
1 Nottingham Place<br>
London, W1U 5LA<br>
</address>
                        <a class="contact" href="tel:020 7935 2502"><span>Tel:</span> 020 7935 2502</a>
                    </li>
                                	<li>
                        <div class="title">North London</div>
                        <address>Head Office:<br>
Upper Floor, <br>
61 Highgate High Street,<br>
London, N6 5JY</address>
                        <a class="contact" href="tel:020 8340 6586"><span>Tel:</span> 020 8340 6586</a>
                    </li>
                                	<li>
                        <div class="title">Surrey</div>
                        <address>Global House <br>
1 Ashley Avenue<br>
Epsom<br>
KT18 5AD<br>
</address>
                        <a class="contact" href="tel:020 3380 5596"><span>Tel:</span> 020 3380 5596</a>
                    </li>
                                </ul>
            </div><!-- .offices-col -->
            
            
            
            <div class="follow-us-col col">
            	<h3>Follow us</h3>
                <ul class="social-media"><!--
	<a class="facebook popup" href="https://www.blsurveyors.com" title="Follow us on "></a>
	-->
    <a class="twitter popup" href="https://twitter.com/Berry_Lodge" title="Follow us on Twitter"></a>
    <!--
    <a class="instagram popup" href="https://www.instagram.com/berry_lodge_surveyors/" title="Follow us on Instagram"></a>
    
    <a class="pintrest popup" href="https://www.blsurveyors.com" title="Follow us on Pintrest"></a>
    -->
    <a class="youtube popup" href="https://www.youtube.com/channel/UC4cb42sRo7g9GcXHHpMpJoA" title="Follow us on Youtube"></a>
    <a class="itunes popup" href="https://itunes.apple.com/gb/podcast/the-party-wall-podcast/id1008968042?mt=2" title="Follow us on iTunes"></a>
</ul>            </div><!-- .follow-us-col -->
            
            
            <div class="footer-terms">
            	<div class="copyright">copyright Berry Lodge Surveyors 2015. All rights reserved.</div>
                <nav class="terms">
                	<div class="menu-footer-base-menu-container"><ul id="menu-footer-base-menu" class="menu"><li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67"><a href="https://www.blsurveyors.com/terms-and-conditions/">Terms and Conditions</a></li>
<li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66"><a href="https://www.blsurveyors.com/privacy-policy/">Privacy Policy</a></li>
</ul></div>                </nav>
                <!--
                <a class="impression-logo popup" href="http://www.weareimpression.co.uk" title="Web design Yorkshire">
                    <img src="https://www.blsurveyors.com/wp-content/themes/blsurveyors/img/global/impression-logo.png" alt="Berry Lodge logo" />
                </a>
                -->
            </div>
            
        </div><!-- .lower-footer-area -->
	</div>
    </footer>




<div id="cboxOverlay" style="display: none;"></div><div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;"><div id="cboxWrapper"><div><div id="cboxTopLeft" style="float: left;"></div><div id="cboxTopCenter" style="float: left;"></div><div id="cboxTopRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxMiddleLeft" style="float: left;"></div><div id="cboxContent" style="float: left;"><div id="cboxTitle" style="float: left;"></div><div id="cboxCurrent" style="float: left;"></div><button type="button" id="cboxPrevious"></button><button type="button" id="cboxNext"></button><button id="cboxSlideshow"></button><div id="cboxLoadingOverlay" style="float: left;"></div><div id="cboxLoadingGraphic" style="float: left;"></div></div><div id="cboxMiddleRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxBottomLeft" style="float: left;"></div><div id="cboxBottomCenter" style="float: left;"></div><div id="cboxBottomRight" style="float: left;"></div></div></div><div style="position: absolute; width: 9999px; visibility: hidden; display: none;"></div></div><div id="mobile-menu" class="sidr right"><div class="sidr-inner">
        <div class="sidr-class-menu-primary-menu-container"><ul id="sidr-id-menu-primary-menu-1" class="sidr-class-menu"><li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-page sidr-class-menu-item-has-children sidr-class-menu-item-23"><a href="https://www.blsurveyors.com/about-us/">About</a>
<ul class="sidr-class-sub-menu">
    <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-page sidr-class-menu-item-1495"><a href="https://www.blsurveyors.com/about-us/clients/">Our Clients</a></li>
    <li class="sidr-class-menu-item sidr-class-menu-item-type-custom sidr-class-menu-item-object-custom sidr-class-menu-item-182"><a title="Our Testimonials" href="https://www.blsurveyors.com/testimonial/">Testimonials</a></li>
    <li class="sidr-class-menu-item sidr-class-menu-item-type-custom sidr-class-menu-item-object-custom sidr-class-menu-item-156"><a href="https://www.blsurveyors.com/podcast/">Our Podcasts</a></li>
</ul>
</li>
<li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-page sidr-class-menu-item-1617"><a href="https://www.blsurveyors.com/projects/">Projects</a></li>
<li class="sidr-class-mega-menu sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-page sidr-class-menu-item-has-children sidr-class-menu-item-24"><a href="https://www.blsurveyors.com/party-wall-surveying/">Surveying Services</a>
<ul class="sidr-class-sub-menu">
    <li class="sidr-class-bold sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-has-children sidr-class-menu-item-2894"><a href="https://www.blsurveyors.com/services/party-wall/">Party Wall</a>
    <ul class="sidr-class-sub-menu">
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2903"><a href="https://www.blsurveyors.com/services/party-wall/surveyors/">Party Wall Surveyor Roles</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2897"><a href="https://www.blsurveyors.com/services/party-wall/agreement/">Party Wall Award/Agreement</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2902"><a href="https://www.blsurveyors.com/services/party-wall/roles/">Party Wall Surveyor Types</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2901"><a href="https://www.blsurveyors.com/services/party-wall/party-wall-notices/">Party Wall Notices</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2900"><a href="https://www.blsurveyors.com/services/party-wall/interactive-guide/">Party Wall Interactive Guide</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-3122"><a href="https://www.blsurveyors.com/services/party-wall/party-wall-faq/">Party Wall FAQ</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2899"><a href="https://www.blsurveyors.com/services/party-wall/fixed-costs/">Party Wall Fixed Costs</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2898"><a href="https://www.blsurveyors.com/services/party-wall/fee-quote/">Party Wall Fee Quote</a></li>
    </ul>
</li>
    <li class="sidr-class-bold sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-has-children sidr-class-menu-item-2895"><a href="https://www.blsurveyors.com/services/surveying/">Surveying</a>
    <ul class="sidr-class-sub-menu">
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2910"><a href="https://www.blsurveyors.com/services/surveying/schedule-condition-report/">Schedule of Condition</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2904"><a href="https://www.blsurveyors.com/services/surveying/boundary-dispute/">Boundary Dispute</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2909"><a href="https://www.blsurveyors.com/services/surveying/rics-homebuyer-report/">RICS Homebuyer Report</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2906"><a href="https://www.blsurveyors.com/services/surveying/full-building-surveys/">Full Building Surveys</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2905"><a href="https://www.blsurveyors.com/services/surveying/defect-analysis-report/">Defect Analysis Report</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2911"><a href="https://www.blsurveyors.com/services/surveying/snagging-list-report/">Snagging List</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2907"><a href="https://www.blsurveyors.com/services/surveying/licence-to-alter/">Licence to Alter</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2908"><a href="https://www.blsurveyors.com/services/surveying/property-mediation/">Property Mediation</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-3022"><a href="https://www.blsurveyors.com/services/surveying/surveying-fee-quote/">Surveying Fee Quote</a></li>
    </ul>
</li>
    <li class="sidr-class-bold sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-has-children sidr-class-menu-item-2896"><a href="https://www.blsurveyors.com/services/valuation/">Valuation</a>
    <ul class="sidr-class-sub-menu">
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-3009"><a href="https://www.blsurveyors.com/services/valuation/market-valuation-2/">Market Valuation</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2912"><a href="https://www.blsurveyors.com/services/valuation/fixed-valuation-costs/">Insurance Valuation</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2914"><a href="https://www.blsurveyors.com/services/valuation/market-valuation/">Retrospective Valuation</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-3017"><a href="https://www.blsurveyors.com/services/valuation/leasehold-extension-valuation/">Leasehold Extension</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-2913"><a href="https://www.blsurveyors.com/services/valuation/leasehold-extensions-explained/">Leasehold Extensions FAQ</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-3014"><a href="https://www.blsurveyors.com/services/valuation/matrimonial-valuation/">Matrimonial Valuation</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-3013"><a href="https://www.blsurveyors.com/services/valuation/probate-valuation/">Probate Valuation</a></li>
        <li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-services sidr-class-menu-item-3023"><a href="https://www.blsurveyors.com/services/valuation/fee-quote/">Valuation Fee Quote</a></li>
    </ul>
</li>
</ul>
</li>
<li class="sidr-class-menu-item sidr-class-menu-item-type-custom sidr-class-menu-item-object-custom sidr-class-menu-item-191"><a href="https://www.blsurveyors.com/video-gallery/">Videos</a></li>
<li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-page sidr-class-menu-item-27"><a href="https://www.blsurveyors.com/blog/">Blog</a></li>
<li class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-page sidr-class-menu-item-28"><a href="https://www.blsurveyors.com/contact-us/">Contact</a></li>
</ul></div>    </div>
</div>