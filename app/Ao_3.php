<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ao_3 extends Model
{
    protected $table='ao_3';
    public function getCreatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value)); 
    }
    public function getUpdatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value));   
    }
    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
