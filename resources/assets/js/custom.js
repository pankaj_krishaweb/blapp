
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap1');

window.Vue = require('vue');
var moment = require('moment');

// import Vuetify from 'vuetify'

// Vue.use(Vuetify)

// import 'vuetify/dist/vuetify.min.css'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const VueUploadComponent = require('vue-upload-component')
Vue.component('file-upload', VueUploadComponent)

// const app = new Vue({
//     el: '#app'
// });

Vue.component('image-component', require('./components/ImageuploadComponent.vue'));
Vue.component('chat-message', require('./components/ChatMessage.vue'));
Vue.component('chat-log', require('./components/ChatLog.vue'));
Vue.component('chat-composer', require('./components/ChatComposer.vue'));
const app = new Vue({
    el: '#app',
    data: {
        messages: [],
        usersInRoom: [],
        jobid: '',
   
    },
    methods: {
        addMessage(message) {
            // Add to existing messages
            if(message.image){
                
            message.created_at=moment(new Date()).format("DD-MM-YYYY HH:mm:ss");
            // console.log("thismessages:"+JSON.stringify(this.messages))
            this.messages.push(message);

            // Persist to the database etc
            message.jobid =this.jobid;
            }
            else{
                message.created_at=moment(new Date()).format("DD-MM-YYYY HH:mm:ss");
                // console.log("thismessages:"+JSON.stringify(this.messages))
                this.messages.push(message);
                
                // Persist to the database etc
                message.jobid =this.jobid;
            axios.post('/admin/messages', message).then(response => {
                // Do whatever;
            })
            $(".chat-log").animate({ scrollTop: $('.chat-log').prop("scrollHeight")}, 1000); 
            }
        }
    },
    created() {
        var array = window.location.href.split('/');
        var lastsegment = array[array.length-1];
        if(lastsegment == "message" || lastsegment == "client"){
            this.jobid = $('#jobid').data('jobid');
        }
        else{

        	this.jobid = lastsegment;
        }
 		// console.log("before get"+lastsegment)
        axios.get('/admin/messages/'+this.jobid).then(response => {
            this.messages = response.data;
        });
 

        Echo.join(`chatroom.${this.jobid}`)
            .here((users) => {
                this.usersInRoom = users;
            })
            .joining((user) => {
                this.usersInRoom.push(user);
            })
            .leaving((user) => {
                this.usersInRoom = this.usersInRoom.filter(u => u != user)
            })
            .listen('MessagePosted', (e) => {
                console.log(e);
                    if (e.message.message == "upload file") {
                        this.messages.push({
                            image: e.message.image,
                            who: 'bubble-left',
                            user: e.user
                        });
                    }
                    else{
                        this.messages.push({
                            message: e.message.message,
                            who: 'bubble-left',
                            user: e.user
                        });
                    }
                
            });
    }
});
