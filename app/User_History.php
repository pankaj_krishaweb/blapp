<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class User_History extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'action','description','login_ip','location'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getCreatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value));
    
    }
    public function getUpdatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value));
    
    }
}
