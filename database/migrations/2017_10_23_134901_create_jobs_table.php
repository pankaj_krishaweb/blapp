<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
                $table->increments('id');
                $table->string('job_no')->nullable();
                $table->string('invoice_no')->nullable();
                $table->string('notice_costs')->nullable();
                $table->string('award_costs')->nullable();
                $table->string('land_registry_costs')->nullable();
                $table->string('printing_postage_costs')->nullable();
              $table->string('vat_amount')->nullable();
              $table->string('final_amount')->nullable();
                $table->string('surveyor_who_had_first_contact_with_owner')->nullable();
                 $table->string('surveyor_dealing_with_day_to_day')->nullable();
                 $table->string('party_wall_notice_date')->nullable();
                $table->string('ten_4_party_wall_notice_date')->nullable();
                $table->string('ten_4_party_wall_notice_expiry_date')->nullable();
                $table->string('schedule_of_condition_date')->nullable();
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
