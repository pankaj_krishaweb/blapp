@extends('layouts.frame')
@section('content')

<div class="container-fluid">
    <div class="container">
    	<div class="row">
	       	<div class="col-md-12">
		  	   	<h1>Our Community</h1> 
		  	   	<ol class="breadcrumb"> 
                    <li><a href="{{ url('admin') }}">Admin</a></li>
                    <li>Our Community</li>    
              	</ol>
		    </div>
		    {{-- <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit"> --}}
		    <a class="wpcf7-form-control wpcf7-submit" href="{!! url('admin/our-community/create') !!}">Add</a>
			<div class="col-md-9 col-sm-9 col-xs-12">
	  			<div class="table-responsive table-bordered">
		  			<table class="table ">
				    	@if($datas->count() > 0)
				    	<thead>
				      		<tr>
				      			<th>ID</th>
				        		<th>Name</th>
				      			<th>Options</th>
				      		</tr>
				    	</thead>
				    	<tbody id="users_list">
				    		@foreach($datas as $data)
				      			<tr>
				      				<td>{{ $data->id }}</td>
						        	<td>{{ $data->name }}</td>				        		
						        	<td>
						        		<a href="our-community/{{$data->id}}/edit" class="btn btn-xs btn-info user-edit-btn">Edit</a>
			                            <a href="our-community/{{$data->id}}" class="btn btn-primary btn-xs">View</a>
			                            <button type="button" class="btn btn-xs btn-danger delete-user-btn" data-id={{ $data->id }}>Delete</button>
						        	</td>
				      			</tr>
				      		@endforeach
				    	</tbody>
			      		@else
			      			<center>No Our Community</center>
			      		@endif
					</table>
				</div>
			</div>
		</div>
	</div>
</div><br>

@endsection
@section('script')
<script type="text/javascript">
	$('.delete-user-btn').on('click',  function(event) {
		event.preventDefault();
		var id = $(this).data('id');
		swal("Are you sure you wish to delete this Our Community?", {
	  buttons: ["Cancel", "Yes Delete User!"],
	}).then(function(value){
		if(value==true){
			$.ajax({
				url: '/admin/our-community/'+ id,
				type: 'DELETE',
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
				data: {id: id},
				success: function(data){
					//console.log(data);
					var status = $.parseJSON(data);
					if(status.status=='good'){
						location.reload();			
					}
				},
			})		
		}
		return false;
	}, 
		function(){
			console.log('Not deleted');
			return false;
		});
	});
</script>
@endsection

