@extends('layouts.frame')

@section('content')
<style>


#job-search-btn{

    background-color: #fff;
    color: #b3b3b3;
    border: solid 0.5px #b3b3b3;
    width: 250px;
    border-radius: 0px;
    text-align: left;
  }
  .open > .dropdown-menu {
    display: block;
    width: 100%;

    margin-top: 0px;
    border-radius: 0px;
}
#myInput{
    border-right: none;
    border-left: none;
    border-top: none;
    border-radius: 0px;
    margin-top: -6px;
}
.loder-img {
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0;
    right: 0;
    background: rgb(63, 186, 166,0.5);
    z-index: 123;
}
.loder-img img{
    position: absolute;
    left: 0;
    top: 50%;
    right: 0;
    margin: 0 auto;
    margin-top: -50px;
}
@media (min-width: 1200px){
    .container {
        width: 100% !important;
        max-width: 1170px !important;
        margin: 0 auto !important;
    }
}

.user-chat-box .user-box{
    text-align: center;
}
.user-chat-box .user-box{
    padding-bottom: 20px
}
.user-chat-box .user-box img{
    margin-bottom: 15px;
}
.user-chat-box .user-box p,
.user-chat-box p.message-sub-title{
    font-family: "Raleway", sans-serif;
    font-size: 20px;
    line-height: 1.4;
    font-weight: 600;
    color: #414861;
    margin-bottom: 0;
}
.user-chat-box .user-box p:last-child,
.user-chat-box p.message-sub-title{
    margin-bottom: 20px;
}

</style>
<div id="app">
    <div class="loder-img" style="display: none;"><img src="{{ asset('images/loading.gif') }}" style="height: 100px;"></div>
    <div class="container-fluid message-page-wrap">
        <div class="container">
            <div class="row user-chat-box">   
                <h1 id="jobid" data-jobid="{{$jobid}}">Messaging</h1>
                @if(Auth::user()->role == 5)
                    <input type="hidden" name="" id="chatcolor" value="{!! Auth::user()->chatcolor !!}">
                    <p class="message-sub-title">Who’s in the Messaging Forum:</p>
                <input type="hidden" name="userChatColor" id="userChatColor" value="{!! Auth::user()->chatcolor !!}">
                    
                    <div class="container">
                        @php 
                            $users = [];
                            $job = App\Job::find($jobid);
                            foreach ($job->users as $user) {
                                // if ($user->role == 5 || $user->role == 7) {
                                if ($user->role == 5 ) {
                                    array_push($users, $user);
                                }
                            }
                        @endphp
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            @foreach($users as $user)
                                <div class="col-md-3 col-sm-6 col-xs-9 user-box">
                                    @if($user->picture)
                                        <img src="/{{ $user->picture }}" style="height: 150px; width: 150px; border: 6px solid; border-color:{!! $user->chatcolor !!};">
                                    @else
                                        <img src="/images/default_profile.png" style="height: 150px; width: 150px; border: 6px solid; border-color:{!! $user->chatcolor !!};">
                                    @endif
                                        <p>{!! $user->name !!}</p>
                                        <p>{!! $user->credentials !!}</p>
                                </div>
                            @endforeach
                        </div>
                </div>
                @endif
                  <div class="col-md-12 col-sm-12 col-xs-12" style="padding: 20px;">
                    <span class="pull-right">
                        @if (Auth::user()->jobs->count()>1)
                            <div class="dropdown">
                                <button class="btn btnblock dropdown-toggle" type="button" data-toggle="dropdown" id="job-search-btn">
                                Select Job
                                    <span class="pull-right"><span class="caret "></span></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <input class="form-control" id="myInput" type="text" placeholder="Search..">
                                    @foreach (Auth::user()->jobs as $job)
                                       <li><a href="{{ url('/client/message/'.$job->id) }}">BLSN{{ $job->id}}</a></li>
                                    @endforeach
                                </ul>   
                            </div>
                        @endif
                    </span>
                </div>
                 

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <chat-log :messages="messages"></chat-log>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid" style="background-color: #414861">
        <div class="container">
            <p class="text-center" style="margin-top: 20px; font-size: 20px; color: #85939f;"><strong>Reply to the conversation here ...</strong></p>
            <chat-composer v-on:messagesent="addMessage"></chat-composer>
        </div>
    </div>
</div>

 <div class="modal fade" id="introModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Intro video</h4>
        </div>
        <div class="modal-body">
            <video width="100%" id="vid" controls>
              <source src="{{ asset('video/intro.mp4') }}" type="video/mp4">
             
              Your browser does not support HTML5 video.
            </video>
        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

@endsection
@section('script')

<script src="{{ asset('js/custom.js') }}" ></script>
<script type="text/javascript">
    $(".chat-log").animate({ scrollTop: $('.chat-log').prop("scrollHeight")}, 1000); 
    localStorage.setItem('jobid', '{{$jobid}}');

    $(document).on('click', '#intro_video', function(event) {
        event.preventDefault();
        jQuery.noConflict();
        $("#introModal").modal('show');

        $("#introModal").on('shown.bs.modal', function () {
            $('#vid').get(0).play();

            
        });
    });


    $(".chat-log").animate({ scrollTop: $('.chat-log').prop("scrollHeight")+3000}, 1000); 

</script>

@endsection

