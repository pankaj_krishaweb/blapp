<?php

namespace App\Src\My_Jobs;

use App\Src\My_Jobs\JobsInterface;
use App\{Job,Ao_1, Ao_2, Ao_3, Ao_4, Ao_5, Ao_6, Ao_7, Ao_8, Ao_9, Ao_10, Bo, Job_Tasks, Task, Payment, Surveying};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class  Partywall implements JobsInterface
{

	public $message;

	public function __construct($id=null, Request $request)
	{
    $this->request=$request;
		if ($id) {
			$this->update($id);
		}
		else{
			$this->create();
		}
	}

	// update Job
	public function update($id)
	{
		$model = Job::find($id);
    $this->message= 'Updated successfully!';	
    $bo=  Bo::where('job_id',$job->id)->first();
    $this->bo($model);
    $ao=  Ao_1::where('job_id',$job->id)->first();
    $this->ao($model);
    $ao2=  Ao_2::where('job_id',$job->id)->first();	
    $this->ao2($model);
    $ao3=  Ao_3::where('job_id',$job->id)->first(); 
    $this->ao3($model);
    $ao4=  Ao_4::where('job_id',$job->id)->first(); 
    $this->ao4($model);
    $ao5=  Ao_5::where('job_id',$job->id)->first(); 
    $this->ao5($model);
    $ao6=  Ao_6::where('job_id',$job->id)->first(); 
    $this->ao6($model);
    $ao7=  Ao_7::where('job_id',$job->id)->first(); 
    $this->ao7($model);
    $ao8=  Ao_8::where('job_id',$job->id)->first(); 
    $this->ao8($model);
    $ao9=  Ao_9::where('job_id',$job->id)->first(); 
    $this->ao9($model);
    $ao10=  Ao_10::where('job_id',$job->id)->first(); 
    $this->ao10($model);
    return back()->with('message', 'Updated successfully!');
	}

	// create job
	public function create()
	{
		$model = new Job;    
	  $message= 'Add a client to the job !';   
	  $this->date($model);	
    # create a folder for the job
    Storage::makeDirectory("public/jobs/{$this->job->id}/my-jobs/");
    Storage::makeDirectory("public/jobs/{$this->job->id}/jobs/");

    $bo= new Bo;
    $bo->job_id = $this->job->id;
    $this->bo($model);

    $ao = new Ao_1;
    $ao->job_id = $this->job->id;
    $this->ao($model);

    $ao2 = new Ao_2;
    $ao2->job_id = $this->job->id;
    $this->ao2($model);

    $ao3 = new Ao_3;
    $ao3->job_id = $this->job->id;
    $this->ao3($model); 

    $ao4 = new Ao_4;
    $ao4->job_id = $this->job->id;
    $this->ao4($model); 

    $ao5 = new Ao_5;
    $ao5->job_id = $this->job->id;
    $this->ao5($model); 

    $ao6 = new Ao_6;
    $ao6->job_id = $this->job->id;
    $this->ao6($model); 

    $ao7 = new Ao_7;
    $ao7->job_id = $this->job->id;
    $this->ao7($model); 

    $ao8 = new Ao_8;
    $ao8->job_id = $this->job->id;
    $this->ao8($model); 

    $ao9 = new Ao_9;
    $ao9->job_id = $this->job->id;
    $this->ao9($model); 

    $ao10 = new Ao_10;
    $ao10->job_id = $this->job->id;
    $this->ao10($model);    
    return redirect('/admin/my-jobs/users/'.$job->id)->with('message', $message);   
	}

	// create Job
	public function data($model, Request $request)
	{  	
		$job=$model;
		$job->invoice_no = $request->invoice_no;
        $job->job_type = $request->job_type; 
		$job->notice_costs =$request->notice_costs;
		$job->award_costs =$request->award_costs;
		$job->land_registry_costs =$request->land_registry_costs;
		$job->printing_postage_costs =$request->printing_postage_costs;
		$job->vat_amount =$request->vat_amount;
		$job->final_amount =$request->final_amount;
		$job->surveyor_who_had_first_contact_with_owner =$request->surveyor_who_had_first_contact_with_owner;
		$job->surveyor_dealing_with_day_to_day =$request->surveyor_dealing_with_day_to_day;
		$job->party_wall_notice_date =$request->party_wall_notice_date;
		$job->ten_4_party_wall_notice_date =$request->ten_4_party_wall_notice_date;
		$job->ten_4_party_wall_notice_expiry_date =$request->ten_4_party_wall_notice_expiry_date;
		$job->schedule_of_condition_date =$request->schedule_of_condition_date;
    $job->save();
    $this->job=$job;       
	}

  public function bo($model, Request $request)
  {
    $job=$model;
    $job->bo->surveyor_full_information=$request->surveyor_full_information;
    $job->bo->surveyor_name=$request->surveyor_name;
    $job->bo->surveyor_qualifications=$request->surveyor_qualifications;
    $job->bo->surveyor_company_name=$request->surveyor_company_name;
    $job->bo->surveyor_company_address=$request->surveyor_company_address;
    $job->bo->surveyor_contact_details=$request->surveyor_contact_details;
    $job->bo->full_name=$request->full_name;
    $job->bo->salutation=$request->salutation;
    $job->bo->property_address_proposed_work=$request->property_address_proposed_work;
    $job->bo->contact_address=$request->contact_address;
    $job->bo->contact_details_telephone_numbers_email=$request->contact_details_telephone_numbers_email;
    $job->bo->owner_referral=$request->owner_referral;
    $job->bo->has_appointed_have_appointed=$request->has_appointed_have_appointed;
    $job->bo->owners_owners=$request->owners_owners;
    $job->bo->is_are=$request->is_are;
    $job->bo->i_we_referral_upper_case=$request->i_we_referral_upper_case;
    $job->bo->i_we_referral_lower_case=$request->i_we_referral_lower_case;
    $job->bo->i_we_referral_upper_case_2=$request->i_we_referral_upper_case_2;
    $job->bo->my_our_referral=$request->my_our_referral;
    $job->bo->he_she_they_referral=$request->he_she_they_referral;
    $job->bo->intend_intends=$request->intend_intends;
    $job->bo->s_s=$request->s_s;
    $job->bo->carry_carries=$request->carry_carries;
    $job->bo->his_her_their=$request->his_her_their;
    $job->bo->save();  
  }

  public function ao($model, Request $request)
  {
    $job=$model;
    $job->ao->surveyor_full_information = $request->a_surveyor_full_information;
    $job->ao->surveyor_name = $request->a_surveyor_name;
    $job->ao->surveyor_qualifications = $request->a_surveyor_qualifications;
    $job->ao->surveyor_company_name = $request->a_surveyor_company_name;
    $job->ao->surveyor_company_address = $request->a_surveyor_company_address;
    $job->ao->surveyor_contact_details = $request->a_surveyor_contact_details;
    $job->ao->full_names = $request->a_full_names;
    $job->ao->salutation = $request->a_salutation;
    $job->ao->property_address_adjoining = $request->a_property_address_adjoining;
    $job->ao->contact_address = $request->a_contact_address;
    $job->ao->contact_details = $request->a_contact_details;
    $job->ao->owners_referral = $request->a_owners_referral;
    $job->ao->has_appointed_have_appointed = $request->a_has_appointed_have_appointed;
    $job->ao->i_we_referral = $request->a_i_we_referral;
    $job->ao->i_we_referral_lower = $request->a_i_we_referral_lower;
    $job->ao->my_our_refferal = $request->a_my_our_refferal;
    $job->ao->he_she_they_referral = $request->a_he_she_they_referral;
    $job->ao->his_her_their = $request->a_his_her_their;
    $job->ao->owners_owners = $request->a_owners_owners;
    $job->ao->is_an_are = $request->a_is_an_are;
    $job->ao->s_s = $request->a_s_s;
    $job->ao->s1_section = $request->a_s1_section;
    $job->ao->s1_description = $request->a_s1_description;
    $job->ao->s2_section = $request->a_s2_section;
    $job->ao->s2_description = $request->a_s2_description;
    $job->ao->s6_section = $request->a_s6_section;
    $job->ao->s6_description = $request->a_s6_description;
    $job->ao->date_of_notice = $request->a_date_of_notice;
    $job->ao->notice_notices = $request->a_notice_notices;
    $job->ao->section_sections = $request->a_section_sections;
    $job->ao->drawings = $request->a_drawings;
    $job->ao->soc_date = $request->a_soc_date;
    $job->ao->third_surveyor = $request->a_third_surveyor;
    $job->ao->save();
  }

  public function ao3($model, Request $request)
  {
    $job=$model;       
    $job->ao3->surveyor_full_information = $request->ao_3_surveyor_full_information;
    $job->ao3->surveyor_name = $request->ao_3_surveyor_name;
    $job->ao3->surveyor_qualifications = $request->ao_3_surveyor_qualifications;
    $job->ao3->surveyor_company_name = $request->ao_3_surveyor_company_name;
    $job->ao3->surveyor_company_address = $request->ao_3_surveyor_company_address;
    $job->ao3->surveyor_contact_details = $request->ao_3_surveyor_contact_details;
    $job->ao3->full_names = $request->ao_3_full_names;
    $job->ao3->salutation = $request->ao_3_salutation;
    $job->ao3->property_address_adjoining = $request->ao_3_property_address_adjoining;
    $job->ao3->contact_address = $request->ao_3_contact_address;
    $job->ao3->contact_details = $request->ao_3_contact_details;
    $job->ao3->owners_referral = $request->ao_3_owners_referral;
    $job->ao3->has_appointed_have_appointed = $request->ao_3_has_appointed_have_appointed;
    $job->ao3->i_we_referral = $request->ao_3_i_we_referral;
    $job->ao3->i_we_referral_lower = $request->ao_3_i_we_referral_lower;
    $job->ao3->my_our_refferal = $request->ao_3_my_our_refferal;
    $job->ao3->he_she_they_referral = $request->ao_3_he_she_they_referral;
    $job->ao3->his_her_their = $request->ao_3_his_her_their;
    $job->ao3->owners_owners = $request->ao_3_owners_owners;
    $job->ao3->is_an_are = $request->ao_3_is_an_are;
    $job->ao3->s_s = $request->ao_3_s_s;
    $job->ao3->s1_section = $request->ao_3_s1_section;
    $job->ao3->s1_description = $request->ao_3_s1_description;
    $job->ao3->s2_section = $request->ao_3_s2_section;
    $job->ao3->s2_description = $request->ao_3_s2_description;
    $job->ao3->s6_section = $request->ao_3_s6_section;
    $job->ao3->s6_description = $request->ao_3_s6_description;
    $job->ao3->date_of_notice = $request->ao_3_date_of_notice;
    $job->ao3->notice_notices = $request->ao_3_notice_notices;
    $job->ao3->section_sections = $request->ao_3_section_sections;
    $job->ao3->drawings = $request->ao_3_drawings;
    $job->ao3->soc_date = $request->ao_3_soc_date;
    $job->ao3->third_surveyor = $request->ao_3_third_surveyor;
    $job->ao3->save();
  }

  public function ao4($model, Request $request)
  {
    $job=$model;       
    $job->ao4->surveyor_full_information = $request->ao_4_surveyor_full_information;
    $job->ao4->surveyor_name = $request->ao_4_surveyor_name;
    $job->ao4->surveyor_qualifications = $request->ao_4_surveyor_qualifications;
    $job->ao4->surveyor_company_name = $request->ao_4_surveyor_company_name;
    $job->ao4->surveyor_company_address = $request->ao_4_surveyor_company_address;
    $job->ao4->surveyor_contact_details = $request->ao_4_surveyor_contact_details;
    $job->ao4->full_names = $request->ao_4_full_names;
    $job->ao4->salutation = $request->ao_4_salutation;
    $job->ao4->property_address_adjoining = $request->ao_4_property_address_adjoining;
    $job->ao4->contact_address = $request->ao_4_contact_address;
    $job->ao4->contact_details = $request->ao_4_contact_details;
    $job->ao4->owners_referral = $request->ao_4_owners_referral;
    $job->ao4->has_appointed_have_appointed = $request->ao_4_has_appointed_have_appointed;
    $job->ao4->i_we_referral = $request->ao_4_i_we_referral;
    $job->ao4->i_we_referral_lower = $request->ao_4_i_we_referral_lower;
    $job->ao4->my_our_refferal = $request->ao_4_my_our_refferal;
    $job->ao4->he_she_they_referral = $request->ao_4_he_she_they_referral;
    $job->ao4->his_her_their = $request->ao_4_his_her_their;
    $job->ao4->owners_owners = $request->ao_4_owners_owners;
    $job->ao4->is_an_are = $request->ao_4_is_an_are;
    $job->ao4->s_s = $request->ao_4_s_s;
    $job->ao4->s1_section = $request->ao_4_s1_section;
    $job->ao4->s1_description = $request->ao_4_s1_description;
    $job->ao4->s2_section = $request->ao_4_s2_section;
    $job->ao4->s2_description = $request->ao_4_s2_description;
    $job->ao4->s6_section = $request->ao_4_s6_section;
    $job->ao4->s6_description = $request->ao_4_s6_description;
    $job->ao4->date_of_notice = $request->ao_4_date_of_notice;
    $job->ao4->notice_notices = $request->ao_4_notice_notices;
    $job->ao4->section_sections = $request->ao_4_section_sections;
    $job->ao4->drawings = $request->ao_4_drawings;
    $job->ao4->soc_date = $request->ao_4_soc_date;
    $job->ao4->third_surveyor = $request->ao_4_third_surveyor;
    $job->ao4->save();
  }
  public function ao5($model, Request $request)
  {
    $job=$model;       
    $job->ao5->surveyor_full_information = $request->ao_5_surveyor_full_information;
    $job->ao5->surveyor_name = $request->ao_5_surveyor_name;
    $job->ao5->surveyor_qualifications = $request->ao_5_surveyor_qualifications;
    $job->ao5->surveyor_company_name = $request->ao_5_surveyor_company_name;
    $job->ao5->surveyor_company_address = $request->ao_5_surveyor_company_address;
    $job->ao5->surveyor_contact_details = $request->ao_5_surveyor_contact_details;
    $job->ao5->full_names = $request->ao_5_full_names;
    $job->ao5->salutation = $request->ao_5_salutation;
    $job->ao5->property_address_adjoining = $request->ao_5_property_address_adjoining;
    $job->ao5->contact_address = $request->ao_5_contact_address;
    $job->ao5->contact_details = $request->ao_5_contact_details;
    $job->ao5->owners_referral = $request->ao_5_owners_referral;
    $job->ao5->has_appointed_have_appointed = $request->ao_5_has_appointed_have_appointed;
    $job->ao5->i_we_referral = $request->ao_5_i_we_referral;
    $job->ao5->i_we_referral_lower = $request->ao_5_i_we_referral_lower;
    $job->ao5->my_our_refferal = $request->ao_5_my_our_refferal;
    $job->ao5->he_she_they_referral = $request->ao_5_he_she_they_referral;
    $job->ao5->his_her_their = $request->ao_5_his_her_their;
    $job->ao5->owners_owners = $request->ao_5_owners_owners;
    $job->ao5->is_an_are = $request->ao_5_is_an_are;
    $job->ao5->s_s = $request->ao_5_s_s;
    $job->ao5->s1_section = $request->ao_5_s1_section;
    $job->ao5->s1_description = $request->ao_5_s1_description;
    $job->ao5->s2_section = $request->ao_5_s2_section;
    $job->ao5->s2_description = $request->ao_5_s2_description;
    $job->ao5->s6_section = $request->ao_5_s6_section;
    $job->ao5->s6_description = $request->ao_5_s6_description;
    $job->ao5->date_of_notice = $request->ao_5_date_of_notice;
    $job->ao5->notice_notices = $request->ao_5_notice_notices;
    $job->ao5->section_sections = $request->ao_5_section_sections;
    $job->ao5->drawings = $request->ao_5_drawings;
    $job->ao5->soc_date = $request->ao_5_soc_date;
    $job->ao5->third_surveyor = $request->ao_5_third_surveyor;
    $job->ao5->save();
  }
  public function ao6($model, Request $request)
  {
    $job=$model;       
    $job->ao6->surveyor_full_information = $request->ao_6_surveyor_full_information;
    $job->ao6->surveyor_name = $request->ao_6_surveyor_name;
    $job->ao6->surveyor_qualifications = $request->ao_6_surveyor_qualifications;
    $job->ao6->surveyor_company_name = $request->ao_6_surveyor_company_name;
    $job->ao6->surveyor_company_address = $request->ao_6_surveyor_company_address;
    $job->ao6->surveyor_contact_details = $request->ao_6_surveyor_contact_details;
    $job->ao6->full_names = $request->ao_6_full_names;
    $job->ao6->salutation = $request->ao_6_salutation;
    $job->ao6->property_address_adjoining = $request->ao_6_property_address_adjoining;
    $job->ao6->contact_address = $request->ao_6_contact_address;
    $job->ao6->contact_details = $request->ao_6_contact_details;
    $job->ao6->owners_referral = $request->ao_6_owners_referral;
    $job->ao6->has_appointed_have_appointed = $request->ao_6_has_appointed_have_appointed;
    $job->ao6->i_we_referral = $request->ao_6_i_we_referral;
    $job->ao6->i_we_referral_lower = $request->ao_6_i_we_referral_lower;
    $job->ao6->my_our_refferal = $request->ao_6_my_our_refferal;
    $job->ao6->he_she_they_referral = $request->ao_6_he_she_they_referral;
    $job->ao6->his_her_their = $request->ao_6_his_her_their;
    $job->ao6->owners_owners = $request->ao_6_owners_owners;
    $job->ao6->is_an_are = $request->ao_6_is_an_are;
    $job->ao6->s_s = $request->ao_6_s_s;
    $job->ao6->s1_section = $request->ao_6_s1_section;
    $job->ao6->s1_description = $request->ao_6_s1_description;
    $job->ao6->s2_section = $request->ao_6_s2_section;
    $job->ao6->s2_description = $request->ao_6_s2_description;
    $job->ao6->s6_section = $request->ao_6_s6_section;
    $job->ao6->s6_description = $request->ao_6_s6_description;
    $job->ao6->date_of_notice = $request->ao_6_date_of_notice;
    $job->ao6->notice_notices = $request->ao_6_notice_notices;
    $job->ao6->section_sections = $request->ao_6_section_sections;
    $job->ao6->drawings = $request->ao_6_drawings;
    $job->ao6->soc_date = $request->ao_6_soc_date;
    $job->ao6->third_surveyor = $request->ao_6_third_surveyor;
    $job->ao6->save();
  }
  public function ao7($model, Request $request)
  {
    $job=$model;       
    $job->ao7->surveyor_full_information = $request->ao_7_surveyor_full_information;
    $job->ao7->surveyor_name = $request->ao_7_surveyor_name;
    $job->ao7->surveyor_qualifications = $request->ao_7_surveyor_qualifications;
    $job->ao7->surveyor_company_name = $request->ao_7_surveyor_company_name;
    $job->ao7->surveyor_company_address = $request->ao_7_surveyor_company_address;
    $job->ao7->surveyor_contact_details = $request->ao_7_surveyor_contact_details;
    $job->ao7->full_names = $request->ao_7_full_names;
    $job->ao7->salutation = $request->ao_7_salutation;
    $job->ao7->property_address_adjoining = $request->ao_7_property_address_adjoining;
    $job->ao7->contact_address = $request->ao_7_contact_address;
    $job->ao7->contact_details = $request->ao_7_contact_details;
    $job->ao7->owners_referral = $request->ao_7_owners_referral;
    $job->ao7->has_appointed_have_appointed = $request->ao_7_has_appointed_have_appointed;
    $job->ao7->i_we_referral = $request->ao_7_i_we_referral;
    $job->ao7->i_we_referral_lower = $request->ao_7_i_we_referral_lower;
    $job->ao7->my_our_refferal = $request->ao_7_my_our_refferal;
    $job->ao7->he_she_they_referral = $request->ao_7_he_she_they_referral;
    $job->ao7->his_her_their = $request->ao_7_his_her_their;
    $job->ao7->owners_owners = $request->ao_7_owners_owners;
    $job->ao7->is_an_are = $request->ao_7_is_an_are;
    $job->ao7->s_s = $request->ao_7_s_s;
    $job->ao7->s1_section = $request->ao_7_s1_section;
    $job->ao7->s1_description = $request->ao_7_s1_description;
    $job->ao7->s2_section = $request->ao_7_s2_section;
    $job->ao7->s2_description = $request->ao_7_s2_description;
    $job->ao7->s6_section = $request->ao_7_s6_section;
    $job->ao7->s6_description = $request->ao_7_s6_description;
    $job->ao7->date_of_notice = $request->ao_7_date_of_notice;
    $job->ao7->notice_notices = $request->ao_7_notice_notices;
    $job->ao7->section_sections = $request->ao_7_section_sections;
    $job->ao7->drawings = $request->ao_7_drawings;
    $job->ao7->soc_date = $request->ao_7_soc_date;
    $job->ao7->third_surveyor = $request->ao_7_third_surveyor;
    $job->ao7->save();
  }
  public function ao8($model, Request $request)
  {
    $job=$model;       
    $job->ao8->surveyor_full_information = $request->ao_8_surveyor_full_information;
    $job->ao8->surveyor_name = $request->ao_8_surveyor_name;
    $job->ao8->surveyor_qualifications = $request->ao_8_surveyor_qualifications;
    $job->ao8->surveyor_company_name = $request->ao_8_surveyor_company_name;
    $job->ao8->surveyor_company_address = $request->ao_8_surveyor_company_address;
    $job->ao8->surveyor_contact_details = $request->ao_8_surveyor_contact_details;
    $job->ao8->full_names = $request->ao_8_full_names;
    $job->ao8->salutation = $request->ao_8_salutation;
    $job->ao8->property_address_adjoining = $request->ao_8_property_address_adjoining;
    $job->ao8->contact_address = $request->ao_8_contact_address;
    $job->ao8->contact_details = $request->ao_8_contact_details;
    $job->ao8->owners_referral = $request->ao_8_owners_referral;
    $job->ao8->has_appointed_have_appointed = $request->ao_8_has_appointed_have_appointed;
    $job->ao8->i_we_referral = $request->ao_8_i_we_referral;
    $job->ao8->i_we_referral_lower = $request->ao_8_i_we_referral_lower;
    $job->ao8->my_our_refferal = $request->ao_8_my_our_refferal;
    $job->ao8->he_she_they_referral = $request->ao_8_he_she_they_referral;
    $job->ao8->his_her_their = $request->ao_8_his_her_their;
    $job->ao8->owners_owners = $request->ao_8_owners_owners;
    $job->ao8->is_an_are = $request->ao_8_is_an_are;
    $job->ao8->s_s = $request->ao_8_s_s;
    $job->ao8->s1_section = $request->ao_8_s1_section;
    $job->ao8->s1_description = $request->ao_8_s1_description;
    $job->ao8->s2_section = $request->ao_8_s2_section;
    $job->ao8->s2_description = $request->ao_8_s2_description;
    $job->ao8->s6_section = $request->ao_8_s6_section;
    $job->ao8->s6_description = $request->ao_8_s6_description;
    $job->ao8->date_of_notice = $request->ao_8_date_of_notice;
    $job->ao8->notice_notices = $request->ao_8_notice_notices;
    $job->ao8->section_sections = $request->ao_8_section_sections;
    $job->ao8->drawings = $request->ao_8_drawings;
    $job->ao8->soc_date = $request->ao_8_soc_date;
    $job->ao8->third_surveyor = $request->ao_8_third_surveyor;
    $job->ao8->save();
  }
  public function ao9($model, Request $request)
  {
    $job=$model;       
    $job->ao9->surveyor_full_information = $request->ao_9_surveyor_full_information;
    $job->ao9->surveyor_name = $request->ao_9_surveyor_name;
    $job->ao9->surveyor_qualifications = $request->ao_9_surveyor_qualifications;
    $job->ao9->surveyor_company_name = $request->ao_9_surveyor_company_name;
    $job->ao9->surveyor_company_address = $request->ao_9_surveyor_company_address;
    $job->ao9->surveyor_contact_details = $request->ao_9_surveyor_contact_details;
    $job->ao9->full_names = $request->ao_9_full_names;
    $job->ao9->salutation = $request->ao_9_salutation;
    $job->ao9->property_address_adjoining = $request->ao_9_property_address_adjoining;
    $job->ao9->contact_address = $request->ao_9_contact_address;
    $job->ao9->contact_details = $request->ao_9_contact_details;
    $job->ao9->owners_referral = $request->ao_9_owners_referral;
    $job->ao9->has_appointed_have_appointed = $request->ao_9_has_appointed_have_appointed;
    $job->ao9->i_we_referral = $request->ao_9_i_we_referral;
    $job->ao9->i_we_referral_lower = $request->ao_9_i_we_referral_lower;
    $job->ao9->my_our_refferal = $request->ao_9_my_our_refferal;
    $job->ao9->he_she_they_referral = $request->ao_9_he_she_they_referral;
    $job->ao9->his_her_their = $request->ao_9_his_her_their;
    $job->ao9->owners_owners = $request->ao_9_owners_owners;
    $job->ao9->is_an_are = $request->ao_9_is_an_are;
    $job->ao9->s_s = $request->ao_9_s_s;
    $job->ao9->s1_section = $request->ao_9_s1_section;
    $job->ao9->s1_description = $request->ao_9_s1_description;
    $job->ao9->s2_section = $request->ao_9_s2_section;
    $job->ao9->s2_description = $request->ao_9_s2_description;
    $job->ao9->s6_section = $request->ao_9_s6_section;
    $job->ao9->s6_description = $request->ao_9_s6_description;
    $job->ao9->date_of_notice = $request->ao_9_date_of_notice;
    $job->ao9->notice_notices = $request->ao_9_notice_notices;
    $job->ao9->section_sections = $request->ao_9_section_sections;
    $job->ao9->drawings = $request->ao_9_drawings;
    $job->ao9->soc_date = $request->ao_9_soc_date;
    $job->ao9->third_surveyor = $request->ao_9_third_surveyor;
    $job->ao9->save();
  }
  public function ao10($model, Request $request)
  {
    $job=$model;       
    $job->ao10->surveyor_full_information = $request->ao_10_surveyor_full_information;
    $job->ao10->surveyor_name = $request->ao_10_surveyor_name;
    $job->ao10->surveyor_qualifications = $request->ao_10_surveyor_qualifications;
    $job->ao10->surveyor_company_name = $request->ao_10_surveyor_company_name;
    $job->ao10->surveyor_company_address = $request->ao_10_surveyor_company_address;
    $job->ao10->surveyor_contact_details = $request->ao_10_surveyor_contact_details;
    $job->ao10->full_names = $request->ao_10_full_names;
    $job->ao10->salutation = $request->ao_10_salutation;
    $job->ao10->property_address_adjoining = $request->ao_10_property_address_adjoining;
    $job->ao10->contact_address = $request->ao_10_contact_address;
    $job->ao10->contact_details = $request->ao_10_contact_details;
    $job->ao10->owners_referral = $request->ao_10_owners_referral;
    $job->ao10->has_appointed_have_appointed = $request->ao_10_has_appointed_have_appointed;
    $job->ao10->i_we_referral = $request->ao_10_i_we_referral;
    $job->ao10->i_we_referral_lower = $request->ao_10_i_we_referral_lower;
    $job->ao10->my_our_refferal = $request->ao_10_my_our_refferal;
    $job->ao10->he_she_they_referral = $request->ao_10_he_she_they_referral;
    $job->ao10->his_her_their = $request->ao_10_his_her_their;
    $job->ao10->owners_owners = $request->ao_10_owners_owners;
    $job->ao10->is_an_are = $request->ao_10_is_an_are;
    $job->ao10->s_s = $request->ao_10_s_s;
    $job->ao10->s1_section = $request->ao_10_s1_section;
    $job->ao10->s1_description = $request->ao_10_s1_description;
    $job->ao10->s2_section = $request->ao_10_s2_section;
    $job->ao10->s2_description = $request->ao_10_s2_description;
    $job->ao10->s6_section = $request->ao_10_s6_section;
    $job->ao10->s6_description = $request->ao_10_s6_description;
    $job->ao10->date_of_notice = $request->ao_10_date_of_notice;
    $job->ao10->notice_notices = $request->ao_10_notice_notices;
    $job->ao10->section_sections = $request->ao_10_section_sections;
    $job->ao10->drawings = $request->ao_10_drawings;
    $job->ao10->soc_date = $request->ao_10_soc_date;
    $job->ao10->third_surveyor = $request->ao_10_third_surveyor;
    $job->ao10->save();
  }
} 