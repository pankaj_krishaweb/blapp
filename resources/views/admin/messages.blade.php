@extends('layouts.frame')

@section('content')
<style>
    .nav-pills>li>a {
    padding: 20px;
}

#job-search-btn{

    background-color: #fff;
    color: #b3b3b3;
    border: solid 0.5px #b3b3b3;
    border-radius: 5px;
    width: 100%;
        border-radius: 0px;
  }
  .open > .dropdown-menu {
    display: block;
    width: 100%;
    margin-top: 0px;
    border-radius: 0px;
    height: auto;
    max-height: 300px;
    overflow-x: hidden;
    
}

.list-group{
    max-height: 300px;
    margin-bottom: 10px;
    overflow:scroll;
    -webkit-overflow-scrolling: touch;
}
</style>
<div id="app">
    <div class="container-fluid">
        <div class="container" style="min-height: 80vh;">
            <div class="row">   
                 <div class="col-md-12">
                    <h1>Messaging</h1>  
                    <p>Please select a job to view messages</p> 
                     <ol class="breadcrumb"> 
                      <li><a href="{{ url('admin') }}">Home</a></li>
                      <li class="admin/jobs">  Messages</li>
                    </ol>
                </div>
                <div class="col-md-4">
                   <div class="list-group">
                        <input type="text" class="form-control" id="job_no_search" placeholder="serach"><br>
                        @foreach ($jobs as $job)
                            <a class="list-group-item" href="{{ url('/admin/message/'.$job->id) }}">
                                BLSN{{  $job->id }} 
                                <span class="badge">{{  $job->messages->count() }} : messages </span>
                            </a>
                        @endforeach
                    </div>   
                </div>

                <div class="col-md-8">
                    @if ($msgs->count() >=1)           
                        <h2>Messages you missed out on</h2>
                            @foreach($msgs as $msg)
                                <div class="well" style="background: #44b39e; color: #fff; border-radius: 30px;">
                                <p class="lead"> {{ $msg->message }}</p>
     
        <hr>
        <div>  
                @if(Auth::user()->role==9)
                    <a  class="btn btn-default" href="{{ url('/admin/message/'.$msg->job_id) }}">View Full conversation </a>
                @elseif(Auth::user()->role==7)
                    <a  class="btn btn-default btn-xs" href="{{ url('/employee/message/'.$job->id) }}">{{  $job->job_no }} </a>
                             
                @endif   
                    
                    <p class="pull-right">{{ $msg->user->name ?? null}} - {{ $msg->created_at ?? null }}</p></div>
       

                    </div>
                   @endforeach
                   @endif
                    
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" style="background-color: #414861">
        <div class="container">
            <p class="text-center" style="margin-top: 20px; font-size: 20px; color: #85939f;"><strong> @if (Request::is('admin/message/*')) Reply to the conversation here ... @endif</strong></p>
            <chat-composer v-on:messagesent="addMessage"></chat-composer>
        </div>
    </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
      $("#job_no_search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".list-group a").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });     
</script>
@endsection

