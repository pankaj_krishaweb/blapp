<html>
<head></head>
<body >
<h1 style="color: #00b29d">You have a New Message</h1>
<p><strong>From: </strong>{{ $user}}</p>
@if(isset($image))
<img src="{!! asset('storage/chat-image/'.$image) !!}" style="height: 150px; width: 150px;">
@else
<p>{{ $msg }}</p>
@endif
<br>
{{-- <img src="{{ asset('/images/email_footer.png') }}">
<p>W: <a href="http://www.blsurveyors.com">www.blsurveyors.com</a> - T: <a href="tel:020 7935 2502">020 7935 2502</a> – E <a href="mailto:info@blsurveyors.com">info@blsurveyors.com</a></p> --}}
<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;" >
<p><strong>Berry Lodge Surveyors</strong></p>
@include('shard.footer')
</body>
</html>
