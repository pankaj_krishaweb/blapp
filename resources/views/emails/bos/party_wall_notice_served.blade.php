<html>
<head>
	<style type="text/css">
		.float-md-left,
		.float-md-right{
			text-align: center;
		}
		h2{
			font-size: 30px;
			line-height: 40px;
			color: #00b29c;
			font-weight: bold;
		}
		@media (min-width: 768px) {
			.title-wrap{
				display: table;
				max-width: 800px;
				width: 100%;
			}
			h2{
				font-size: 47px;
				line-height: 57px;
			}
			.title-wrap .float-md-left,
			.title-wrap .float-md-right{
				display: table-cell;
				vertical-align: middle;
			}
			.title-wrap h2{
				font-size: 47px;
				line-height: 1.5;
			}
			.float-md-left{
				float: left;
				width: calc(100% - 250px);
				text-align: left;
			}
			.float-md-right{
				float: right;
				width: 250px;
				text-align: right;
			}
			.float-md-right img{
				max-width: 100%;
			}
		}
	</style>
	
</head>
<body >
	<div class="text-center title-wrap">
		<div class="float-md-left">
			<h2>We’ve Served the Party Wall Notices in today’s post</h2>
		</div>
		<div class="float-md-right">
			<img src="{!! asset('images/bos/3.png') !!}" width="200px">
		</div>
	</div>
	<div>


		<p>Dear {{$salutation}},</p>
		<p>Good News! We are pleased to confirm that your Surveyor {!! $surveyor_name !!} has formally served the Party Wall Notices for the proposed construction works taking place at {!! $property_address_proposed_work !!}</p>
		<p>The Party Wall Notice are served via Royal Mail First class postage so should arrive with the adjoining owners tomorrow or the following day postage pending.</p>
		<p>The Party Wall Notice has a 14 day validity period (plus 2 days allowing for postage) after which time if we don’t receive a response we can serve a further Party Wall Notice (a 10(4) Party Wall Notice) upon the adjoining owners giving them a final and further 10 days to respond, after which time if we still haven’t had a response we can appoint a surveyor on their behalf.</p>

		<p>We can discuss these procedures in greater detail should it become applicable, in the mean time if you would like to hear a little more about this topic we would advise having a quick watch of our <a href="https://youtu.be/sdYN3H2efO0">{{-- (should link to https://youtu.be/sdYN3H2efO0) --}}informative Party Wall Surveying Video</a>.</p>

		<p>We can confirm the 10(4) Party Wall Notice date is {!! $ten_4_party_wall_notice_date !!}, however in the mean time we will of course update you as soon as I receive a response.</p>

		<p>Login to the  <a href="https://app.blsurveyors.com/">Berry Lodge Client Portal</a> now to see the status of your job and access PDF copies of the Party Wall Notices for your records.</p>
		<p>Kind Regards, </p>

		<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;">
		<p><strong>Berry Lodge Surveyors</strong></p>
	</div>
	@include('shard.footer')

</div>
</body>
</html>