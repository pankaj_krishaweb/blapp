<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surveying extends Model
{
      protected $table="surveying";

      public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
