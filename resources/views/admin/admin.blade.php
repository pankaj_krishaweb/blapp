@extends('layouts.frame')

@section('content')
<style type="text/css">
    h4{
            font-weight: bold;
    color: #414860;
    }
    h5{
color: #cccccc;
padding-top: 35px; 
/*padding-bottom: 20px; */
    }
    h2 {
    color: #00b29c;
    font-weight: bold;
    font-size: 4em;
}
</style>
<div class="container-fluid">
    <div class="container" style="min-height: 80vh;">
            <h1>Admin</h1>
            <div class="row">
                <div class="col-md-6">
                    <div style="    min-height: 20px;    padding: 19px;">
                        <a href="{{ url('/admin/progress-report') }}" class="btn btn-default btn-block ">Progress Report</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div style="    min-height: 20px;    padding: 19px;">
                        <a href="{{ url('/admin/users') }}" class="btn btn-default btn-block ">User Management</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div style="    min-height: 20px;    padding: 19px;">
                        <a href="{{ url('admin/storage') }}" class="btn btn-default btn-block">Storage</a>
                    </div>
                </div> 
                <div class="col-md-6">
                    <div style="    min-height: 20px;    padding: 19px;">
                        <a href="https://meetings.webex.com/collabs/auth" class="btn btn-default btn-block" target="_blank">Book Webex meeting</a>
                    </div>
                </div>
                @if(Auth::user()->role == 9)
                <div class="col-md-6">
                    <div style="    min-height: 20px;    padding: 19px;">
                        <a href="{!! url('admin/client-video') !!}" class="btn btn-default btn-block">Client Video</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div style="    min-height: 20px;    padding: 19px;">
                        <a href="{!! url('admin/our-community') !!}" class="btn btn-default btn-block">Our Community</a>
                    </div>
                </div>
                @endif  

                    @if ($sitepayment->count()>1)
                    <div class="col-md-12">
                        <div class="panel-group">
                          <div class="panel panel-info">
                            <div class="panel-heading">Payments from website</div>
                            <div class="panel-body">
                                 @foreach ($sitepayment as $payment)
                                    <p> 
                                        Name: <strong>{{ $payment->name}}</strong> 
                                        Email: <strong>{{ $payment->email}}</strong> 
                                        Date: <strong>{{ $payment->created_at}}</strong>
                                        <span class="pull-right">Amount: {{ $payment->amount}}</span>
                                    </p>
                                 @endforeach

                                 {{ $sitepayment->links() }}
                            </div>
                          </div>
                        </div> 
                    </div>
                @endif               
            </div>
        </div>   
    </div>
</div>
@endsection
@section('script')

<script>
    
$(document).ready(function() {

    $(".tasks").click(function() {

        var taskid = $(this).attr('data-key');
        
        if ($(this).is(":checked")) {
           var status="1";
        } else {
            var status="0";
        
        }

          $.ajax({
        url: '/tasks/checked',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {taskid: taskid, status: status},
        success: function(data){
        console.log(data);
        location.reload()
            
        },
    })  
    });

});
</script>
@endsection
