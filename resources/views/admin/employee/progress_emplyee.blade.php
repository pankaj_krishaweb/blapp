@extends('layouts.frame')

@section('content')
<style type="text/css">
    h4{
            font-weight: bold;
    color: #414860;
    }
    h5{
color: #cccccc;
padding-top: 35px; 
/*padding-bottom: 20px; */
    }
    h2 {
    color: #00b29c;
    font-weight: bold;
    font-size: 4em;
}
</style>
<div class="container-fluid">
    <div class="container" style="min-height: 80vh;">
            <h1>Progress Report</h1>
            <br>
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3"> <h4>Job Number</h4></div>
                <div class="col-md-3 col-sm-3 col-xs-3"> <h4>Address</h4></div>
          
                <div class="col-md-2 col-sm-2 col-xs-2 pull-right"> <h4 class="text-center">Tasks Completed</h4></div>
                <div class="col-md-2 col-sm-2 col-xs-2 pull-right"> <h4 class="text-center"> Outstanding Tasks</h4></div>
            </div>
            <br>
            @foreach($users->jobs as $user)
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3"><h5 style="color: #414861;">{{ $user->job_no }}</h5> </div>
                <div class="col-md-3 col-sm-3 col-xs-3"><h5>{{ $user->bo->bo_property_address_proposed_work }}</h5> </div>
                
                <div class="col-md-2 col-sm-2 col-xs-2 pull-right"> <h2 class="text-center"> {{ $user->completed }}</h2></div>
                <div class="col-md-2 col-sm-2 col-xs-2 pull-right"> <h2 class="text-center"> {{ $user->outstanding }}</h2></div>
            </div>
            <hr>
            @endforeach
        </div>   
    </div>
</div>
@endsection
@section('script')

<script>
    
$(document).ready(function() {

    $(".tasks").click(function() {

        var taskid = $(this).attr('data-key');
        
        if ($(this).is(":checked")) {
           var status="1";
        } else {
            var status="0";
        
        }

          $.ajax({
        url: '/tasks/checked',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {taskid: taskid, status: status},
        success: function(data){
        console.log(data);
        location.reload()
            
        },
    })  
    });

});
</script>
@endsection
