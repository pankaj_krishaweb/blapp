<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('invoice_no') ? ' has-error' : '' }}">
       <label for="invoice_no" class="control-label">Invoice No</label>
       <input id="invoice_no" type="text" class="form-control" name="invoice_no" value="@isset( $jobs->invoice_no ){{  $jobs->invoice_no }} @endisset {{ old('invoice_no') }}" >
   </div>
</div> 
<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('land_registry_costs') ? ' has-error' : '' }}">
        <label for="land_registry_costs" class="control-label">Land Registry Costs</label>
        <input id="land_registry_costs"  class="form-control autocomplete" name="land_registry_costs" value="@isset( $jobs->land_registry_costs ){{$jobs->land_registry_costs}}@endisset{{ old('land_registry_costs') }}" data-toggle="dropdown" autocomplete="off" >
        <ul class="dropdown-menu" role="menu">
        <li><a class="land_cost">£6.00</a></li>
        <li><a class="land_cost">£9.00</a></li>
        <li><a class="land_cost">£12.00</a></li>
        <li><a class="land_cost">£18.00</a></li>
        <li><a class="land_cost">£24.00</a></li>
        </ul>
    </div>
</div>


<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('vat_amount') ? ' has-error' : '' }}">
        <label for="vat_amount" class="control-label">Vat Amount</label>
        <input id="vat_amount" type="text" class="form-control" name="vat_amount" value="@isset( $jobs->vat_amount ){{  $jobs->vat_amount }} @endisset {{ old('vat_amount') }}" readonly="" >
    </div>
</div>
<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('final_amount') ? ' has-error' : '' }}">
        <label for="final_amount" class="control-label">Final Amount</label>
        <input id="final_amount" type="text" class="form-control" name="final_amount" value="@isset( $jobs->final_amount ){{  $jobs->final_amount }} @endisset {{ old('final_amount') }}" >
    </div>
</div>
<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('surveying_service') ? ' has-error' : '' }}">
       <label for="surveying_service" class="control-label">Surveying Service</label>
       <input id="surveying_service"  class="form-control autocomplete" name="surveying_service" value="@isset( $jobs->surveying->surveying_service ){{$jobs->surveying->surveying_service }}@endisset{{old('surveying_service') }}" autocomplete="off"  data-toggle="dropdown">
        <ul class="dropdown-menu" role="menu" id="surveying_service_dropdown">
            <li><a class="">RICS HomeBuyer Report</a></li>
            <li><a class="post_cost_option">Valuation Report</a></li>
            <li><a class="">Drive by Valuation</a></li>
            <li><a class="">Full Building Survey </a></li>
            <li><a id="boundary">Boundary Determination Report</a></li>
            <li><a class="">Schedule of Condition Report </a></li>
            <li><a class="">Defect Analysis Report</a></li>
            <li><a class="">Snagging List </a></li>
            <li><a class="">Property Doctor Photographic Review</a></li>
            <li><a class="">Party Wall Notification Letter </a></li>
           <li><a class="">Surveyor's Written Opinion </a></li>
        </ul>
   </div>
</div>
<div class="col-md-6 col-sm-6" id="post_cost">
    <div class="form-group{{ $errors->has('printing_postage_costs') ? ' has-error' : '' }}">
        <label for="printing_postage_costs" class="control-label">Printing &amp; Postage Costs</label>
        <input id="printing_postage_costs" type="text" class="form-control printing_postage_costs" name="printing_postage_costs" value="@isset( $jobs->printing_postage_costs ){{  $jobs->printing_postage_costs }} @endisset {{ old('printing_postage_costs') }}" readonly="" >
    </div>
</div>

<div class="col-md-6 col-sm-6" style="display: none" id="boundary_post_cost">
    <div class="form-group{{ $errors->has('printing_postage_costs') ? ' has-error' : '' }}">
       <label for="printing_postage_costs" class="control-label">Printing &amp; Postage Costs</label>
       <input id="post_printing_postage_costs"  class="form-control autocomplete" name="printing_postage_costs1" value="" autocomplete="off"  data-toggle="dropdown">
        <ul class="dropdown-menu" role="menu">
            <li><a class="post_cost">£11.95</a></li>
            <li><a class="post_cost">£20.95</a></li>
        </ul>
   </div>
</div>
<div @if(isset($jobs->surveying->surveying_service)) @if($jobs->surveying->surveying_service == 'Boundary Determination Report') style="display: block;" @else style="display: none;" @endif @endif id="boundary_div">
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('boundary_date_of_confirmation_letter') ? ' has-error' : '' }}">
           <label for="boundary_date_of_confirmation_letter" class="control-label">Date of Confirmation Letter</label>
           <input id="boundary_date_of_confirmation_letter" type="text" class="form-control date" name="boundary_date_of_confirmation_letter" value="@isset( $jobs->surveying->boundary_date_of_confirmation_letter ){{$jobs->surveying->boundary_date_of_confirmation_letter }}@endisset{{old('boundary_date_of_confirmation_letter') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('address_of_boundary_determination') ? ' has-error' : '' }}">
           <label for="address_of_boundary_determination" class="control-label">Address of Boundary Determination</label>
           <input id="address_of_boundary_determination" type="textarea" class="form-control" name="address_of_boundary_determination" value="@isset( $jobs->surveying->address_of_boundary_determination ){{$jobs->surveying->address_of_boundary_determination }}@endisset{{old('address_of_boundary_determination') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6"> 
        <div class="form-group{{ $errors->has('boundary_determination_address') ? ' has-error' : '' }}">
           <label for="boundary_determination_address" class="control-label">Boundary Determination Address HMLR Title No</label>
           <input id="boundary_determination_address" type="textarea" class="form-control" name="boundary_determination_address" value="@isset( $jobs->surveying->boundary_determination_address ){{$jobs->surveying->boundary_determination_address }}@endisset{{old('boundary_determination_address') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('type_of_property') ? ' has-error' : '' }}">
           <label for="type_of_property" class="control-label">Type of Property</label>
           <input id="type_of_property"  class="form-control autocomplete" name="type_of_property" value="@isset( $jobs->surveying->type_of_property ){{$jobs->surveying->type_of_property }}@endisset{{old('type_of_property') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Detatched</a></li>
                <li><a>Semi detached</a></li>
                <li><a>Terraced</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('age_of_property') ? ' has-error' : '' }}">
           <label for="age_of_property" class="control-label">Age of Property</label>
           <input id="age_of_property"  class="form-control autocomplete" name="age_of_property" value="@isset( $jobs->surveying->age_of_property ){{$jobs->surveying->age_of_property }}@endisset{{old('age_of_property') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>1900's</a></li>
                <li><a>1930's</a></li>
                <li><a>1960's</a></li>
                <li><a>Modern (1990's onwards)</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('address_of_adjoining_property') ? ' has-error' : '' }}">
           <label for="address_of_adjoining_property" class="control-label">Address of Adjoining Property</label>
           <input id="address_of_adjoining_property" type="textarea" class="form-control" name="address_of_adjoining_property" value="@isset( $jobs->surveying->address_of_adjoining_property ){{$jobs->surveying->address_of_adjoining_property }}@endisset{{old('address_of_adjoining_property') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('adjoining_property') ? ' has-error' : '' }}">
           <label for="adjoining_property" class="control-label">Adjoining Property HMLR Title No</label>
           <input id="adjoining_property" type="textarea" class="form-control" name="adjoining_property" value="@isset( $jobs->surveying->adjoining_property ){{$jobs->surveying->adjoining_property }}@endisset{{old('adjoining_property') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('name_of_legal_owner') ? ' has-error' : '' }}">
           <label for="name_of_legal_owner" class="control-label">Name of Legal owner of Adjoining Property</label>
           <input id="name_of_legal_owner" type="textarea" class="form-control" name="name_of_legal_owner" value="@isset( $jobs->surveying->name_of_legal_owner ){{$jobs->surveying->name_of_legal_owner }}@endisset{{old('name_of_legal_owner') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('boundary_being_determined') ? ' has-error' : '' }}">
           <label for="boundary_being_determined" class="control-label">Boundary Being Determined</label>
           <input id="boundary_being_determined"  class="form-control autocomplete" name="boundary_being_determined" value="@isset( $jobs->surveying->boundary_being_determined ){{$jobs->surveying->boundary_being_determined }}@endisset{{old('boundary_being_determined') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Front</a></li>
                <li><a>Rear</a></li>
                <li><a>Left</a></li>
                <li><a>Right</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('direction_of_boundary') ? ' has-error' : '' }}">
           <label for="direction_of_boundary" class="control-label">Direction of Boundary</label>
           <input id="direction_of_boundary"  class="form-control autocomplete" name="direction_of_boundary" value="@isset( $jobs->surveying->direction_of_boundary ){{$jobs->surveying->direction_of_boundary }}@endisset{{old('direction_of_boundary') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Horizontal (side to side)</a></li>
                <li><a>Vertical (front to rear)</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('plane_of_boundary') ? ' has-error' : '' }}">
           <label for="plane_of_boundary" class="control-label">plane of Boundary</label>
           <input id="plane_of_boundary"  class="form-control autocomplete" name="plane_of_boundary" value="@isset( $jobs->surveying->plane_of_boundary ){{$jobs->surveying->plane_of_boundary }}@endisset{{old('plane_of_boundary') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Constant (doesn’t change direction)</a></li>
                <li><a>Non constant (change direction)</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('inspection_weather') ? ' has-error' : '' }}">
           <label for="inspection_weather" class="control-label">Inspection Weather</label>
           <input id="inspection_weather"  class="form-control autocomplete" name="inspection_weather" value="@isset( $jobs->surveying->inspection_weather ){{$jobs->surveying->inspection_weather }}@endisset{{old('inspection_weather') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Clear</a></li>
                <li><a>Sunny</a></li>
                <li><a>Overcast</a></li>
                <li><a>Cloudy</a></li>
                <li><a>Stormy</a></li>
                <li><a>Snowing</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('inspection_humidity') ? ' has-error' : '' }}">
           <label for="inspection_humidity" class="control-label">Inspection Humidity</label>
           <input id="inspection_humidity"  class="form-control autocomplete" name="inspection_humidity" value="@isset( $jobs->surveying->inspection_humidity ){{$jobs->surveying->inspection_humidity }}@endisset{{old('inspection_humidity') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Dry</a></li>
                <li><a>wet</a></li>
                <li><a>Very Wet</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('local_authority') ? ' has-error' : '' }}">
           <label for="local_authority" class="control-label">Local Authority</label>
           <input id="local_authority"  class="form-control autocomplete" name="local_authority" value="@isset( $jobs->surveying->local_authority ){{$jobs->surveying->local_authority }}@endisset{{old('local_authority') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>City of London</a></li>
                <li><a>City of Westminster</a></li>
                <li><a>Kensington and Chelsea</a></li>
                <li><a>Hammersmith and Fulham</a></li>
                <li><a>Wandsworth</a></li>
                <li><a>Lambeth</a></li>
                <li><a>Southwark</a></li>
                <li><a>Tower Hamlets</a></li>
                <li><a>Hackney</a></li>
                <li><a>Islington</a></li>
                <li><a>Camden</a></li>
                <li><a>Brent</a></li>
                <li><a>Ealing</a></li>
                <li><a>Hounslow</a></li>
                <li><a>Richmond</a></li>
                <li><a>Kingston</a></li>
                <li><a>Merton</a></li>
                <li><a>Sutton</a></li>
                <li><a>Croydon</a></li>
                <li><a>Bromley</a></li>
                <li><a>Lewisham</a></li>
                <li><a>Greenwich</a></li>
                <li><a>Bexley</a></li>
                <li><a>Havering</a></li>
                <li><a>Barking and Dagenham</a></li>
                <li><a>Redbridge</a></li>
                <li><a>Newham</a></li>
                <li><a>Waltham Forest</a></li>
                <li><a>Haringey</a></li>
                <li><a>Enfield</a></li>
                <li><a>Barnet</a></li>
                <li><a>Harrow</a></li>
                <li><a>Hillingdon</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('historic_maps_were_present') ? ' has-error' : '' }}">
           <label for="historic_maps_were_present" class="control-label">How Many Historic Maps were present?</label>
           <input id="historic_maps_were_present" type="text" class="form-control" name="historic_maps_were_present" value="@isset( $jobs->surveying->historic_maps_were_present ){{$jobs->surveying->historic_maps_were_present }}@endisset{{old('historic_maps_were_present') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('planning_records_of_assistance') ? ' has-error' : '' }}">
           <label for="planning_records_of_assistance" class="control-label">Were planning records of assistance?</label>
           <input id="planning_records_of_assistance" type="text" class="form-control autocomplete" name="planning_records_of_assistance" value="@isset( $jobs->surveying->planning_records_of_assistance ){{$jobs->surveying->planning_records_of_assistance }}@endisset{{old('planning_records_of_assistance') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu"  role="menu">
           <li><a>Were</a></li>
           <li><a>Were not</a></li>
           </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('date_of_report') ? ' has-error' : '' }}">
           <label for="date_of_report" class="control-label">Date of Report</label>
           <input id="date_of_report" type="text" class="form-control date" name="date_of_report" value="@isset( $jobs->surveying->date_of_report ){{$jobs->surveying->date_of_report }}@endisset{{old('date_of_report') }}" >
       </div>
    </div>
</div>

<!-- <div @if(isset($jobs->surveying->surveying_service)) @if($jobs->surveying->surveying_service == 'Valuation Report') style="display: block;" @else style="display: none;" @endif @endif id="valuation_div">
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('valuation_type') ? ' has-error' : '' }}">
           <label for="valuation_type" class="control-label">Valuation Type</label>
           <input id="valuation_type" type="text" class="form-control" name="valuation_type" value="@isset( $jobs->surveying->valuation_type ){{$jobs->surveying->valuation_type }}@endisset{{old('valuation_type') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('address_of_valued_property') ? ' has-error' : '' }}">
           <label for="address_of_valued_property" class="control-label">Address Of Valued Property</label>
           <input id="address_of_valued_property" type="textarea" class="form-control" name="address_of_valued_property" value="@isset( $jobs->surveying->address_of_valued_property ){{$jobs->surveying->address_of_valued_property }}@endisset{{old('address_of_valued_property') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('type_of_property') ? ' has-error' : '' }}">
           <label for="type_of_property" class="control-label">Type of Property</label>
           <input id="type_of_property"  class="form-control autocomplete" name="type_of_property" value="@isset( $jobs->surveying->type_of_property ){{$jobs->surveying->type_of_property }}@endisset{{old('type_of_property') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Purpose Built Flat</a></li>
                <li><a>Conversion Flat</a></li>
                <li><a>Detached House</a></li>
                <li><a>Semi-detached House</a></li>
                <li><a>Terraced House</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6"> 
        <div class="form-group{{ $errors->has('age_of_property') ? ' has-error' : '' }}">
           <label for="age_of_property" class="control-label">Age Of Property</label>
           <input id="age_of_property" type="textarea" class="form-control" name="age_of_property" value="@isset( $jobs->surveying->age_of_property ){{$jobs->surveying->age_of_property }}@endisset{{old('age_of_property') }}" >
       </div>
    </div>

    
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('floors_in_the_property') ? ' has-error' : '' }}">
           <label for="floors_in_the_property" class="control-label">Floors In The Property</label>
           <input id="floors_in_the_property" type="textarea" class="form-control" name="floors_in_the_property" value="@isset( $jobs->surveying->floors_in_the_property ){{$jobs->surveying->floors_in_the_property }}@endisset{{old('floors_in_the_property') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('valuation_date') ? ' has-error' : '' }}">
           <label for="valuation_date" class="control-label">Valuation Date</label>
           <input id="valuation_date" type="textarea" class="form-control" name="valuation_date" value="@isset( $jobs->surveying->valuation_date ){{$jobs->surveying->valuation_date }}@endisset{{old('valuation_date') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('valuation_property_ownership_type') ? ' has-error' : '' }}">
           <label for="valuation_property_ownership_type" class="control-label">Valuation Property  Ownership Type</label>
           <input id="valuation_property_ownership_type"  class="form-control autocomplete" name="valuation_property_ownership_type" value="@isset( $jobs->surveying->valuation_property_ownership_type ){{$jobs->surveying->valuation_property_ownership_type }}@endisset{{old('valuation_property_ownership_type') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Leasehold</a></li>
                <li><a>Freehold</a></li>
                <li><a>Shared Ownership</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('inspection_weather') ? ' has-error' : '' }}">
           <label for="inspection_weather" class="control-label">Inspection Weather</label>
           <input id="inspection_weather"  class="form-control autocomplete" name="inspection_weather" value="@isset( $jobs->surveying->inspection_weather ){{$jobs->surveying->inspection_weather }}@endisset{{old('inspection_weather') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Overcast</a></li>
                <li><a>Sunny</a></li>
                <li><a>Snowing</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('inspection_humidity') ? ' has-error' : '' }}">
           <label for="inspection_humidity" class="control-label">Inspection Humidity</label>
           <input id="inspection_humidity"  class="form-control autocomplete" name="inspection_humidity" value="@isset( $jobs->surveying->inspection_humidity ){{$jobs->surveying->inspection_humidity }}@endisset{{old('inspection_humidity') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Dry</a></li>
                <li><a>Raining</a></li>
            </ul>
       </div>
    </div>
</div> -->

<div @if(isset($jobs->surveying->surveying_service)) @if($jobs->surveying->surveying_service == 'Valuation Report') style="display: block;" @else style="display: none;" @endif @endif id="valuation_div">

    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('valuation_type') ? ' has-error' : '' }}">
           <label for="valuation_type" class="control-label">Valuation Type</label>
           <input id="valuation_type"  class="form-control autocomplete" name="valuation_type" value="@isset( $jobs->surveying->valuation_type ){{$jobs->surveying->valuation_type }}@endisset{{old('valuation_type') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Market</a></li>
                <li><a>Insurance (Rebuild)</a></li>
                <li><a>Retrospective</a></li>
                <li><a>Matrimonial</a></li>
                <li><a>Probate</a></li>
            </ul>
       </div>
    </div>

    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('valuation_date_of_confirmation_letter') ? ' has-error' : '' }}">
           <label for="valuation_date_of_confirmation_letter" class="control-label">Date of Confirmation Letter</label>
           <input id="valuation_date_of_confirmation_letter" type="text" class="form-control date" name="valuation_date_of_confirmation_letter" value="@isset( $jobs->surveying->valuation_date_of_confirmation_letter ){{$jobs->surveying->valuation_date_of_confirmation_letter }}@endisset{{old('valuation_date_of_confirmation_letter') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('address_of_valued_property') ? ' has-error' : '' }}">
           <label for="address_of_valued_property" class="control-label">Address Of Valued Property</label>
           <input id="address_of_valued_property" type="textarea" class="form-control" name="address_of_valued_property" value="@isset( $jobs->surveying->address_of_valued_property ){{$jobs->surveying->address_of_valued_property }}@endisset{{old('address_of_valued_property') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('v_type_of_property') ? ' has-error' : '' }}">
           <label for="type_of_property" class="control-label">Type of Property</label>
           <input id="type_of_property"  class="form-control autocomplete" name="v_type_of_property" value="@isset( $jobs->surveying->type_of_property ){{$jobs->surveying->type_of_property }}@endisset{{old('type_of_property') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Purpose Built Flat</a></li>
                <li><a>Conversion Flat</a></li>
                <li><a>Detached House</a></li>
                <li><a>Semi-detached House</a></li>
                <li><a>Terraced House</a></li>
            </ul>
       </div>
    </div>
    {{-- <div class="col-md-6 col-sm-6"> 
        <div class="form-group{{ $errors->has('v_age_of_property') ? ' has-error' : '' }}">
           <label for="age_of_property" class="control-label">Age Of Property</label>
           <input id="age_of_property" type="textarea" class="form-control" name="v_age_of_property" value="@isset( $jobs->surveying->age_of_property ){{$jobs->surveying->age_of_property }}@endisset{{old('age_of_property') }}" >
       </div>
    </div> --}}

    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('v_age_of_property') ? ' has-error' : '' }}">
           <label for="age_of_property" class="control-label">Age Of Property</label>
           <input id="age_of_property"  class="form-control autocomplete" name="v_age_of_property" value="@isset( $jobs->surveying->age_of_property ){{$jobs->surveying->age_of_property }}@endisset{{old('age_of_property') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>1900's</a></li>
                <li><a>1930's</a></li>
                <li><a>1960's</a></li>
                <li><a>1980's</a></li>
                <li><a>1990's</a></li>
                <li><a>2000's</a></li>
            </ul>
       </div>
    </div>

    
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('floors_in_the_property') ? ' has-error' : '' }}">
           <label for="floors_in_the_property" class="control-label">Floors In The Property</label>
           <input id="floors_in_the_property" type="textarea" class="form-control" name="floors_in_the_property" value="@isset( $jobs->surveying->floors_in_the_property ){{$jobs->surveying->floors_in_the_property }}@endisset{{old('floors_in_the_property') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('valuation_date') ? ' has-error' : '' }}">
           <label for="valuation_date" class="control-label">Valuation Date</label>
           <input id="valuation_date" type="textarea" class="form-control date" name="valuation_date" value="@isset( $jobs->surveying->valuation_date ){{$jobs->surveying->valuation_date }}@endisset{{old('valuation_date') }}" >
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('valuation_property_ownership_type') ? ' has-error' : '' }}">
           <label for="valuation_property_ownership_type" class="control-label">Valuation Property  Ownership Type</label>
           <input id="valuation_property_ownership_type"  class="form-control autocomplete" name="valuation_property_ownership_type" value="@isset( $jobs->surveying->valuation_property_ownership_type ){{$jobs->surveying->valuation_property_ownership_type }}@endisset{{old('valuation_property_ownership_type') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Leasehold</a></li>
                <li><a>Freehold</a></li>
                <li><a>Shared Ownership</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('v_inspection_weather') ? ' has-error' : '' }}">
           <label for="inspection_weather" class="control-label">Inspection Weather</label>
           <input id="inspection_weather"  class="form-control autocomplete" name="v_inspection_weather" value="@isset( $jobs->surveying->inspection_weather ){{$jobs->surveying->inspection_weather }}@endisset{{old('inspection_weather') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Overcast</a></li>
                <li><a>Sunny</a></li>
                <li><a>Snowing</a></li>
            </ul>
       </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="form-group{{ $errors->has('v_inspection_humidity') ? ' has-error' : '' }}">
           <label for="inspection_humidity" class="control-label">Inspection Humidity</label>
           <input id="inspection_humidity"  class="form-control autocomplete" name="v_inspection_humidity" value="@isset( $jobs->surveying->inspection_humidity ){{$jobs->surveying->inspection_humidity }}@endisset{{old('inspection_humidity') }}" autocomplete="off"  data-toggle="dropdown">
            <ul class="dropdown-menu" role="menu">
                <li><a>Dry</a></li>
                <li><a>Raining</a></li>
            </ul>
       </div>
    </div>
</div>
<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('date_of_inspection') ? ' has-error' : '' }}">
       <label for="date_of_inspection" class="control-label">Date of Inspection</label>
       <input id="date_of_inspection" type="text" class="form-control date" name="date_of_inspection" value="@isset( $jobs->surveying->date_of_inspection ){{$jobs->surveying->date_of_inspection }}@endisset{{old('date_of_inspection') }}" >
   </div>
</div>


<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('cost_of_service') ? ' has-error' : '' }}">
        <label for="cost_of_service" class="control-label">Cost of Service</label>
        <input id="cost_of_service" type="text" class="form-control" name="cost_of_service" value="@isset( $jobs->surveying->cost_of_service ){{$jobs->surveying->cost_of_service }}@endisset{{old('cost_of_service') }}" readonly="">
    </div>
</div>

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('surveyor_who_had_first_contact_with_client') ? ' has-error' : '' }}">
        <label for="surveyor_who_had_first_contact_with_client" class="control-label">Surveyor Who had first contact with client</label>
        <input id="surveyor_who_had_first_contact_with_client"  class="form-control autocomplete" name="surveyor_who_had_first_contact_with_client" value="@isset( $jobs->surveying->surveyor_who_had_first_contact_with_client ){{$jobs->surveying->surveyor_who_had_first_contact_with_client }}@endisset{{old('surveyor_who_had_first_contact_with_client') }}"  autocomplete="off"  data-toggle="dropdown">
         <ul class="dropdown-menu"  role="menu">
        <li><a>BM</a></li>
        <li><a>BD</a></li>
        <li><a>TM</a></li>
        <li><a>KA</a></li>
        </ul>
    </div>
</div>




<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('surveyor_dealing_with_file') ? ' has-error' : '' }}">
        <label for="surveyor_dealing_with_file" class="control-label">Surveyor dealing with file: </label>
        <input id="surveyor_dealing_with_file"  class="form-control autocomplete" name="surveyor_dealing_with_file" value="@isset( $jobs->surveying->surveyor_dealing_with_file ){{$jobs->surveying->surveyor_dealing_with_file }}@endisset{{old('surveyor_dealing_with_file') }}" autocomplete="off"  data-toggle="dropdown">
         <ul class="dropdown-menu"  role="menu">
        <li><a>BM</a></li>
        <li><a>BD</a></li>
        <li><a>TM</a></li>
        <li><a>KA</a></li>
        </ul>
    </div>
</div>


<div class="col-md-6 col-sm-6">
    <!-- <div class="form-group{{ $errors->has('s_surveyor_full_information') ? ' has-error' : '' }}">
        <label for="s_surveyor_full_information" class="control-label">Surveyor Full Information:</label>
        <input id="s_surveyor_full_information" class="form-control autocomplete" name="s_surveyor_full_information" value="@isset( $jobs->surveying->s_surveyor_full_information ){{$jobs->surveying->s_surveyor_full_information }}@endisset{{old('s_surveyor_full_information') }}"  data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>Bradley Mackenzie BA(Hons) MSc MSc FFPWS MCIArb MRICS - RICS Registered Valuer - RICS Accredited Mediator, Berry Lodge Surveyors, Upper Floor, 61 Highgate High Street, London, N6 5JY Telephone: 020 3213 0623 Email: bm@blsurveyors.com</a></li>
            <li><a>Beau Monroe-Davies FDA MFBE MFPWS MCIArb - RICS Accredited Mediator, Berry Lodge Surveyors, Upper Floor, 61 Highgate High Street, London, N6 5JY Telephone: 020 3213 0622 Email: bd@blsurveyors.com
            </a></li>
            <li><a>Karim El Shenawi-Ali BSc(Hons) MFPWS, Berry Lodge Surveyors, Upper Floor, 61 Highgate High Street, London, N6 5JY Telephone: 020 3213 0620 Email: ka@blsurveyors.com</a></li>
            <li><a>Tughan Musa BSc(Hons) MFPWS, Berry Lodge Surveyors, Upper Floor, 61 Highgate High Street, London, N6 5JY Telephone: 020 3213 0624 Email: tm@blsurveyors.com</a></li>
        </ul>
        <ul class="dropdown-menu" role="menu">
            @foreach ($surveyor as $person)
                <li data-sr="b-sr" 
                data-b-name="{{ $person->name}}" 
                data-b-qualifications="{{ $person->qualifications}}" 
                data-b-company="{{ $person->company}}"
                data-b-address="{{ $person->address}}"
                data-b-contact="Telephone: {{ $person->telephone}} Email: {{ $person->email}}"><a>{{ $person->name}} {{ $person->qualifications}} , {{ $person->company}}, {{ $person->address}} Telephone: {{ $person->telephone}} Email: {{ $person->email}}</a></li>
            @endforeach    
        </ul>
    </div> -->
    <div class="form-group{{$errors->has('s_surveyor_full_information') ? ' has-error' : ''}}">
        <label for="s_surveyor_full_information" class="control-label">Surveyor Full Information:</label>
        <input id="s_surveyor_full_information"  class="form-control autocomplete" name="s_surveyor_full_information" value="@isset($jobs->surveying->s_surveyor_full_information ){{$jobs->surveying->s_surveyor_full_information}}@endisset{{old('s_surveyor_full_information')}}" data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            @foreach ($surveyor as $person)
                <li data-sr="b-sr" 
                data-b-name="{{ $person->name}}" 
                data-b-qualifications="{{ $person->qualifications}}" 
                data-b-company="{{ $person->company}}"
                data-b-address="{{ $person->address}}"
                data-b-email="{!! $person->email !!}"
                data-b-contact="Telephone: {{ $person->telephone}} Email: {{ $person->email}}"><a>{{ $person->name}} {{ $person->qualifications}} , {{ $person->company}}, {{ $person->address}} Telephone: {{ $person->telephone}} Email: {{ $person->email}}</a></li>
            @endforeach    
        </ul>       
    </div>
</div>
<input type="hidden" name="surveryor_email" id="surveyor_email">
<div class="col-md-6 col-sm-6">
    <!-- <div class="form-group{{ $errors->has('surveryor_name') ? ' has-error' : '' }}">
        <label for="surveryor_name" class="control-label">Surveyor Name:</label>
        <input id="surveryor_name" type="text" class="form-control" name="surveryor_name" value="@isset( $jobs->surveying->surveryor_name ){{$jobs->surveying->surveryor_name }}@endisset{{old('surveryor_name') }}" >
    </div> -->
    <div class="form-group{{$errors->has('surveryor_name') ? ' has-error' : ''}}">
        <label for="surveryor_name" class="control-label">Surveyor Name:</label>
        <input id="surveryor_name" type="surveryor_name" class="form-control" name="surveryor_name" value="@isset($jobs->surveying->surveryor_name ){{$jobs->surveying->surveryor_name}}@endisset{{old('surveryor_name')}}" >
        
    </div>
</div>

<div class="col-md-6 col-sm-6">
   <!--  <div class="form-group{{ $errors->has('s_surveyor_qualifications') ? ' has-error' : '' }}">
        <label for="s_surveyor_qualifications" class="control-label">Surveyor Qualifications:    </label>
        <input id="s_surveyor_qualisications" type="text" class="form-control" name="s_surveyor_qualifications" value="@isset( $jobs->surveying->s_surveyor_qualifications ){{$jobs->surveying->s_surveyor_qualifications }}@endisset{{old('s_surveyor_qualifications') }}" >
    </div> -->
    <div class="form-group{{$errors->has('s_surveyor_qualifications') ? ' has-error' : ''}}">
        <label for="s_surveyor_qualifications" class="control-label">Surveyor Qualifications:</label>
        <input id="s_surveyor_qualifications" type="s_surveyor_qualifications" class="form-control" name="s_surveyor_qualifications" value="@isset($jobs->surveying->s_surveyor_qualifications ){{$jobs->surveying->s_surveyor_qualifications}}@endisset{{old('s_surveyor_qualifications')}}" >
        
    </div>
</div>

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('a_surveyor_contact_details') ? ' has-error' : '' }}">
        <label for="surveyor_contact_details" class="control-label">Surveyor Contact Details:</label>
        <input id="surveyor_contact_details" type="text" class="form-control" name="surveyor_contact_details" value="@isset( $jobs->surveying->surveyor_contact_details ){{$jobs->surveying->surveyor_contact_details }}@endisset{{ old('a_surveyor_contact_details') }}" >
    </div>
</div>

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('client_full_name') ? ' has-error' : '' }}">
        <label for="client_full_name" class="control-label">Client Full Name(s):  </label>
        <input id="client_full_name"  class="form-control " name="client_full_name" value="@isset( $jobs->surveying->client_full_name ){{$jobs->surveying->client_full_name }}@endisset{{old('client_full_name') }}">
    </div>
</div>

 

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('client_salutation') ? ' has-error' : '' }}">
        <label for="client_salutation" class="control-label">Client Salutation: </label>
        <input id="client_salutation" class="form-control " name="client_salutation" value="@isset( $jobs->surveying->client_salutation ){{$jobs->surveying->client_salutation }}@endisset{{old('client_salutation') }}">
    </div>
</div>



<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('address_of_inspection') ? ' has-error' : '' }}">
        <label for="address_of_inspection" class="control-label">Address of Inspection:  </label>
        <input id="address_of_inspection" type="text" class="form-control" name="address_of_inspection" value="@isset( $jobs->surveying->address_of_inspection ){{$jobs->surveying->address_of_inspection }}@endisset{{old('address_of_inspection') }}">
    </div>
</div>

   

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('client_contact_address') ? ' has-error' : '' }}">
        <label for="client_contact_address" class="control-label">Client Contact Address:  </label>
        <input id="client_contact_address" type="text" class="form-control" name="client_contact_address" value="@isset( $jobs->surveying->client_contact_address ){{$jobs->surveying->client_contact_address }}@endisset{{old('client_contact_address') }}" >
    </div>
</div>



<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('client_contact_details') ? ' has-error' : '' }}">
        <label for="client_contact_details" class="control-label">Client Contact Details (Telephone Numbers and Email):   </label>
        <input id="client_contact_details"  class="form-control" name="client_contact_details" value="@isset( $jobs->surveying->client_contact_details ){{$jobs->surveying->client_contact_details }}@endisset{{old('client_contact_details') }}" >
    </div>
</div>
  

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('client_i_we_upper') ? ' has-error' : '' }}">
        <label for="client_i_we_upper" class="control-label">Client I/we (referral) Upper case:</label>
        <input id="client_i_we_upper"  class="form-control autocomplete" name="client_i_we_upper" value="@isset( $jobs->surveying->client_i_we_upper ){{$jobs->surveying->client_i_we_upper }}@endisset{{old('client_i_we_upper') }}" autocomplete="off"  data-toggle="dropdown">
        <ul class="dropdown-menu" role="menu">
            <li><a>I</a></li>
            <li><a>We</a></li>
        </ul>
    </div>
</div>

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('client_i_we_lower') ? ' has-error' : '' }}">
        <label for="client_i_we_lower" class="control-label">Client I/we (referral) Lower case:</label>
        <input id="client_i_we_lower"  class="form-control autocomplete" name="client_i_we_lower" value="@isset( $jobs->surveying->client_i_we_lower ){{$jobs->surveying->client_i_we_lower }}@endisset{{old('client_i_we_lower') }}" autocomplete="off"  data-toggle="dropdown">
        <ul class="dropdown-menu" role="menu">
            <li><a>I</a></li>
            <li><a>we</a></li>
        </ul>
    </div>
</div>
  <div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('survey_email') ? ' has-error' : '' }}">
        <label for="survey_email" class="control-label">Surveyor to receive Client Messages</label>
        <input id="survey_email"  class="form-control autocomplete" name="survey_email" 
        value="@isset($jobs->survey_email){{$jobs->survey_email}}@endisset{{ old('survey_email') }}" autocomplete="off"  data-toggle="dropdown" >
        <ul class="dropdown-menu" role="menu">
            @foreach($surveyors_email as $surveyor)
            <li ><a>{{$surveyor->name }}, Email :{{ $surveyor->email}}</a></li>
            @endforeach
        </ul>
    </div>
</div>

<div class="col-md-12 col-sm-12">
    <div class="form-group{{ $errors->has('additional_info') ? ' has-error' : '' }}">
        <label for="additional_info" class="control-label">Additional Information for File Sheet (Client Comment/Job Comment):</label>
        <input id="additional_info" type="text" class="form-control" name="additional_info" value="@isset( $jobs->surveying->additional_info ){{$jobs->surveying->additional_info }}@endisset{{old('additional_info') }}" >
    </div>
</div>
