<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\User_History;

class EnquiryController extends Controller
{
    public function index($id=null){
    	if($id == null){ 
  		$enq= \App\Enquiry::latest()->first();
      $job= \App\Job::where('enq_id', $enq->id)->first();
  	 }
  	else{
  		$enq= \App\Enquiry::find($id);
      $job= \App\Job::where('enq_id', $id)->first();
  	}
	   
    	return view('admin.enquiries', ["enq"=>$enq, "all"=>\App\Enquiry::orderBy('id', 'desc')->get(), 'id'=>$id , 'job'=> $job ?? null]);
    }

    public function update(Request $req){
  	if($req->id == null){ 
  		 $enq = new \App\Enquiry;
  	}
  	else{
  		$enq= \App\Enquiry::find($req->id); 
		
  	}
  	
  	$enq->user_id = Auth::user()->id;
    	$enq->name = $req->name ?? null;
    	$enq->contact = $req->contact ?? null;
    	$enq->area = $req->area ?? null;
      $enq->postcode = $req->postcode ?? null;
    	$enq->calls = $req->calls ?? null;
    	$enq->quote = $req->quote;
    	$enq->hear = $req->hear;
    	$enq->details = $req->details;
    	$enq->notes = $req->notes;
    	$enq->save();
    	$user_history = new User_History;
      $user_history->user_id = Auth::user()->id;
      if($req->id == null){ 
        $user_history->action = "create enquiry";
        $user_history->description = (Auth::user()->name." has created enquiry");
      }
      else{
        $user_history->action = "update enquiry";
        $user_history->description = (Auth::user()->name." has updated enquiry of Id ".$req->id);
      }
      $user_history->save();
	   return view('admin.enquiries', ["enq"=>$enq, "all"=>\App\Enquiry::orderBy('id', 'desc')->get(), 'id'=>null ]);
    }

    public function delete(Request $req){

      $enq= \App\Enquiry::find($req->enq_id); 
      $enq->delete();
      $user_history = new User_History;
      $user_history->user_id = Auth::user()->id;
      $user_history->action = "delete enquiry";
      $user_history->description = (Auth::user()->name." has deleted enquiry Id".$req->enq_id);
      $user_history->save();
      return 'done';
    }
    
    public function upload(Request $request){

       $upload = $request->file('file');
    
       
        $store = $upload->storeAs(
            "public/enquiries/{$request->job_id}", time().'-'.$upload->getClientOriginalName()
        );


        $doc = new \App\EnqDocs;
        $doc->job_id =$request->job_id;
        $doc->user_id = Auth::user()->id;
        $doc->folder =$request->filetype;
  
        $doc->original_name =$upload->getClientOriginalName();
        $doc->type =$upload->getClientOriginalExtension();
        $doc->mimetype =$upload->getMimeType();
        $doc->link = str_replace('public','storage',$store);
        $doc->size = $upload->getSize();      
        $doc->save();
	return $doc;
	
  }

     public function deleteDoc(Request $request){ 
        $doc =  \App\EnqDocs::find($request->doc_id);
        $del=  Storage::delete(str_replace('storage','public', $doc->link) );
        if ($del) {
        $doc->delete();
        return 'done';
        }
    }

}
