<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Mail};
use Illuminate\Support\Facades\Storage;

class JobsController extends Controller
{
    public function jobs(){
        $docs = \App\Document::all();
        $jobdocs = $docs->groupBy('job_id');
        //$jobs = \App\Job::all();
        $jobs = \App\Job::where('status', 1)->get();
        return view('admin.jobs', ['jobs'=>$jobs, 'docs'=>$jobdocs,]);
    }

    public function job($job_id=null, $type =null){
        $user =Auth::user();

        if ($user->role==3 || $user->role== 1 ) {

            if( $user->jobs->count() < 1 ){
                $error= "No Jobs";
                return view('client.waiting',compact('error'));
            }

           $job_id ? $job = $user->jobs->where('id', $job_id)->first() : $job = $user->jobs->last() ;

           if ($type) {
              return view('client.document_type', ['job'=>$job, 'type'=>$type, 'job_id'=>$job_id ]);
           }
            return view('client.document', ['job'=>$job, 'type'=>$type, 'job_id'=>$job_id ]);
        }

        $jobs = \App\Job::all();
        $docs = \App\Document::where('job_id', $job_id)->get();
        if ($type) {
           return view('admin.jobs_documents_type', ['jobs'=>$jobs, 'docs'=>$docs, 'job_id'=>$job_id, 'type'=>$type]);
        }

        $job_info = \App\Job::find($job_id);
        return view('admin.jobs_documents', ['jobs'=>$jobs, 'docs'=>$docs, 'job_id'=>$job_id, 'job_info'=>$job_info]);

    }


    public function archived(){
        $docs = \App\Document::all();
        $jobdocs = $docs->groupBy('job_id');
        $jobs = \App\Job::where('status', 8)->get();
        return view('admin.jobs', ['jobs'=>$jobs, 'docs'=>$jobdocs,]);
    }


    public function upload(Request $request){


        $upload = $request->file('file');

        $store = $upload->storeAs(
            "public/jobs/{$request->job_id}/jobs", time().'-'.$upload->getClientOriginalName()
        );



        $doc = new \App\Document;
        $doc->job_id =$request->job_id;
        $doc->user_id = Auth::user()->id;
        $doc->original_name =$upload->getClientOriginalName();
        $doc->extension =$upload->getClientOriginalExtension();
        $doc->type=$request->filetype;
        $doc->mimetype =$upload->getMimeType();
        $doc->link = str_replace('public','storage',$store);
        $doc->size = $upload->getSize();
        $doc->save();


        $job = \App\Job::find($request->job_id);

        foreach ($job->users  as $user){
            if ($user->notification == 1 && $user->id != Auth::user()->id){
                if ($user->role==3) {
                    $sento= env('TEST_MAIL');
                    Mail::send('emails.document_upload', [  'client_name' => $user->name, 'surveyor_name'=>$job->users->whereIn('role', [7,9] )->first()->name ],
                        function ($message) use($sento) {
                            $message->subject('New Document - Berry Lodge Surveyors');
                            $message->to($sento);
                        }
                    );
                }
                elseif($user->role==7){
                    $sento= env('TEST_MAIL');
                    Mail::send('emails.document_upload_admin', [  'client_name' => Auth::user()->name, 'job_id'=>$request->job_id, 'address'=> $job->bo->property_address_proposed_work],
                        function ($message) use($sento) {
                            $message->subject('New Document - Berry Lodge Surveyors');
                            $message->to($sento);
                        }
                    );
                }
            }
        }


        return $doc;
    }

    public function delete(Request $request){
        $doc =  \App\Document::find($request->doc_id);
        $del=  Storage::delete(str_replace('storage','public', $doc->link) );
        if ($del) {
        $doc->delete();
        return 'done';
        }
    }


}
