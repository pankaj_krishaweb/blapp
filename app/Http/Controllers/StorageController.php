<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\{Auth, Storage, URL, Session};


class StorageController extends Controller
{
    private $rootfolder; 
    //protected $redirectTo = '/home';
    public function __construct()
    {
    	
    	 //if ( !Session::has('name') ){
    	 /*if(Auth::check()) { }else{
		 	
    		 header('Location: '.URL::to('/'));
		 }*/
    	
        $this->rootfolder = "InHouse";
    }
   
    public function index(Request $request){        
        
        $sel_folder = $request->input('sel_folder');
        $role = Auth::user()->role;
        
        $nav_action = $request->input('nav_action');
        
        if( $nav_action == 'nav_back' ){
           $tmp = explode("/", $sel_folder);
           $sel_folder = (count($tmp) > 0)? implode("/", array_slice($tmp, 0, -1)): "";
        }
        
        if( $nav_action == 'add_folder' ){
           $sel_uplfolder = $request->input('sel_uplfolder');
           $new_folder = $request->input('new_folder');
           $new_folder = ($new_folder)? preg_replace('/[^a-zA-Z0-9_.\\s+]/', '', $new_folder):"";
           Storage::disk('public')->makeDirectory("$sel_uplfolder/$new_folder");
        }
        
        if( $nav_action == 'del_folder' ){
           $sel_delfolder = $request->input('sel_delfolder');
           Storage::disk('public')->deleteDirectory($sel_delfolder);
           \App\InhouseStorage::where(['path' => $sel_delfolder])->delete();
        }
        
        if( $nav_action == 'del_file' ){
           $sel_delfile = $request->input('sel_delfile');
           Storage::disk('public')->delete($sel_delfile);
        }
        
        if( $nav_action == 'move_file' ){
           $movefrom_f = $request->input('movefrom_f');
           $moveto_f = $request->input('moveto_f');
           $foldersplit = explode("/", $movefrom_f);
           $file = end($foldersplit);
           if( file_exists($moveto_f."/$file") ){
              $files = Storage::disk('public')->files($moveto_f);
             $count = count($files) + 1;
             //$filename .= "_$count";
             $filename = preg_replace('~\.(?!.*\.)~', "_copy$count.", $filename);
           }
           Storage::disk('public')->move($movefrom_f, $moveto_f."/$file");
        }
        
        if( $nav_action == 'admin_chk' ){
           $admin_folder = $request->input('admin_folder');
           $admin_chk = $request->input('admin_chk');
           \App\InhouseStorage::updateOrCreate(['path' => $admin_folder], ['is_admin' => $admin_chk]);
           //Storage::disk('public')->delete($sel_delfile);
        }
        

        if( $nav_action=='search_file' ){
           $files = Storage::disk('public')->files($sel_folder);
           if( count($files) < 1 ){
              /*$spath = explode("/", $sel_folder);
              if( count($spath) > 2 ){
                 $tmpex = array_splice($spath, -2);
                 $sel_folder = implode("/", $spath);
              }else{
                 $sel_folder = dirname($sel_folder);
              }*/
              $sel_folder = dirname($sel_folder);
              //$sel_folder = Storage::disk('public')->path($sel_folder);
              
           }
        }
        
        $sel_folder = (!empty($sel_folder))? $sel_folder : $this->rootfolder;
        //$sel_folder = "InHouse";
        $directories = Storage::disk('public')->directories($sel_folder);
      
        $directories = array_diff($directories, ['.', '..']);
        if( empty($directories) or $nav_action=='search_file' ){
           $directories = [$sel_folder];
        }/*else{
           $stg_table = \App\InhouseStorage::all()->pluck('path')->toArray();
        }*/
        $stg_table = \App\InhouseStorage::all()->where('is_admin',1)->pluck('path')->toArray();
        
        $folders = array();
        foreach($directories as $k=>$each){
           if( in_array($each, $stg_table) and $role!=9 ){
              continue;
           }
           $foldersplit = explode("/", $each);
           $folder = end($foldersplit);
           
           $sub_folders = Storage::disk('public')->directories($each);
           $sub_folders = ($sub_folders)?? array();
           
           $folders[$k]['parent'] = $sel_folder;
           $folders[$k]['folder'] = $folder;
           $folders[$k]['folder_full'] = $each;
           $folders[$k]['files'] = Storage::disk('public')->files($each);
           
           $jsonar[$k]['text'] = $folder;
           $jsonar[$k]['href'] = $each;
           
           //$folders[$k]['sub_folders'] = $sub_folders;
           foreach($sub_folders as $j=>$subeach){
              if( in_array($subeach, $stg_table) and $role!=9 ){
                 continue;
              }
              $subsplit = explode("/", $subeach);
              $folder = end($subsplit);
              $folders[$k]['sub_folders'][$j]['parent'] = $subsplit[0];
              $folders[$k]['sub_folders'][$j]['folder'] = $folder;
              $folders[$k]['sub_folders'][$j]['folder_full'] = $subeach;
              
              $jsonar[$k]['nodes'][$j]['text'] = $folder;
              $jsonar[$k]['nodes'][$j]['href'] = $subeach;
           }
        }
        
        $alldirs = Storage::disk('public')->allDirectories($this->rootfolder);
        $allfiles = Storage::disk('public')->allFiles($this->rootfolder);
        $alldirs = array_merge($alldirs, $allfiles);
        $alljsonar = $alldirs_e = array();
        /*foreach($alldirs as $allf){
           $alldirs_e[] = [$allf];
        }*/
        
        $alljsonar = $this->listFolders($this->rootfolder);
        
        $jsondt = json_encode($alljsonar);

        //dd($folders);
        return view('admin.storage', ['folders'=>$folders, 'rootdir'=>$this->rootfolder, 'sel_folder'=>$sel_folder, 'stgtable'=>$stg_table, 'jsondt'=>$jsondt, 'alldirs'=>$alldirs]);
        //return view('admin.storage');
    }
    
    
    public function listFolders($path)
    {
       $dh = Storage::disk('public')->directories($path); //scandir($dir);
       $return = array();

       foreach ($dh as $folder) {

         $foldersplit = explode("/", $folder);
         $foldername = end($foldersplit);
         $return[] = array('text'=>$foldername, 'href'=>$folder, 'nodes' => $this->listFolders($folder));
       }
       return $return;
    }
    
    public function upload(Request $request){
    
       //$doc = new \App\JobDocs;
       $target_folder = "";
       if( !empty($request->target_dir) ){
       	//$target_dir = ($request->target_dir)? preg_replace('/[^a-zA-Z0-9_.]/', '_', $request->target_dir):"";
         $target_dir = $request->target_dir;
   	 }
       
	    $target_dir = ($target_dir)?: $this->rootfolder; //;
    
   	 $upload = $request->file('file');
       $filename = $upload->getClientOriginalName();
       if($filename){
          $filename = preg_replace('/[^a-zA-Z0-9_.\\s+]/', '', $filename);
       }
       $exists = Storage::disk('public')->exists("$target_dir/$filename");
       if($exists){
          $files = Storage::disk('public')->files($target_dir);
          $count = count($files) + 1;
          //$filename .= "_$count";
          $filename = preg_replace('~\.(?!.*\.)~', "_copy$count.", $filename);
       }
       $store = $upload->storeAs(
           /*"public/$target_dir/", time().'-'.$upload->getClientOriginalName()*/
           "public/$target_dir/", $filename
       );

       /*$doc->original_name =$upload->getClientOriginalName();
       $doc->type =$upload->getClientOriginalExtension();
       $doc->mimetype =$upload->getMimeType();
       $doc->link = str_replace('public','storage',$store);
       $doc->size = $upload->getSize();
       $doc->save();
       return $doc;*/
  }
    
    

}