 <div class="col-md-9 col-xs-12" style="margin-bottom: -1px;">
            <div class="row" id="admin-menu">
         
        
        <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client/discount') }}">
                <div class="admin-menu-item @if (Request::is('client/discount*')) item-active @endif">
                    <i class="fa fa-thumbs-up" style="font-size:24px"></i><br><p style="font-family: Poppins; line-height: 1.25;">Discount Codes</p>
                </div>
            </a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client/our-community') }}">
                <div class="admin-menu-item @if (Request::is('client/our-community*')) item-active @endif">
                    <i class="fa fa-users fa-fw" style="font-size:24px"></i><br><p style="font-family: Poppins; line-height: 1.25;">Our Community</p>
                </div>
            </a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client/message') }}">
                <div class="admin-menu-item @if (Request::is('client') || Request::is('client/message*')) item-active @endif">
                    <i class="fa fa fa-commenting-o" style="font-size:24px"></i><br><p style="font-family: Poppins; line-height: 1.25;">Send Messages</p>
                </div>
            </a>
        </div>
        
        {{-- <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client') }}">
                <div class="admin-menu-item @if (Request::is('client') || Request::is('client/tasks*')) item-active @endif">
                    <i class="fa fa-clipboard" style="font-size:24px"></i><p style="font-family: Poppins; line-height: 1.25;">Job <br> Tasks</p>
                </div>
            </a>
        </div> --}}
        <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client/meet-your-surveyor') }}">
                <div class="admin-menu-item @if (Request::is('client/meet-your-surveyor*')) item-active @endif">
                    <i class="fa fa-user fa-fw" style="font-size:24px"></i><br><p style="font-family: Poppins; line-height: 1.25;">Meet Your Surveyor</p>
                </div>
            </a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{!! url('client/terms-and-condition') !!}" target="_blank">
                <img src="{!! asset('images/T&C.png') !!}" style="width: 80px;height: 80px;">
            </a>
        </div>
              
            </div>
        </div>