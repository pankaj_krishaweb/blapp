<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SitePayment extends Model
{
    protected $table = 'site_payments';

     public function getCreatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value)); 
    }
    public function getUpdatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value));   
    }
}
