<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('job_type') ? ' has-error' : '' }}">
       <label for="job_type" class="control-label">Job Type</label>
       <input id="job_type" type="text" class="form-control" name="job_type" value="@isset( $jobs->job_type ){{  $jobs->job_type }} @endisset {{ old('job_type') }}" >
   </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('invoice_no') ? ' has-error' : '' }}">
       <label for="invoice_no" class="control-label">Invoice No</label>
       <input id="invoice_no" type="text" class="form-control" name="invoice_no" value="@isset( $jobs->invoice_no ){{  $jobs->invoice_no }} @endisset {{ old('invoice_no') }}" >
   </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('notice_costs') ? ' has-error' : '' }}">
        <label for="notice_costs" class="control-label">Notice Costs</label>
        <input id="notice_costs" type="text" class="form-control" name="notice_costs" value="@isset( $jobs->notice_costs ){{  $jobs->notice_costs }} @endisset {{ old('notice_costs') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('award_costs') ? ' has-error' : '' }}">
        <label for="award_costs" class="control-label">Award Costs</label>
        <input id="award_costs" type="award_costs" class="form-control" name="award_costs" value="@isset( $jobs->award_costs ){{  $jobs->award_costs }} @endisset {{ old('award_costs') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('land_registry_costs') ? ' has-error' : '' }}">
        <label for="land_registry_costs" class="control-label">Land Registry Costs</label>
        <input id="land_registry_costs"  class="form-control autocomplete" name="land_registry_costs" value="@isset( $jobs->land_registry_costs ){{  $jobs->land_registry_costs }}@endisset {{ old('land_registry_costs') }}" data-toggle="dropdown" autocomplete="off" >
        <ul class="dropdown-menu" role="menu">
        <li><a>£6.00</a></li>
        <li><a>£9.00</a></li>
        <li><a>£12.00</a></li>
        <li><a>£18.00</a></li>
        <li><a>£24.00</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('printing_postage_costs') ? ' has-error' : '' }}">
        <label for="printing_postage_costs" class="control-label">Printing &amp; Postage Costs</label>
        <input id="printing_postage_costs" type="text" class="form-control" name="printing_postage_costs" value="@isset( $jobs->printing_postage_costs ){{  $jobs->printing_postage_costs }} @endisset {{ old('printing_postage_costs') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('vat_amount') ? ' has-error' : '' }}">
        <label for="vat_amount" class="control-label">Vat Amount</label>
        <input id="vat_amount" type="text" class="form-control" name="vat_amount" value="@isset( $jobs->vat_amount ){{  $jobs->vat_amount }} @endisset {{ old('vat_amount') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('final_amount') ? ' has-error' : '' }}">
        <label for="final_amount" class="control-label">Final Amount</label>
        <input id="final_amount" type="text" class="form-control" name="final_amount" value="@isset( $jobs->final_amount ){{  $jobs->final_amount }} @endisset {{ old('final_amount') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('surveyor_who_had_first_contact_with_owner') ? ' has-error' : '' }}">
        <label for="surveyor_who_had_first_contact_with_owner" class="control-label">Surveyor Who had first contact with owner</label>
        <input id="surveyor_who_had_first_contact_with_owner"  class="form-control autocomplete" name="surveyor_who_had_first_contact_with_owner" 
        value="@isset($jobs->surveyor_who_had_first_contact_with_owner){{$jobs->surveyor_who_had_first_contact_with_owner}}@endisset{{ old('surveyor_who_had_first_contact_with_owner') }}" autocomplete="off"  data-toggle="dropdown" >
        <ul class="dropdown-menu" role="menu">
            <li><a>BM</a></li>
            <li><a>BD</a></li>
            <li><a>TM</a></li>
            <li><a>KA</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('surveyor_dealing_with_day_to_day') ? ' has-error' : '' }}">
        <label for="surveyor_dealing_with_day_to_day" class="control-label">Surveyor dealing with day to day</label>
        <input id="surveyor_dealing_with_day_to_day" class="form-control autocomplete" name="surveyor_dealing_with_day_to_day" value="@isset( $jobs->surveyor_dealing_with_day_to_day ){{  $jobs->surveyor_dealing_with_day_to_day }}@endisset{{ old('surveyor_dealing_with_day_to_day') }}" data-toggle="dropdown" autocomplete="off" >
        <ul class="dropdown-menu" role="menu">
            <li><a>BM</a></li>
            <li><a>BD</a></li>
            <li><a>TM</a></li>
            <li><a>KA</a></li>
        </ul>
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('party_wall_notice_date') ? ' has-error' : '' }}">
        <label for="party_wall_notice_date" class="control-label">Party Wall Notice Date</label>
        <input id="party_wall_notice_date" type="text" class="form-control" name="party_wall_notice_date" value="@isset( $jobs->party_wall_notice_date ){{  $jobs->party_wall_notice_date }}@endisset {{ old('party_wall_notice_date') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ten_4_party_wall_notice_date') ? ' has-error' : '' }}">
        <label for="ten_4_party_wall_notice_date" class="control-label">10(4) Party Wall Notice Date</label>
        <input id="ten_4_party_wall_notice_date" type="text" class="form-control" name="ten_4_party_wall_notice_date" value="@isset( $jobs->ten_4_party_wall_notice_date ){{  $jobs->ten_4_party_wall_notice_date }} @endisset {{ old('ten_4_party_wall_notice_date') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('ten_4_party_wall_notice_expiry_date') ? ' has-error' : '' }}">
        <label for="ten_4_party_wall_notice_expiry_date" class="control-label">10(4) Party Wall Notice Expiry Date</label>
        <input id="ten_4_party_wall_notice_expiry_date" type="text" class="form-control" name="ten_4_party_wall_notice_expiry_date" value="@isset( $jobs->ten_4_party_wall_notice_expiry_date ){{  $jobs->ten_4_party_wall_notice_expiry_date }} @endisset {{ old('ten_4_party_wall_notice_expiry_date') }}" >
    </div>
</div>

<div class="col-md-4 col-sm-4">
    <div class="form-group{{ $errors->has('schedule_of_condition_date') ? ' has-error' : '' }}">
        <label for="schedule_of_condition_date" class="control-label">Schedule of Condition Date</label>
        <input id="schedule_of_condition_date" type="text" class="form-control" name="schedule_of_condition_date" value="@isset( $jobs->schedule_of_condition_date ){{  $jobs->schedule_of_condition_date }} @endisset {{ old('schedule_of_condition_date') }}" >
    </div>
</div>