@extends('layouts.frame')

@section('content')
<style type="text/css">
    h4{
            font-weight: bold;
    color: #414860;
    }
    h5{
color: #cccccc;
padding-top: 35px;
/*padding-bottom: 20px; */
    }
    h2 {
    color: #00b29c;
    font-weight: bold;
    font-size: 4em;
}
 #job-search-btn{

    background-color: #fff;
    color: #b3b3b3;
    border: solid 0.5px #b3b3b3;
    border-radius: 5px;
    width: 100%;
        border-radius: 0px;
  }
  .open > .dropdown-menu {
    display: block;
    width: 100%;

    margin-top: 0px;
    border-radius: 0px;
}
.list-group{
    max-height: 400px;
    margin-bottom: 10px;
    overflow:scroll;
    -webkit-overflow-scrolling: touch;
}
iframe{
 width: 100%;
  height: 600px;
}
.list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus {
    background-color: #41485f;
    border-color: #19baa6;
}
</style>
<div class="container-fluid">
  <div class="container" style="min-height: 80vh;">
    <div class="row">
      <div class="col-md-12">
        <h1>Enquiries - @isset ($enq->id) EBLSN{{$enq->id}} @endisset</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin') }}">Home</a></li>
          @if ($id)
            <li>  <a href="{{ url('admin/enquiries') }}">Enquiries</a></li>
            <li class="admin/jobs">EBLSN{{ $id }}</li>
          @else
            <li class="admin/jobs">  Enquiries</li>
          @endif
        </ol>
      </div>
      <div class="col-md-4 col-sm-4">
        <a href="{{ url('/admin/enquiries/add') }}" class="btn btn-default btn-block ">New </a> <br>
        <input type="text" class="form-control" id="job_no_search" placeholder="search"><br>
        <div class="list-group" >
          @foreach ($all as $e)
            <a class="list-group-item {{ $e->id ==$id ? 'active':'' }}" href="{{ url("/admin/enquiries/{$e->id}") }}">EBLSN{{ $e->id}} <span class="pull-right">{{ $e->postcode}}</span></a>
          @endforeach
        </div>
      </div>
      <div class="col-md-8 col-sm-8">
        <form action="{{ url('/admin/enquiries') }}" method="post" style="margin-bottom: 50px;">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $enq->id ?? null }}">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label for="created_at">Created:</label>
                      <input type="text" class="form-control" id="created_at" name="created_at" value="{{ $enq->created_at ?? null}}" readonly>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                      <label for="updated_at">Updated:</label>
                      <input type="text" class="form-control" id="updated_at" name="updated_at" value="{{ $enq->updated_at ?? null}}" readonly>
                    </div>
                </div>                
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                      <label for="name">Name:</label>
                      <input type="text" class="form-control" id="name" name="name" value="{{ $enq->name ?? null}}">
                    </div>
                </div>
            </div>

            <div class="row">
                    <div class="col-md-12">
                    <div class="form-group">
                      <label for="contact">Contact Details:</label>
                      <input type="text" class="form-control" id="contact" name="contact" value="{{ $enq->contact ?? null}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                      <label for="area">Address:</label>
                      <input type="text" class="form-control" id="area" name="area" value="{{ $enq->area ?? null }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                      <label for="postcode">Postcode:</label>
                      <input type="text" class="form-control" id="postcode" name="postcode" value="{{ $enq->postcode ?? null }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                      <label for="calls">Surveyor:</label>
                      <input type="text" class="form-control" id="calls" name="calls" value="{{ $enq->calls ?? null}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label for="quote">Quote:</label>
                      <input type="text" class="form-control" id="quote" name="quote" value="{{ $enq->quote ?? null }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                      <label for="hear">How did you hear about us:</label>
                      <input type="text" class="form-control" id="hear" name="hear" value="{{ $enq->hear ?? null}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                      <label for="details">Text:</label>
                       <textarea class="form-control" rows="5" id="details" name="details">{{ $enq->details ?? null }}</textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                      <label for="notes">Further Notes:</label>
                       <textarea class="form-control" rows="5" id="notes" name="notes">{{ $enq->notes ?? null }}</textarea>
                    </div>
                </div>
            </div>


            @if (isset($enq->id))
              <button type="submit" class="btn btn-default"> Update</button>
              <button type="button" class="btn btn-danger pull-right" id="delete-enq-btn" data-enq="{{ $enq->id }}"> Delete </button>
              @if (isset($job))
                <a href="/admin/my-jobs/view/{{ $job->id }}" class="btn btn-success" >View Job BLSN{{ $job->id}}</a>
              @else
                <button type="button" class="btn btn-success" id="assign-job-btn">Create Job</button>
              @endif
            @else
              <button type="submit" class="btn btn-default"> Add</button>
            @endif


        </form>
        @isset ($enq->id)
          <div class="panel panel-default">
            <div class="panel-heading" style="background-color:  #00B29D; color: #fff;">Uploads</div>
            <div class=" panel-body">
              <div class="row">
                @foreach (\App\EnqDocs::where('job_id',  $enq->id )->get() as $upload)
                  <div class="col-md-6">
                    <div  class="row">
                      <p class="col-md-7 col-sm-7 col-xs-7"><i class="fa fa-file-word-o" aria-hidden="true"  style="font-size: 25px;"></i>  {{ $upload->original_name  }}</p>
                      <div class="col-md-5 col-sm-5 col-xs-5 pull-right">
                        <a href="{{ asset($upload->link) }}" class="btn btn-success btn-xs pull-right" download><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                        <button data-id="{{ $upload->id }}" class="btn btn-danger btn-xs pull-right delete-upload-doc"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                        <button type="button" class="btn btn-primary btn-xs pull-right view-doc" data-link="{{ asset($upload->link) }}" data-type="{{ $upload->type}}">View</button>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
              @if (\App\EnqDocs::where('job_id',  $enq->id )->count()==0)
                 <p class="text-danger"> No uploads for this job</p>
              @endif
            </div>
          </div>
        @endisset
      </div>
    </div>
  </div>
</div>
@if($enq or false)
<div class="container-fluid" style="background-color: #414861">
  <div class="container">
    <div class="row" style="padding-top: 50px;">
      <div class="col-md-6 col-sm-6 col-xs-12">
         <h4  style="color: #fff;"><strong>Upload a File</strong></h4>
        <p>Here you can upload documents, just drag and drop anywhere on the screen to start the process. Please be aware that you can only upload the following file types:  jpg, mov, mp4, pdf, doc, docx, zip</p>
      </div>
      <div id="actions"  class="col-md-6 col-sm-6 col-xs-12">
        <div class="main-buttons  text-right">
          <button class="btn btn-success fileinput-button"><i class="glyphicon glyphicon-plus"></i><span> Add files...</span></button>
          <button type="submit" class="btn btn-primary start"><i class="glyphicon glyphicon-upload"></i><span>Upload</span></button>
          <button type="reset" class="btn btn-danger cancel"><i class="glyphicon glyphicon-ban-circle"></i><span>Cancel </span></button>
        </div>
        <div class="progress-bars">
          <span class="fileupload-process">
            <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="background-color: #414860;">
              <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
            </div>
          </span>
        </div>
      </div>
  </div>
  <div class="table table-striped" class="files" id="previews">
    <div id="template" class="file-row row" style="margin-top: 20px;">
      <div class="col-md-2 col-sm2">
        <span class="preview"><img data-dz-thumbnail /></span>
      </div>
      <div class="col-md-2 col-sm-2">
        <p><span class="name" data-dz-name></span> - <span class="size" data-dz-size></span></p>
        <strong class="error text-danger" data-dz-errormessage></strong>
      </div>
      <div class="col-md-5 col-sm-2" style="color: #fff;">
         <label class="radio-inline">
          <input type="radio" value="1"  class="folder-type"> document
         </label>
         <label class="radio-inline">
              <input type="radio" value="2"  class="folder-type">drawing
         </label>
         <label class="radio-inline">
              <input type="radio" value="3"  class="folder-type">file
         </label>
      </div>
      <div class="col-md-1 col-sm-1">
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="background-color: #414860; box-shadow: none;">
          <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
        </div>
      </div>
      <div class="col-md-2 col-sm-2">
        <div class="text-right">
          <button class="btn btn-primary btn-xs start"><i class="glyphicon glyphicon-upload"></i><span>Start</span></button>
          <button data-dz-remove class="btn btn-danger btn-xs cancel"><i class="glyphicon glyphicon-ban-circle"></i><span>Cancel</span></button>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>


@endif

{{-- doc preview modal --}}
  <div class="modal fade" id="docViewModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Doc View</h4>
        </div>
        <div class="modal-body" id="doc-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js" integrity="sha256-Rnxk6lia2GZLfke2EKwKWPjy0q2wCW66SN1gKCXquK4=" crossorigin="anonymous"></script>
@if($enq or false)
<script type="text/javascript">

// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
var previewNode = document.querySelector("#template");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
  url: "/admin/enquiries/upload", // Set the url
  thumbnailWidth: 80,
  thumbnailHeight: 80,
  parallelUploads: 20,
  params:{ job_id: {{ $enq->id  }} },
  headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  previewTemplate: previewTemplate,
  autoQueue: false, // Make sure the files aren't queued until manually added
  previewsContainer: "#previews", // Define the container to display the previews
  clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
});

myDropzone.on("addedfile", function(file) {

  file.previewElement.querySelectorAll('.folder-type').forEach( function(radio) {
    radio.setAttribute("name", file.name);
   });

  // Hookup the start button
  file.previewElement.querySelector(".start").onclick = function() {
    if($(`.folder-type[name="${file.name}"]:checked`).length==0) {

      swal("Upload "+file.name, "You need to select the folder to upload into!", "info");
      console.log($(`.folder-type[name="${file.name}"]:checked`).length)
    return;
  }
    myDropzone.enqueueFile(file);
  };
});

// Update the total progress bar
myDropzone.on("totaluploadprogress", function(progress) {
  document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
});

myDropzone.on("sending", function(file, xhr, data) {
// Show the total progress bar when upload starts


document.querySelector("#total-progress").style.opacity = "1";
var selected = $(`.folder-type[name="${file.name}"]:checked`);


if (selected.length > 0) {

    data.append("filetype",  selected.val() );
}
  // And disable the start button
  file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
});

// Hide the total progress bar when nothing's uploading anymore
myDropzone.on("queuecomplete", function(progress) {
  document.querySelector("#total-progress").style.opacity = "0";
});

// Setup the buttons for all transfers
// The "add files" button doesn't need to be setup because the config
// `clickable` has already been specified.
document.querySelector("#actions .start").onclick = function() {

      var check = true;
        $("input:radio").each(function(){
            var name = $(this).attr("name");
            if($(`input:radio[name="${name}"]:checked`).length == 0){
                check = false;
            }
        });

        if(!check){
          swal("Upload", "You need to select the upload folder for all files!", "info");

            return;
        }
        // else{
        //     alert('Please select one option in each question.');
        //      return;
        // }

  myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
};
document.querySelector("#actions .cancel").onclick = function() {
  myDropzone.removeAllFiles(true);
};


myDropzone.on("success", function(file, res) {
  $('.dz-success').remove();
  console.log(res);
});

myDropzone.on("queuecomplete", function(file) {
	window.location = window.location.href;
	//location.reload();
});


</script>
@endif
<script>
    $(document).ready(function(){
      $("#job_no_search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".list-group a").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });

$(document).on('click', '#new', function(event) {
    event.preventDefault();


});
$(document).ready(function() {

    $(".tasks").click(function() {

        var taskid = $(this).attr('data-key');

        if ($(this).is(":checked")) {
           var status="1";
        } else {
            var status="0";

        }

      $.ajax({
        url: '/tasks/checked',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {taskid: taskid, status: status},
        success: function(data){
          console.log(data);
          location.reload()
        },
      })
    });

});


$(document).on('click', '.view-doc', function(event) {
  event.preventDefault();
   $("#docViewModal").modal();
   if ($(this).attr('data-type')=='doc' || $(this).attr('data-type')=='docx' || $(this).attr('data-type')=='pdf' ) {
    $('#doc-body').html('<iframe src="https://docs.google.com/gview?url='+encodeURI($(this).attr('data-link'))+'&embedded=true"></iframe>');
   }
   else if($(this).attr('data-type')=='jpg' || $(this).attr('data-type')=='jpeg' || $(this).attr('data-type')=='png' ){
    $('#doc-body').html('<img src="'+$(this).attr('data-link')+'" style="width:100%">');
   }
   else {
    $('#doc-body').html('<h2> Sorry on-line preview not available</h2>');
   }

});

// assign to job
$(document).on('click', '#assign-job-btn', function(event) {

swal("What kind of job would you like to set-up", {
  buttons: {

    partywall: {
      text: "Party Wall",
      value: "partywall",
    },
    survey: {
      text: "Other Survey Job",
      value: "survey",
    },
  },
})
.then((value) => {
  switch (value) {

    case "partywall":

    $.ajax({
        url: '/admin/jobs/add/partywall',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {enq_id: {{ $enq->id ?? '' }} },
        success: function(data){
          console.log(data);
           if (data) {
              window.location.href = "/admin/my-jobs/view/"+data;
            }
        },
      })

    break;

    case "survey":

       $.ajax({
        url: '/admin/jobs/add/survey',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {enq_id: {{ $enq->id ?? '' }} },
        success: function(data){
          console.log(data);
          if (data) {
          window.location.href = "/admin/my-jobs/view/"+data;
        }
        },
      })
      break;

    default:

  }
});
});


// delete enquirey
$(document).on('click', '#delete-enq-btn', function(event) {

event.preventDefault();
  var enq_id = $(this).attr('data-enq');

  swal("Are you sure you wish to delete this enquiry ?", {
  buttons: ["Cancel", "Yes Delete enquiry!"],
}).then(function(value){
  if(value==true){
    $.ajax({
      url: '/admin/enquiries/delete',
      type: 'POST',
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
      data: {enq_id: enq_id},
      success: function(data){
        console.log(data)

        if(data=='done'){
           location.reload();
        }
      },
    })
  }
  return false;
},
  function(){
    console.log('Not deleted');
    return false;
  });


});


 // delete doc
 $(document).on('click', '.delete-upload-doc', function(event) {

  event.preventDefault();
  var doc_id = $(this).attr('data-id');
  var doc = $(this).parent().parent();
  swal("Are you sure you wish to delete this document?", {
  buttons: ["Cancel", "Yes Delete Document!"],
}).then(function(value){
  if(value==true){
    $.ajax({
      url: '/admin/enquiries/doc/delete',
      type: 'POST',
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
      data: {doc_id: doc_id},
      success: function(data){
        console.log(data)
        // var status = $.parseJSON(data);
        if(data=='done'){
          location.reload();
        }
      },
    })
  }
  return false;
},
  function(){
    console.log('Not deleted');
    return false;
  });



});
</script>
@endsection
