@extends('layouts.frame')

@section('content')
<style>
    nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #84939e;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #84939e;
}
#job-search-btn{

    background-color: #fff;
    color: #b3b3b3;
    border: solid 0.5px #b3b3b3;
    border-radius: 5px;
    width: 100%;
    border-radius: 0px;
  }
  .open > .dropdown-menu {
    display: block;
    width: 100%;
    margin-top: 0px;
    border-radius: 0px;
    height: auto;
    max-height: 200px;
    overflow-x: hidden;
}

.list-group{
    max-height: 300px;
    margin-bottom: 10px;
    overflow:scroll;
    -webkit-overflow-scrolling: touch;
}
.list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus {
    background-color: #41485f;
    border-color: #19baa6;
}
.loder-img {
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0;
    right: 0;
    background: rgb(63, 186, 166,0.5);
    z-index: 123;
}
.loder-img img{
    position: absolute;
    left: 0;
    top: 50%;
    right: 0;
    margin: 0 auto;
    margin-top: -50px;
}
@media (min-width: 1200px){
    .container {
        width: 100% !important;
        max-width: 1170px !important;
        margin: 0 auto !important;
    }
}
</style>
<div id="app">
    <div class="loder-img" style="display: none;"><img src="{{ asset('images/loading.gif') }}" style="height: 100px;"></div>
    <div class="container-fluid">
        <div class="container">
            <div class="row">   
                <div class="col-md-12">
                    <h1>
                        Messaging 
                        <span class="badge" style="font-size: 35px; background-color: #1abaa5; border-radius: 35px;"> 
                            @{{ usersInRoom.length }}
                        </span>
                    </h1>  
                    <p>Please select a job to view messages</p> 
                    <ol class="breadcrumb"> 
                        <li><a href="{{ url('admin') }}">Home</a></li>
                       
                            <li><a href="{{ url('admin/message') }}">Message</a></li>
                       
                        <li class="admin/jobs">  BLSN{{ $job->id }}</li>
                    </ol>
                </div>
                <div class="col-md-4 col-sm-4 hidden-xs">   
                    <input type="text" class="form-control" id="job_no_search" placeholder="serach"><br>
                    <div class="list-group">                        
                        @foreach ($jobs as $j)
                            <a class="list-group-item {{ $j->id ==$job->id ? 'active':'' }}" href="{{ url('/admin/message/'.$j->id) }}">
                                BLSN{{  $j->id }} 
                                <span class="badge">{{  $j->messages->count() }} : messages </span>
                            </a>
                        @endforeach
                    </div>                       
                </div>

                <div class="col-md-8 col-sm-8 col-xs-12">           
                    <chat-log :messages="messages"></chat-log>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" style="background-color: #414861">
        <div class="container">
            <p class="text-center" style="margin-top: 20px; font-size: 20px; color: #85939f;"><strong>Reply to the conversation here ...</strong></p>
            <chat-composer v-on:messagesent="addMessage"></chat-composer>
        </div>
    </div>
</div>

 <div class="modal fade" id="introModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Intro video</h4>
        </div>
        <div class="modal-body">
            <video width="100%" id="vid" controls>
                <source src="{{ asset('video/intro.mp4') }}" type="video/mp4">
                Your browser does not support HTML5 video.
            </video>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')

<script src="{{ asset('js/custom.js') }}" ></script>
<script type="text/javascript">
    $(document).ready(function(){
      $("#job_no_search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".list-group a").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });  

        $(document).on('click', '#intro_video', function(event) {
        event.preventDefault();
        jQuery.noConflict();
        $("#introModal").modal('show');

        $("#introModal").on('shown.bs.modal', function () {
            $('#vid').get(0).play();

            
        });
    });
    $(".chat-log").animate({ scrollTop: $('.chat-log').prop("scrollHeight")+3000}, 1000); 
</script>

@endsection

