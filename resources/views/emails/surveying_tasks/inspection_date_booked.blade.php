<html>
<head>
	
	<style type="text/css">
		.float-md-left,
		.float-md-right{
			text-align: center;
		}
		h2{
			font-size: 30px;
			line-height: 40px;
			color: #00b29c;
			font-weight: bold;
		}
		@media (min-width: 768px) {
			.title-wrap{
				display: table;
				max-width: 800px;
				width: 100%;
			}
			h2{
				font-size: 47px;
				line-height: 57px;
			}
			.title-wrap .float-md-left,
			.title-wrap .float-md-right{
				display: table-cell;
				vertical-align: middle;
			}
			.title-wrap h2{
				font-size: 47px;
				line-height: 1.5;
			}
			.float-md-left{
				float: left;
				width: calc(100% - 250px);
				text-align: left;
			}
			.float-md-right{
				float: right;
				width: 250px;
				text-align: right;
			}
			.float-md-right img{
				max-width: 100%;
			}
		}
	</style>
</head>
<body >
	<div class="text-center title-wrap">
		<div class="float-md-left">
			<h2>It’s a date! <br>We’ve booked in the inspection!</h2>
		</div>
		<div class="float-md-right">
			<img src="{!! asset('images/surveying/2.png') !!}" width="200px">
		</div>
	</div>
	<div>
		<p>Dear {{$client_name}},</p>

		<p>Good News! We are pleased to confirm that we’ve progressed your {!! $surveying_service !!} Surveying Matter!</p>

		<p>{!! $surveyor_name !!} is due to inspect the subject property on {!! $date_of_inspection !!} he will be sure to update you further once he has visited site.</p>
		<p>Login to the <a href="{{ env('APP_URL') }}">Berry Lodge Client Portal</a> now to see the status of your job.</p>

		<p>Kind Regards, </p>

		<img src="{!! asset('images/docs/signature.png') !!}" width="510" style="width: 510px; max-width: 100%; height: auto;" >
		<p><strong>Berry Lodge Surveyors</strong></p>
	</div>
	@include('shard.footer')


</body>
</html>