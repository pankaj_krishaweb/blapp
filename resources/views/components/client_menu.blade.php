 <div class="col-md-9 col-xs-12" style="margin-bottom: -1px;">
    <div class="row" id="admin-menu" style="margin-right: -9px;">
        <div class="admin-menu-tab col-md-2 col-sm-3 col-xs-6 pull-left">
            <a href="{!! url('client/terms-and-condition') !!}" target="_blank">
                <div class="admin-menu-item logo-wrap">
                    <img src="{!! asset('images/T&C.png') !!}" >
                </div>
            </a>
        </div>
        <div class="admin-menu-tab col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client/payment') }}">
                <div class="admin-menu-item @if (Request::is('client/payment*')) item-active @endif">
                    <i class="fa fa-shopping-cart" style="font-size:24px"></i><br><p style="font-family: Poppins; line-height: 1.25;">Make Payment</p>
                </div>
            </a>
        </div>
        <div class="admin-menu-tab col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client/discount') }}">
                <div class="admin-menu-item @if (Request::is('client/discount*')) item-active @endif">
                    <i class="fa fa-thumbs-up" style="font-size:24px"></i><br><p style="font-family: Poppins; line-height: 1.25;">Discount Codes</p>
                </div>
            </a>
        </div>
        <div class="admin-menu-tab col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client/our-community') }}">
                <div class="admin-menu-item @if (Request::is('client/our-community*')) item-active @endif">
                    <i class="fa fa-users fa-fw" style="font-size:24px"></i><br><p style="font-family: Poppins; line-height: 1.25;">Our Community</p>
                </div>
            </a>
        </div>
        <div class="admin-menu-tab col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client/message') }}">
                <div class="admin-menu-item @if (Request::is('client/message*')) item-active @endif">
                    <i class="fa fa fa-commenting-o" style="font-size:24px"></i><br><p style="font-family: Poppins; line-height: 1.25;">Send Messages</p>
                </div>
            </a>
        </div>
        <div class="admin-menu-tab col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client/documents') }}">
                <div class="admin-menu-item @if (Request::is('client/documents*')) item-active @endif">
                  <i class="fa fa-file-o" style="font-size:24px"></i><br><p style="font-family: Poppins; line-height: 1.25;">Document Storage</p>
                </div>
            </a>
        </div>
        <div class="admin-menu-tab col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client') }}">
                <div class="admin-menu-item @if (Request::is('client') || Request::is('client/tasks*')) item-active @endif">
                    <i class="fa fa-clipboard" style="font-size:24px"></i><p style="font-family: Poppins; line-height: 1.25;">Job <br> Tasks</p>
                </div>
            </a>
        </div>
        <div class="admin-menu-tab col-md-2 col-sm-3 col-xs-6 pull-right">
            <a href="{{ url('client/meet-your-surveyor') }}">
                <div class="admin-menu-item @if (Request::is('client/meet-your-surveyor*')) item-active @endif">
                    <i class="fa fa-user fa-fw" style="font-size:24px"></i><br><p style="font-family: Poppins; line-height: 1.25;">Meet Your Surveyor</p>
                </div>
            </a>
        </div>

    </div>
</div>
