<?php

namespace App\Http\Middleware;

use Closure;

class MustBeEmployee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user= $request->user();
        if ($user) {
            if ($user->role==7 or $user->role==9 or $user->role==11) {
                return $next($request);
            }
        }
        return redirect('login');
        // abort(404, "You don't have permission");
    }
}
