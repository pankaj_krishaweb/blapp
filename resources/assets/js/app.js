require('./bootstrap');

$(document).ready(function() {

    $(".email_notification").click(function() {

        var id = $(this).attr('data-id');
        var notification = $(this).attr('data-val');


        $.ajax({
        url: '/user/notification',
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
        data: {id: id, notification: notification},
        success: function(data){
        console.log(data);
  
            
        },
    })  
    });

});