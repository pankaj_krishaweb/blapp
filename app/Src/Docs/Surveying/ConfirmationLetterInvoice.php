<?php

namespace App\Src\Docs\Surveying;
use Illuminate\Support\Facades\Storage;
use File;
class  ConfirmationLetterInvoice
{
	public $documentFolder="Confirmation Letter & Invoice";


	public $documentName="Client Service Conf Letter";

    	public function create(\App\Job $job){
    		// $domPdfPath = base_path( 'vendor/dompdf/dompdf');
    		// \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
    		// \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
	    	# make storage directory
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(11);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();
		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		$section->addTextBreak();
		$section->addText(ucwords($job->surveying->client_full_name));
		$section->addText(ucwords($job->surveying->client_contact_address));
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addTextBreak();
		$section->addText('Dear '.$job->surveying->client_salutation.',');
		$section->addTextBreak();
		$section->addText('Re: '.ucwords($job->surveying->valuation_type).' '.ucwords($job->surveying->surveying_service).' of '.ucwords($job->surveying->address_of_inspection),['bold' => true]);
		$section->addText('Authorisation to Proceed', ['bold' => true]);
		$section->addTextBreak();
		$section->addText('Thank you for confirming you would like me to undertake a '.$job->surveying->valuation_type.' '.$job->surveying->surveying_service.' of '.$job->surveying->address_of_inspection.'. I can confirm the total fixed fee in respect of this matter will be £'.$job->final_amount.' + VAT. ');
		$section->addTextBreak();
		$section->addText('*I can confirm that we are due to inspect '.$job->surveying->address_of_inspection.' on '.$job->surveying->date_of_inspection.'. ');
		$section->addTextBreak();
		$section->addText('*I can confirm that we are in the process of arranging inspection and will confirm as soon as the date has been arranged.  ');
		$section->addTextBreak();
		$section->addText('Should you have any questions please do not hesitate to ask and I will be more than happy to clarify. ');
		$section->addTextBreak();

		$section->addText('Kind Regards, ');
		$section->addTextBreak();
		$section->addimage('images/docs/signature.png',['height' => 80, 'width' => 300]);
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);
		$section->addTextBreak(2);

		$section->addText('....................................................................................................................');
		$section->addText('Signed by: '.$job->surveying->client_full_name.'				Date');
		$section->addTextBreak();
		$section->addText($job->surveying->client_i_we_upper.' confirm that '.$job->surveying->client_i_we_lower.' agree for '.$job->surveying->surveryor_name.' '.$job->surveying->s_surveyor_qualifications.' to undertake and prepare a '.$job->surveying->valuation_type.' '.$job->surveying->surveying_service.' of '.$job->surveying->address_of_inspection.' for the agreed fee of £'.$job->final_amount.' plus VAT which is to be paid prior to the release of the '.$job->surveying->valuation_type.' '.$job->surveying->surveying_service.'. ');

		// invoice
		
		$section->addPageBreak();
		$section->addText(ucwords($job->surveying->client_full_name));
		$section->addText(ucwords($job->surveying->client_contact_address));
		$section->addText(date("d F Y"), ['size'=>10], [ 'align' => 'right' ]);
		$textrun = $section->addTextRun(['alignment' => 'right']);
		$textrun->addText("Invoice Reference: ", ['bold' => true, 'size'=>10]);
		$textrun->addText('BLSN'.$job->invoice_no, ['size'=>10]);

		$textrun = $section->addTextRun(['alignment' => 'right']);
		$textrun->addText("Our Ref: ", ['bold' => true, 'size'=>10]);
		$textrun->addText('BLSN'.$job->id, ['size'=>10]);


		$section->addText('INVOICE NUMBER '.$job->invoice_no, ['bold' => true, 'size'=>14]);
		$section->addText('RE: '.$job->surveying->valuation_type.' '.$job->surveying->surveying_service.' / '.$job->surveying->address_of_inspection, ['bold' => true, 'size'=>10]);
		$section->addTextBreak();
		$phpWord->addTableStyle('myTable', ['borderColor' => '006699', 'borderSize'  => 6, 'cellMargin'  => 10, 'width' =>100]);
		$table = $section->addTable('myTable');
		$table->addRow();
		$table->addCell(6500)->addText('DESCRIPTION OF SERVICE:', ['bold'=>true, 'size'=>10]);
		$table->addCell(2500)->addText('AMOUNT:', ['bold'=>true, 'size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText($job->surveying->surveying_service , ['size'=>10]);
		$table->addCell(2500)->addText("£".$job->final_amount, ['size'=>10]);
		// if ($job->surveying->valuation_type) {
			
		// $table->addRow();
		// $table->addCell(6500)->addText($job->surveying->valuation_type, ['size'=>10]);
		// }
		$table->addRow();
		$table->addCell(6500)->addText('Vat at 20%', ['size'=>10]);
		$table->addCell(2500)->addText("£".$job->vat_amount, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('Land Registry Costs', ['size'=>10]);
		$table->addCell(2500)->addText($job->land_registry_costs, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('Printing & Postage Costs ', ['size'=>10]);
		$table->addCell(2500)->addText($job->printing_postage_costs, ['size'=>10]);
		$table->addRow();
		$table->addCell(6500)->addText('TOTAL DUE: ', ['bold'=>true, 'size'=>10]);
		$table->addCell(2500)->addText("£".$job->surveying->cost_of_service, ['bold'=>true, 'size'=>10]);
		$section->addTextBreak();
		$section->addText('Please kindly make payment within 7 days of the invoice date. ', ['size'=>10]);
		$section->addTextBreak();
		$section->addText('For ease of identification please enter the Invoice number '.$job->invoice_no.' as the payment reference.', ['size'=>10]);
		$section->addTextBreak();
		$section->addText('Payment Methods',['bold'=>true,'size'=>10, 'underline'=>'single']);
		$section->addTextBreak();
		$section->addText('Bank Payment',['bold'=>true, 'size'=>10, 'underline'=>'single']);
		$section->addTextBreak();
		$section->addText('Please make a bank/online payment to the following account:', ['size'=>10]);
		$section->addTextBreak();
		$section->addText('BERRY LODGE SURVEYORS ',['bold'=>true, 'size'=>10]);

		$textrun = $section->addTextRun();
		$textrun->addText("Bank Account Number: 			", ['size'=>10]);
		$textrun->addText('2 4 0 9 6 3 2 6', ['bold' => true, 'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("Sort Code:				", ['size'=>10]);
		$textrun->addText('5 6 - 0 0 - 1 4', ['bold' => true, 'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("Bank:					", ['size'=>10]);
		$textrun->addText('Natwest', ['bold' => true, 'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("Branch:					", ['size'=>10]);
		$textrun->addText('Baker Street, 69 Baker Street, W1U 6AT', ['bold' => true, 'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("IBAN: 					", ['size'=>10]);
		$textrun->addText('B40NWBK56001424096326', ['bold' => true,'size'=>10]);
		$textrun = $section->addTextRun();
		$textrun->addText("BIC: 					", ['size'=>10]);
		$textrun->addText('NWBKGB2L', ['bold' => true, 'size'=>10]);
		$section->addTextBreak();
		$section->addText('THANK YOU IN ADVANCE FOR YOUR PAYMENT', ['bold' => true, 'size'=>10], [ 'align' => 'center' ]);
		$section->addTextBreak();
		$section->addText('Company VAT number is 218 9003 17', ['size'=>10], [ 'align' => 'center' ]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");

		// $phpWord = \PhpOffice\PhpWord\IOFactory::load(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");
		// $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
		// $xmlWriter->save(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.pdf"); 
		// $file = new \Geqo\DocToPDF(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx");
		// $file->setTargetDir(base_path()."/storage/app/public/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}");
		// $file->execute();
		return "/storage/jobs/{$job->id}/my-jobs/surveying/{$this->documentFolder}/{$this->documentName}.docx";
    	}
}
