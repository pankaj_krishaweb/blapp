<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('invoice_no') ? ' has-error' : '' }}">
       <label for="invoice_no" class="control-label">Invoice No</label>
       <input id="invoice_no" type="text" class="form-control" name="invoice_no" value="@isset( $jobs->invoice_no ){{  $jobs->invoice_no }} @endisset {{ old('invoice_no') }}" >
   </div>
</div> 
<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('land_registry_costs') ? ' has-error' : '' }}">
        <label for="land_registry_costs" class="control-label">Land Registry Costs</label>
        <input id="land_registry_costs"  class="form-control autocomplete" name="land_registry_costs" value="@isset( $jobs->land_registry_costs ){{$jobs->land_registry_costs}}@endisset{{ old('land_registry_costs') }}" data-toggle="dropdown" autocomplete="off" >
        <ul class="dropdown-menu" role="menu">
        <li><a>£6.00</a></li>
        <li><a>£9.00</a></li>
        <li><a>£12.00</a></li>
        <li><a>£18.00</a></li>
        <li><a>£24.00</a></li>
        </ul>
    </div>
</div>
<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('printing_postage_costs') ? ' has-error' : '' }}">
        <label for="printing_postage_costs" class="control-label">Printing &amp; Postage Costs</label>
        <input id="printing_postage_costs" type="text" class="form-control" name="printing_postage_costs" value="@isset( $jobs->printing_postage_costs ){{  $jobs->printing_postage_costs }} @endisset {{ old('printing_postage_costs') }}" >
    </div>
</div>

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('vat_amount') ? ' has-error' : '' }}">
        <label for="vat_amount" class="control-label">Vat Amount</label>
        <input id="vat_amount" type="text" class="form-control" name="vat_amount" value="@isset( $jobs->vat_amount ){{  $jobs->vat_amount }} @endisset {{ old('vat_amount') }}" >
    </div>
</div>
<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('final_amount') ? ' has-error' : '' }}">
        <label for="final_amount" class="control-label">Final Amount</label>
        <input id="final_amount" type="text" class="form-control" name="final_amount" value="@isset( $jobs->final_amount ){{  $jobs->final_amount }} @endisset {{ old('final_amount') }}" >
    </div>
</div>
<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('surveying_service') ? ' has-error' : '' }}">
       <label for="surveying_service" class="control-label">Surveying Service</label>
       <input id="surveying_service"  class="form-control autocomplete" name="surveying_service" value="@isset( $jobs->surveying->surveying_service ){{$jobs->surveying->surveying_service }}@endisset{{old('surveying_service') }}" autocomplete="off"  data-toggle="dropdown">
        <ul class="dropdown-menu" role="menu">
            <li><a>RICS HomeBuyer Report</a></li>
            <li><a>Valuation Report</a></li>
            <li><a>Drive by Valuation</a></li>
            <li><a>Full Building Survey </a></li>
            <li><a>Boundary Determination Report</a></li>
            <li><a>Schedule of Condition Report </a></li>
            <li><a>Defect Analysis Report</a></li>
            <li><a>Snagging List </a></li>
            <li><a>Property Doctor Photographic Review</a></li>
            <li><a>Party Wall Notification Letter </a></li>
           <li><a>Surveyor's Written Opinion </a></li>
        </ul>
   </div>
</div>

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('date_of_inspection') ? ' has-error' : '' }}">
       <label for="date_of_inspection" class="control-label">Date of Inspection</label>
       <input id="date_of_inspection" type="text" class="form-control" name="date_of_inspection" value="@isset( $jobs->surveying->date_of_inspection ){{$jobs->surveying->date_of_inspection }}@endisset{{old('date_of_inspection') }}" >
   </div>
</div>


<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('cost_of_service') ? ' has-error' : '' }}">
        <label for="cost_of_service" class="control-label">Cost of Service</label>
        <input id="cost_of_service" type="text" class="form-control" name="cost_of_service" value="@isset( $jobs->surveying->cost_of_service ){{$jobs->surveying->cost_of_service }}@endisset{{old('cost_of_service') }}" >
    </div>
</div>

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('surveyor_who_had_first_contact_with_client') ? ' has-error' : '' }}">
        <label for="surveyor_who_had_first_contact_with_client" class="control-label">Surveyor Who had first contact with client</label>
        <input id="surveyor_who_had_first_contact_with_client"  class="form-control autocomplete" name="surveyor_who_had_first_contact_with_client" value="@isset( $jobs->surveying->surveyor_who_had_first_contact_with_client ){{$jobs->surveying->surveyor_who_had_first_contact_with_client }}@endisset{{old('surveyor_who_had_first_contact_with_client') }}"  autocomplete="off"  data-toggle="dropdown">
         <ul class="dropdown-menu"  role="menu">
        <li><a>BM</a></li>
        <li><a>BD</a></li>
        <li><a>TM</a></li>
        <li><a>KA</a></li>
        </ul>
    </div>
</div>




<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('surveyor_dealing_with_file') ? ' has-error' : '' }}">
        <label for="surveyor_dealing_with_file" class="control-label">Surveyor dealing with file: </label>
        <input id="surveyor_dealing_with_file"  class="form-control autocomplete" name="surveyor_dealing_with_file" value="@isset( $jobs->surveying->surveyor_dealing_with_file ){{$jobs->surveying->surveyor_dealing_with_file }}@endisset{{old('surveyor_dealing_with_file') }}" autocomplete="off"  data-toggle="dropdown">
         <ul class="dropdown-menu"  role="menu">
        <li><a>BM</a></li>
        <li><a>BD</a></li>
        <li><a>TM</a></li>
        <li><a>KA</a></li>
        </ul>
    </div>
</div>


<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('s_surveyor_full_information') ? ' has-error' : '' }}">
        <label for="s_surveyor_full_information" class="control-label">Surveyor Full Information:</label>
        <input id="s_surveyor_full_information" class="form-control autocomplete" name="s_surveyor_full_information" value="@isset( $jobs->surveying->s_surveyor_full_information ){{$jobs->surveying->s_surveyor_full_information }}@endisset{{old('s_surveyor_full_information') }}"  data-toggle="dropdown" autocomplete="off"  >
        <ul class="dropdown-menu" role="menu">
            <li><a>Bradley Mackenzie BA(Hons) MSc MSc MCIArb MRICS - RICS Registered Valuer - RICS Accredited Mediator, Berry Lodge Surveyors, Upper Floor, 61 Highgate High Street, London, N6 5JY Telephone: 020 3213 0623 Email: bm@blsurveyors.com</a></li>
            <li><a>Beau Monroe-Davies FDA MFBE MFPWS MCIArb - RICS Accredited Mediator, Berry Lodge Surveyors, Upper Floor, 61 Highgate High Street, London, N6 5JY Telephone: 020 3213 0622 Email: bd@blsurveyors.com
            </a></li>
            <li><a>Karim El Shenawi-Ali BSc(Hons) MFPWS, Berry Lodge Surveyors, Upper Floor, 61 Highgate High Street, London, N6 5JY Telephone: 020 3213 0620 Email: ka@blsurveyors.com</a></li>
            <li><a>Tughan Musa BSc(Hons) MFPWS, Berry Lodge Surveyors, Upper Floor, 61 Highgate High Street, London, N6 5JY Telephone: 020 3213 0624 Email: tm@blsurveyors.com</a></li>
        </ul>
    </div>
</div>

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('surveryor_name') ? ' has-error' : '' }}">
        <label for="surveryor_name" class="control-label">Surveyor Name:</label>
        <input id="surveryor_name" type="text" class="form-control" name="surveryor_name" value="@isset( $jobs->surveying->surveryor_name ){{$jobs->surveying->surveryor_name }}@endisset{{old('surveryor_name') }}" >
    </div>
</div>

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('s_surveyor_qualifications') ? ' has-error' : '' }}">
        <label for="s_surveyor_qualifications" class="control-label">Surveyor Qualifications:    </label>
        <input id="s_surveyor_qualisications" type="text" class="form-control" name="s_surveyor_qualifications" value="@isset( $jobs->surveying->s_surveyor_qualifications ){{$jobs->surveying->s_surveyor_qualifications }}@endisset{{old('s_surveyor_qualifications') }}" >
    </div>
</div>
  

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('client_full_name') ? ' has-error' : '' }}">
        <label for="client_full_name" class="control-label">Client Full Name(s):  </label>
        <input id="client_full_name"  class="form-control " name="client_full_name" value="@isset( $jobs->surveying->client_full_name ){{$jobs->surveying->client_full_name }}@endisset{{old('client_full_name') }}">
    </div>
</div>

 

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('client_salutation') ? ' has-error' : '' }}">
        <label for="client_salutation" class="control-label">Client Salutation: </label>
        <input id="client_salutation" class="form-control " name="client_salutation" value="@isset( $jobs->surveying->client_salutation ){{$jobs->surveying->client_salutation }}@endisset{{old('client_salutation') }}">
    </div>
</div>



<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('address_of_inspection') ? ' has-error' : '' }}">
        <label for="address_of_inspection" class="control-label">Address of Inspection:  </label>
        <input id="address_of_inspection" type="text" class="form-control" name="address_of_inspection" value="@isset( $jobs->surveying->address_of_inspection ){{$jobs->surveying->address_of_inspection }}@endisset{{old('address_of_inspection') }}">
    </div>
</div>

   

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('client_contact_address') ? ' has-error' : '' }}">
        <label for="client_contact_address" class="control-label">Client Contact Address:  </label>
        <input id="client_contact_address" type="text" class="form-control" name="client_contact_address" value="@isset( $jobs->surveying->client_contact_address ){{$jobs->surveying->client_contact_address }}@endisset{{old('client_contact_address') }}" >
    </div>
</div>



<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('client_contact_details') ? ' has-error' : '' }}">
        <label for="client_contact_details" class="control-label">Client Contact Details (Telephone Numbers and Email):   </label>
        <input id="client_contact_details"  class="form-control" name="client_contact_details" value="@isset( $jobs->surveying->client_contact_details ){{$jobs->surveying->client_contact_details }}@endisset{{old('client_contact_details') }}" >
    </div>
</div>
  

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('client_i_we_upper') ? ' has-error' : '' }}">
        <label for="client_i_we_upper" class="control-label">Client I/we (referral) Upper case:</label>
        <input id="client_i_we_upper"  class="form-control autocomplete" name="client_i_we_upper" value="@isset( $jobs->surveying->client_i_we_upper ){{$jobs->surveying->client_i_we_upper }}@endisset{{old('client_i_we_upper') }}" autocomplete="off"  data-toggle="dropdown">
        <ul class="dropdown-menu" role="menu">
            <li><a>I</a></li>
            <li><a>We</a></li>
        </ul>
    </div>
</div>

<div class="col-md-6 col-sm-6">
    <div class="form-group{{ $errors->has('client_i_we_lower') ? ' has-error' : '' }}">
        <label for="client_i_we_lower" class="control-label">Client I/we (referral) Lower case:</label>
        <input id="client_i_we_lower"  class="form-control autocomplete" name="client_i_we_lower" value="@isset( $jobs->surveying->client_i_we_lower ){{$jobs->surveying->client_i_we_lower }}@endisset{{old('client_i_we_lower') }}" autocomplete="off"  data-toggle="dropdown">
        <ul class="dropdown-menu" role="menu">
            <li><a>I</a></li>
            <li><a>we</a></li>
        </ul>
    </div>
</div>
  

<div class="col-md-12 col-sm-12">
    <div class="form-group{{ $errors->has('additional_info') ? ' has-error' : '' }}">
        <label for="additional_info" class="control-label">Additional Information for File Sheet (Client Comment/Job Comment):</label>
        <input id="additional_info" type="text" class="form-control" name="additional_info" value="@isset( $jobs->surveying->additional_info ){{$jobs->surveying->additional_info }}@endisset{{old('additional_info') }}" >
    </div>
</div>