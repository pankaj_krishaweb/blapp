<html>
<head></head>
<body>

<h1>Document upload</h1>

<p>Job number: <strong>BLSN{{ $job_id}}</strong> </p>
<p>Property address: <strong>{{ $address }}</strong> </p>
<p>Clients name: <strong>{{ $client_name}}</strong></p>


<p>Berry Lodge Surveyors</p>
<br>



<img src="{{ asset('/images/email_footer.png') }}">
<p>W: <a href="http://www.blsurveyors.com">www.blsurveyors.com</a> - T: <a href="tel:020 7935 2502">020 7935 2502</a> – E <a href="mailto:info@blsurveyors.com">info@blsurveyors.com</a></p>
</body>
</html>