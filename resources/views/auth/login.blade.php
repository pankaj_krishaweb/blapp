@extends('layouts.frame')

@section('content')
<style>
    @media only screen and (min-width: 1200px) {

    .custom-container-reboot { padding-left: 50px; padding-top: 48px; }
    }
</style>
<div class="container" style="min-height: 75vh;">
     <h1 class="custom-container-reboot" style="font-family: 'Poppins', sans-serif !important; font-weight: 600;">Login</h1>
     <br>
    <div class="row">
      
        <div class="col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1">        
          <p style="font-weight: bold; font-size: 1.3em; margin-left: -16px;">Please enter your details below to log in.</p>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}


                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <p style="color: #ccc;">
                                    <label for="email" class="control-label">E-Mail Address *</label>
                                </p>
                                <input id="email" type="email" class="form-control input-lg" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">  
                            <p style="color: #ccc;">                          
                                <label for="password" class=" control-label">Password *</label>
                            </p>
                                <input id="password" type="password" class="form-control input-lg" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                {{--  <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a> --}}
                            </div>

                        {{-- <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me
                                    </label>
                                </div>
                        </div> --}}

                            <div class="form-group text-center">
                                <button type="submit" class="members-login-btn btn pull-right" style="background-color: #00B39D; border-bottom: solid 2px #02937E; padding-left: 25px; padding-right: 25px;">
                                   <span style="color: #fff;"> Sign in</span>
                                </button>

                               
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
