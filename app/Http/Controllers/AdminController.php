<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Job_Tasks;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Job_User;
class AdminController extends Controller
{

    public function index(){
		$user =Auth::user();
        // user is employee
    	if ($user->role==7 ) {
			foreach ($user->jobs as $job) {
				$tasks =	\App\Job::find($job->id);
				$job->completed=$tasks->tasks->where('status',1)->count();
				$job->outstanding=$tasks->tasks->where('status',0)->count();	
			}
            $user->jobs = $user->jobs->sortByDesc('id');
			return view('admin.employee.progress_report', ['users'=>$user ]);	
    	}

        // user is admin 2 user
        if ($user->role==11) {
            foreach ($user->jobs as $job) {
                $tasks =    \App\Job::find($job->id);
                $job->completed=$tasks->tasks->where('status',1)->count();
                $job->outstanding=$tasks->tasks->where('status',0)->count();    
            }
            $user->jobs = $user->jobs->sortByDesc('id');
            return view('admin.admin2user.index', ['users'=>$user ]);   
        }
        // user is client
    	elseif($user->role==3 ||  $user->role==1){
            
            if($user->jobs->count() < 1 ){    
                return view('client.waiting', ['error'=>'No Jobs']);
            }
            $job= $user->jobs->last();
            $tasks= \App\Job::find($user->jobs->first()->id)->tasks->where('is_internal', 0);
    		return view('client.tasks', ['job'=>$job,  'tasks'=> $tasks ]);
    	}	
        // User is Admin 
    	elseif($user->role==9){
            
    		$users= \App\User::whereIN('role', [7,9])->get();
   //  		foreach ($users as $user) {
			// 	foreach ($user->jobs as $job) {
			// 		$tasks =	\App\Job::find($job->id);
			// 		$user->completed+=$tasks->tasks->where('status',1)->count();
			// 		$user->outstanding+=$tasks->tasks->where('status',0)->count();
			// 	}	
			// }
            $sitepayment =   \App\SitePayment::paginate(15);
    		return view('admin.admin', [ 'sitepayment'=> $sitepayment]);
    	}	
        // user is third party 
    	elseif($user->role==5){          
            if($user->jobs->count() == 0){
                return view('client.waiting',['error'=>'No Jobs']);
            }
            return redirect('/client/message');
    		return view('client.message', ['jobid'=>$user->jobs->first()->id]);
    	}
        // user is meditation client				       
    }

    public function progress(){
        $user =Auth::user();
        if ($user->role==11) {
            foreach ($user->jobs as $job) {
                $tasks =    \App\Job::find($job->id);
                $job->completed=$tasks->tasks->where('status',1)->count();
                $job->outstanding=$tasks->tasks->where('status',0)->count();    
            }
            $user->jobs = $user->jobs->sortByDesc('id');
            return view('admin.employee.progress_report', ['users'=>$user ]);
        }
        else{
            $users= \App\User::whereIN('role', [7,9])->get(); 
            return view('admin.progress_report', ['users'=>$users ]);
        }
    }

    public function progressReportUser($user_id){      
        $user =  \App\User::find($user_id);
        $user->jobs = $user->jobs->sortByDesc('id');
        return view('admin.progress_report_user', [ 'user' => $user]); 
    }

    public function progressReportUserJob($user_id, $job_id){
        
        $user =  \App\User::find($user_id);
        $job= \App\Job::find($job_id);


        return view('admin.progress_report_user_job', ['job'=>$job,  'user' => $user]);
        
    }

    public function receiveMail(Request $request,$id)
    {
        $email = Job_Tasks::find($id);
        return $email;
    }

    public function clientVideo()
    {
        return view('admin.client_video');
    }

    public function post_clientVideo(Request $request)
    {
        $rules=[
            'client_video' => 'required',
        ];
        $message=[
            'client_video.required' => 'URL field is required'
        ];
        //passed rules to validator
        $validator = Validator::make($request->all(),$rules,$message);
        //if validation fails then it will redirect back with errors
        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = User::find(Auth::user()->id);
        $user->client_video = $request->get('client_video');
        $user->save();

        return redirect('admin/client-video')->withSuccess('URL Uploaded Succesfully');
    }

    public function getTermsCondition(){
        return view('client.terms_and_condition');
    }
    public function surveyorRole(Request $request){
        $job = Job_User::where([['job_id','=',$request->get('job_id')],['user_id','=',$request->get('user_id')]])->first();
        $job->user_role = $request->get('role');
        $job->save();
        return response('success',202);
    }
}
