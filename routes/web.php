<?php

Route::get('/', 'HomeController@index');
Route::get('admin', 'AdminController@index')->middleware('employee');
Route::get('admin/client-video', 'AdminController@clientVideo')->middleware('employee');
Route::resource('admin/our-community', 'OurCommunitiesController');
Route::post('admin/post-client-video', 'AdminController@post_clientVideo')->middleware('employee');
Route::get('/admin/receive-mail/{id}', 'AdminController@receiveMail')->middleware('auth');
Route::post('admin/video-record', 'VideoController@video')->middleware('employee');
Route::get('admin/progress-report', 'AdminController@progress')->middleware('employee');
Route::get('/admin/progress-report/{user}', 'AdminController@progressReportUser')->middleware('employee');
Route::get('/admin/progress-report/{user_id}/{job_id}', 'AdminController@progressReportUserJob')->middleware('employee');

// messages
Route::get('admin/message', 'MessageController@intro')->middleware('auth');

Route::get('admin/message/{jobid}', 'MessageController@index')->middleware('auth');
Route::post('admin/message-image', 'MessageController@sendImage')->middleware('auth');
Route::get('admin/messages/{jobid?}', 'MessageController@getMessages')->middleware('auth');
Route::post('admin/messages', 'MessageController@postMessage')->middleware('auth');

// users
Route::get('admin/users', 'UserController@index')->middleware('admin');
Route::get('admin/users/{role}', 'UserController@index')->middleware('admin');
Route::post('admin/add-user', 'UserController@addUser')->middleware('admin');
Route::post('admin/delete-user', 'UserController@deleteUser')->middleware('admin');
Route::post('admin/user-history', 'UserController@userHistory')->middleware('admin');
Route::post('admin/user-edit', 'UserController@userEdit')->middleware('admin');
Route::post('user/notification', 'UserController@notification')->middleware('employee');

Route::get('admin/enquiries/{id?}', 'EnquiryController@index')->middleware('employee');
Route::post('admin/enquiries', 'EnquiryController@update')->middleware('employee');
Route::post('admin/enquiries/upload', 'EnquiryController@upload')->middleware('employee');
Route::post('admin/enquiries/delete', 'EnquiryController@delete')->middleware('employee');
Route::post('/admin/enquiries/doc/delete', 'EnquiryController@deleteDoc')->middleware('employee');


// my jobs
Route::get('admin/my-jobs', 'MyJobsController@index')->middleware('employee');
Route::get('/admin/my-jobs/archived', 'MyJobsController@archived')->middleware('employee');
Route::get('admin/my-jobs/sketch-pad/{job_id}', 'MyJobsController@sketchPad')->middleware('employee');

Route::get('/admin/my-jobs/add/{type}', 'MyJobsController@add')->middleware('employee');
Route::get('/admin/my-jobs/view/{jobid}', 'MyJobsController@view')->middleware('employee');
Route::get('/admin/my-jobs/tasks/{job_id}', 'MyJobsController@task')->middleware('employee');
Route::post('/admin/jobs/tasks', 'MyJobsController@addTask')->middleware('employee');
Route::post('admin/jobs/add/partywall/{jobid?}', 'MyJobsController@create')->middleware('employee');


Route::post('/admin/jobs/add/survey/{jobid?}', 'MyJobsController@createSurvey')->middleware('employee');

Route::post('/admin/my-jobs/tasks/delete', 'MyJobsController@deleteTask')->middleware('employee');
Route::post('/admin/my-job/delete-payment', 'MyJobsController@deletePayment')->middleware('employee');
Route::post('/admin/my-job/settled-payment', 'MyJobsController@settlePayment')->middleware('employee');
Route::post('admin/my-jobs/upload', 'MyJobsController@upload')->middleware('employee');
Route::post('admin/my-jobs/archive', 'MyJobsController@archive')->middleware('employee');



Route::post('/admin/job/payment', 'MyJobsController@payment')->middleware('employee');
Route::post('/admin/job/add-payment', 'MyJobsController@addPayment')->middleware('employee');


Route::post('admin/job-history', 'MyJobsController@userHistory')->middleware('employee');

Route::get('/admin/my-jobs/users/{job_id}', 'MyJobsController@users')->middleware('employee');
Route::get('/admin/my-jobs/users/{job_id}/{user_id}', 'MyJobsController@addUser')->middleware('employee');
Route::get('/admin/my-jobs/users/delete/{job_id}/{user_id}', 'MyJobsController@deleteUser')->middleware('employee');

Route::post('admin/my-jobs/delete', 'MyJobsController@deleteJob')->middleware('employee');

// tasks
Route::get('admin/tasks', 'TaskController@index')->middleware('employee');
Route::post('admin/render', 'TaskController@render')->middleware('auth');
Route::get('admin/tasks/{type}', 'TaskController@index')->middleware('employee');
Route::post('admin/add-task', 'TaskController@addTask')->middleware('employee');
Route::post('admin/delete-task', 'TaskController@delTask')->middleware('employee');
Route::post('admin/rename-task', 'TaskController@renameTask')->middleware('employee');
Route::post('admin/send-mail', 'TaskController@sendMail')->middleware('employee');

// storage
Route::get('admin/storage', 'StorageController@index')->middleware('employee');
Route::post('admin/storage', 'StorageController@index')->middleware('employee');
Route::post('admin/storage/upload', 'StorageController@upload')->middleware('employee');

Route::post('admin/storage/server/{action?}', 'StorageController@server')->middleware('employee');
Route::get('admin/storage/server/{action?}', function () { return view('admin.engine-fm.server'); });
Route::post('admin/jobs/surveyor-role','AdminController@surveyorRole')->middleware('employee');
//Route::get('/home', 'HomeController@index');
/*Route::get('admin/storage/server/{action?}', function ($action="") { 

return view('admin.engine-fm.server');
});*/

Route::get('admin/my-jobs/doc/regen',  'ExportController@regen')->middleware('employee');


Route::get('admin/jobs', 'JobsController@jobs')->middleware('employee');
Route::get('admin/jobs/archived', 'JobsController@archived')->middleware('employee');
Route::get('admin/jobs/{job_id}/{type?}', 'JobsController@job')->middleware('employee');
Route::post('/admin/jobs/doc/delete', 'JobsController@delete')->middleware('employee');
Route::post('/admin/upload',  'JobsController@upload')->middleware('auth');

// doc generator
Route::get('admin/my-jobs/doc/{jobid}',  'ExportController@gen')->middleware('employee');
Route::get('admin/my-jobs/surveying/{jobid}',  'ExportController@genSurveying')->middleware('employee');

Route::get('admin/my-jobs/download/{jobid}',  'ExportController@download')->middleware('employee');
Route::get('admin/my-jobs/pdf/{jobid?}',  'ExportController@pdf')->middleware('employee');
Route::get('admin/my-jobs/download-standard-documents/{jobid}',  'ExportController@downloadStandardPdf')->middleware('employee');
Route::get('admin/my-jobs/download-uploaded-documents/{jobid}',  'ExportController@downloadUploadedPdf')->middleware('employee');
Route::post('admin/my-jobs/download-sketchpad/{jobid}',  'ExportController@downloadSketch')->middleware('employee');


Route::post('tasks/checked',  'TaskController@checked')->middleware('auth');

Route::post('admin/my-jobs/doc/delete',  'MyJobsController@deleteDoc')->middleware('auth');
Route::post('admin/my-jobs/doc/transfer',  'MyJobsController@transferDoc')->middleware('auth');
Route::post('admin/my-jobs/doc/download-file',  'MyJobsController@downloadFile')->middleware('auth');
Route::get('admin/my-jobs/doc/download-user-history/{user}',  'MyJobsController@downloadUserHistory')->middleware('auth');
Route::post('admin/my-jobs/doc/download-user-history',  'MyJobsController@selectUserHistory')->middleware('auth');
Route::post('admin/my-jobs/doc/rename-file',  'MyJobsController@rename')->middleware('auth');
Route::post('admin/my-jobs/doc/get-standard-folder',  'MyJobsController@getStandardFolder')->middleware('auth');
Route::post('admin/my-jobs/doc/get-uploaded-folder',  'MyJobsController@getUploadedFolder')->middleware('auth');

Route::post('admin/user-note',  'NoteController@store')->middleware('auth');
Route::post('admin/user-note/update',  'NoteController@update')->middleware('auth');

Route::get('admin/user-note/details/{id}',  'NoteController@edit')->middleware('auth');
Route::get('admin/user-note/history/{id}',  'NoteController@history')->middleware('auth');
Route::delete('admin/user-notes/delete/{note_id}',  'NoteController@destroy')->middleware('auth');


// client
Route::get('meditation', 'AdminController@index')->middleware('auth');
Route::get('client', 'AdminController@index')->middleware('auth');
Route::get('client/message/{jobid?}', 'MessageController@client')->middleware('auth');
Route::get('client/discount/{jobid?}', 'DiscountController@index')->middleware('auth');
Route::post('client/discount-post/{jobid?}', 'DiscountController@dicountPost')->middleware('auth');
Route::get('client/our-community/{jobid?}', 'DiscountController@ourCommunity')->middleware('auth');
Route::get('client/meet-your-surveyor/{jobid?}', 'DiscountController@meetYourSurveyour')->middleware('auth');
Route::get('client/tasks/{jobid?}', 'TaskController@client')->middleware('auth');
Route::get('client/documents/{job_id?}', 'JobsController@job')->middleware('auth');
Route::get('client/documents/{job_id}/{type?}', 'JobsController@job')->middleware('auth');
Route::get('client/payment/{job_no?}', 'MyJobsController@clientPayment')->middleware('auth');
Route::post('client/payment-process', 'MyJobsController@paymentProcess')->middleware('auth');
Route::post('client/message-image', 'MessageController@sendImage')->middleware('auth');
Route::get('client/terms-and-condition', 'AdminController@getTermsCondition')->middleware('auth');


Auth::routes();

// test method
Route::get('test', function(){

	\Illuminate\Support\Facades\Mail::send('emails.test', [  'user' => 'bob',  'msg' => ' msg bob'], function ($message){
            $message->subject('Contact  test Form');
            $message->to('ali@tonbul.com');  
          });
	echo 'sent';
});

Route::get('video-test', function () {
    return view('video');
});