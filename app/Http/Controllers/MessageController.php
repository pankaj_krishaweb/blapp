<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\MessagePosted;
use App\{Message,Job, Job_User,User};
use Illuminate\Support\Facades\{Auth, Mail};
use App\User_History;

class MessageController extends Controller
{
    public function intro() {
        $jobs =Job::all();

        $allmsg= Message::all();
        $lastlogin=Auth::user()->history->where('action','Login attempt')->last();

        $msgs =$allmsg->where('created_at', '>=',  $lastlogin->created_at)->unique('user_id');
        return view('admin.messages', ['jobs'=>$jobs, 'msgs'=>$msgs]);

    }


    public function client($jobid=null) {
        if(Auth::user()->jobs->count() == 0){
            $error= "No Jobs";
            return view('client.waiting',compact('error'));
        }
        if (!$jobid) {
            return view('client.message', ['jobid'=>Auth::user()->jobs->last()->id]);
        }
        if (Auth::user()->jobs->where('id',$jobid)->count()==0) {
            abort(500);
        }
        return view('client.message', ['jobid'=>$jobid]);
    }

	public function index($jobid=null) {
        $user = Auth::user();
        $job = Job::find($jobid);

        $jobs =Job::all();
        return view('admin.message',['jobs'=>$jobs, 'job'=>$job ]);
    }
    
    public function admin() {
        return view('message');
    }

	public function getMessages($id=null){
        
        $user = Auth::user();  
        $messages= Message::with('user')
                        ->when($id, function ($query) use ($id) {
                            return $query->where('job_id', $id);
                        },
                        function ($query) {
                            return $query->where('job_id', $user->jobs->last()->id);
                        })->get();
            
         foreach ($messages as $msg) {
            if ($msg->user_id==Auth::user()->id) {
               $msg->who="bubble-right";
               
            }else{
            $msg->who="bubble-left";
            } 
            if (Auth::user()->role == 5) {                
                $msg->color= $msg->user->chatcolor ? $msg->user->chatcolor : '';
            }
            else{
                $msg->color = "";
            }
        }

        // filter message if user is third party 
        // if ($user->role==5) {
        //     $id = Auth::user()->id;
        //     $all=  $messages->filter(function ($value,$id) {
        //         return $value->user->id == $id || $value->user->role != 5;
        //     });
        //     return $all->flatten();
        // }
        
        return $messages;
	}

	public function postMessage(Request $request) {
        //dd($id);                                
    // Store the new message
    $user = Auth::user();
 
    if (!$request->jobid) {
        $job_id= $request->job_id;
    } 
    else{
        $job_id=$request->jobid;
    }


    $message = $user->messages()->create([
        'message' => $request->message,
        'job_id' => $job_id,
    ]);

    $user_history = new User_History;
    $user_history->user_id = Auth::user()->id;
    $user_history->action = "Send Message ";
    $user_history->description = ($user->name." send message to job id ".$job_id);
    $user_history->save();

    // Announce that a new message has been posted
    // broadcast(new MessagePosted($message, $user, $job_id))->toOthers();

    // post 
    if ($user->role == 3) {
        $job = Job::find($job_id);
        if ($job->survey_email) {
            $surveyor = explode(":",$job->survey_email);
            $otherUser = User::where('email','=',$surveyor[1])->first();
            if ($otherUser->notification ==1) {
                $sento =$surveyor[1];
                $address = $job->surveying != [] ? $job->surveying->address_of_inspection : '';
                Mail::send('emails.message_notification_surveyor', 
                    [  'user' => $user->name, 'msg'=>$request->message ], 
                    function ($message) use($sento,$job,$address){
                        // $message->subject('New Message - Berry Lodge Surveyors');
                        $message->subject('New Message -'.$job->id."-".$address);
                        $message->to($sento);  
                    }
                );
            }
        }
    }
    elseif ($user->role == 5) {
        foreach ( Job::find($job_id)->users as $otherUser) 
        {
            if ($otherUser->notification ==1 && $otherUser->id !== $user->id && $otherUser->role == 5)
            {
                $sento =$otherUser->email;
                Mail::send('emails.message_notification', 
                    [  'user' => $user->name, 'msg'=>$request->message ], 
                    function ($message) use($sento){
                        $message->subject('Your Surveyor has sent you a message in the Berry Lodge Client Login');
                        $message->to($sento);  
                    }
                );
            }
        }
    }
    else{
        foreach ( Job::find($job_id)->users as $otherUser) {
            // if ($user->role == 3) {

            //     if ($otherUser->notification ==1 && $otherUser->id !== $user->id && (($otherUser->role == 7 || $otherUser->role == 9) && $otherUser->qualifications != null)) {
            //         $sento =$otherUser->email;
            //         $job = Job::find($job_id);
            //         $address = $job->surveying != [] ? $job->surveying->address_of_inspection : '';
            //         Mail::send('emails.message_notification_surveyor', 
            //             [  'user' => $user->name, 'msg'=>$request->message ], 
            //             function ($message) use($sento,$job,$address){
            //                 // $message->subject('New Message - Berry Lodge Surveyors');
            //                 $message->subject('New Message -'.$job->id."-".$address);
            //                 $message->to($sento);  
            //             }
            //         );
            //     }
            // }
            // else{
                if ($otherUser->notification ==1 && $otherUser->id !== $user->id && $otherUser->role == 3) {
                    $sento =$otherUser->email;
                    Mail::send('emails.message_notification', 
                        [  'user' => $user->name, 'msg'=>$request->message ], 
                        function ($message) use($sento){
                            $message->subject('Your Surveyor has sent you a message in the Berry Lodge Client Login');
                            $message->to($sento);  
                        }
                    );
                }
                
            // }
        }
    }


    return ['status' => 'OK'];
	}

    public function sendImage(Request $request){
        $user = Auth::user();
        $jobid = $request->get('jobid');
        if($request->file)
        {
            $fileName = time().'.'.$request->file->getClientOriginalExtension();
            $request->file->move(storage_path('app/public/chat-image'), $fileName);


          // $image = $request->get('image');
          // $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          // \Image::make($request->get('image'))->save(storage_path('app/public/chat-image/').$name);
        

               $message = new Message;
               $message->message = "upload file";
               $message->image = "chat-image/".$fileName;
               $message->user_id = $user->id;
               $message->job_id = $jobid;
               $message->save();
                broadcast(new MessagePosted($message, $user, $jobid))->toOthers();
                if ($user->role == 3) {
                   
                    $job = Job::find($jobid);
                    if ($job->survey_email) {
                         $surveyor = explode(":",$job->survey_email);
                       $otherUser = User::where('email','=',$surveyor[1])->first();
                        if ($otherUser->notification ==1) {
                            $sento =$surveyor[1];
                            $address = $job->surveying != [] ? $job->surveying->address_of_inspection : ''; 
                            Mail::send('emails.message_notification_surveyor', 
                                [  'user' => $user->name, 'image'=>$fileName  ], 
                                function ($message) use($sento,$job,$address){
                                    $message->subject('New Message -'.$job->id."-".$address);
                                    $message->to($sento);  
                                }
                            );
                        }
                    }
                } 
            // if ($user->role == 3) {
            //     foreach ( Job::find($jobid)->users as $otherUser) {
            //         if ($otherUser->notification ==1 && $otherUser->id !== $user->id && (($otherUser->role == 7 || $otherUser->role == 9) && $otherUser->qualifications != null)) {
            //             $sento =$otherUser->email;
            //             $job = Job::find($jobid);
            //             $address = $job->surveying != [] ? $job->surveying->address_of_inspection : ''; 
            //             Mail::send('emails.message_notification_surveyor', 
            //                 [  'user' => $user->name, 'image'=>$name  ], 
            //                 function ($message) use($sento,$job,$address){
            //                     $message->subject('New Message -'.$job->id."-".$address);
            //                     $message->to($sento);  
            //                 }
            //             );
            //         }
            //     }
            // }
                else{
                    foreach ( Job::find($jobid)->users as $otherUser) {
                        if ($otherUser->notification ==1 && $otherUser->id !== $user->id && $otherUser->role == 3) {
                            $sento =$otherUser->email;
                            Mail::send('emails.message_notification', 
                                [  'user' => $user->name, 'image'=>$fileName  ], 
                                function ($message) use($sento){
                                    $message->subject('Your Surveyor has sent you a message in the Berry Lodge Client Login');
                                    $message->to($sento);  
                                }
                            );
                        }
                    }   
                }
                $user_history = new User_History;
                $user_history->user_id = Auth::user()->id;
                $user_history->action = "Send File ";
                $user_history->description = ($user->name." send file to job id ".$jobid);
                $user_history->save();
               return response()->json(['success' => $fileName], 200);
        }
        else{
            return response()->json(['error'=>'PLease try after some time'],400);
        }
    }
}
