<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ao_9 extends Model
{
    protected $table='ao_9';
    public function getCreatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value)); 
    }
    public function getUpdatedAtAttribute($value) {
            return  date('d-m-Y H:i', strtotime($value));   
    }
    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
