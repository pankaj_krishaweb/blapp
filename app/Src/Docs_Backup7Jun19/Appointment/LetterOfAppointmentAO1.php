<?php

namespace App\Src\Docs\Appointment;
use Illuminate\Support\Facades\Storage;

class  LetterOfAppointmentAO1
{
	public $documentFolder="Appointment Letters";
	public $documentName="Letter of Appointment AO 1";

    	public function create(\App\Job $job){
	    	# make storage directory 
	    	$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();

		$section->addTextBreak(6);
		$section->addText(ucwords($job->ao->full_names), ['bold' => true],['alignment' => 'center']);
		$section->addText('of', ['bold' => true],['alignment' => 'center']);
		$section->addText(ucwords($job->ao->contact_address), ['bold' => true],['alignment' => 'center']);
		$section->addTextBreak();
		$section->addText($job->ao->surveyor_name.' '.$job->ao->surveyor_qualifications, ['bold' => true]);
		$section->addText($job->ao->surveyor_company_name, ['bold' => true]);
		$section->addText($job->ao->surveyor_company_address, ['bold' => true]);
		$section->addTextBreak();
		$section->addText(date("d/m/Y"), [], [ 'align' => 'right' ]);
		$section->addTextBreak();
		$section->addText('Dear '.$job->ao->surveyor_name);
		$section->addTextBreak();
		$section->addText('Re: '.$job->ao->property_address_adjoining,['bold' => true]);
		$section->addText('The Party Wall etc. Act 1996	',['bold' => true]);
		$section->addTextBreak();
		$section->addText('As '.$job->ao->owners_referral.' of the above property '.$job->ao->i_we_referral_lower.' hereby authorise you to sign, issue and receive any Notices relating to the Party Wall etc. Act 1996 which affect '.$job->ao->my_our_refferal.' property.');
		$section->addTextBreak(1);
		$section->addText($job->ao->i_we_referral.' appoint you, '.$job->ao->surveyor_name.' '.$job->ao->surveyor_qualifications.'  to act as '.$job->ao->my_our_refferal.' surveyor in accordance with Section 10 of the the Party Wall etc. Act 1996.');

		$section->addTextBreak();
		$section->addText('Yours sincerely, ');
		$section->addTextBreak(3);
		$section->addText('…………………………………………',['bold' => true]);
		$section->addText($job->ao->full_names,['bold' => true]);
		$section->addTextBreak(3);
		$section->addText('…………………………………………',['bold' => true]);
		$section->addText($job->ao->full_names,['bold' => true]);
		
		
		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";
    	}

}
