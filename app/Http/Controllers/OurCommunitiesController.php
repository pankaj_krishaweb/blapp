<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OurCommunity;
use Validator;
use Illuminate\Support\Facades\Storage;

class OurCommunitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd('hello');
        $datas = OurCommunity::all();
        return view('admin.our-community.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.our-community.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'name' => 'required',
            'description' => 'required',
            'contact' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ];
        //passed rules to validator
        $validator = Validator::make($request->all(),$rules);
        //if validation fails then it will redirect back with errors
        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $photoName = $request->logo->getClientOriginalName();
        $request->logo->move(storage_path('app/public/our-community'), $photoName);

        $data = new OurCommunity;
        $data->name = $request->name;
        $data->logo = 'our-community/'.$request->logo->getClientOriginalName();
        $data->description = $request->description;
        $data->contact = $request->contact;
        $data->save();

        return redirect('admin/our-community')->withSuccess("Add Succesfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = OurCommunity::find($id);
        return view('admin.our-community.show',compact('data','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = OurCommunity::find($id);
        return view('admin.our-community.edit',compact('data','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            'name' => 'required',
            'description' => 'required',
            'contact' => 'required',
        ];
        //passed rules to validator
        $validator = Validator::make($request->all(),$rules);
        //if validation fails then it will redirect back with errors
        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ($request->hasFile('logo')) 
        {
            $photoName = $request->logo->getClientOriginalName();
            $request->logo->move(storage_path('app/public/our-community'), $photoName);
        }

        $data = OurCommunity::find($id);
        $data->name = $request->name;
        if ($request->hasFile('logo')){ $data->logo = 'our-community/'.$request->logo->getClientOriginalName(); }
        $data->description = $request->description;
        $data->contact = $request->contact;
        $data->update();

        return redirect('admin/our-community')->withSuccess("Add Succesfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = OurCommunity::find($id);
        $data->delete();
        return json_encode(['status'=>'good'], true);
    }
}
