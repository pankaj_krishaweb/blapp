@extends('layouts.frame')

@section('content')
<style>.panel-body {
    padding: 5px 15px;
}
.panel-primary > .panel-heading {
    color: #fff;
    background-color: #00b29c;
    border-color: #00b29c;
}.panel-primary {
    border-color: #00b29b;
}

.panel-info > .panel-heading {
    color: #ffffff;
    background-color: #00b29b;
    border-color: #414860;
}
.panel-info > .panel-heading .badge {
    color: #ffffff;
    background-color: #414860;
}
</style>
<div class="container-fluid">
    	<div class="container" style="min-height: 400px; padding-bottom: 50px;">
       	<h1 id="job_id" data-id="{{ $job_id }}">Job Tasks</h1>
 		<ol class="breadcrumb"> 
			<li><a href="{{ url('admin') }}">Home</a></li>
   			<li><a href="{{ url('admin/my-jobs') }}">My Job</a></li>
  			<li class="admin/jobs">Tasks</li>
		</ol>
         	<div class="row">
         		<div class="col-md-7">
         			<h2>Current Job Tasks</h2>
  		 		<div class="panel panel-primary">
		      			<div class="panel-heading">Job Name <span class="pull-right">Delete </span></div>
			      			@if($job_tasks->count()< 1)
			       			<div class="panel-body"> Currently no tasks assigned to this job </div>
			      			@else
				       		@foreach($job_tasks as $item)
								<div class="panel-body">
									<div class="row">
										<div class="col-md-1 col-sm-1 col-xs-2 ">
											@if($item->status==1) <i class="fa fa-check" aria-hidden="true" style="color: #00B29D"></i> @else &nbsp; @endif
										</div> 
										<div class="col-md-10 col-sm-10 col-xs-8" >
											{{ $item->task }} 
										</div>
										<div class="col-md-1 col-sm-1 col-xs-2" >
											<i   data-id="{{ $item->id }}" data-task="{{ $item->task }}" class="fa fa-trash-o delete-task-from-job pull-right" aria-hidden="true" style="color: red;"></i>
										</div>
									</div>
								</div>
							@endforeach
			      			@endif
					</div>
				</div>
  	         		<div class="col-md-5">
					<h2>Tasks</h2>
					@foreach ($tasks as $key=> $task)
      					@if ($key ==1 && $job->job_type == "AOS")
						<div class="panel panel-info">
      						<div class="panel-heading">
		      					<a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}" style="color: #fff;">	
		      						Adjoining Owners Surveyor  
		      						<i class="fa fa-chevron-down"></i>
		      					</a>
      							<button data-key="{{ $key }}"  class="badge pull-right add-task">Add All</button>
      						</div>
      					@elseif($key ==2 && $job->job_type == "BOS")
      					<div class="panel panel-info">
	      					<div class="panel-heading">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}" style="color: #fff;">	
									Building Owners Surveyor 
									<i class="fa fa-chevron-down"></i>
								</a>
								<button data-key="{{ $key }}"  class="badge pull-right add-task">Add All</button>
							</div>
      					@elseif($key ==3 && $job->job_type == "AS")
      					<div class="panel panel-info">
	      					<div class="panel-heading">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}" style="color: #fff;">	
									Agreed Surveyor 
									<i class="fa fa-chevron-down"></i>
								</a>
								<button data-key="{{ $key }}"  class="badge pull-right add-task">Add All</button>
							</div>
						@elseif($key ==4 && $job->job_type != "AS" && $job->job_type != "BOS" && $job->job_type != "AOS")
						<div class="panel panel-info">
							<div class="panel-heading">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}" style="color: #fff;">	
									Surveying Tasks
									<i class="fa fa-chevron-down"></i>
								</a>
								<button data-key="{{ $key }}"  class="badge pull-right add-task">Add All</button>
							</div>
      					@endif
      				{{-- </div> --}}
  					<div id="collapse{{ $key }}" class="panel-collapse collapse">
		 				@foreach($task as $item)
						<div class="panel-body">{{ $item->task }} <button  data-key="{{ $item->id }}" class="badge pull-right add-task">Add</button></div>
					@endforeach
					</div>
				    	{{-- </div> --}}
					@endforeach
  				</div>
 			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).on('click', '.add-task', function(event) {
	event.preventDefault();
	if ($(this).text()=="Add All"){
		var	type = 'all' ;
	}
	else if($(this).text()=="Add") {
		var	type = 'sub' ;
	}
	var key = $(this).attr('data-key');
	var job_id = $('#job_id').attr('data-id');
	$.ajax({
		url: '/admin/jobs/tasks',
		type: 'POST',
		headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
		data: {job_id: job_id, key:key, type:type},
		success: function(data){
			var status = $.parseJSON(data);		
			if(status.status=='good'){
				swal("Task added successfully ").then(() => {
				 location.reload();
				});				
			}
		},
		error: function(data){
	    	var errors = $.parseJSON(data.responseText);
		    console.log(errors);
			var displayerror='<div class="alert alert-dismissible alert-danger">';
		   $.each(errors, function(index, value) {
		      displayerror+='<li>'+value+'</li>';
		    });
		    displayerror+='</div>';
		    $('#error-display').html(displayerror);
	    	}	
	})
});

$(document).on('click', '.delete-task-from-job', function(event) {
	event.preventDefault();
	var task= $(this).attr('data-task');
	var task_id =$(this).attr('data-id');
	var job_id = $('#job_id').attr('data-id');
	var list = $(this);
	swal({
	  	title: "Are you sure?",
	  	text: "You wish to deleted task: "+task,
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
		if (willDelete) {
			$.ajax({
				url: '/admin/my-jobs/tasks/delete',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  },
				data: {task_id: task_id, job_id: job_id},
				success: function(data){
					console.log(data);
					list.parent().parent().remove();	
				},
			})	
		    	swal("Deleted! "+task, {
		      		icon: "success",
		    	});
		} 
		else {
		    swal("Your task is safe!");
		}
	})
});
</script>
@endsection 
