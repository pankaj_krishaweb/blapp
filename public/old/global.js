/*
*	Website Javascript Code
*/
var $ = jQuery.noConflict();
$(function(){

	hashLinkredirect();
	

	
	//	Media Queries Shiv
	moderizr();

	//	Colourbox - Lightbox popup
	lightbox();
	
	//	Mobile/Main Menu Slider
	mobileMenu();
	mainMenu();
	
	//	Nice popup function
	nicePopUp();
	
	//	Flexsliders
	imageSliders();
	
	//	Flexable Images
	flexImages('.flex-img,.mce img, .mce iframe');
	
	////////////////////////////////
	//*********	Pages ***********
	////////////////////////////////
	carrerForm();
	interactiveGuidePage();
	projectsPage();
	contactFormPicker();


	$(".contact-form-wrapper .career-choice.top-dropdown select").dropkick({
	  mobile: true
	});

	

});	//	Jquery on Ready


/*
*	When page has loaded
*/
$(window).bind("load", function() {
	moveClouds();
	
	////////////////////////////////
	//*********	Pages ***********
	////////////////////////////////
	contactPage();
	
});	//	Jquery on page loaded






	  


