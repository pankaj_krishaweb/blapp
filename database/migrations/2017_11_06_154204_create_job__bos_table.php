<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobBosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job__bos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id')->unsigned();

                $table->string('surveyor_full_information')->nullable();
              $table->string('surveyor_name')->nullable();
              $table->string('surveyor_qualifications')->nullable();
              $table->string('surveyor_company_name')->nullable();
              $table->string('surveyor_company_address')->nullable();
              $table->string('surveyor_contact_details')->nullable();
              $table->string('surveyor_full_name')->nullable();
              $table->string('surveyor_salutation')->nullable();
              $table->string('property_address_proposed_work')->nullable();
              $table->string('contact_address')->nullable();
              $table->string('contact_details_telephone_numbers_email')->nullable();
              $table->string('owner_referral')->nullable();
              $table->string('has_appointed_have_appointed')->nullable();
              $table->string('owners_bo_is')->nullable();
              $table->string('i_we_referral_upper_case')->nullable();
              $table->string('i_we_referral_lower_case')->nullable();
              $table->string('i_we_referral_upper_case_2')->nullable();
              $table->string('my_our_referral')->nullable();
              $table->string('he_she_they_referral')->nullable();
              $table->string('intend_intends')->nullable();
              $table->string('s_s')->nullable();
              $table->string('carry_carries')->nullable();
              $table->string('his_her_their')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job__bos');
    }
}
