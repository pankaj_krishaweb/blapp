<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
Use Illuminate\Http\Request; 
use App\{User,Role, User_History};

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
    protected function redirectTo()
    {
        
        switch (Auth::user()->role) {
            case  9:
               $url = '/admin';
                break;
            case 7:
               $url =  "/admin";
                break;
            case 11:
               $url =  "/admin";
                break;
            case 5:
               $url =  "client";
                break;
            case 3:
              $url =   "client";
                break;
            case 1:
               $url =   "client";
                break;
            default:
               $url =  "nothing";
        }
        $ip = \Request::ip();
        $location =get_object_vars(\Location::get($ip));
       User_History::create([
            'user_id' => Auth::user()->id,
            'action' => 'Login attempt',
            'description'=> 'login successful from IP '.\Request::ip(),
            'login_ip' =>  \Request::ip(),
            'location' => ($location['countryName'].",".$location['regionName'].",".$location['cityName'].",".$location['zipCode'])
        ]);
        return $url;

    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest', ['except' => 'logout']);
        $this->middleware('guest')->except('logout');
    }
}
