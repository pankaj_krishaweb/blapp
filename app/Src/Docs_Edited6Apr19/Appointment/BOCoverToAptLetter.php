<?php

namespace App\Src\Docs\Appointment;
use Illuminate\Support\Facades\Storage;

class  BOCoverToAptLetter
{
	public $documentFolder="Appointment Letters";
	public $documentName="BO Cover to Apt Letter";

    	public function create(\App\Job $job){
    		# make storage directory 
    		$dir = Storage::makeDirectory("public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/");
		\PhpOffice\PhpWord\Settings::setCompatibility(false);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultFontName('Gill Sans');
		$phpWord->setDefaultParagraphStyle(array('align' => 'both'));
		$section = $phpWord->addSection();

		$header = $section->addHeader();
		$header->addimage('images/bgberry-lodge-top.jpg', ['width' => 460]);		
		$footer = $section->addFooter();
		$footer->addimage('images/docs/footer.png', ['width' => 460]);
		
		$section->addTextBreak(8);
		$section->addText(ucwords($job->bo->full_name));
		$section->addText(ucwords($job->bo->contact_address));
		$section->addTextBreak();
		$section->addText(date("d F Y"), [], [ 'align' => 'right' ]);
		$section->addText('Our Ref: BLSN'.$job->id, ['bold' => true],['alignment' => 'right']);
		$section->addTextBreak();
		$section->addText('Dear '.$job->bo->salutation.',');
		$section->addTextBreak();
		$section->addText(ucwords('Re: '.$job->bo->property_address_proposed_work).' / '.ucwords($job->ao2->property_address_adjoining).' / '.ucwords($job->ao->property_address_adjoining),['bold' => true]);
		$section->addText('The Party Wall etc. Act 1996 ',['bold' => true]);
		$section->addTextBreak();
		$section->addText('Thank you for confirming you would like me to act as your Party Wall Surveyor in respect of the proposed works at '.$job->bo->property_address_proposed_work.', I would be more than happy to take on the appointment.'); 
		$section->addTextBreak();
		$section->addText('Please find attached 2 copies of the appointment letter for signature and return, one copy for your files and another copy to be returned to me.'); 
		$section->addTextBreak();
		$section->addText('As soon as I have the signed appointment to hand I will get the Party Wall '.$job->ao->notice_notices.' served, in the mean time I will get a folder set up and the Party Wall '.$job->ao->notice_notices.' drafted.'); 
		$section->addTextBreak();	
		$section->addText('Should you have any questions please do not hesitate to ask and I will be more than happy to clarify.'); 
		$section->addTextBreak();
		$section->addText('Kind Regards, ');
		$section->addTextBreak(4);
		$section->addText(ucwords($job->bo->surveyor_name));
		$section->addText(ucwords($job->bo->surveyor_qualifications));
		$section->addText('BERRY LODGE SURVEYORS',['bold' => true]);

		# Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save("/var/www/blapp/storage/app/public/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx");

		return "/storage/jobs/{$job->id}/my-jobs/{$this->documentFolder}/{$this->documentName}.docx";

    }


}
